---
layout: post
title: "Kamala Harris - The Truths We Hold"
description: "The Truths We Hold (2019) is an intimate self-portrait of one of the rising forces in contemporary American political life: Californian Senator and civil rights activist Kamala Harris. Combining the personal with the political, Harris sheds light on her early years as the daughter of immigrants, her legal career in the Golden State and the causes she has championed as an elected representative in Trump’s America."
image: https://images.blinkist.com/images/books/5c7fcfa56cee070007fe9945/1_1/470.jpg
---

<article class="shared__reader__blink reader__container__content" lang="en">
 <div class="chapter chapter" data-chapterid="5c7fd02d6cee070007fe9947" data-chapterno="0">
  <h1>
   What’s in it for me? A portrait of a presidential hopeful.
  </h1>
  <div class="chapter__content">
   <p>
    The American political landscape has undergone a dramatic shift. While Donald Trump was shaking things up in the Republican Party, Hillary Clinton was fighting off the surprisingly strong challenge of Bernie Sanders. In both parties, there’s a widespread sense that old orthodoxies are up for debate and that voters’ positions on the key issues of the day are shifting.
   </p>
   <p>
    That’s reflected in the emergence of new faces in an increasingly diverse Senate and Congress. While the 2016 elections are usually remembered for Trump’s unexpected victory, the election of a group of politicians who do things differently might prove to be just as important in the long run. One woman who’s been making waves since taking up her seat in the Senate is Kamala Harris.
   </p>
   <p>
    The daughter of a Jamaican father and an Indian mother, a lifelong campaigner for equality and an experienced legal mind, Harris quickly made a name for herself on Capitol Hill with her outspoken commitment to reform. No wonder that ever since she announced her candidacy, she’s widely tipped to go far in the Democratic Party primaries for the 2020 presidential elections.
   </p>
   <p>
    But who is Kamala Harris? Well, that’s just what we’ll be looking at in these blinks. From her childhood in Oakland to her work as a District Attorney in San Francisco and her senatorial career, these blinks will paint a portrait of a lawyer and politician on the rise. So read on to find out:
   </p>
   <ul>
    <li>
     why Harris decided to become a prosecutor;
    </li>
    <li>
     what her positions on healthcare, security and immigration are; and
    </li>
    <li>
     why she believes that everyone deserves a second chance.
    </li>
   </ul>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5c7fd07c6cee07000722a7f3" data-chapterno="1">
  <h1>
   Kamala Harris’s parents were gifted immigrants, and she knew early on that she wanted to be a lawyer.
  </h1>
  <div class="chapter__content">
   <p>
    Kamala Harris was born in Oakland, California, in 1964. Her father, Donald Harris, a Jamaican, came to the US to study economics at the University of California, Berkeley. Her mother, Shyamala Gopalan, came from Southern India. Gopalan’s parents encouraged their daughter to apply to Berkeley – a university she’d never seen in a country she’d never stepped foot in – when she was just 19.
   </p>
   <p>
    Gopalan arrived in 1958 and enrolled in a doctorate program in nutrition and endocrinology. She quickly became involved with the black community and threw herself into the civil rights movement. It was during a protest in Berkeley that she first met fellow activist Donald Harris. After falling in love, the couple decided to settle down in Oakland for good. Gopalan embarked on her career as a researcher specializing in breast cancer while he began teaching economics.
   </p>
   <p>
    Kamala Harris’s early childhood was a happy one. The family’s home was filled with books, Indian spices and her father’s jazz records. When John Coltrane wasn’t on in the background, Harris’s mother – a talented vocalist who’d won awards in India – sang along to gospel tunes by the likes of Aretha Franklin. But these carefree days wouldn’t last. Donald and Shyamala had married young and drifted apart over time.
   </p>
   <p>
    Donald headed to Wisconsin to pursue his academic work. Shyamala was offered a job at McGill University in Montreal, Canada. The opportunity was too good to turn down and she accepted. Kamala found the move difficult. She missed her friends and sunny California. As if that wasn’t bad enough, Montreal wasn’t just a colder and lonelier city for the twelve-year-old – it was also French-speaking! She remembers joking that she sounded like a duck during her first days at school as the only thing she could say was “
    <em>
     Quoi? Quoi? Quoi?
    </em>
    ” or “What? What? What?”
   </p>
   <p>
    Harris did eventually settle in, however, and her thoughts turned to her future. What did she want to do with her life? She’d always done well in school, and there was the inspiring example set by her mother. But her heroes weren’t doctors or academics: the people she admired most were lawyers like Thurgood Marshall, the first African-American on the Supreme Court, and Constance Baker Motley, a New York State Senator. Both were giants of the civil rights movement who had championed justice. How, she wondered, could she become like them?
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5c7fd0bc6cee070007fe994d" data-chapterno="2">
  <h1>
   Harris decided to become a prosecutor after studying at Howard University.
  </h1>
  <div class="chapter__content">
   <p>
    After high school, Harris returned to the US to study economics and politics at Howard University in Washington, D.C. It was an institution she’d heard a lot about from relatives who’d studied there. Established just after the Civil War to provide talented black students with the education other institutions denied them, Howard University had endured through over a century of segregation and discrimination.
   </p>
   <p>
    Harris arrived in the fall of 1982. The freshman orientation session was an eye-opener. She recalls looking around and thinking “This is heaven!” She’d never been among so many people who looked like her yet were so different. Her peers included both the children of Howard alumni and kids whose parents had never been to college; urbanites and rural folk; Africans and Caribbeans. The message all of them were given was that they were – in the words of a Nina Simone song – “young, gifted and black.”
   </p>
   <p>
    It didn’t take Harris long to find her place at the college. She chaired the economics society, honed her rhetoric with the debate team and joined a sorority. During the weekends, she could be found in downtown Washington protesting against apartheid in South Africa. Between all that, she squeezed in an internship at the Federal Trade Commission where she was responsible for reading the newspapers and cutting out articles that mentioned the organization.
   </p>
   <p>
    By the time she graduated in 1986, she’d made up her mind to become a prosecutor. Her family was skeptical. They felt the law had all too often been used as a weapon of injustice against black Americans and other marginalized groups. But Harris was convinced there was more to it than that. Hadn’t brave prosecutors used the law to take on the Ku Klux Klan in the South? Hadn’t US Attorney General Robert Kennedy used the law when he sent Department of Justice officials to protect the Freedom Riders – activists who protested segregation on public transport in the South – in 1961?
   </p>
   <p>
    Harris was determined to stick to her guns and used one of her mother’s favorite sayings in her defense: “Don’t let anyone tell you who you are. You tell
    <em>
     them
    </em>
    who you are!” If she was going to become a prosecutor, it certainly wasn’t to uphold injustices – she wanted to do things on her own terms, as a champion of equality.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5c7fd1186cee070007fe994e" data-chapterno="3">
  <h1>
   One of the defining moments in Harris’s legal career came before she’d even passed her exams.
  </h1>
  <div class="chapter__content">
   <p>
    Harris first stepped foot in the Alameda County Superior Courthouse in Oakland, California, in the summer of 1988. She was in her final year of law school and was about to take the bar exam which would qualify her as a legal practitioner. Things were moving quickly: she’d already been offered an internship in the office of the District Attorney – DA for short – and was about to get her first taste of the law in action.
   </p>
   <p>
    As an intern, Harris had little power or influence. Her job was to learn the ins and outs of a working court and get to know the criminal justice system from the inside. It would be a revelatory experience, and it was in the courtrooms in Alameda that she first discovered what kind of lawyer she wanted to become.
   </p>
   <p>
    Harris’s supervisor was finishing up a drug bust case late one Friday afternoon. Officers had arrested a number of people in a raid, including an innocent bystander – a woman who’d simply been in the wrong place at the wrong time. Harris had never laid eyes on her; all she knew about her was what was written in the file in front of her. But that was enough to know she needed help.
   </p>
   <p>
    Even though she’d done nothing wrong, it looked like this woman was going to spend the weekend in jail because the judge wouldn’t get to her case before Monday. Harris began wondering what would happen to the woman’s children. Would someone care for them or would Child Protective Services be called? “My God,” she remembers thinking, “she could lose her kids!”
   </p>
   <p>
    Everything was on the line for this unlucky woman. All Harris could think about was her frightened family. She rushed into action, begging the clerk to ask the judge to return for five minutes. Finally, after pleading her case, the judge arrived just before the court was due to close. He listened to the case, reflected briefly and pounded the gavel. Just like that, the woman had been set free in time for dinner with her children!
   </p>
   <p>
    Harris describes this as a defining moment in her life – an unforgettable lesson in how high the stakes were for the people she’d encounter in the legal system and the importance of using every scrap of power at her disposal to see that justice was done.
   </p>
  </div>
 </div>
 <div class="chapter supplement" data-chapterid="5c7fd1186cee070007fe994e" data-chapterno="3">
  <h1>
  </h1>
  <div class="chapter__content">
   <p>
    “
    <em>
     It was revelatory, a moment that proved how much it mattered to have compassionate people working as prosecutors.
    </em>
    ”
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5c7fd1406cee07000722a7f9" data-chapterno="4">
  <h1>
   Practical experience on the frontline of the justice system taught Harris more than law school.
  </h1>
  <div class="chapter__content">
   <p>
    Harris finished law school in the spring of 1989 and took the bar exam in July. Her future looked bright, and she was already looking forward to the life she’d always dreamed of. Best of all? She already had a job lined up as a deputy DA in Oakland!
   </p>
   <p>
    In November, however, she received a letter notifying her that she’d failed her examinations. The blow was softened by her new employer’s understanding attitude. The DA’s office agreed to keep her on as a clerk while she prepared to resit the exam. Still, being surrounded by peers who’d already passed the bar was humiliating. The low point came when she overheard a colleague remarking “But she’s so smart, how could she have not passed?”
   </p>
   <p>
    Harris persevered and passed on her second attempt. The day she was sworn in as an officer of the court remains one of the proudest moments of her life. But here’s what she learned in those gloomy months between her exams: neither law school nor tests really prepare you for life in the justice system. In fact, there’s nothing like getting stuck in and learning on the job.
   </p>
   <p>
    The principle she clung on to as she acclimatized to the work was that every crime against an American is a crime against society as a whole. That’s the reason prosecutors start proceedings by announcing themselves as she did on her first case: “Kamala Harris, for the people.” The legal system inevitably encounters situations in which the powerful have harmed the less powerful. Rather than expecting the weaker party to secure justice alone, Harris feels prosecutors like her can give them a voice by making it a collective endeavor.
   </p>
   <p>
    It’s a big responsibility. After all, prosecutors’ words partly decide people’s fates. They determine whether charges should be brought and, if so, which charges. Plea agreements, sentencing and bail recommendations are also in their hands. Harris was just starting her career but she already had the power to deprive someone of their liberty with a flick of her pen.
   </p>
   <p>
    But the most challenging cases also taught Harris about the limits of her powers. She remembers trying to get a six-year-old girl who’d been molested by her sixteen-year-old brother to tell her story. It was impossible to articulate to a jury what she’d suffered. Without this testimony, however, the brother went free. All Harris could do was lock herself in a bathroom and weep.
   </p>
  </div>
 </div>
 <div class="chapter supplement" data-chapterid="5c7fd1406cee07000722a7f9" data-chapterno="4">
  <h1>
  </h1>
  <div class="chapter__content">
   <p>
    “
    <em>
     Despite all that prosecutorial power, I’m not sure I’ve ever felt quite so powerless
    </em>
    .”
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5c7fd1666cee070007fe994f" data-chapterno="5">
  <h1>
   Harris’s experience as a DA in San Francisco convinced her to run for elected office.
  </h1>
  <div class="chapter__content">
   <p>
    By 1998, Harris had already spent nine years in the Alameda County DA’s office and had earned her spurs as a prosecutor. It was a job she loved, but other opportunities were on the horizon. That year she was hired to run the career criminal unit – a section dealing with repeat offenders – at the San Francisco DA’s Office.
   </p>
   <p>
    The job had upsides and downsides. On the one hand, it was a step up. She would be running her own department and overseeing a team of prosecutors – a great chance to grow. On the other hand, it was an office with a distinctly dubious reputation. The place itself was run-down, shabby and disorganized. When she arrived, there was just one computer for every two lawyers and no filing system. Lawyers were rumored to simply toss out old files once their cases had been concluded!
   </p>
   <p>
    The unit wasn’t on the best terms with the local police department, either. Officers moaned about poor conviction rates while prosecutors complained about the police increasing their workload by booking people for minor violations while failing to apprehend more serious offenders. Then there was the toxic working environment – made worse by the arbitrary firings that periodically decimated the staff.
   </p>
   <p>
    Harris tried to turn things around but found the problems too grave for a single person to solve. When she was given a chance to do things differently, she jumped at the opportunity. After being offered a new assignment by Louise Renne, the first woman to hold the top position of city attorney, she took charge of the division for children and family services. With Renne’s backing, Harris spearheaded a new approach to the problem of sexually exploited youths and set up safe houses for former juvenile sex workers, allowing them to get out of brothels and giving them support and treatment. The work felt meaningful and empowering. Most of all, it was proof for Harris that she was capable of crafting creative policy solutions.
   </p>
   <p>
    And it was that experience which turned her sights to elected office. Harris’s success in the family division stood out precisely because everything else seemed to be falling apart. The DA’s office was losing talented career prosecutors who felt overworked and underappreciated. Violent felons, meanwhile, were getting off scot-free. Something needed to be done at a higher level. Harris believed she was the right woman for the job of DA of San Francisco.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5c7fd1b76cee07000722a7fa" data-chapterno="6">
  <h1>
   Harris ran for DA to tackle systemic injustices like mass incarceration.
  </h1>
  <div class="chapter__content">
   <p>
    Early on in her campaign to become San Francisco’s DA in 2003, Harris commissioned a poll to see how many people knew her. The answer? Six percent! That made her campaign against Terence Hallinan, the popular incumbent, a tough one. But through a lot of hard work, Harris persevered and was inaugurated in 2004.
   </p>
   <p>
    She had run because she knew she was up to the job, but there was more at stake than her desire to prove herself. The DA’s office was a mess and change was long overdue. Harris’s victory was already progressive. The election of a black woman was a breakthrough in a justice system as unrepresentative as America’s. This still hasn’t changed: according to a 2015 report, 95 percent of all elected prosecutors are white and 79 percent are men.
   </p>
   <p>
    Then there was policy. Harris wanted to do something about mass incarceration. No nation imprisons more people than the US. In 2018, the total prison population was 2.1 million people – larger than the population of fifteen states! A lot of that is down to the “War on Drugs,” a zero-tolerance crusade launched in the 1970s which often ends up imposing draconian sentences on marginalized people for minor offenses.
   </p>
   <p>
    Take Harris’s friend Lateefa. She grew up in a rough neighborhood and got caught shoplifting as a juvenile. Lateefa turned her life around and became a brilliant community organizer whose work earned her a prestigious MacArthur “Genius Grant” in 2003. When Harris was elected, she asked herself: “What if Lateefa had been caught with a bag of weed rather than for shoplifting?”
   </p>
   <p>
    The answer was that she’d probably be in prison. It was that insight that inspired Harris's “Back on Track” program, an initiative to give non-violent first-time offenders alternative options. Rather than putting them into the criminal justice system, the program sends them to boot camps where they complete job training and classes on everything from financial literacy to parenting.
   </p>
   <p>
    The program was gradually rolled out in other cities like Los Angeles. After two years, only 10 percent of Back on Track participants had reoffended compared to the average of 50 percent for people convicted of similar crimes. The program also cost just $5,000 a head rather than the $40,000 it costs to house an inmate in a county jail for a year!
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5c7fd1d26cee070007fe9950" data-chapterno="7">
  <h1>
   Becoming a senator was a natural extension of the work Harris was doing as California’s DA.
  </h1>
  <div class="chapter__content">
   <p>
    In 2015, the Democratic senator for California, Barbara Boxer, announced that she wouldn’t stand for reelection after 24 years in office. Harris had made a name for herself as a progressive DA and was ideally placed to replace the liberal Boxer. After mulling it over, she threw her hat in the ring and announced her candidacy.
   </p>
   <p>
    The campaign was a hardfought one. While a Democratic candidate was virtually guaranteed to win, coming out on top against the party’s other leading lights was a different matter altogether – especially with an opponent as tough as the seasoned congresswoman Loretta Sanchez. Harris spent months touring California in what became known as the “Kamoji,” a bus featuring a massive emoji of Harris’s face. Harris was eventually elected to the US Senate to represent California in November 2016.
   </p>
   <p>
    It was bittersweet victory. Harris’s personal triumph was overshadowed by the unexpected victory of Donald Trump. In her acceptance speech, Harris urged Californians not to despair, reminding them that the fight for equality will always be an uphill struggle in which few gains will ever be permanent.
   </p>
   <p>
    After being sworn in by outgoing Vice President Joe Biden on January 3, 2017, Harris took aim at the incoming White House administration. The child of immigrants who’d experienced prejudice firsthand, she quickly became a fierce opponent of Trump’s immigration policies. Harris believed that these weren’t just cruel – they were also counterproductive.
   </p>
   <p>
    Take the decision to step up deportations of all undocumented immigrants, regardless of whether they’d committed crimes or not. The threat of arrest, Harris pointed out, was forcing millions of families to retreat from society. They were too scared to send their children to school, access medical care and – worst of all from a law and order point of view – report crimes.
   </p>
   <p>
    One of Harris’s first acts as a senator was to push the incoming Secretary of Homeland Security, General John Kelly, to declare that he wouldn’t use personal information given by immigrants in the hope of gaining citizenship to deport them. Kelly evaded her questions but didn’t rule it out. She felt forced to vote against his confirmation. His eventual appointment was just one of the reasons she would continue to clash with the Trump administration.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5c7fd2036cee070007fe9951" data-chapterno="8">
  <h1>
   Harris has advocated for the rights of Central American immigrants in the US.
  </h1>
  <div class="chapter__content">
   <p>
    Many immigrants arriving in the US come from the Northern Triangle, an area of Central America which includes El Salvador, Guatemala and Honduras. The life of those nations’ inhabitants is scarred by horrific violence. Between 2011 and 2014, nearly 50,000 people were murdered in the Triangle. Escape to the US is often the only option left to desperate families.
   </p>
   <p>
    Immigration from Central America had been on Harris’s radar for some time. In 2014, for example, an unprecedented number of minors fled the triangle, but the “welcome” they received in the US shocked Harris. When busses carrying 140 undocumented children and parents made their way to a local processing center in the small Californian town of Murrieta, enraged locals blocked their route and waved around signs reading “You’re not welcome!” and “Nobody wants you!”
   </p>
   <p>
    In Washington, Congress was also pushing for a quicker decision-making process which would decide asylum seekers’ fates in just two weeks. Harris’s experience working with vulnerable children convinced her that wasn’t nearly long enough. That these minors didn’t even have access to lawyers made the matter even worse.
   </p>
   <p>
    Legal representation makes a big difference. Asylum seekers have a 90 percent chance of losing their case without representation; with a lawyer, that drops to 50 percent. Harris hit the phones and convinced legal firms to commit to working these cases pro bono. She also sponsored legislation to redirect $3 million toward nonprofits supporting asylum claimants.
   </p>
   <p>
    Harris’s familiarity with the issue made her an ideal candidate to lead the charge against the Trump administration when it revoked the temporary protected status of immigrants from the Northern Triangle in January 2017. Trump’s executive order threatened to rob around 350,000 immigrants of their rights. Other legislation aimed at making it harder to apply for the right to remain had already led to a 10 percent decline in the number of immigrants granted asylum.
   </p>
   <p>
    One of the most shocking policies to deter immigrants was the decision to separate children from their parents at the border. Harris has been one of the policy’s most outspoken critics. In the Senate, she continued to make life hard for John Kelly with probing questions. In the media, she threw herself into a campaign to expose the terrible conditions in the camps where children were being held. In June 2018 the practice ended.
   </p>
  </div>
 </div>
 <div class="chapter supplement" data-chapterid="5c7fd2036cee070007fe9951" data-chapterno="8">
  <h1>
  </h1>
  <div class="chapter__content">
   <p>
    “
    <em>
     A society is judged by the way it treats its children—and history will judge us harshly for this. Most Americans know that already. Most Americans are appalled and ashamed
    </em>
    .”
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5c7fd25c6cee07000722a800" data-chapterno="9">
  <h1>
   Harris wants to reform the American healthcare system and reconceptualize healthcare as a right.
  </h1>
  <div class="chapter__content">
   <p>
    In 2008, Harris and her sister went out for dinner with their mother. When their mother arrived wearing a fancy outfit and a new hairdo out of keeping with her usual casual outfits, they knew something was up. What they didn’t know was just how bad her news was: she had colon cancer.
   </p>
   <p>
    That day ranks as one of the worst in Harris’s life. But here’s the thing: all of us will have at least one of these days. At some point, everyone will have a profound interaction with the healthcare system.
   </p>
   <p>
    But as Harris sees it, that system is broken in the US. No country spends more on healthcare than America, yet it still lags behind other OECD nations in terms of outcomes. In fact, life expectancy is
    <em>
     decreasing
    </em>
    in many areas. When it comes to maternal mortality, the US is one of only thirteen countries worldwide which has seen an upswing over the last 25 years. So where’s all that money going? For-profit healthcare providers. Medical bills are the number one cause of personal bankruptcy, and insurance premiums continue to rise.
   </p>
   <p>
    Worst of all, the system is radically unjust. One study carried out in 2016 found that there’s a ten-year gap in life expectancy between the poorest and most affluent areas of the US. To put that into perspective, being poor in today’s America can reduce your life expectancy more than a lifetime of smoking cigarettes!
   </p>
   <p>
    So what’s the solution? Well, improving outcomes demands a transformation of the whole system. That, Kamala believes, begins by reconceptualizing healthcare as a right. In concrete terms, it means shifting to a system in which healthcare coverage isn’t dependant on how much you can pay, but rather on what you
    <em>
     need
    </em>
    . The top priority should be to ensure results rather than profits. At its simplest, it’s about making sure that getting sick never ends in financial ruin and bankruptcy. How? A
    <em>
     single-payer
    </em>
    system in which taxation covers everyone’s access to healthcare.
   </p>
   <p>
    The other key element of potential reform is increasing investment in research and development. Organizations like the National Institutes of Health need extra funding so that they can close the innovation gap left by pharmaceutical companies who are too focused on profits to pursue much-needed research.
   </p>
  </div>
 </div>
 <div class="chapter supplement" data-chapterid="5c7fd25c6cee07000722a800" data-chapterno="9">
  <h1>
  </h1>
  <div class="chapter__content">
   <p>
    “
    <em>
     In a system where the quality of your care does indeed depend on your station in life, the reality is that healthcare is still a privilege in this country.
    </em>
    ”
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5c7fd28b6cee07000722a801" data-chapterno="10">
  <h1>
   Harris had a ringside seat for the investigations into Russian meddling in the 2016 election.
  </h1>
  <div class="chapter__content">
   <p>
    When Harris first arrived in the US Senate, she was surprised to learn that there was a free seat on the Senate Select Committee on Intelligence. When she asked her predecessor Barbara Boxer about it, she was told that senators avoided the job because it didn’t put them in the limelight – after all, most the issues discussed by the committee are confidential matters of national security.
   </p>
   <p>
    That didn’t bother Harris. This was a chance to learn about the threats to her constituents and country in real time. Events, however, would conspire to thrust the usually secretive committee into the center of public attention. After she was sworn in on January 6, 2017, an intelligence report concluding that Russian cyber operations had attempted to influence the 2016 presidential election went public. The Select Committee on Intelligence, and Harris, suddenly found themselves in the eye of the storm!
   </p>
   <p>
    So what has the committee discovered in its biweekly, two-hour sessions with representatives from 17 national intelligence agencies? The most important thing, Harris believes, is that the US must recognize that it’s under attack. Campaigns to influence public opinion in the US spearheaded by the Russian government have been nefarious and effective.
   </p>
   <p>
    That’s because Russia was pushing at an open door. Social media platforms like Facebook, Twitter and YouTube can be easily hijacked by committed trolls disseminating fake news to undermine American democracy and stoke divisions – which is just what Russian cyber operations did. During the 2016 election, one person bore the brunt of these trolling operations: presidential hopeful Hillary Clinton.
   </p>
   <p>
    The reason these campaigns have been so fruitful is that they target hot-button issues like LGBTQ rights, immigration and race. The hateful afterlife of prejudice and discrimination remains the nation’s Achilles’ heel – a point of vulnerability that will be exploited by enemies as long as these issues go unresolved.
   </p>
   <p>
    And that’s why Harris has spent her life fighting against injustice. As she’s reminded every time she talks to a gifted kid whose life has been put back on track thanks to her program or a brilliant PhD student whose parents arrived in the US as undocumented migrants. Hatred and cruelty do nothing but hold America back.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5c7fd2a26cee070007fe995f" data-chapterno="11">
  <h1>
   Final summary
  </h1>
  <div class="chapter__content">
   <p>
    The key message in these blinks:
   </p>
   <p>
    <strong>
     The daughter of two gifted immigrants who met and fell in love at the height of the civil rights struggle, Kamala Harris knew early on that she wanted to devote her life to the fight for justice. After settling on law as her vocation, she made a name for herself as a progressive prosecutor and a District Attorney in San Francisco. Believing she could do more for the communities she served as an elected representative, she ran for the US Senate in 2016. Since then, she’s become known as one of the Trump administration’s most outspoken critics and a rising star in the Democratic Party.
    </strong>
   </p>
   <p>
    <strong>
     Got feedback?
    </strong>
   </p>
   <p>
    We’d sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
   </p>
   <p>
    <strong>
     What to read next:
    </strong>
    <strong>
     <em>
      The Audacity of Hope
     </em>
    </strong>
    <strong>
     , by Barack Obama
    </strong>
   </p>
   <p>
    An outspoken progressive with a legal background shaking things up in the Democratic Party – remind you of anyone? Unsurprisingly, Kamala Harris is often described as the female counterpart of Barack Obama. Want to find out why? Well, why not check out our blinks to
    <em>
     The Audacity of Hope
    </em>
    , the book which helped launch the former president on his path to the White House.
   </p>
  </div>
 </div>
</article>
