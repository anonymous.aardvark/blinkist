---
layout: post
title: "Mary Pipher - Women Rowing North"
description: "Women Rowing North (2019) explores how women can continue to flourish as they enter their sixties and seventies. Through poignant stories from real women’s lives, these blinks examine the possibilities for happiness, friendship and community engagement in the later stages of life."
image: https://images.blinkist.com/images/books/5c83d8dc6cee070008d61295/1_1/470.jpg
---

<article class="shared__reader__blink reader__container__content" lang="en">
 <div class="chapter chapter" data-chapterid="5c83d9316cee070008d61296" data-chapterno="0">
  <h1>
   What’s in it for me? Age with a flourish.
  </h1>
  <div class="chapter__content">
   <p>
    As a woman, the prospect of growing older might fill you with dread. In a society that prizes women for their youth and sex appeal, can you embrace a new, life-affirming identity as you hit your sixties and seventies?
   </p>
   <p>
    Yes, you can! Let these blinks take you on a journey to uncover how, with the right attitude, growing older can mean becoming more active, passionate and engaged. Join cultural anthropologist and clinical psychologist Mary Pipher as she explains how to meet this phase of life with joy, courage and even bliss.
   </p>
   <p>
    You’ll explore the issues facing older people, such as illness, caregiving and retirement. You’ll also discover how some women have used their abundance of life experience to tackle these issues and emerge warmer, wiser and happier.
   </p>
   <p>
    In these blinks, you’ll learn
   </p>
   <ul>
    <li>
     why older women are invaluable to their local communities;
    </li>
    <li>
     the real reason behind society’s rampant ageism; and
    </li>
    <li>
     how serious illness can lead to transcendent bliss.
    </li>
   </ul>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5c83d94f6cee07000738ba74" data-chapterno="1">
  <h1>
   Ageism, particularly against older women, is common in American society.
  </h1>
  <div class="chapter__content">
   <p>
    Growing older can be alienating and challenging, especially for women. The author, now in her seventies, recently came face-to-face with this challenge when a little girl approached her in a park and sweetly asked, “Where do old ladies come from?”
   </p>
   <p>
    This question might seem astonishing. But it’s not really that surprising, given the extent to which American culture excludes and disempowers older women. This alienation is so widespread that this little girl’s confusion is understandable. Consider how often older American women are portrayed negatively in popular culture. For instance, mothers-in-law are commonly the butt of derisive jokes or viewed as bossy, nagging nuisances. The word ‘witch’ is often used to describe older women, as well.
   </p>
   <p>
    While sexist ageism overtly portrays older women negatively, this sort of prejudice manifests in more subtle ways as well, like in the total erasure of older women from popular culture. There is a dire underrepresentation of older people, and particularly older women, in American films. A 2017 study conducted by the Media Diversity and Social Change Initiative found that less than 12 percent of films that won an Academy Award between 2014 and 2016 featured older people. More shockingly still, older women were featured in close to none of them.
   </p>
   <p>
    This discrimination against older people in Hollywood is a reflection of the prejudice endemic in American society – a culture that values youth and beauty, while fearing and denigrating the aged. This fear and hatred, known as
    <em>
     gerontophobia,
    </em>
    was highlighted in a 2012 study by the Yale School of Public Health. The study assessed the extent to which social groups on Facebook dedicated to the elderly expressed disapproval of older people in their group descriptions. The researchers found that nearly all of them used negative stereotypes about the elderly, often denigrating and infantilizing them.
   </p>
   <p>
    Ultimately, unkind and discriminatory attitudes toward older people stem from ignorance. While older people know how it feels to be a child, a teenager and a middle-aged person, no other age group has experienced being old. Thus, younger age groups struggle to imagine the reality of advanced age and may lack empathy and understanding. Ageism may be understandable, but it’s still hugely damaging. And not just to the old, but to the young, as well.
   </p>
  </div>
 </div>
 <div class="chapter supplement" data-chapterid="5c83d94f6cee07000738ba74" data-chapterno="1">
  <h1>
  </h1>
  <div class="chapter__content">
   <p>
    “
    <em>
     After all, ageism is a prejudice against one’s future self.”
    </em>
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5c83d9666cee070008d61297" data-chapterno="2">
  <h1>
   Caregiving is both challenging and rewarding for older women.
  </h1>
  <div class="chapter__content">
   <p>
    In most societies throughout history, the job of caring for children, the elderly and the ill has fallen to women. In twenty-first-century America, little has changed thus far. Just like her mother and grandmother who came before her, the author and her generation of women were taught the importance of female sacrifice and encouraged to put the needs of others before their own.
   </p>
   <p>
    For most older women, providing care for those around them is nothing new. It’s likely they’ve already spent half a lifetime looking after the needs of their children, helping neighbors, bringing casseroles to friends’ families in times of need and making hospital visits to sick relatives. However, as women enter their sixties and seventies, the demands of caregiving can suddenly seem overwhelming.
   </p>
   <p>
    While many women rely on their partners for reciprocal care when they reach older age, this support may quickly crumble, leaving the women to do everything. Willow, one older woman that the author interviewed, reported that she and her husband Saul both enjoyed a fulfilling relationship and independent careers until he was unexpectedly diagnosed with Parkinson’s disease. Within a year, Willow found herself dressing Saul, helping him bathe and organizing the extra care, such as physical therapy, that he needed to manage his illness. Overwhelmed with her new responsibilities, Willow had less time to dedicate to her career and eventually quit her job. She even began to resent her husband for his dependence on her.
   </p>
   <p>
    A 2015 study by the National Alliance for Caregiving discovered that around two-thirds of caregivers experience depression. Further, nearly 40 percent of caregivers say they find care work to be very stressful. Considering that most caregivers are women, particularly older women, the mental health risks facing women of advanced age are clear. As Willow’s experience and these statistics demonstrate, caregiving can be difficult and exhausting for older women.
   </p>
   <p>
    However, fulfilling the role of caregiver for loved ones in need can be immensely rewarding and meaningful, too. Willow also remarks, for instance, that caring for Saul has helped her become a better person, and that she’s gradually gone from viewing herself as one of life’s victims to a willing volunteer instead. In America, a staggering 40 percent of people report that their lives hold no meaning. Thus, the opportunities for personal growth that caregiving presents should also be celebrated.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5c83d97b6cee07000738ba75" data-chapterno="3">
  <h1>
   As an older woman, your happiness is in your own hands.
  </h1>
  <div class="chapter__content">
   <p>
    Marlene, another older woman the author spoke to, grew up poor and has remained in poverty. Divorced, living in subsidized housing and forced into a career she didn’t choose due to her lifelong epilepsy, Marlene could easily have let herself be miserable. But her life motto? “I choose joy.” Remarkably, Marlene reports that she is happy.
   </p>
   <p>
    We might assume that Marlene’s humble circumstances rule out happiness. However, research by psychology professor Sonja Lyubomirsky has found that external circumstances only partially contribute to our overall happiness. Our genetics determine as much as 50 percent of how happy we are. The remainder comes down to a mix of attitude, behavior and circumstances. This research should speak to older women in particular. While it’s often not possible to change situations or events, we
    <em>
     can
    </em>
    alter our attitudes and behaviors.
   </p>
   <p>
    So, how can we change our outlook as we age? Lyubomirsky believes the key is to mentally reframe our present circumstances and approach them more positively. When Willow quit her job to care for Saul, she managed to see the positives. She realized that she was more grateful than ever for her life. Incredibly, a positive mental attitude can influence how we age as well. Research from the field of epigenetics has shown that our attitude toward aging has a tangible impact on our DNA.
   </p>
   <p>
    If we’re going to live fulfilled, happy lives as we age, though, we need to translate a positive attitude into positive behaviors. Unfortunately, too many older people aren’t taking actions to live more happily. For instance, a recent study conducted by marketing research firm Nielsen found that the average retired American watches around 50 hours of television per week. Does that sound like a joyous way to pass the day?
   </p>
   <p>
    Instead of switching on the box, try to put more thought into how you spend your precious time. After all, now that you’re retired, you actually have time to take kayaking lessons, cooking classes or to volunteer at your local refugee center. Undertaking activities like these will help you develop your unique gifts and engage with the people around you – something you’ll never achieve by watching TV.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5c83d9906cee070008d61298" data-chapterno="4">
  <h1>
   Retirement is a great opportunity to make positive changes to your local community.
  </h1>
  <div class="chapter__content">
   <p>
    When Nora retired, she and her husband Roger wanted to give something back to their community. After realizing that their suburban neighborhood was lacking a local park, they started raising funds and seeking permission to create one. And after four years of organizing volunteers and marshaling resources, their park was ready. It will continue to serve the community long after Nora and Roger are gone.
   </p>
   <p>
    Nora’s story goes to show that retirement doesn’t need to be a time of powering down. Instead, it’s the perfect time in your life to take action – and you can do that close to home. Working within the community is perfectly suited to older women.
   </p>
   <p>
    Many older women have lived in the same neighborhood for many years. This usually means they know how the system works. They understand their local area’s unique challenges and know how to handle bureaucracy to create change that serves the interests of the wider community. Additionally, some older women are fortunate enough to be natural connectors, meaning they connect those around them to other people and the resources they need.
   </p>
   <p>
    If you’re reluctant to get involved with your local community and wondering what you’d bring to the table, remember that you’re probably more capable than you think. After all, you now have several decades of wisdom on your side, as well as the spare time you’ll need to get your teeth into the sort of long-term, complex issues that other people shy away from. Your experience with different life stages also puts you in a great position to assist children, younger adults and the elderly. Even if you’re more interested in tackling global issues, you’ll likely still make the biggest impact in your immediate area. In other words, global problems are often local problems, too.
   </p>
   <p>
    So, whether you’re hoping to address climate change, education or political issues, bear in mind that the greatest opportunities for change are usually up for grabs right around the corner.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5c83d9ab6cee07000738ba76" data-chapterno="5">
  <h1>
   Our friends are the solace of our later years.
  </h1>
  <div class="chapter__content">
   <p>
    Women have been working alongside other women for millennia, bringing up children together, singing together over campfires and grieving for the dead together. Today, women still seek each other’s friendship. However, in the twenty-first century, it can be difficult to maintain these precious bonds. Not only does catching up with friends often take planning, long-distance travel and commitment, but modern culture also places little value on sustaining meaningful, lifelong friendships.
   </p>
   <p>
    In our earlier lives, we had work commitments and family to keep us busy and give our lives purpose. This often changes once we hit our sixties, when our children have flown the nest. We may be divorced or widowed, and our careers are behind us or soon will be. When these areas of life require less attention, we rely on our friends to bring hope, joy and intimacy into our lives. They keep everything in perspective, listen to us without judgment and help us laugh at our troubles. In other words, our friendships in later life give us the understanding, comfort and companionships that we need to flourish.
   </p>
   <p>
    In this stage of life, friends also offer something that our families often can’t. While we typically end up playing the role of caregiver to various family members, we have a different dynamic altogether with our friends. In the best friendships, we feel the other person is looking after us. This can bring a joy distinct from the loving obligation we feel toward our family.
   </p>
   <p>
    However, it is not easy to establish the sort of meaningful friendships that sustain us in our older years. Just like any other relationship, our friendships need generous attention and time. For instance, the author’s friend Louise always visits her friends and brings them pastries when they have a hospital appointment. So, make sure you tend to your friendships with love and dedication if you want their blossoms to keep your later years sweet.
   </p>
  </div>
 </div>
 <div class="chapter supplement" data-chapterid="5c83d9ab6cee07000738ba76" data-chapterno="5">
  <h1>
  </h1>
  <div class="chapter__content">
   <p>
    <em>
     “Friends remind us there is nothing so terrible it can’t be talked about on a walk or over a cup of coffee.”
    </em>
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5c83d9c26cee070008d61299" data-chapterno="6">
  <h1>
   Our final steps on life’s journey can be both difficult and blissful.
  </h1>
  <div class="chapter__content">
   <p>
    When the author saw her friend Jackie, she was struck by how frail and thin she looked. Though she was not yet sixty, Jackie had terminal cancer. Sitting on a bench overlooking a peaceful lake, the two friends talked. As they did, the author had a realization. Jackie was not only suffering from heartache as she made the journey to her life’s end. She was also experiencing bliss.
   </p>
   <p>
    Terminal illness and the last steps toward life’s conclusion can be a distressing time replete with pain, anger and worry. But this period of life also holds opportunities for transcendent contentment, joy and bliss.
   </p>
   <p>
    Jackie had initially felt outraged about her terminal diagnosis. Why had this happened to her, she wanted to know. Why, when she had led a healthy and productive life, had God handed her this fate? As well as her existential concerns, Jackie was in a lot of pain. The cancer was weakening her bones, causing them to break. She felt nauseous whenever she ate, and the treatment she underwent was making her confused a lot of the time. And yet, at times Jackie’s life was filled with an incredible sense of wonder and gratitude.
   </p>
   <p>
    After her diagnosis, Jackie experienced bliss when she realized that so many people in her life truly loved her. Previously, she was a steely businesswoman, reluctant to let anyone look after her. Now, however, she was filled with such intense happiness about the things her loved ones did for her, even small acts of kindness, that she frequently cried from overwhelming gratitude.
   </p>
   <p>
    Indeed, as Jackie and the author sat beside the peaceful lake, Jackie was able to see, through fresh eyes, the enchanting beauty of the world around her. Knowing she only had a little time left made everything seem more precious – the meadowlarks singing, the reflection of the clouds shimmering on the lake’s surface and the swallows that played around them.
   </p>
   <p>
    Though it’s a humbling thought that none of us know when our time will be over, this need not frighten us. Indeed, sitting by the lake that day, Jackie eventually gestured at the magnificence of the nature around her and said,
    <em>
     “
    </em>
    This is happiness – to be dissolved into something complete and great.”
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5c83d9d86cee07000738ba77" data-chapterno="7">
  <h1>
   Final summary
  </h1>
  <div class="chapter__content">
   <p>
    The key message in these blinks:
   </p>
   <p>
    <strong>
     Growing older, especially as a woman, is fraught with difficulties. Encountering society’s ageism, as well as dealing with newfound caregiving duties and chronic illness can all make old age feel overwhelming. Luckily, with community engagement, the company of good friends and a sense of appreciation for the wonder of the present moment, we can age with grace and confidence.
    </strong>
   </p>
   <p>
    <strong>
     Got feedback?
    </strong>
   </p>
   <p>
    We’d sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
   </p>
   <p>
    <strong>
     What to read next:
    </strong>
    <strong>
     <em>
      Bloody Brilliant Women
     </em>
    </strong>
    <strong>
     , by Cathy Newman
    </strong>
   </p>
   <p>
    Now that you know that growing older means becoming more active and engaged, find inspiration on how to do just that with
    <em>
     Bloody Brilliant Women
    </em>
    . Shining a light on the remarkable women who changed the face of British history, you’ll discover the female pioneers, revolutionaries and geniuses left out of most history books.
   </p>
   <p>
    Join author Cathy Newman on a whirlwind tour of female greatness, from the 1880s to the present day, as she finally brings the stories of these influential women into focus. To discover how women have influenced the fate of one of the world’s most powerful nations, head over to the blinks to
    <em>
     Bloody Brilliant Women.
    </em>
   </p>
  </div>
 </div>
</article>
