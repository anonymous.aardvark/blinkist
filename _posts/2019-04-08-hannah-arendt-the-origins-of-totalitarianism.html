---
layout: post
title: "Hannah Arendt - The Origins of Totalitarianism"
description: "The Origins of Totalitarianism (1951) is a landmark work by Hannah Arendt, in which she traces the anti-Semitic and imperialist roots of modern-day totalitarianism in Europe. Starting with the rise of the nation-state in the seventeenth century, Arendt reveals the prejudices and myths that empowered the Nazism and Stalinism of the early twentieth century, and that can lead to the erosion of free-thinking democracy. She also gives clear warning on how to avoid predatory totalitarian movements in the future."
image: https://images.blinkist.com/images/books/5c7fa58c6cee070007fe98c1/1_1/470.jpg
---

<article class="shared__reader__blink reader__container__content" lang="en">
 <div class="chapter chapter" data-chapterid="5c7fa64d6cee07000722a7a0" data-chapterno="0">
  <h1>
   What’s in it for me? Get a landmark account of how totalitarianism can corrupt so many.
  </h1>
  <div class="chapter__content">
   <p>
    We’ve come a long way and made a great many advancements over the years, but the fact remains that we’re only a few generations away from one of the most devastating wars humanity has known. It’s comforting to think that we’ve moved past such brutalities, but if we truly hope to prevent future atrocities we must stay mindful of how quickly a democratic society can turn against its people.
   </p>
   <p>
    With some luck, Hannah Arendt was able to escape the Nazis as a refugee in the 1930s.
    <em>
     The Origins of Totalitarianism
    </em>
    is considered her masterwork by many, but it was not without its controversy when it was first published only a few years after WWII.
   </p>
   <p>
    Arendt traces events in Europe that led to the rise of totalitarianism in Germany and the Soviet Union, and the role ineffective democratic governments played in setting the stage for these brutal regimes. As her account lays bare, we need to be vigilant in maintaining free discourse and making sure people don’t fall through the cracks of society, because when they do, terrible things can happen.
   </p>
   <p>
    In these blinks you’ll find out
   </p>
   <ul>
    <li>
     how catastrophically dangerous conspiracy theories can be;
    </li>
    <li>
     how a criminal trial shed light on a country’s racial divisions; and
    </li>
    <li>
     why a totalitarian movement will target lonely people.
    </li>
   </ul>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5c7fa6686cee070007fe98c5" data-chapterno="1">
  <h1>
   European Jews were historically isolated from mainstream society but kept close to those in charge.
  </h1>
  <div class="chapter__content">
   <p>
    Totalitarianism has emerged at different times and in different places throughout human history. But in Europe, during the twentieth century, it was inseparably linked to anti-Semitism. The reasons behind this anti-Semitism are complex, and to try to explain it, we’ll have to turn back the clock and look at how Europe’s class system changed over the years.
   </p>
   <p>
    By the mid-seventeenth century, Europe had long been operating under the rules of feudalism, which meant that society was primarily divided into two categories: peasants and nobility.
   </p>
   <p>
    Within this structure, Jewish people had traditionally worked in the position of moneylenders. They managed financial accounts for the nobility, including their loans, and in return, they received interest payments, as well as some special benefits that other non-nobles didn’t receive.
   </p>
   <p>
    But then came the Peace of Westphalia, a series of treaties signed in 1648, which essentially put an end to feudalism in much of Europe. From its ashes rose a new kind of society – one controlled by governments instead of monarchs. Under this new governmental rule, communities began to grow more homogenous and develop their own unique nationalities, which is how different regions in Europe came to be known as
    <em>
     nation-states
    </em>
    .
   </p>
   <p>
    During the transition away from feudalism, the Jewish people who’d worked as nobility’s financial managers began to work for governments. But it was soon apparent that these more complex systems generated more work, so more Jewish people, including those who hadn’t previously benefited from feudal arrangements, began to rise in status.
   </p>
   <p>
    However, the reality of this status was neither here nor there – for the Jews found that they were considered outsiders by everyone.
   </p>
   <p>
    Their service to government brought them special access to elite circles and events, and this did more than just make the working class view them as having an unfair advantage. In fact, the growing number of Jews ascending the social ladder led to a popular conspiracy theory that there was a Jewish plot to take over all of Europe.
   </p>
   <p>
    The ruling classes of Europe didn’t accept the Jews as their own, either. Instead, they saw Jews as a “vice” – something deemed to be unwholesome yet had to be endured due to the role Jews played in society. Therefore, some Jews were accepted on an individual basis, though they were still looked down upon, even by people who benefited greatly from their assistance.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5c7fa6866cee07000722a7a1" data-chapterno="2">
  <h1>
   Racist imperialism and pan-nationalism emerged in the new but weak nation-state.
  </h1>
  <div class="chapter__content">
   <p>
    With the advent of the nation-state, there was some hope that modern society could be both strong and fair. But this hope diminished rapidly, as the pursuit of power in this new world took over.
   </p>
   <p>
    In the wake of feudalism, the bourgeoisie were on the rise, and replacing the dwindling nobility as the most powerful economic group. However, the new governments only allowed their capitalist ambitions to go so far. So, in order for business to expand, businessmen had to look beyond their own borders, thereby giving birth to generations of imperialism.
   </p>
   <p>
    Naturally, there are ethical and legal questions surrounding imperialism, since the practice is largely defined by extracting money and resources from one country and bringing them to another while leaving the laws of the colonizing nation-state behind. So, to justify imperialist expansion, racism was put to use.
   </p>
   <p>
    Historically, when one country conquered another country or people, the victorious power would impose its laws upon the newly acquired territory. But this can be detrimental to the business of imperialism when the laws require things like due process and the right of the native population to be recognized as equal under the rule of law. Such laws run counter to imperialism’s main goal: the expansion of power and profit.
   </p>
   <p>
    Instead of applying laws that were consistent with those back home, imperialist powers controlled and managed native populations by bureaucratic decrees that enabled high profits at the expense of human rights. To justify this, racist views were employed to suggest that native populations in conquered territories were inferior beings and therefore not subject to laws that protected workers in the nation-states.
   </p>
   <p>
    Later on, the same sort of imperialist principles began to be used on home soil by those involved in what were known as
    <em>
     pan-nationalist movements
    </em>
    . This involved a uniting of disparate people based on a commonality such as language. In the pan-German and pan-Slavic movements, people who spoke the same language banded together, eventually overpowering local laws, using racism to justify the privileged status they bestowed upon themselves.
   </p>
   <p>
    The pan-nationalist movements in Europe rewrote history to paint their peoples as coming from superior lineages, and their enticing falsehoods about a “natural order” to humanity proved powerful in indoctrinating the masses. For Germany’s Nazi Party, the Aryan race was painted as the “master race” that had to defend itself against the “evil Jews,” who were robbing the Aryans of their place as the world’s rightful rulers.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5c7fa6b26cee070007fe98c6" data-chapterno="3">
  <h1>
   As the nation-state crumbled, Jews took the blame for society’s ills.
  </h1>
  <div class="chapter__content">
   <p>
    As the nineteenth century progressed, governments continued to lose power to imperialists and pan-nationalist movements. The nation-state was disintegrating, and Jews would suffer immensely because of it, even though they were initially unaware of how dire the situation was becoming.
   </p>
   <p>
    With government power on the decline, there was a corresponding growth in resentment against Jewish people. This had to do with the Jews still being relatively wealthy, even though their role in government finance was carrying less weight.
   </p>
   <p>
    As a result, they became an easy target for those looking for a scapegoat to explain why the nation-state was floundering and government was ineffective. In fact, they began to be labeled as parasites, seen as receiving money from the state for no good reason, on top of being thought of as outsiders who never fully assimilated into mainstream society.
   </p>
   <p>
    The truth was that their generations of consistent work had left them financially stable as others suffered from the era’s political and financial upheavals. Nevertheless, the fallacy of a Jewish conspiracy aimed at world domination persisted, even though unstable governments meant that the Jews had little to no influence at the time.
   </p>
   <p>
    At the turn of the twentieth century, another incident added more fuel to the theory that Jews were to blame for society’s ills.
   </p>
   <p>
    In 1894, the trial known as the Dreyfus affair began. It involved Alfred Dreyfus, a Jewish French army captain who was wrongly convicted of selling military secrets to the Germans. From the beginning, anti-Semitic groups were very public in their accusations that Dreyfus was proof that all Jews were enemies of the French people.
   </p>
   <p>
    Eventually, twelve years on, Dreyfus was proven innocent, but the divisions caused by the affair were not easily settled. Some people called for the case to be reopened, and some believed that the fact that Dreyfus was a Jew was all the evidence they needed to pronounce him guilty.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5c7fa6ce6cee07000722a7a2" data-chapterno="4">
  <h1>
   Up to and following WWI, more and more people joined the “classless masses,” ideal subjects for totalitarianism.
  </h1>
  <div class="chapter__content">
   <p>
    As a result of the nation-state’s instability, an increasing number of people were losing their places in society at the start of the twentieth century.
   </p>
   <p>
    Political parties represented the interests of the upper and middle classes, and there was a growing number of poor, disenfranchised people in Europe who felt unrepresented by any political party. They became known as the
    <em>
     classless masses
    </em>
    , and their numbers only increased after the widespread destruction of World War I.
   </p>
   <p>
    Following the war, even the elite took sides with the masses by leaving behind any liberal ideas of free-thinking tolerance and embracing the idea of tearing down the establishment and status quo.
   </p>
   <p>
    Being isolated and angry individuals, those who made up the classless masses got easily swept up in the totalitarian movements that grew in the wake of WWI. As the author puts it, these people were “atomised,” meaning they were isolated and lost any social or community-minded perspective. Their concerns were purely self-interested. This made them vulnerable to being taken in by the pan-nationalist movements, which were not beholden to any nation or class, yet provided them with a sense of meaning and belonging.
   </p>
   <p>
    The next step for the pan-nationalist movements was totalitarianism, as they used the masses to reveal a fatal flaw in democratic systems.
   </p>
   <p>
    Those still involved in the politics of democracy made the big mistake of believing the masses to be inconsequential. As far as they were concerned, these populists were incapable of making a difference, since most of them didn’t vote in elections, even if they had the right to do so. But it wouldn’t be long before this belief was proven dreadfully wrong.
   </p>
   <p>
    Remarkably, the leaders of the totalitarian movements in Europe got the masses so engaged that they became voters, and allowed these leaders to gain enough political power to demolish the democratic process and eliminate the chance of any new political rivals.
   </p>
   <p>
    It’s important to recognize the real flaw that led to this development: totalitarianism is given an open invitation to assume power when democracy fails to represent the majority of the people truly. So, when the majority of the population feels disenfranchised and isn’t politically engaged enough to vote, don’t be surprised if someone takes advantage of this and makes his voice heard in order to bring revolutionary change.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5c7fa6e96cee070007fe98c7" data-chapterno="5">
  <h1>
   The masses were now ready to be indoctrinated by totalitarian propaganda.
  </h1>
  <div class="chapter__content">
   <p>
    When totalitarianism takes hold in a society, it’s a sign that the people have disengaged from analytical and political thought. Within a totalitarian society, the only thing that really matters is the leader’s vision for the future. If factual evidence is presented to disprove that vision or present a viable alternative, it’s always twisted into an attempt by an enemy to mislead the public.
   </p>
   <p>
    After their formation in the years following WWI, the Nazis consistently steered people away from analytical thought and repeatedly hammered them with the fake story of a Jewish conspiracy and its threat to their way of life. Over and over again, the public was warned that steps must be taken to prevent the Jews from succeeding, or else a horrible, oppressive future lay ahead. And since there was only room for one vision in the Nazis’ totalitarian society, this tall tale became the public’s reality.
   </p>
   <p>
    Naturally, the story cast the leaders of the Nazi party as the heroic Aryan protectors who were already winning this war for the future of civilization. By spreading this story and positioning Aryans as fulfilling their destiny by defeating the Jews, the Nazis were strengthening their position all the more.
   </p>
   <p>
    It was no coincidence that Josef Stalin, the dictator of the Soviet Union from 1943 to 1953, used similar tactics to consolidate control over the pan-Slavic masses of the greater Soviet empire. In his story, he was trying to protect the nation’s honorable, hard-working Communists against the evil Trotskyist conspirators.
   </p>
   <p>
    One of the most powerful tools totalitarian leaders use when selling their stories is propaganda, as it effectively fills the void that a failed democracy can leave behind.
   </p>
   <p>
    Once again, the people of Germany and the Soviet Union were particularly susceptible to propaganda because they’d lost their sense of purpose, identity and meaning within their respective nation-states.
   </p>
   <p>
    They felt let down and unrepresented by their governments – and when people are angry, unemployed, isolated and disengaged, as many were in these nations, they’re ready and waiting for something like Nazism or Stalinism to fill that void and give them something to believe in. When people are in such an extreme mental state, the outlandish ideas that are found in propaganda are more readily accepted. And when their reality is properly skewed by propaganda, they’ll be even more ready to follow their leader blindly.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5c7fa71d6cee07000722a7a3" data-chapterno="6">
  <h1>
   At the helm of totalitarianism is an ideology that distorts history to fit the reality of the ideology.
  </h1>
  <div class="chapter__content">
   <p>
    Another typical sign of totalitarianism is that the propaganda and ideology make careful efforts to rewrite history in a way that justifies a totalitarian regime’s actions. In the case of the Nazis, their skewed version of history presented Aryans as the eternal master race, and suggested that all of history had been leading up to the moment in the 1930s when Germany would be able to fulfill its destiny of world conquest.
   </p>
   <p>
    For this kind of revisionist story to make sense, a totalitarian party must suppress individual free thinking and turn the once-isolated masses into instruments ready to obey the leader’s will. When this happens, the only sensible version of history will be the one that aligns with the party’s ideology.
   </p>
   <p>
    What is often the case, however, is that the stories, ideology and propaganda are all covering up the real agenda behind the movement, which is about expansion and attaining power. Certainly, this was behind the Nazi ideology of the Aryan master race saving the world from a Jewish conspiracy.
   </p>
   <p>
    It’s no secret that the top-ranking Nazis responsible for perpetuating the myth about the Aryan master race were well aware of its fictitiousness. They were also aware that the propaganda about their leader being superhuman was entirely fictitious as well.
   </p>
   <p>
    However, they were fully committed to the idea that they were building a society of people who would control the world. And they saw their ability to manipulate the masses to follow orders effectively as a sign that the real goal behind the Nazi mythology, that of expansion and power, was within their grasp.
   </p>
   <p>
    The Nazis targeted the Jews as a common enemy to engage and mobilize the masses. But they also needed such an enemy to keep the masses in line. So, if they’d succeeded in murdering every last Jewish person, it’s clear that the Nazis would have needed to invent another enemy.
   </p>
   <p>
    The only law of the totalitarian state is the goal at the core of the movement. And the role of ideology is to engage and motivate the people needed to reach that goal, and to cloak it in any other ideology that is effective in reaching those people.
   </p>
   <p>
    In the end, the movement, and its expansion, are the only things that matter. Therefore, anything that doesn’t serve the mission doesn’t matter, and everything else serves the ideology.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5c7fa73d6cee070007fe98c8" data-chapterno="7">
  <h1>
   Totalitarianism is truly all-encompassing.
  </h1>
  <div class="chapter__content">
   <p>
    Anyone who becomes part of the masses in a totalitarian movement becomes less human and more like a cog in a machine. Human characteristics like free thought and autonomy are extinguished as people become one anonymous gear.
   </p>
   <p>
    One of the main ways in which totalitarianism succeeds in dehumanizing people is by deliberately taking away all spontaneity – for being spontaneous, making choices and changing your mind are at the heart of what it means to be free.
   </p>
   <p>
    In a totalitarian state, members of the masses are never acting in their own interests, but rather in adherence to the ideology. In this way, you could say that the masses are not in full, conscious control of their lives, but are in fact acting out the leader’s will.
   </p>
   <p>
    And this is why it’s impossible to have a reasonable discussion with one of the masses and ask logical questions about the ideology because this requires freethinking and reflection. And when people surrender their right, and their responsibility, to make informed decisions in full consciousness, they become unable or unwilling to assess their actions critically, see them for what they truly are and be fully conscious of their effects.
   </p>
   <p>
    When you’re in full, conscious control of your own life, you can look back and think about what you have done and why, as well as whether you should do something different next time. If your motivations for taking action weren’t truly yours in the first place, you have no stable place from which to start meaningful reflection.
   </p>
   <p>
    Repetition and terror are also used constantly to neutralize free will and spontaneity.
   </p>
   <p>
    The repeated and casual use of violence can not only scare people into adhering to ideology, but it can also numb them to violence and killing. And when violence becomes a constant, mechanical and impersonal threat, both the victim and the perpetrator become desensitized to it. When this happens, both sides become dehumanized, which makes the aggression even more likely to continue and escalate.
   </p>
   <p>
    We can see concentration camps as the practice of dehumanization taken to its lethally effective extreme.
   </p>
   <p>
    A lot of propaganda and rhetoric were used to dehumanize Jewish people, such as commonly depicting them in the media as vermin. And when the concentration camps were put into use, the final act of murder was performed in such a routine and impersonal way that it didn’t require any one individual to make a decision, thereby effectively dehumanizing the killers as well as the dead.
   </p>
   <p>
    Once set in motion, the camps were a self-perpetuating machine, kept running with the inhumane dissociation that goes hand-in-hand with totalitarianism.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5c7fa7696cee07000722a7a4" data-chapterno="8">
  <h1>
   Whenever people are lonely, when humans are “atomised,” there is a risk that totalitarianism might succeed.
  </h1>
  <div class="chapter__content">
   <p>
    Throughout human history, totalitarianism has reared its ugly head repeatedly, and there’s no reason to believe it won’t happen again. Fortunately, we know some of the warning signs, including a key contributor to the ideal conditions for its growth: loneliness.
   </p>
   <p>
    When people are isolated and feel left out or discarded by their communities, they become a prime target for a totalitarian movement – because when you lose your community, you also lose your sense of self. If you feel like society has decided you are disposable or unwanted, chances are you’ll be at a loss for meaningful connections with others. This also comes with a sense that there’s no place for you in society anymore, so why bother participating in anything?
   </p>
   <p>
    At the core of this state, however, is a longing for something else to come along, fill this void and make people feel like they belong once more. This naturally makes them extremely vulnerable to the rhetoric of a totalitarian movement, which is full of promises to make them part of some bigger plan.
   </p>
   <p>
    Since loneliness of this kind exists everywhere, we must always be vigilantly aware of totalitarianism and the serious threat it presents to human rights.
   </p>
   <p>
    On the bright side, there is always hope for preventing totalitarianism from taking root; the key is to keep alive the aspect of humanity it seeks to destroy, which is spontaneity.
   </p>
   <p>
    Though totalitarian regimes have thrived by destroying human spontaneity, it was this same spontaneity that allowed totalitarianism to emerge in the first place.
   </p>
   <p>
    First of all, it’s important to recognize that making spontaneous decisions without fully grasping their consequences can lead to unstable governments and the loss of human rights – all of which paves the way for totalitarian leaders eager to play God.
   </p>
   <p>
    This is why it’s imperative to learn from these past mistakes and use our humanity, including our spontaneity and freethinking individuality, to steer our societies away from totalitarianism. We must choose to participate and build more-inclusive and less-fractured communities. And to ensure further that there will be less loneliness, we must build governments that are representative of everyone.
   </p>
   <p>
    It’s also important that we keep government power in check, with laws and policies that will prevent leadership with totalitarian ambitions from turning a democratic government into a dictatorial one. With the right laws in place, we can give ourselves the chance for human spontaneity to react to the warning signs of totalitarianism before it becomes an uncontrollable epidemic.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5c7fa7906cee070007fe98c9" data-chapterno="9">
  <h1>
   Final summary
  </h1>
  <div class="chapter__content">
   <p>
    The key message in these blinks:
   </p>
   <p>
    <strong>
     Societies are susceptible to totalitarianism when a disenfranchised majority feels like the democratic system has failed it. When this disconnect happens, people become “atomised” and vulnerable to totalitarian movements. The ideology of totalitarian movements provides the people with an enemy against which to unite, while it also eliminates human spontaneity and free will. It does this through repetitive ideology, which uses propaganda and terror, causing members of the public to carry out the will of the leader instead of making their own decisions. By protecting the human characteristics of freethinking spontaneity, and building representative governments, we can better avoid future totalitarian regimes.
    </strong>
   </p>
   <p>
    <strong>
     Got feedback?
    </strong>
   </p>
   <p>
    We’d sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
   </p>
   <p>
    <strong>
     What to read next:
    </strong>
    <strong>
     <em>
      The Road to Serfdom
     </em>
    </strong>
    <strong>
     , by Friedrich August von Hayek
    </strong>
   </p>
   <p>
    Now that you’re familiar with one seminal account of what happened in the years leading up to WWII, why not check out another classic take on what went wrong in Europe in those tumultuous times?
    <em>
     The Road to Serfdom
    </em>
    (1944) came out while the war was still raging, and offered both an economic and a philosophical perspective on the dangers of a socialist system with too much governmental control.
   </p>
   <p>
    It’s never a bad idea to learn from history’s mistakes, so discover how a government can chip away at democracy and learn the warning signs to make sure we never go down that catastrophic road again.
   </p>
  </div>
 </div>
</article>
