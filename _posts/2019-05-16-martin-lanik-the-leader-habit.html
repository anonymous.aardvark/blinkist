---
layout: post
title: "Martin Lanik - The Leader Habit"
description: "The Leader Habit (2018) details an approach to leadership development that favors practice and habit learning over rote learning of knowledge. By looking at the ways habits are developed, and by breaking down the skills necessary for successful leadership into microbehaviors, these blinks offer a structure for becoming a better leader through short training exercises."
image: https://images.blinkist.com/images/books/5cbcca206cee070008bad256/1_1/470.jpg
---

<article class="shared__reader__blink reader__container__content" lang="en">
 <div class="chapter chapter" data-chapterid="5cbcca3d6cee070007d5c956" data-chapterno="0">
  <h1>
   What’s in it for me? Learn leadership through guidance and practice, without trawling through a ton of books.
  </h1>
  <div class="chapter__content">
   <p>
    You’ve seen the stuffed shelves in self-improvement sections in libraries and bookshops. Every snake oil salesperson seems to be pushing a new book devoted to leadership skills and management. It’s a hot topic and everyone seems to have a opinion. Not that all of these books are bad of course. Some come packed with interesting anecdotes and lengthy theories. They may even make you laugh. But when it comes to actual practice, they’re often distinctly lacking. And in leadership, what matters is results.
   </p>
   <p>
    What you need is guidance, some clear advice and easy-to-implement leadership skills, obtainable through simple steps and training exercises. This is where these blinks will help. They will introduce you to a technique you can use to become a better leader.
   </p>
   <p>
    The “leader habit” can be learned through simple exercises, responding to cues that occur in everyday situations. What’s more these exercises only require a few minutes’ work at a time. Step by step, you’ll build new habits that will assist you in becoming a better leader, more attuned to and aware of the world and the people around you.
   </p>
   <p>
    In these blinks you’ll learn:
   </p>
   <ul>
    <li>
     what grumpy customers are really trying to communicate;
    </li>
    <li>
     how a pizza delivery team became better drivers
     <em>
      en masse
     </em>
     ; and
    </li>
    <li>
     how to identify and improve a poorly performing team.
    </li>
   </ul>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5cbcca506cee070008bad257" data-chapterno="1">
  <h1>
   Leadership skills are best understood as a series of learned habits.
  </h1>
  <div class="chapter__content">
   <p>
    It makes little difference what field you work in – leadership skills matter. And we’re not just talking about company management here; these skills even make a difference when you’re running a household.
   </p>
   <p>
    So, how do you actually become a better leader?
   </p>
   <p>
    Leadership theorists used to argue that great leaders were genetically gifted, predisposed to behave as leaders. However, studies have disproved that view. We now know that 70 percent of the qualities associated with great leadership are learned behaviors. In response to this discovery, since 1996 there has been a massive boom in leadership development literature.
   </p>
   <p>
    Yet, this has hardly led to a new generation of gifted leaders.
   </p>
   <p>
    Worse, evidence suggests that there is a
    <em>
     negative
    </em>
    correlation between the rise in prominence of the field, and the confidence that Americans have in their leaders.
   </p>
   <p>
    If books aren’t working, what’s the best way to learn about leadership? Rather than seeing leadership as learnable knowledge, it would help to think of it as a set of
    <em>
     skills
    </em>
    . A skill can be learned through exercises, training your automatic behavior so that a
    <em>
     habit
    </em>
    is developed.
   </p>
   <p>
    Let’s look at the example of Laura. She was an emergency room nurse who was repeatedly passed over for management roles. Laura hadn’t noticed that her colleagues saw her as abrupt, emotionally erratic and argumentative. In short, she was hardly leadership material.
   </p>
   <p>
    To polish her CV and ensure she got the promotion she desired, Laura decided to take part in the author’s leadership development program. She learned to practice an important exercise: asking colleagues open-ended questions that ideally hinged on “What” or “How.” To remind herself to do this, she made a note on her hand. She soon found that her colleagues were a lot more responsive if she asked for their opinions. Using this technique, her relationships began to improve and her confidence grew.
   </p>
   <p>
    You too could try adapting your behavior in this manner, and developing this simple habit.
   </p>
   <p>
    Soon enough, Laura didn’t need written reminders. She not only succeeded in getting the management position she aspired to, but her relationships with friends and family members improved. All it took was training in the right skills.
   </p>
   <p>
    But how do you do this?
   </p>
   <p>
    The following blinks will introduce you the author’s
    <em>
     Leader Habit Formula
    </em>
    . The idea is that the habits essential for leadership can be practiced with simple five-minute training exercises.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5cbcca656cee070007d5c957" data-chapterno="2">
  <h1>
   Complex leadership skills can be easily trained by learning microbehaviors.
  </h1>
  <div class="chapter__content">
   <p>
    Learning new skills takes time. Just think of a piano player. She doesn’t sit down to play a sonata from start to finish the first time around. Instead, she learns it section by section, little by little, repeating it often. The same is true of leadership development.
   </p>
   <p>
    When the author Martin Lanik developed the Leader Habit Formula, he identified core leadership skills. He then compartmentalized these into
    <em>
     microbehaviors.
    </em>
    In essence, then, habits are ingrained microbehaviors.
   </p>
   <p>
    Lanik wanted to be sure of which habits would be the most critical for leadership. He and his research team decided to evaluate over 800 global leaders, using their research to establish two leadership categories:
    <em>
     Getting Things Done
    </em>
    and
    <em>
     Focusing on People
    </em>
    . They determined that the best leaders showed skills in both aspects, but generally excelled at one over the other.
   </p>
   <p>
    Each category encompassed three groups of skills. In total, 22 core skills were split between the six groups. The first category of Getting Things Done, for instance, included the skills
    <em>
     Planning and Execution
    </em>
    . The core skills within the group included
    <em>
     Manage Priorities
    </em>
    ,
    <em>
     Plan and Organize Work
    </em>
    ,
    <em>
     Delegate Well
    </em>
    and
    <em>
     Create Urgency
    </em>
    .
   </p>
   <p>
    The author and his team didn’t stop there. They wanted to really boil down the skills into their essential components, so that each could be easily developed. Consequently, they determined that 79 microbehaviors were present within the 22 skills.
   </p>
   <p>
    If a potential leader could acquire one microbehavior at a time, then he would ultimately be able to link a series of microbehaviors, and so master a complex skill.
   </p>
   <p>
    Let’s look at the core skill Manage Priorities.
   </p>
   <p>
    The microbehaviors that make up this skill involve dividing a project into explicit tasks, establishing a hierarchy of importance within the tasks, estimating a task’s completion time and ensuring that the logic governing prioritization is clear to all involved with the project.
   </p>
   <p>
    The idea is to develop each of these microbehaviors in yourself so that they’re completely automatic before you start the next one. This is known as
    <em>
     chaining
    </em>
    .
   </p>
   <p>
    Once you’re able to practice
    <em>
     chaining
    </em>
    , you’ll start learning the much more difficult skill of prioritizing within the teams you manage, as well as within your personal life.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5cbcca826cee070008bad258" data-chapterno="3">
  <h1>
   The Leader Habit Formula helps to develop habits by linking microbehaviors to simple cues and intrinsic rewards.
  </h1>
  <div class="chapter__content">
   <p>
    You know the score. New Year’s rolls around, and you convince yourself that splashing out on a gym membership is the way to go. But, if you’re like millions of others, that resolution barely makes it to February.
   </p>
   <p>
    There’s no shame in this though: it’s proof that developing a new habit is no easy task.
   </p>
   <p>
    This is where the Leader Habit Formula comes in. Its foundation is a cycle of cues, behaviors and rewards. It develops automatized patterns of behavior that eventually require little effort. Once they’re set, sound habits will follow.
   </p>
   <p>
    Let’s look at this in detail. In the Leader Habit Formula, microbehaviors are linked to a
    <em>
     cue
    </em>
    . These are then cultivated with a simple exercise to assist habit development.
   </p>
   <p>
    One microbehavior required for Influencing Others is working out unstated or implied questions that others might have. By doing this, you can better fulfil their needs.
   </p>
   <p>
    The cue for this microbehavior is hearing someone complain. A cue like this occurs in real time and comes about naturally with no need for further prodding. This means that it’s scientifically easier to remember, say, than some note you’ve just scribbled on a piece of paper.
   </p>
   <p>
    What’s needed now is a simple and efficient exercise that you can use to influence others. So, the next time you hear someone kicking up a fuss, thank them for their concern and address it directly: What exactly do they want?
   </p>
   <p>
    For example, a customer bemoaning a one-off late fee on a subscription payment most likely wants to have the fee waived on this occasion.
   </p>
   <p>
    The next stage in the Leader Habit Formula is focusing on those microbehaviors that are loaded with inbuilt rewards.
   </p>
   <p>
    A microbehavior is
    <em>
     intrinsically rewarding
    </em>
    to you if it shares something in common with your personality.
   </p>
   <p>
    First, you’ll have to figure out what your personality type is. If you take the Leader Habit Quiz online, you’ll also learn which skills you’ll find most easy to amplify.
   </p>
   <p>
    For instance, if the quiz shows that you’re naturally a little reserved, but also very caring, it’ll indicate Active Listening as a behavior that is likely to be intrinsically rewarding to you.
   </p>
   <p>
    You’ll be surprised at just how much you appreciate working on tasks with intrinsic rewards. Studies have shown that compensation of this kind is far more individually valued than splashy, expensive rewards like vacations or shopping sprees.
   </p>
  </div>
 </div>
 <div class="chapter supplement" data-chapterid="5cbcca826cee070008bad258" data-chapterno="3">
  <h1>
  </h1>
  <div class="chapter__content">
   <p>
    It takes an average of 66 days to form a new fully automatic microbehavior.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5cbcca9a6cee070007d5c958" data-chapterno="4">
  <h1>
   “Keystone Habits” are shortcuts for developing positive habits and improving your self-image.
  </h1>
  <div class="chapter__content">
   <p>
    Learning a microbehavior doesn’t happen overnight. In fact, it takes an average of 66 days. If you worked it out, you might reckon that the 79 microbehaviors we referred to in blink two would take more than 14 years to master.
   </p>
   <p>
    Thankfully, there’s a shortcut you can use so that you don’t need to learn the habits one by one. It’s called a
    <em>
     Keystone Habit
    </em>
    , and one such habit can encourage the correction of other habits.
   </p>
   <p>
    Behavior analysts at Virginia Polytechnic Institute noticed this phenomenon while trying to encourage pizza delivery drivers to wear seat belts. They found that discussing the benefits of seat belts and putting up signs about safety around the office had an additional subsidiary effect: not only did the drivers buckle up more, they also started using their indicator signals more. In other words, wearing seat belts was a Keystone Habit connected to other safe driving habits.
   </p>
   <p>
    Keystone Habits can also be applied to leadership skills. For instance, if you work on better decision making, you’ll find that your ability to analyze information, amongst other abilities, will also improve.
   </p>
   <p>
    In practice, try getting to grips with a Keystone Habit from both of the categories Getting Things Done or Focusing on People.
   </p>
   <p>
    Another benefit of Keystone Habits are that they can enhance your self-image, making it likelier that you’ll keep developing leadership skills.
   </p>
   <p>
    John is a good example of this. He was finding it tough to get promoted to an executive position, as colleagues thought he was too authoritarian.
   </p>
   <p>
    The author tasked John with asking for other people’s views whenever they indicated that they were dissatisfied. John soon learned that, by simply listening to the responses, he began respecting people’s concerns. As a result, others were soon more disposed to listen to his own ideas.
   </p>
   <p>
    Here’s where it gets interesting. The exercise also fostered other skills in John.
   </p>
   <p>
    Soon after his desired promotion, John had to give some negative feedback to an underperforming director. Much to his surprise, he found himself turning what could have been an awkward discussion into a full-blown coaching session. Evidently, he’d learned something about mentoring along his journey!
   </p>
   <p>
    It was just the first step. As John began to utilize his improved leadership abilities, he discovered he also had the capacity to influence people, negotiate and overcome resistance.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5cbccaad6cee070008bad259" data-chapterno="5">
  <h1>
   Developing task-oriented skills helps you to become an effective leader.
  </h1>
  <div class="chapter__content">
   <p>
    Do you think of yourself more as a task-oriented leader, or a people-oriented leader?
   </p>
   <p>
    First and foremost, if you’re going to lead a  team you must be able to get stuff finished. In short you’ll need to become more efficient at task-oriented behaviors.
   </p>
   <p>
    Let’s look more closely at Plan and Organize Work, a skill that is essential for successfully completing tasks. It forms part of Getting Things Done. The idea is to frame delegation work and resources in a way that a team can easily understand.
   </p>
   <p>
    The first microbehavior demanded here is the creation of a detailed project plan. This plan should contain information regarding individual task responsibilities and a clear set of deadlines. When the project is next discussed, be sure to give the team a few minutes to suggest one actionable task and its associated deadline. For instance, they could set themselves a deadline of September 20, by which they need to produce the draft of a new brochure.
   </p>
   <p>
    The next group of skills falls under the heading Solving Problems and Decision Making. These are
    <em>
     Analyze Information
    </em>
    ,
    <em>
     Think Through Solutions
    </em>
    ,
    <em>
     Make Good Decisions
    </em>
    and
    <em>
     Focus on Customers
    </em>
    .
   </p>
   <p>
    Finding a Common Theme is an essential microbehavior needed for analyzing information.
   </p>
   <p>
    Try it out. When you next find yourself thinking about a problem, list it out using clear bullet points. Then spend a few moments trying to identify a common theme.
   </p>
   <p>
    Imagine, for instance, that your employees have been clashing, missing deadlines and working on finishing the wrong tasks. What’s the common theme here? It’s simple: your team lacks coordination.
   </p>
   <p>
    There is one final skill set that forms part of the Getting Things Done category. This is Leading Change. It breaks down into:
    <em>
     Sell the Vision
    </em>
    ,
    <em>
     Innovate
    </em>
    and
    <em>
     Manage Risk
    </em>
    .
   </p>
   <p>
    All habit-making exercises will ultimately mold your leadership abilities. If you’re set on becoming a better innovator, try brainstorming creative solutions to a problem. The next time you’re faced with an issue, take five minutes to think about what solution could be achieved – this time with infinite financial resources. The sky’s the limit!
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5cbccac86cee070007d5c959" data-chapterno="6">
  <h1>
   Engendering people-oriented skills will assist you in becoming a respected leader.
  </h1>
  <div class="chapter__content">
   <p>
    When we think about what makes a good leader, we tend to imagine people gifted with natural charisma who effortlessly influence others. But nature has nothing to do with it. With a little practice, you too can become a mover and shaker.
   </p>
   <p>
    According to a study run by the University of Central Florida, people-oriented leaders have a mild advantage in achieving results. There are three elements to people-oriented leadership skills. These are:
    <em>
     Persuasion and Influence
    </em>
    ,
    <em>
     Growing People and Teams
    </em>
    , and
    <em>
     Interpersonal Skills
    </em>
    .
   </p>
   <p>
    The Persuasion and Influence subset is made up of three skills:
    <em>
     Influence Others
    </em>
    ,
    <em>
     Overcome Individual Resistance
    </em>
    , and
    <em>
     Negotiate Well
    </em>
    .
   </p>
   <p>
    Let’s look specifically now at how you can learn to Overcome Individual Resistance.
   </p>
   <p>
    If you want to lead a successful team, it’s critical that your team is ready and able to change their methods and target. However, change is often met with hostility.
   </p>
   <p>
    If you see that a team member is less than enthusiastic, you’re going to have to acknowledge their concerns. You do this by asking him what his concerns are. Once you’ve addressed the specific challenges that concern him, you’ll be able to successfully assist him in overcoming his fears. The trick is to  highlight the benefits of change and identify the areas where you see eye-to-eye.
   </p>
   <p>
    The next skill set that forms part of people-oriented leadership is Growing People and Teams. The skills here are:
    <em>
     Empower Others
    </em>
    ,
    <em>
     Mentor and Coach
    </em>
    , and
    <em>
     Build Team Spirit
    </em>
    .
   </p>
   <p>
    Take Empower Others. If you find allowing others to make decisions difficult, you’ll need to train yourself in empowering others. When someone next conveys their misgivings or starts venting, be sure to demonstrate your support. At the same time, don’t just bail him out of his responsibilities. Instead acknowledge his concerns, and ask how you can help.
   </p>
   <p>
    Finally, the Interpersonal Skills required for people-oriented leadership include learning to
    <em>
     Build Strategic Relationships
    </em>
    ,
    <em>
     Show Care
    </em>
    ,
    <em>
     Listen Actively
    </em>
    ,
    <em>
     Communicate Clearly
    </em>
    , and
    <em>
     Speak with Charisma
    </em>
    .
   </p>
   <p>
    Let’s look at the last one, Speak with Charisma. It’s not actually that hard to become a charismatic speaker, you just have to fire up the imagination. You can start by using vivid metaphors and sparkling similes.
   </p>
   <p>
    For example, if you’re pushing a new dental hygiene app, what about comparing it to the fitness tracking device Fitbit – but for teeth!
   </p>
   <p>
    The path to becoming a better leader is plain enough. Step by step, if you layer good habit upon good habit, you’ll find yourself with a network of behaviors that will precipitate exactly those skills that you’re going to need as a world-class leader.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5cbccadc6cee070008bad25a" data-chapterno="7">
  <h1>
   Final summary
  </h1>
  <div class="chapter__content">
   <p>
    The key message in these blinks:
   </p>
   <p>
    <strong>
     Leadership isn’t about knowledge – it’s best understood as a series of habits that link leadership microbehaviors to specific cues. If you can change the way you respond to situations, it’s possible to develop exemplary leadership skills. Five minutes a day is all you need. In using the power of chaining and establishing keystone habits, you’ll fast-track your development and improve your self-image so that you can become an effective leader.
    </strong>
   </p>
   <p>
    Actionable advice:
   </p>
   <p>
    <strong>
     Develop team-building activities to create team spirit.
    </strong>
   </p>
   <p>
    In order to lead a successful team, you need to facilitate cohesion by creating a sense of team spirit. When you think of team-building activities, you probably think of after-work drinks or bowling. But team-building should be a habit that is implemented on a daily basis. The next time you communicate with someone – either in person or by email – take five minutes to put her in touch with another person from whom you think she could benefit. This could be personal or professional – just tell her why you think she would enjoy meeting that person.
   </p>
   <p>
    <strong>
     Got feedback?
    </strong>
   </p>
   <p>
    We’d sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
   </p>
   <p>
    <strong>
     What to read next:
    </strong>
    <strong>
     <em>
      The Coaching Habit
     </em>
    </strong>
    <strong>
     , by Michael Bungay Stanier
    </strong>
   </p>
   <p>
    In these blinks you’ve learned what it takes, in practice, to become a great leader. But working as part of a team or within a community requires more than leadership for task completion. Sometimes you have to think of the individual. Sometimes you have to be ready to coach and mentor. The blinks to
    <em>
     The Coaching Habit
    </em>
    (2016) shows just how coaching works, component by component. Coaching isn’t about giving advice, but demonstrating how success can truly be achieved.
   </p>
  </div>
 </div>
</article>
