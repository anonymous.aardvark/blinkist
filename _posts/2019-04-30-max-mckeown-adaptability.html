---
layout: post
title: "Max McKeown - Adaptability"
description: "Adaptability (2012) examines a skill that’s becoming ever more important in today’s fast-paced and highly fickle business environment: the ability to adapt. It’s what makes the difference between successful innovators who go on to thrive and stick-in-the-muds who struggle to survive or simply go under. Packed with illuminating portraits of both, these blinks analyze adaptability in action everywhere from the golf course to the battlefield and the boardroom."
image: https://images.blinkist.com/images/books/5c9b94406cee070007cb64ec/1_1/470.jpg
---

<article class="shared__reader__blink reader__container__content" lang="en">
 <div class="chapter chapter" data-chapterid="5c9b95056cee070007d5189e" data-chapterno="0">
  <h1>
   What’s in it for me? An analysis of adaptability in action.
  </h1>
  <div class="chapter__content">
   <p>
    What do Allied defense departments battling Nazi Germany in the Second World War, the creators of the iconic Mini Cooper and Starbucks have in common? In a word,
    <em>
     adaptability
    </em>
    – the ability to change with the times and piggyback on technological and social changes to achieve success.
   </p>
   <p>
    Adaptability is a skill that’s becoming indispensable in today’s frenetic world of business. Markets and consumer tastes are fickle, and nothing is as likely to land you in hot water as complacency. That’s something business strategist Max McKeown knows a thing or two about from his work with clients like Microsoft and Sony, who’ve mastered the art of anticipating the future and changing course accordingly.
   </p>
   <p>
    But don’t just take his word for it. In these blinks, we’ll analyze adaptability in action everywhere from a small Italian village battling post-financial crisis austerity measures, to the boardroom of a multinational coffee chain and the post-war British car industry.
   </p>
   <p>
    Along the way, you’ll find out
   </p>
   <ul>
    <li>
     why Ford turned down a US government bailout proposal in 2008;
    </li>
    <li>
     how Netflix almost came unstuck by pushing adaptability too hard; and
    </li>
    <li>
     why it sometimes takes a radical to do the right thing.
    </li>
   </ul>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5c9b95596cee070007d5189f" data-chapterno="1">
  <h1>
   Life is unpredictable even for the successful, which is why adaptability is so important.
  </h1>
  <div class="chapter__content">
   <p>
    In 2009, American golf star Tiger Woods’s position in the sport’s global rankings took a nosedive. By 2011 he’d slipped from number one to 58th place. It was an odd sight, especially since Woods had been known for his incredible consistency since bursting onto the scene in 1996. So what went wrong?
   </p>
   <p>
    Well, it was a sign of the golfing god’s mortality. Life is unpredictable, and even the most successful people can’t always dodge the slings and arrows of outrageous fortune. It turned out that Woods’s on-field performance problems had roots in his personal life.
   </p>
   <p>
    In 2009, the media ran a story about him crashing his car outside his house. Neighbors told reporters they’d seen his wife chasing after him with a golf club in her hands. Rumors of substance abuse and infidelity began circulating and major sponsors including Gatorade and Gillette dropped their formerly prized sponsee.
   </p>
   <p>
    Woods didn’t let any of that get to him, however. In fact, he demonstrated just how important adaptability is when you’re faced with setbacks. He continued working hard, even as his stats slipped, and learned how to deal with the media pressure he was under. His perseverance paid off. By March 2013, he was back at the top of world golf rankings.
   </p>
   <p>
    That makes Woods a poster boy for what the author calls, “High adaptability, high achievement people,” or HAHAs for short. That’s a pretty fitting acronym: HAHAs are people who can laugh in the face of adversity and, over time, claw their way back to the top.
   </p>
   <p>
    What sets them apart is their ability to focus on solutions rather than problems. They look on the bright side of life even as things seem to be falling apart around them, and they remain determined to achieve their goals. They’re also typically unafraid to ask for help and reach out to people who can support them in their struggle to reassert themselves.
   </p>
   <p>
    But these blinks aren’t just about golf. Now that we’ve seen what adaptability looks like in practice, let’s take a look at how it works in business contexts.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5c9b95746cee070007cb64ef" data-chapterno="2">
  <h1>
   Only companies that have perfected the art of adaptability will truly succeed.
  </h1>
  <div class="chapter__content">
   <p>
    At the height of the 2008 financial crisis, the US government offered to bail out car manufacturing giant Ford. Despite being up to its eyeballs in debt and looking like it might go under any day, the company ultimately turned down the proposal.
   </p>
   <p>
    There was a good reason for this refusal. Chairman Bill Ford was convinced that companies that fail to adapt fail, period. Sure, an injection of government cash might have solved Ford’s short-term liquidity issues, but it wouldn’t have gotten to the root of the problem – its longstanding failure to adapt to the new realities of the automobile market.
   </p>
   <p>
    The company’s board of directors and its managers decided on an alternative strategy and hatched a plan they called “The Way Forward.” At the heart of this roadmap was a reappraisal of Ford’s relationship with the environment, an issue it had long sidestepped. If the carmaker wanted to remain relevant to American consumers, it would have to move toward their views on environmental matters.
   </p>
   <p>
    What followed was a pretty radical overhaul. Ford downsized by around 25 percent and implemented a system to produce cars more quickly. Most importantly, it shifted to manufacturing smaller and more fuel-efficient cars.
   </p>
   <p>
    The carmaker dodged a bullet, but the company almost left it too late. A better idea than waiting it out and hoping for the best would’ve been to take a leaf out of Toyota’s book. The Japanese corporation is a master at adapting to changing market conditions, a characteristic that allowed it to increase its share of the global car market from 7.3 percent in 1995 to 15 percent in 2005.
   </p>
   <p>
    So what’s the secret of its success? Well, the company has a solid reputation for quality products, but what really sets it apart is its constant search for improvements that satisfy changing consumer preferences. This allowed the company to stay ahead of the curve and anticipate changes in the market long before competitors like Ford saw them coming.
   </p>
   <p>
    To take just a couple of examples, Toyota was already developing low-emission cars in 1992 and hybrid petrol and electricity-powered vehicles in 1995!
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5c9b95a66cee070007d518a0" data-chapterno="3">
  <h1>
   Sometimes adaptability means swimming against the current, and that can help the environment.
  </h1>
  <div class="chapter__content">
   <p>
    In the summer of 2011, the Italian government was facing financial issues. Deciding to cut back on expenditures, it introduced a reform to incorporate villages with fewer than 1,000 inhabitants into larger administrative units headed by a single mayor. But one small village called Filettino resisted these measures. It understood that adaptability sometimes means swimming against the current.
   </p>
   <p>
    As far as the villagers were concerned, reacting to new developments didn’t mean rushing into old reforms or embracing change for change’s sake. In their view, one way of adapting was to defend the status quo, and that’s precisely what the mayor of Filettino decided to do.
   </p>
   <p>
    Defying the government, he declared the village’s independence and issued its own currency. It was called the fiorito, meaning ‘flowering’ in Italian – a reference to the settlement’s belief that it would continue to flourish. The village looked to the past for inspiration, harking back to the age before unification when Italy was ruled by many small city-states, kingdoms and principalities. In the end, its act of resistance preserved Filettino’s independence and sense of community.
   </p>
   <p>
    Headstrong Italian villagers aren’t the only beneficiaries of swimming against the current, however. Bucking trends and following one’s own path has also helped a number of companies thrive.
   </p>
   <p>
    Take Levi Strauss as an example. Manufacturing jeans is traditionally a resource-intensive operation. The finishing process alone requires around ten separate washes and gallons of water. If the jeans have a special pattern or a fade effect, that requirement increases even further. That was accepted as a given until the American firm decided to shake things up.
   </p>
   <p>
    Rather than simply going along with the idea that the only thing that matters in business is generating profits, Levi Strauss began factoring environmental considerations into their calculations. Soon enough the company had figured out a way of finishing jeans without using any water at all. By using stones to soften the fabric and rinsing them with a special type of resin, the company cut its water consumption by an incredible 96 percent!
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5c9b95d86cee070007cb64f0" data-chapterno="4">
  <h1>
   Adaptability is an innate part of the way the brain functions.
  </h1>
  <div class="chapter__content">
   <p>
    It was a regular day in New York in 1985 when Pedro Bach-y-Rita, a Spanish teacher who’d lived a happy and successful life in the city for many years, suddenly collapsed. He’d suffered a massive stroke and was left paralyzed. The doctors treating Bach-y-Rita claimed there was nothing they could do for him.
   </p>
   <p>
    But they were wrong.
   </p>
   <p>
    What they had underestimated was how adaptable humans are. Both of Bach-y-Rita’s sons were in medical school when their father fell ill. Unhappy with his doctors’ diagnoses, they decided to physically re-educate him from scratch, as though he were a baby. The first task they set themselves was teaching him to crawl using kneepads and the support of a wall.
   </p>
   <p>
    Once he’d mastered that, they began setting more difficult tasks like catching balls, which were designed to train his motor systems. Bach-y-Rita made remarkable progress under their supervision. He was soon sitting and, a little later, walking. Astonishingly, within a
    <em>
     year
    </em>
    he was back at work teaching Spanish at the City College of New York, where he remained until his retirement.
   </p>
   <p>
    So how did Bach-y-Rita regain control over his basic motor functions despite the serious brain damage he’d suffered during his stroke? It comes down to the plasticity of the human brain. Essentially, the undamaged parts of his brain took control of the damaged areas.
   </p>
   <p>
    Bach-y-Rita’s son Paul returned to medical school after helping his father. He later became one of the first scientists to verify the theory of
    <em>
     neural plasticity
    </em>
    – the idea that the human brain’s capacities and functions aren’t set in stone but can adapt and change.
   </p>
   <p>
    In one experiment, Paul demonstrated that blindfolded participants were able to catch balls thanks to a head-mounted camera which relayed images to their brains through sensory receptors on their tongues. That’s a shining example of how different neural pathways and neurons in the brain can adapt and tackle new tasks, like interpreting images.
   </p>
  </div>
 </div>
 <div class="chapter supplement" data-chapterid="5c9b95d86cee070007cb64f0" data-chapterno="4">
  <h1>
  </h1>
  <div class="chapter__content">
   <p>
    <em>
     “People severely damaged by strokes...could now be rehabilitated by accessing the brain’s inbuilt adaptability.”
    </em>
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5c9b95f36cee070007d518b2" data-chapterno="5">
  <h1>
   Effective adaptation means learning from mistakes and resisting the urge to go back to square one.
  </h1>
  <div class="chapter__content">
   <p>
    When people first fail at a task, they tend to lower their expectations and claim that at least they learned something from their mistakes. The crucial question, though, is
    <em>
     what
    </em>
    past failures teach us: Do we learn how to
    <em>
     fail
    </em>
    , or how to
    <em>
     improve
    </em>
    so we don’t fail again?
   </p>
   <p>
    Adaptability is, in large part, the art of learning from mistakes – ideally, the mistakes of others! Take the British automobile industry. In the 1950s, virtually every carmaker was set on developing ever more powerful – and fuel-guzzling – vehicles. They were so fixated on this goal that they overlooked a massive segment of the market: young, urban and environmentally-minded people.
   </p>
   <p>
    The advantage thus passed to German competitors who were churning out popular, compact microcars like the Messerschmitt KR200. As a result, British brands were being crowded out altogether. Did that make them change their approach? Hardly. There was, however, one notable exception: a small team of auto engineers at the British Motor Company headed by designer Sir Alec Issigonis.
   </p>
   <p>
    Observing their peers’ stubborn refusal to change with the times, the team decided to spearhead the production of a new style of car – the iconic Mini marketed by Morris Mini-Minor. Learning from the mistakes of others and adapting to new consumer preferences quickly led to success. Over the following decades, the British Motor Company went on to sell over five million Minis.
   </p>
   <p>
    That said, you can take learning from mistakes too far, as PepsiCo found out in 2009 when it developed a new brand image for Tropicana orange juice. When the company’s massive marketing campaign backfired and sales plummeted by 20 percent, PepsiCo panicked.
   </p>
   <p>
    Realizing that the branding made the product look anonymous and cheap rather than promoting awareness of its quality, they scrambled back to square one. Instead of making a few minor tweaks and fixing the mistake, they reverted to the old branding. The upshot? They blew $33 million to change absolutely nothing!
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5c9b960e6cee070007cb64f1" data-chapterno="6">
  <h1>
   Successful companies understand that experimentation is crucial to adaptability.
  </h1>
  <div class="chapter__content">
   <p>
    In 1940, Adolf Hitler ordered the shutdown of all weapons development research programs expected to take longer than six months to reach completion. It was a costly miscalculation for Germany. Why? Because experimentation is the cornerstone of effective adaptation.
   </p>
   <p>
    The mistake would haunt Nazi Germany as the tide gradually turned against it in the Second World War. While German scientists were shackled to short-term targets, their Allied counterparts were given free rein to experiment with new weapon technologies for as long as they pleased. When defense scientist William Butement came up with an idea to develop a proximity fuse, for example, he was encouraged to delve deeper.
   </p>
   <p>
    A proximity fuse is a useful piece of gadgetry if you’re in the middle of a shooting war. A bomb fitted with the fuse’s radar technology will only detonate when it’s sufficiently close to its targets. Older bomb types, on the other hand, used standard timers that often exploded long before – or after – they’d reached their targets. Needless to say, the bombs with the proximity fuses did the most damage.
   </p>
   <p>
    Crafting the perfect proximity fuse took time and dedication, and it was only after years of experimentation with prototypes that the finished article was ready for deployment. It came just in the nick of time. In 1944, a German counteroffensive caught the Allies off guard and the outcome of the Battle of the Bulge hung in the balance. Lucky that they had the perfect weapon to beat back the onslaught: artillery units equipped with lethally accurate proximity fuse shells.
   </p>
   <p>
    Experimentation also has its uses in the metaphorical war between businesses. Take Apple, a company that has long been synonymous with a culture of experimentation. When its new products meet a negative reception, it shelves them and gets to work on a superior alternative. You may or may not remember Apple’s first handheld computer – the clunky, error-prone Newton. The Newton crashed and burned and Apple responded by going back to the drawing board. The result? Its designers used the prototype to develop the iPod, iPhone and iPad.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5c9b962d6cee070007d518b3" data-chapterno="7">
  <h1>
   Rushing to adapt can lead to a crash, and failing to think ahead isn’t any better.
  </h1>
  <div class="chapter__content">
   <p>
    Learning to drive is thrilling. The world suddenly becomes broader as exciting new possibilities come into focus. But you have to be careful. It’s easy to get carried away and put your foot down. That’s when things go wrong and you find yourself at risk.
   </p>
   <p>
    The same thing happens to businesses when they push too hard: the wheels come off and they crash. Take Netflix. The company understood that the future of movie watching was streaming before any of its competitors, and it steamed ahead. What was the problem, then? It was
    <em>
     too
    </em>
    quick for its subscribers, who were still satisfied with the old offer which allowed them to rent DVDs and stream for $9.99 a month.
   </p>
   <p>
    In 2011, Netflix decided to push its customers and split their offering into two separate components: rentals and streaming, now priced at $7.99 each – a pretty hefty price hike for people who wanted to continue using both services.
   </p>
   <p>
    The decision didn’t go down well. Netflix hemorrhaged a million subscribers and the value of its shares fell by 25 percent. The service did eventually recover, of course, but if it hadn’t been so hasty it would have achieved its current success much sooner.
   </p>
   <p>
    Failing to anticipate changes in the market is even more damaging, however, as Blockbuster found out. Founded in 1985, the movie rental business quickly grew over the following decades. By 2008, it had thousands of stores across the US. But despite its dominant position in the market, it was caught entirely unawares by the streaming revolution.
   </p>
   <p>
    Even worse, it failed to launch its own streaming service while Netflix began establishing itself. That would have been simple enough – after all, Blockbuster held all the aces: it was a household name with a large customer base and plenty of capital.
   </p>
   <p>
    But Blockbuster wasn’t nimble enough to adapt to new realities. It continued focusing on customer experience in its bricks-and-mortar stores even after a new CEO was hired in 2007. By 2010, the writing was on the wall. Blockbuster declared bankruptcy and was gobbled up by the American TV company Dish Network.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5c9b96476cee070007cb64f2" data-chapterno="8">
  <h1>
   Radical leadership is often the only solution when companies lose sight of their goals.
  </h1>
  <div class="chapter__content">
   <p>
    It’s easy to look good when the economy’s booming and you’re backed by a reputation built up over decades. But success can breed complacency. The result of taking your eye off the ball? You lose sight of your goals.
   </p>
   <p>
    Take Starbucks. Everything had been plain sailing for decades when it hit the rocks in 2007. The coffee chain’s chairman Howard Schultz had an idea about what had gone wrong: its dominant position had incubated an arrogant outlook and the company had stopped taking care of its customers.
   </p>
   <p>
    He was right. Formerly loyal patrons weren’t happy and had started going elsewhere in search of their coffee fix. That same year, Starbucks closed over 900 stores and fired 1,000 of its employees. That was the end of the growth policy championed by the company’s CEO between 2002 and 2007, Jim Donald, who had become obsessed with opening more and more stores. The mad rush to expand had resulted in Starbucks losing touch with its founding values.
   </p>
   <p>
    In the end, Starbucks scraped through this rough patch. So how did it turn things around? Well, it had a leader who was prepared to push through radical reforms: Howard Schultz. One of the first things he did after taking charge was to close 7,000 Starbucks stores in the US and give every barista extra training to help them up their coffee game. When a test revealed that McDonald’s had better-tasting coffee than Starbucks, Schultz introduced new roasting and grinding processes.
   </p>
   <p>
    None of that was rocket science. Schultz simply understood that a coffee chain has to do two things to retain its customers’ loyalty: prepare a great cup of joe and offer a selection of delicious cakes and pastries. But sometimes it takes a radical visionary to do the commonsensical thing. By 2010, his policy had paid off. Starbucks had recovered and its revenue increased to $10.7 billion!
   </p>
   <p>
    That just goes to show how important adaptability is when you’re struggling to get ahead or just treading water. While you shouldn’t rush change, it’s worth keeping an open mind, experimenting and seeing what incremental changes you can make to improve your situation.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5c9b96666cee070007d518b4" data-chapterno="9">
  <h1>
   Final summary
  </h1>
  <div class="chapter__content">
   <p>
    The key message in these blinks:
   </p>
   <p>
    <strong>
     Adaptability is all about looking ahead, reading the signs and using the prevailing winds to chart your course. Once you’ve mastered that art, you can plan ahead and avoid being caught off guard by sudden changes. That’s especially true in business. The most successful companies consistently demonstrate an ability to change with the times, experiment with new solutions and adapt themselves to customers’ changing desires and needs.
    </strong>
   </p>
   <p>
    Actionable advice:
   </p>
   <p>
    <strong>
     Believe in the impossible.
    </strong>
   </p>
   <p>
    What stops us adapting? Well, nothing throttles innovation like the belief that some things are simply impossible. Take it from American biologist George Church. He was convinced he could design a machine that could decode the entire human genome. Ignoring the naysayers, Church pressed ahead with his vision and constructed his device. While the procedure initially cost a staggering $3 billion, Church managed to reduce the price to just $5,000 over the years. Today, there’s a real chance that it might become affordable enough to be integrated into routine medical testing, opening up the possibility of all sorts of medical breakthroughs!
   </p>
   <p>
    <strong>
     Got feedback?
    </strong>
   </p>
   <p>
    We’d love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
   </p>
   <p>
    <strong>
     What to read next:
    </strong>
    <strong>
     <em>
      Team of Teams
     </em>
    </strong>
    <strong>
     , by General Stanley McChrystal with Tantum Collins, David Silverman and Chris Fussell
    </strong>
   </p>
   <p>
    As you’ve learned in these blinks, organizations thrive when they’re adaptable and able to respond decisively to new risks. Few people know that better than General Stanley McChrystal, a former US Navy SEAL who cut his teeth in high-risk environments where agility can mean the difference between life and death.
   </p>
   <p>
    So if you’ve enjoyed this look at adaptability in action, why not dip into our blinks to
    <em>
     Team of Teams
    </em>
    , a study of what adaptable teams can do for your business.
   </p>
  </div>
 </div>
</article>
