---
layout: post
title: "Jeremie Kubicek and Steve Cockram - 5 Voices"
description: "5 Voices (2016) is a handbook designed to help you communicate more effectively in your professional and personal life. To do this, leadership experts Jeremie Kubicek and Steve Cockram argue, you need to get a handle on what type of communicator you are. These blinks outline five communicative strategies and provide a wealth of tips and tricks that’ll help you get your point across without getting in peoples’ faces."
image: https://images.blinkist.com/images/books/5ce86e596cee0700073a5aa8/1_1/470.jpg
---

<article class="shared__reader__blink reader__container__content" lang="en">
 <div class="chapter chapter" data-chapterid="5ce86e7c6cee070007a4b8cc" data-chapterno="0">
  <h1>
   What’s in it for me? A crash course in communication.
  </h1>
  <div class="chapter__content">
   <p>
    The ancient Greek philosopher Plato developed his theories through conversations. Dialogue, he believed, reflects the way great ideas first emerge: through a kind of verbal sparring match, each contestant clarifies and improves on what the other has just said. At its best, communication can be open, receptive and constantly moving toward a shared understanding.
   </p>
   <p>
    If you’ve had a row with your partner or been in a meeting recently, you’ll know that communication in the real world can fall significantly short of this ideal. It’s all too easy to get bogged down in your own way of seeing things, to stop listening to others and to start repeating yourself.
   </p>
   <p>
    So what’s going wrong? Well, Jeremie Kubicek and Steve Cockram believe they’ve found the cause of all this unnecessary strife: if you don’t understand how to talk to others, it’s likely because you haven’t realized that everyone uses different “voices.” Individuals switch between multiple voices, but usually fall back on one dominant voice.
   </p>
   <p>
    Getting to grips with the communicative advantages and disadvantages of each of these unique voices, makes us more mindful about when to use one and refrain from using another. And that makes both workplace and personal conversations much more productive and rewarding.
   </p>
   <p>
    In these blinks, you’ll learn:
   </p>
   <ul>
    <li>
     how to identify the five different voices;
    </li>
    <li>
     when to give way and when to hold your ground in a verbal contest; and
    </li>
    <li>
     how to encourage those who usually stay quiet to speak up.
    </li>
   </ul>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5ce86e946cee0700073a5aa9" data-chapterno="1">
  <h1>
   Nurturers look out for others rather than themselves, and often go unheard.
  </h1>
  <div class="chapter__content">
   <p>
    When it comes to communication, the authors identify five different “voices” we all use. We switch between them depending on the situations we find ourselves in, but most of us fall back on a single dominant voice – our primary means of self-expression. So what are these voices? Well, say you’re someone who values the input of others and emphasizes group harmony: your predominant voice is that of a
    <em>
     nurturer
    </em>
    .
   </p>
   <p>
    Nurturers go out of their way to look out for those around them. They often put others’ needs ahead of their own and are most comfortable in environments where kindness and shared values come first. Nurturers are firmly anchored in the present: they want to take action now to ensure that relationships and the companies they work for flourish in the future. Nurturers also tend to believe that people are more important than profits.
   </p>
   <p>
    A nurturing manager, for example, will put in extra hours to talk to employees and make sure that they’re happy with their work, feel motivated and have everything they need to do their jobs well. Outside work, a nurturer is the kind of person who’ll cancel his own plans to help a friend move into a new apartment.
   </p>
   <p>
    That all sounds positive, right? It is, but nurturing types often run into a problem: they struggle to make themselves heard. This can have a big knock-on effect in organizations – after all, nurturers will keep their eyes on the wellbeing of people when everyone else is scrambling to meet targets. Without them, companies risk embracing a dog-eat-dog ethos that’s bound to undermine morale in the long run.
   </p>
   <p>
    But because they’re modest by nature, nurturers often undervalue their own contributions and end up keeping their heads down rather than speaking up. And here’s a crazy thing: according to the authors’ calculations, around 43 percent of the population is made up of nurturers. That’s a huge number of people who aren’t having their concerns taken seriously! We’ll explore what can be done about this later on. But before that, let’s take a closer look at the second voice.
   </p>
  </div>
 </div>
 <div class="chapter supplement" data-chapterid="5ce86e946cee0700073a5aa9" data-chapterno="1">
  <h1>
  </h1>
  <div class="chapter__content">
   <p>
    “
    <em>
     Nurturers at their core are all about taking care of people.
    </em>
    ”
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5ce86eae6cee070007a4b8cd" data-chapterno="2">
  <h1>
   Creatives are the voices of innovation, but people often struggle to understand their ideas.
  </h1>
  <div class="chapter__content">
   <p>
    What’s your dominant voice? Let’s find out. If you’re highly imaginative, always looking to the future and daydreaming about the next big invention or business idea, chances are you’re a
    <em>
     creative
    </em>
    . This is the voice of innovation and change.
   </p>
   <p>
    You’ll find creatives in every walk of life. The mind behind the Post-it Note is a creative. So are artists, engineers and hobbyists tinkering in garages or coming up with new ways to insulate buildings. What they all have in common is that they’re interested in answering old questions in new ways, and set on redefining the questions being asked.
   </p>
   <p>
    The authors estimate that creatives are a pretty rare species: by their calculations, just nine percent of the population falls into this category. Creatives tend to gravitate toward organizations and institutions that reward out-of-the-box thinking – areas like academia, technology, the arts and the nonprofit sector.
   </p>
   <p>
    Working with creatives can be both intensely rewarding and supremely frustrating for other personality types. That’s because there’s a thin line between the brilliantly innovative and the downright unrealistic. But that’s not the only issue: creatives also struggle to make others understand their ideas.
   </p>
   <p>
    Why? Well, creativity is about inspiration and serendipity, and thus can be pretty messy. When creatives attempt to explain their thought processes to the more logically minded, their insights will often leave their audience cold or confused. That’s a painful experience for creatives and it often leads to them retreating into their shells.
   </p>
   <p>
    Take Paul, a graphic designer who worked with the authors. When he pitched his ideas for a branding campaign, he was met with a wall of silence. A week later, another team member pitched almost the exact same idea with greater clarity and charisma, and the team loved it. Few experiences are more likely to undermine the self-confidence of a creative! The best way to avoid such situations is for creatives to pay more attention to the way they communicate their ideas, and for non-creatives to make an effort to show that they value creative contributions.
   </p>
  </div>
 </div>
 <div class="chapter supplement" data-chapterid="5ce86eae6cee070007a4b8cd" data-chapterno="2">
  <h1>
  </h1>
  <div class="chapter__content">
   <p>
    <em>
     “Creatives communicate and function best when they truly know that their contributions are valued and appreciated.”
    </em>
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5ce86ec26cee0700073a5aaa" data-chapterno="3">
  <h1>
   Guardians seek to protect traditional values and are highly pragmatic.
  </h1>
  <div class="chapter__content">
   <p>
    If you mapped different voices onto political beliefs,
    <em>
     guardians
    </em>
    would undoubtedly be conservatives. Whether you regard conservatism as a hidebound defense of old evils or a necessary bulwark against new ones, there’s no disputing that it’s an important voice in society.
   </p>
   <p>
    So what exactly do guardians stand for? Well, as the name suggests, their primary aim is to protect the tried and tested ways of doing things. Like nurturers, guardians are focused on the present. Their motivation isn’t to protect the interests of people, however, but to prevent what they see as unnecessary change. That’s because they see more positives than negatives in the current system, and emphasize its successes rather than its shortcomings.
   </p>
   <p>
    Like conservatives, guardians often get a bad press. In an age that cherishes openness and innovation, guardians are likely to come across as boring and restrictive. As a result, they’re often underestimated. Lots of people regard arguing in favor of the status quo as the least sophisticated position, and thus conclude that those who do so can’t be all that clever! This dismissive view tends to overlook the fact that guardians usually have very good reasons for defending the present order.
   </p>
   <p>
    Guardians are also defined by their pragmatism and realism – qualities that allow them to quickly identify the drawbacks of new ideas. By rejecting overly ambitious and unfeasible schemes, guardians often end up saving organizations huge amounts of time, energy and money that might otherwise have been squandered on dead-end projects.
   </p>
   <p>
    That also goes for deals and opportunities that seem too good to be true. If your company is presented with an offer for cut-price magic beans, you can be sure that the guardians will be doing the due diligence on precisely what riches the beans are likely to yield over the next quarter. Boring they may be, but guardians are frequently all that stands between an organization and the kind of reckless, impulsive decisions that can undo years of good work in a single day!
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5ce86ed66cee070007a4b8ce" data-chapterno="4">
  <h1>
   Connectors bring people together, forge new bonds and maintain existing relationships.
  </h1>
  <div class="chapter__content">
   <p>
    Imagine throwing a party and inviting people from the different areas of your life – your friends, family, colleagues and neighbors. It might be tricky to get everyone on the same page and feeling comfortable around each other, right? Well, it’s in such situations that
    <em>
     connectors
    </em>
    come into their own. These are the people who effortlessly bring individuals together, and who can get on with strangers as if they’ve known them for years.
   </p>
   <p>
    You can spot a connector by a couple of telltale signs. If there’s a problem in the office, a connector will reach out to a peer who’s well placed to resolve the issue quickly and efficiently. Connecters also tend to be generous with their contacts. If you’re looking for a new job and a connector knows someone who’s hiring, she’ll almost certainly put the two of you in touch. The downside of having such brilliant people skills is that it’s easy to end up with lots of acquaintances but few true friends – the most common drawback of being a social butterfly.
   </p>
   <p>
    That said, connectors thrive in more formal settings like the workplace. In fact, they’re invaluable additions to any team or company. This stands to reason: they’re invariably excellent motivators and are infectiously enthusiastic about virtually every activity. And if they’re fascinated by something or find it rewarding, they won’t keep it to themselves. A connector who suddenly discovers a love of Nordic walking will soon have the rest of her team stomping around in the countryside with her!
   </p>
   <p>
    All these traits boil down to one thing: connectors cherish collaboration, and will always go out of their way to get everyone involved and having fun in the workplace. This makes them a great asset to any organization that relies on teamwork.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5ce86ef46cee0700073a5aab" data-chapterno="5">
  <h1>
   Pioneers are fanatical about achieving their goals and don’t shy away from making tough calls.
  </h1>
  <div class="chapter__content">
   <p>
    Some people are simply impossible to argue with. It’s not necessarily that they have the loudest voices – though they often
    <em>
     do
    </em>
    – so much as the fact that there’s no budging them from their position. Whether they’re right or wrong, they’ll argue their case with such conviction that countering them is like trying to turn back the tide. If that rings a bell, there’s a good chance you know a
    <em>
     pioneer
    </em>
    !
   </p>
   <p>
    If pioneers are stubborn, it’s because they’re strategic thinkers who have a very strong and clear vision of their personal and professional goals. Getting in the way of this is like stepping onto the tracks in front of a freight train: you’re going to get flattened. Their other defining trait is ambition. Pioneers don’t open a mere coffee shop or burger joint – they envisage their modest enterprise as the first step toward a globe-spanning commercial empire as ubiquitous and successful as Starbucks or McDonald’s!
   </p>
   <p>
    Those intent on achieving fame and fortune should be prepared to make some tough calls, and that’s just what pioneers do. As they see it, work is simultaneously a game and a winner-takes-all battle. Whatever the objective, pioneers are in it to win it by any means necessary. As in chess, pawns sometimes have to be sacrificed to secure long-term objectives.
   </p>
   <p>
    Take it from Jane Fardon, the English CEO of a struggling cosmetics business. A recession had undermined the company to such an extent that, from a financial point of view at least, the only sensible decision was to shutter the operation. But remember what we said about everyone having multiple voices? Well, Jane’s inner nurturer was telling her to hold out – after all, if she closed the business down her employees would be out of work.
   </p>
   <p>
    With the authors’ help, however, she realized that her strongest voice was her inner pioneer and that she should learn to trust that instinct. She dissolved the company and rebuilt her cosmetics empire from the ground up. Today, the new business is thriving and Jane has settled on a compromise to end the conflict between her pioneering and nurturing voices: while she makes the big strategic calls, the day-to-day running of the company is in the hands of trusted nurturers and guardians.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5ce86f086cee070007a4b8cf" data-chapterno="6">
  <h1>
   Encourage nurturers to speak up and allow creatives to think outside the box.
  </h1>
  <div class="chapter__content">
   <p>
    By now you should have a good understanding of the five voices. But what happens when you bring them all together in one room? If you’ve been in a team meeting lately, you may already know the answer: the loudest people end up talking at cross purposes while the quiet types sit it out on the sidelines. But it doesn’t have to be that way. In the next couple of blinks, we’ll take a look at what you can do to improve each team member’s experience.
   </p>
   <p>
    Let’s start with nurturers. As we’ve seen, nurturers often undervalue their own contributions and rarely speak up. The key here is to explicitly encourage them to make themselves heard, and to rein in more vocal members when nurturers are talking. That’s important because the more opinions a nurturer hears before his own turn comes around, the less confident he’ll be about presenting his ideas.
   </p>
   <p>
    Start by taking a moment to convince a nurturer that his views really are valued. To do that, you’ll have to make it clear that you understand that his voice best expresses the concerns of the people who depend on your company, whether these be employees or customers. The next step is to have a word with other team members and tell them that criticism of the nurturer’s ideas is off-limits for the time being. You don’t want to wrap him in cotton wool, but it’s helpful to protect him as he becomes more confident. If his ideas really aren’t feasible, keep your critique factual, and make a solid case explaining why the ideas won’t work, rather than getting personal.
   </p>
   <p>
    Next up: creatives. Here you’ll need to redefine the creative’s role to give her free rein to think outside the box. To do that, you’ll have to first accept that the craziest ideas can lead to something brilliant if they’re given time and careful consideration, so you should resist the urge to reject them outright. Finally, you’ll need to learn to live with the fact that some ideas will be duds – just think of them as rough sketches leading toward that masterpiece! Explaining this to other team members and asking them to refrain from strong criticism, and to instead pose clarifying, constructive questions, should help speed the process along.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5ce86f246cee0700073a5aac" data-chapterno="7">
  <h1>
   Letting guardians ask tough questions, encouraging connectors’ passions and asking pioneers to speak last will lead to more positive meetings.
  </h1>
  <div class="chapter__content">
   <p>
    So far we’ve looked at how to encourage the quieter voices in your team to share their ideas and insights. Now it’s time to explore how to manage the more boisterous types, starting with guardians.
   </p>
   <p>
    As we know, guardians are skeptical about change. While others might view them as a nuisance, they see their tough questions as an essential part of a healthy company culture. And here’s the thing: they’re right. That means it’s your job to explain to other team members that guardians aren’t the enemy and that when they show up to meetings with reams of carefully structured notes and insist on going over every point in minuscule detail, it’s for the good of the organization.
   </p>
   <p>
    So how do you do that? Well, it’s a good idea to remind other personality types of the dangers guardians are trying to protect the company from. After all, rushing headlong into poorly planned projects that are doomed to fail isn’t in the interest of anyone, including the creative types who may have proposed said projects!
   </p>
   <p>
    This brings us to connectors. Here, it’s all about timing. Once the guardians are done with their forensic analysis and cross-examination, hand things over to the connectors and let them deploy their social skills to drum up enthusiasm and motivate the whole team. One last tip: connectors are usually highly emotional, so don’t forget to caution them against taking criticism too personally.
   </p>
   <p>
    Finally, there are the pioneers. You may have guessed that it’s a good idea to ask them to speak last because they tend to be the most dominant characters in a meeting. If they over-egg the pudding even after waiting for their turn, and jump in with overly harsh criticism of other members, remind them to keep things constructive and empathetic.
   </p>
   <p>
    Putting these tips into practice will help you keep the nurturers, creatives, guardians, connectors and pioneers in your organization working together as a team and bringing their ideas to the table.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5ce86f386cee070007a4b8d0" data-chapterno="8">
  <h1>
   The most effective team managers lead by example and control their own voices.
  </h1>
  <div class="chapter__content">
   <p>
    When a flight attendant runs through a plane’s safety features, he reminds you to secure your own oxygen mask before helping others, especially children. There’s a good reason for this: you’re not going to be of any use to anyone if you’re unconscious! Team leaders are in a similar position in the workplace – if they’re not in control of their own voices, it’s unlikely they’ll be able to help other team members control theirs.
   </p>
   <p>
    That’s why it’s so important to lead by example and remember that your voice is responsible for the team. Of course, it’s hard to keep track of your main voice when you’re getting bogged down in discussions that aren’t really your priority. The key is to focus on the attributes of your authentic voice.
   </p>
   <p>
    If you’re a nurturer, try to keep your focus on representing a harmonious working environment and humane company values. If you’re a guardian, your goal should be to embody and safeguard the values of proven systems and traditions, not to mention fiscal conservatism! If you’re a creative, stay true to yourself by thinking outside the box, but be equally attentive to communicating your ideas as clearly as you can. If you’re a connector, make sure you’re facilitating open, clear dialogue between team members. Finally, if you’re a pioneer, your goal is to keep your eye on the strategic context and step up to the plate when difficult decisions need to be made.
   </p>
   <p>
    That doesn’t mean you should always use your most dominant voice. In fact, your “secondary voices” will often be sources of new insights. Take the author Steve Cockram. He’s naturally a pioneer, although he also has pronounced nurturing and guardian tendencies. Over time, he’s worked out that he performs best when spending around 70 percent of his time in pioneer mode to get the bulk of his strategic consulting work done, and switching to his other voices when it comes to tasks like editing books or checking legal contracts.
   </p>
   <p>
    So now you know all about the five different voices, what they stand for, and how to bring them together in team settings. Once you start applying this new knowledge at work and in your private life, you’ll be amazed how much easier communicating with even the most difficult people can be!
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5ce86f4c6cee0700073a5aad" data-chapterno="9">
  <h1>
   Final summary
  </h1>
  <div class="chapter__content">
   <p>
    The key message in these blinks:
   </p>
   <p>
    <strong>
     There are five different communicative styles or “voices.” These belong to five different character types – the nurturer, the creative, the guardian, the connector and the pioneer – which each have their role to play in group settings. Whether it’s a family or a company team, a group is at its best when each unique voice is given a chance to make itself heard and everyone feels that their views are being taken into account.
    </strong>
   </p>
   <p>
    Actionable advice:
   </p>
   <p>
    <strong>
     Question ingrained biases against certain voices in your culture.
    </strong>
   </p>
   <p>
    Different cultures do things differently. That’s a no-brainer, right? Well, like fish in water, we often fail to notice this precisely because it’s so obvious. The upshot is that certain voices hold greater sway than those that don’t mesh with dominant cultural norms. In a country that prizes rugged individualism, like the US, pioneering voices are much more likely to make themselves heard. In a country like Switzerland, by contrast, a preference for order and efficiency gives guardian voices an advantage in the workplace. That’s why it’s a great idea to take a step back and check if ingrained biases are marginalizing certain voices in your team. So ask yourself: are your American guardian colleagues getting a fair hearing? How about those Swiss pioneers?
   </p>
   <p>
    <strong>
     Got feedback?
    </strong>
   </p>
   <p>
    We’d sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
   </p>
   <p>
    <strong>
     What to read next:
    </strong>
    <strong>
     <em>
      Becoming the Boss
     </em>
    </strong>
    <strong>
     , by Lindsey Pollak
    </strong>
   </p>
   <p>
    Right, you’ve learned how identifying and honing different voices can help you communicate more clearly. That’s a great first step on your road to becoming an effective leader, but you might still be wondering what else there is to being a boss – after all, taking charge of a team is a pretty daunting task!
   </p>
   <p>
    Luckily, we’ve got just what you need. So if you’d like to find out how to overcome the hurdles today’s leaders are most likely to face, as well as get some insights into managing employees and personal growth, why not check out our blinks to
    <em>
     Becoming the Boss
    </em>
    , by Lindsey Pollak.
   </p>
  </div>
 </div>
</article>
