---
layout: post
title: "Jocko Willink & Leif Babin - The Dichotomy of Leadership"
description: "The Dichotomy of Leadership (2018) chronicles the extraordinary experiences of two ex-Navy SEAL commanders. While stationed in Baghdad and Ramadi during the Iraq War, Jocko Willink and Leif Babin collected experiences which helped them become effective leaders. After returning to civilian life, they realized these leadership skills were equally effective in the business world. They figured out that, in both combat and non-combat contexts, you can only overcome the dichotomies of leadership and effectively run an organization by finding a sense of balance between opposing forces."
image: https://images.blinkist.com/images/books/5ca9a7646cee070007843440/1_1/470.jpg
---

<article class="shared__reader__blink reader__container__content" lang="en">
 <div class="chapter chapter" data-chapterid="5ca9a78e6cee07000759a4a8" data-chapterno="0">
  <h1>
   What’s in it for me? Discover the important dichotomies involved in effective leadership.
  </h1>
  <div class="chapter__content">
   <p>
    War is cruel and often unnecessary, but important lessons can emerge through the cloud of death and destruction that results from human conflict. This was brought to light with Jocko Willink and Leif Babin’s first book,
    <em>
     Extreme Ownership.
    </em>
    In it, they imparted lessons they learned as Navy SEAL commanders during the 2003 American invasion and subsequent occupation of Iraq.
   </p>
   <p>
    Facing well-organized and heavily-armed insurgents in a foreign territory they discovered the importance of effective leadership. After returning to civilian life, they realized that these war-induced lessons weren’t only applicable in a military setting – they could also be used to increase profits for companies.
   </p>
   <p>
    However, Willink and Babin felt that their first book was misinterpreted by some. Being a good leader isn’t just about being “extreme” – it involves careful balancing a number of dichotomies. This is often hard to achieve, however. Only by harnessing these dichotomies can you implement the wisdom of the hardy Navy SEALs in your organization.
   </p>
   <p>
    In these blinks, you’ll learn
   </p>
   <ul>
    <li>
     what the
     <em>
      ultimate dichotomy of leadership
     </em>
     is and why you should heed it;
    </li>
    <li>
     how having a big-picture mindset led Babin to saving an Iraqi family; and
    </li>
    <li>
     how the story of one Navy SEAL’s tragic death helped save a mining company.
    </li>
   </ul>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5ca9a7bb6cee070007843441" data-chapterno="1">
  <h1>
   Care for your individual team members, but know that you may have to sacrifice individuals to save the group.
  </h1>
  <div class="chapter__content">
   <p>
    In 2006, Jocko Willink – one of the authors – found himself in Ramadi, Iraq. He’d already been stationed there as a SEAL task unit commander back in 2003, but things were different this time. During the first tour, the US military had things under control, but in 2006 the insurgent
    <em>
     mujahideen
    </em>
    had unleashed urban guerilla warfare on the city. Willink knew that it was only a matter of time before one of his countrymen would fall in battle.
   </p>
   <p>
    On August 2, insurgents mounted an all-out attack on southern Ramadi and a member of Willink’s task unit, Marc Lee, was killed in the ensuing gunfight.
   </p>
   <p>
    Leif Babin – the other author – had led the platoon into battle as commander and was distraught over the loss. It had been Babin’s call for the SEALs to join the US army in the gunfight – a decision that Willink had approved.
   </p>
   <p>
    Regretting the loss of Lee, Babin told Willink that he wished he’d not led his men into the battle. But Willink informed him that, sometimes, you have to take risks for the greater good. If they’d left their comrades in the army to fight alone, the death toll might have been greater.
   </p>
   <p>
    This is the ultimate
    <em>
     dichotomy of leadership –
    </em>
    taking care of your team members, while also knowing that you may have to place them in harm’s way for the good of the team. It’s a hard pill to swallow, but recognition of this dichotomy is necessary for any successful leader, both in military or business contexts.
   </p>
   <p>
    For example, after Willink had re-entered civilian life as a leadership consultant he was tasked with convincing the regional manager of a struggling mining company to lay off 80 employees to cut costs. The manager cared deeply about his people and was not willing to let anyone go.
   </p>
   <p>
    Willink helped the manager see the light by explaining the ultimate dichotomy he learned in Ramadi, and how this applies to the business world. If the manager didn’t lay off some employees, corporate might replace him with someone who didn’t care about his team members as much. More people might be laid off and corporate might even shut down the whole mine.
   </p>
   <p>
    The manager came through and let go of 80 people he cared about. The company moved back toward profitability, and the remaining 600 employees had jobs for the near future.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5ca9a7df6cee07000759a4a9" data-chapterno="2">
  <h1>
   Only spend your leadership capital on things that really count.
  </h1>
  <div class="chapter__content">
   <p>
    Babin experienced his first friendly fire incident in Ramadi in 2006. US Army tanks were unknowingly firing at his unit’s position, so Babin acted fast – under heavy fire, he reprogramed his radio to Army wavelengths and was able to stop the incident.
   </p>
   <p>
    Without the leadership of Willink, this incident might have resulted in fatalities. Months earlier Willink had ordered Babin and the rest of the task unit to learn how to reprogram their SEAL radios to Army wavelengths. Not understanding the importance of the task, however, none of the platoon did so.
   </p>
   <p>
    Days later, the task unit was to engage in a dangerous night raid and Willink asked if everybody had learned how to reprogram the radios. When he learned none had, Willink made sure they all knew how. Those skills would go on to save Babin’s life in the friendly fire incident.
   </p>
   <p>
    A separate incident in 2005 also tested Willink’s leadership skills. Since the Vietnam War, Navy SEALs have sewn unconventional patches onto their uniforms – often with crude, even offensive, slogans. Willink’s task unit was no different. He ordered them to remove all the unprofessional patches.
   </p>
   <p>
    The task force was demoralized and resolved to make a new, less-crude patch they would all wear. When Willink spotted the new patches he wasn’t angry and made no mention of them. He let it slide.
   </p>
   <p>
    Willink knew that leaders only have a certain amount of
    <em>
     leadership capital
    </em>
    , and that when his ran out, his orders would start carrying less weight. He knew there was a time to stand firm and expend leadership capital. While the radio incident was the right time to take a stand, this wasn’t. He also knew the patches created comradery among the men, so he let them bend the rules.
   </p>
   <p>
    Spending leadership capital wisely is equally important in the business world.
   </p>
   <p>
    For example, one executive vice president of a company that Babin was assessing for his leadership program was getting very annoyed with his departmental leaders. Why? Because they were always on their phones answering emails during meetings. So the VP decided to ban phones from the meetings, much to the disgruntlement of the departmental leaders.
   </p>
   <p>
    Soon after, the EVP realized that the departmental leaders weren’t heeding the company’s new standard operating procedures - a much graver fault. The EVP had spent too much of his leadership capital on the strategically minor ‘no cellphone’ policy instead of getting his leaders to implement the important new procedures.
   </p>
  </div>
 </div>
 <div class="chapter supplement" data-chapterid="5ca9a7df6cee07000759a4a9" data-chapterno="2">
  <h1>
  </h1>
  <div class="chapter__content">
   <p>
    <em>
     “There is a time to stand firm and enforce rules and there is a time to give ground and allow the rules to bend.”
    </em>
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5ca9a7f36cee070007843442" data-chapterno="3">
  <h1>
   Show your team the “why” rather than smothering them with direction.
  </h1>
  <div class="chapter__content">
   <p>
    Back in 2003, Willink found himself leading a platoon in a mission in occupied Baghdad. The enemy at this point wasn’t the well-organized insurgents of 2006 – they were little more than armed criminals.
   </p>
   <p>
    The ease of fighting against these enemies had led his men toward arrogance, and most had even stopped wearing their bulletproof back plates so that they could move faster.
   </p>
   <p>
    Once Willink realized this, he had two options: he could either increase platoon inspections and micromanage his men, or he could explain to them why it was important for them to wear their back plates. He opted for the latter, telling them that no matter how much faster they could move without the plates, it wasn’t faster than a bullet. And, no matter how amateur the enemy was, there was always a chance they could be outflanked and taken from behind.
   </p>
   <p>
    From then on, subsequent routine inspections found that the men had put their back plates back on. Instead of micromanaging his men, Willink had shown them the
    <em>
     why –
    </em>
    by doing so, he’d empowered them to be accountable to themselves.
   </p>
   <p>
    This is no different when it comes to smothering subordinates with excessive accountability in the business world.
   </p>
   <p>
    Willink once consulted a company whose leaders had just implemented new data entry software for offsite technicians to fill in after installing products for customers. The data would provide information to other areas of the company to help them improve service and, hopefully, drive sales.
   </p>
   <p>
    But the technicians were resisting the new data entry tasks, with most of them only half-heartedly completing them after installations.
   </p>
   <p>
    Management opted first for increased accountability checks. Penalties were imposed for technicians who didn’t do the data entry, but this only resulted in them entering useless one- or two-word answers in most fields and the resulting information wasn’t helping anyone at the company.
   </p>
   <p>
    Willink informed the perplexed managers that increasing accountability further wouldn’t help. What they needed to do was to empower the technicians to be accountable to themselves. They needed to know the
    <em>
     why.
    </em>
   </p>
   <p>
    So Willink helped the management communicate to the technicians that if the software was used correctly, the company could improve its service. By doing so, they’d make more money – and the company would grow. Additionally, technicians would likely get pay raises and even advance up the ladder in the company as it grew. It was, thus, in their interest to use the software.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5ca9a8056cee07000759a4aa" data-chapterno="4">
  <h1>
   If you want to be a good leader, be a good follower.
  </h1>
  <div class="chapter__content">
   <p>
    Sometimes, for the good of your team, you have to submit to your boss’s will, even if you find it unjust. Babin once made this clear to Jim, a client who was struggling with his boss. Jim had made his boss jealous by impressing his boss’ boss, and Jim’s boss had taken this out on him by giving him a low performance rating.
   </p>
   <p>
    This affected Jim’s whole team and meant they would all get smaller bonuses, as these were tied to performance ratings.
   </p>
   <p>
    But Jim had a plan to make it right – he’d go to his boss’ boss with numbers to prove his team’s actual performance and force his boss to give him a higher rating.
   </p>
   <p>
    Babin saw through this, however. It would be a Pyrrhic victory, he explained, as this would only turn his boss against him in the future.
   </p>
   <p>
    Instead, Babin explained that to be a good leader, you have to also follow. If Jim wanted what was best for his team, he needed to accept the performance rating so that the team wouldn’t suffer continued bad ratings into the future.
   </p>
   <p>
    But it’s not only important to follow your superiors. Sometimes, as a leader, you need to follow those who follow you.
   </p>
   <p>
    In Ramadi in 2006, deep in the night, Babin and his platoon had the task of securing a tall building. By doing so, they could cover a US Army battalion that was following close behind. Ultimately, the operation would allow them to retake areas of the city from the terrorist group al-Qaeda.
   </p>
   <p>
    Babin had his eyes set on a suitable building, but a lower-ranking officer, Kyle, disagreed and suggested a different one. Kyle was a talented sniper and had more experience than the rest of the team on these sorts of covering missions.
   </p>
   <p>
    Minutes passed without Babin making a decision, but he finally decided to defer to Kyle, regardless of his lower rank. Babin knew that for the whole team to succeed, the leader sometimes has to be a follower.
   </p>
   <p>
    Kyle’s call turned out to be right. The platoon cleared all enemies from the area, a feat they wouldn’t have been able to accomplish in the other, shorter building the author had in mind.
   </p>
   <p>
    In both military and business contexts, leaders must be willing to put their authority aside and follow others.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5ca9a81f6cee070007843443" data-chapterno="5">
  <h1>
   While it’s important to plan effectively, overplanning is often counterproductive.
  </h1>
  <div class="chapter__content">
   <p>
    Ramadi was one of the most dangerous cities in the world in 2006. So, when Babin got wind of a special ops team that was requesting his platoon’s assistance in a high-risk mission there, he needed to know all of the planning details.
   </p>
   <p>
    It turned out that the special ops team hadn’t planned for any contingencies or worst-case scenarios.  The mission was to approach a target in broad daylight in a convoy, driving straight down one of the most dangerous roads in the city.
   </p>
   <p>
    “What if an improvised explosive device (IED) detonated under a vehicle?,” Babin asked the special ops unit leader.
   </p>
   <p>
    “It won’t,” he replied.
   </p>
   <p>
    Babin was shaken, and decided to ask a senior officer what he thought about this lack of contingency planning. The officer confirmed his suspicions that this wasn’t a mission that should be executed.
   </p>
   <p>
    Babin tried to convince the special ops unit leader to plan for at least some contingencies, but to no avail. No SEALs would take part in the operation, Babin decided.
   </p>
   <p>
    The mission went on to fail – a vehicle was blown up by an IED and men were seriously wounded, all because of a lack of planning.
   </p>
   <p>
    Sometimes
    <em>
     overplanning
    </em>
    can be just as dangerous. On a previous mission in Ramadi that same year, Babin and his platoon had been tasked with supporting Marines on a 36-hour operation. He’d drawn up all possible contingencies of what might go wrong and how he might react.
   </p>
   <p>
    In doing so, his rucksack became increasingly laden; extra weapons, extra water, extra everything. During the mission, it was so heavy that he could hardly move. Instead of leading his men, he lost situational awareness and could hardly focus on anything aside from keeping up with the platoon.
   </p>
   <p>
    Though the mission was successful, the author’s overplanning could well have cost lives.
   </p>
   <p>
    These experiences taught Babin important lessons. If you plan for every potential problem, you spread yourself too thin, and additional problems are created. But if you don’t plan at all, you can’t react to possible situations that arise.
   </p>
   <p>
    Instead, a balanced approach to planning should be employed by leaders. Take three or four of the most likely contingencies into account when planning your next operation, as well as the worst-case scenario that might arise, and share this information with your team.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5ca9a8326cee07000759a4ab" data-chapterno="6">
  <h1>
   Know the details of the mission, but also be detached enough that you can see the big picture.
  </h1>
  <div class="chapter__content">
   <p>
    In 2006, Babin and his platoon arrived in Ramadi, and were immediately tasked with their first capture/kill mission. It would take place the night after they arrived.
   </p>
   <p>
    Time was of the essence. Babin spent the whole day planning the details of the mission, creating PowerPoint presentations, and several other long documents. As the clock ticked away, he realized he wouldn’t finish all the necessary plans by the time of the mission, so he asked Willink to delay it.
   </p>
   <p>
    Willink declined – the platoon was more than ready for the mission, he said, and it would proceed that night.
   </p>
   <p>
    Willink understood the wider importance of the mission that Babin did not. The platoon was new in town and needed to build momentum right away, in addition this would boost the confidence of all the men involved. Willink knew this was paramount, whereas Babin had let his strategic vision get bogged down in details.
   </p>
   <p>
    The mission was easy, as Willink predicted. From then on, Babin focused on big picture stuff and left the bulk of the details to his platoon.
   </p>
   <p>
    This detachment paid off right away.
   </p>
   <p>
    Months later, while on another capture/kill mission with an Iraqi army unit, Babin’s platoon was in a small, smoke-filled room when AK-47 fire burst out. He immediately assumed it must’ve come from beyond the door to the next room. A platoon member quickly readied a grenade to blast the door open – which would kill everyone inside the next room.
   </p>
   <p>
    While the platoon focused on such tactical decisions, Babin employed his big-picture thinking, looking around the whole room. He noticed an Iraqi army soldier looking down in confusion at his own AK-47 that he’d accidentally discharged. The bullets weren’t coming from the next room after all.
   </p>
   <p>
    Telling his grenade-wielding soldier to stand down, Babin instead placed a small explosive to blow the door open. In the room was a terrified, unarmed Iraqi family. If Babin hadn’t focused on the bigger picture, the whole family would have been killed.
   </p>
   <p>
    These lessons are equally applicable to the business world. Sometimes, you have to step back from the details in order to see the bigger picture. As a leader, getting away from the daily grind of office life – for example on a leadership retreat – can help you focus on bigger picture issues like long-term company strategy, rather than the day-to-day of conducting business.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5ca9a8876cee070007843444" data-chapterno="7">
  <h1>
   Final summary
  </h1>
  <div class="chapter__content">
   <p>
    The key message in these blinks:
   </p>
   <p>
    <strong>
     There are a number of dichotomies that both military and business leaders must understand in order to effectively manage teams. The ultimate leadership dichotomy involves caring for your individual team members while also being ready to sacrifice them for the sake of the group. Finding balance in how you spend your leadership capital is also key – only spend it on things that are important to moving your organization forward. And while you should hold your people accountable, don’t smother them with direction. Finally, always keep in mind that overplanning can be counterproductive, as can focusing too much on small details. So instead, keep your mind on the big picture, and plan only for the likeliest contingencies.
    </strong>
   </p>
   <p>
    Actionable advice:
   </p>
   <p>
    <strong>
     Be humble, but don’t be passive.
    </strong>
   </p>
   <p>
    Although this is a difficult dichotomy to balance, it’s extremely important. Leaders need to accept constructive criticism from subordinates, and keep their egos in check at all times. At the same time, being overly humble isn’t the solution. Leaders must also be ready to stand up for the good of the whole team when there are negative consequences from not doing so – even in the face of criticism.
   </p>
   <p>
    <strong>
     Got feedback?
    </strong>
   </p>
   <p>
    We’d sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
   </p>
   <p>
    <strong>
     What to read next:
    </strong>
    <strong>
     <em>
      Extreme Ownership
     </em>
    </strong>
    <strong>
     , by Jocko Willink and Leif Babin
    </strong>
   </p>
   <p>
    Making decisions under extreme conditions can be tough. Particularly when those conditions are the Iraq War and you’re a Navy SEAL commander. These blinks have shown the necessary dichotomies leaders must take into account when making nuanced decisions.
   </p>
   <p>
    But to get to the nitty-gritty roots of this decision-making process, check out our blinks to the authors’ first book –
    <em>
     Extreme Ownership.
    </em>
    More riveting reports of the Iraq War and the resulting lessons learned lie within. Learn how to utilize the four Laws of Combat and the mindset of Extreme Ownership. And, as we’ve seen in these blinks, how such military tactics apply equally on the battlefields of both business and war.
   </p>
  </div>
 </div>
</article>
