---
layout: post
title: "Patty Azzarello - Move"
description: "Move (2017) provides an actionable framework for establishing long-term organizational change and introduces the MOVE model, which helps businesses overcome chronic issues ranging from employee skepticism and task prioritization to making restructuring an integral part of company culture."
image: https://images.blinkist.com/images/books/5b2ff989b238e10007cdd536/1_1/470.jpg
---

<article class="shared__reader__blink reader__container__content" lang="en">
 <div class="chapter chapter" data-chapterid="5b2ff9beb238e10007cdd537" data-chapterno="0">
  <h1>
   What’s in it for me? Learn how to implement long-lasting change.
  </h1>
  <div class="chapter__content">
   <p>
    You’ve got it. Out of the blue, while you’re taking a shower, it hits you like a thunderbolt: the idea that will transform your company into a unicorn – no, a decacorn! All you’ve got to do is introduce the necessary organizational changes, and it’ll be no time before you’re considering a company valuation in the double-digit billions.
   </p>
   <p>
    But then, after this flash of genius, your brilliant vision smacks into the brick wall of reality. As you try to convince executives and employees to follow you, you feel as though you’ve entered a swamp made of naysaying, doubt, fatigue, poor planning, scarce resources, miscommunication and any number of other corporate ills.
   </p>
   <p>
    So how do you get out of this morass and translate your transformative vision into a business bonanza?
   </p>
   <p>
    That’s what these blinks are all about. From long-term planning to savvy resource allocating, team construction to team cohesiveness, you’ll learn all the skills necessary to make the change happen.
   </p>
   <p>
    You’ll also find out
   </p>
   <ul>
    <li>
     how to make your team members more like sled dogs;
    </li>
    <li>
     the benefits of burning your ships; and
    </li>
    <li>
     when being ruthless is a good idea.
    </li>
   </ul>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5b2ff9d0b238e10006f8a896" data-chapterno="1">
  <h1>
   For a change initiative to work, your company needs to plan out the Middle phase.
  </h1>
  <div class="chapter__content">
   <p>
    Remember that time you decided to start jogging every morning? Remember how everything went so well at first? How, driven by the vision of a fitter, healthier existence, you sprang out of bed, put on your running shoes and clocked up a few miles?
   </p>
   <p>
    And remember how one day of rest became two? And how two became a week? And how you don’t run at all now?
   </p>
   <p>
    Even if you’ve never jogged a mile in your life, one thing is almost certain: you’ve experienced the
    <em>
     Middle
    </em>
    phase, that tedious and work-intensive period that precedes long-term results and follows the initial excitement of getting started.
   </p>
   <p>
    Many people get bogged down in this phase, which is why dealing with it is the first concern of the
    <em>
     MOVE model
    </em>
    , which stands for
    <em>
     Middle
    </em>
    ,
    <em>
     Organization
    </em>
    ,
    <em>
     Valor
    </em>
    and
    <em>
     Everyone
    </em>
    .
   </p>
   <p>
    So, to get through the Middle, you’ve got to stop being vague and establish small, clear and achievable targets. Here’s an example to demonstrate how this works:
   </p>
   <p>
    Once, the author was working as a consultant for a home electronics business whose goal was as large as it was amorphous; it wanted to improve sales in Europe.
   </p>
   <p>
    The author broke down this big, broad goal by asking two questions.
   </p>
   <p>
    “What,” she asked, “would happen if we achieved this?” And the executives responded that they’d enjoy greater sales in one of Europe’s most important countries, Germany. “And what,” the author pressed on, “would that look like?”
   </p>
   <p>
    Well, the executives discussed the matter, and soon some definite steps began to emerge. They’d need to hire personnel in Germany, for instance, and create partnerships with top German retailers, as well as launch advertising campaigns in the country.
   </p>
   <p>
    In addition to making vague aims more concrete by asking these two questions, you should also keep matters urgent by defining small interim missions.
   </p>
   <p>
    Let’s say you have a large mission – to make bigger deals, and more of them – and that you’ve resolved to close ten such deals by year’s end. Well, since people tend to put off non-urgent tasks, you’ll have to figure out a way to add urgency to this undertaking.
   </p>
   <p>
    So set some shorter deadlines. For instance, aim to have 30 potential deals lined up in nine months, and recognize that for this to happen you need to identify 50 target accounts within half a year. These short-term goals will keep things urgent and propel you past the long-term finish line.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5b2ff9eab238e10007cdd538" data-chapterno="2">
  <h1>
   During the Middle phase, allocate resources wisely and measure outcomes.
  </h1>
  <div class="chapter__content">
   <p>
    It’s an old business bromide: “What gets measured gets done.” And though this adage may contain a kernel of truth, it leaves a crucial question unanswered: by which metrics should success be measured?
   </p>
   <p>
    Using a poor metric is a surefire way to throw a monkey wrench into the works, so it’s imperative that you can separate effective metrics from ineffective ones.
   </p>
   <p>
    When measuring outcomes, you need to choose metrics that are both relevant and able to accurately forecast higher-level results. Here’s an example:
   </p>
   <p>
    Let’s say your company, like many others, strives to provide top-notch customer service. Well, what you
    <em>
     don’t
    </em>
    want to do is measure things such as the speed of customer-complaint resolution. This might seem like a good metric, but it’s not. After all, it doesn’t really matter how quickly an issue is resolved; what matters is customer satisfaction, and high-speed problem-solving doesn’t guarantee that.
   </p>
   <p>
    So, rather than focusing on isolated activities or particular stages within a process, measure only the desired outcome.
   </p>
   <p>
    An effective metric for customer satisfaction is the number of returning customers. Another is customer referrals, since recommending a company to a friend is usually a sign of satisfaction.
   </p>
   <p>
    Now, knowing what to measure is one thing. Actually measuring it is quite another. You may have to get creative. You could, for instance, have service representatives conclude their interactions with customers by asking whether the customer would recommend your product.
   </p>
   <p>
    Once you figure out how to measure the important stuff, be sure to put resources toward improving those measurements.
   </p>
   <p>
    This is usually a challenge. It’s easy to miscalculate the number of resources such improvement will require – and, of course, resources are usually in short supply. So, to make these calculations less challenging and more accurate, try using the following technique:
   </p>
   <p>
    Sketch out two scenarios, each with a corresponding funding plan.
   </p>
   <p>
    Here’s how this might work. Let’s say a company is having a hard time acquiring customers.
   </p>
   <p>
    For this company, the first scenario would be a low cost one. They’d neither fire nor hire anyone, and the budget would increase by only 10%. This scenario would allow them to tackle only two pressing problems in the next half year.
   </p>
   <p>
    Scenario two, on the other hand, would be a high cost one. They’d increase their budget by 100 percent and hire two new employees – changes that would make them a tip-top customer magnet within their market.
   </p>
   <p>
    Armed with this information, the company can pinpoint its priorities and move resources from one department to another if necessary.
   </p>
  </div>
 </div>
 <div class="chapter supplement" data-chapterid="5b2ff9eab238e10007cdd538" data-chapterno="2">
  <h1>
  </h1>
  <div class="chapter__content">
   <p>
    “
    <em>
     Your strategy is not what you say it is. Your strategy is where you put your resources.”
    </em>
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5b2ffa01b238e10006f8a897" data-chapterno="3">
  <h1>
   Teams should be designed to deliver outcomes.
  </h1>
  <div class="chapter__content">
   <p>
    Snow spreads white as far as the eye can see. The sound of the sled dogs panting, each breath a small cloud of steam. The cold air taut with anticipation. Briefly, the tow line tenses as the team of dogs braces itself for the coming moment – and then the command: “Mush!”
   </p>
   <p>
    The author noticed something while on a dog-sledding trip. Each sled dog understood its job, and each was aligned with all the others, eager to begin working toward the team mission. So when the time came to “mush” – that is, to go – they weren’t only ready; they were champing at the proverbial bit.
   </p>
   <p>
    So what imbued each canine with such purpose and energy, and how can you imbue your team members with these same qualities? Well, it’s a matter of organization, which is what the O in the MOVE model stands for.
   </p>
   <p>
    First, you’ve got to figure out whether you have all the right pieces and if any new pieces need to be added. You can do this by using the
    <em>
     Blank-Sheet Organization Chart
    </em>
    , which is what the author resorted to when restructuring a majorly disorganized company.
   </p>
   <p>
    Take a blank sheet of paper and, after you’ve determined the desired outcome and which skill sets will be required to bring about that outcome, start drawing up an ideal team. Don’t leave any role undefined, and get detailed about what will be required of each role, be it negotiation know-how or the ability to drive innovation. Finally, talk to team members you trust and flesh out the roles and requirements using their feedback.
   </p>
   <p>
    Before the author got there, the company she restructured was a mess. Roles within its eight business units were vague, resulting in work overlap, among other problems. The Blank-Sheet Organization Chart transformed the company into a cohesive whole – which is exactly what it can do for your business.
   </p>
   <p>
    Once you’ve figured out where current team members will be most useful and how many new members will have to be found to take on vacant roles, you’ll be well on your way to overcoming challenges of Iditarod-like proportions.
   </p>
  </div>
 </div>
 <div class="chapter supplement" data-chapterid="5b2ffa01b238e10006f8a897" data-chapterno="3">
  <h1>
  </h1>
  <div class="chapter__content">
   <p>
    <em>
     “Are you leading the team you have or the team you need?”
    </em>
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5b2ffa15b238e10007cdd539" data-chapterno="4">
  <h1>
   Leaders engage team members by revealing the bigger picture and showing that they care.
  </h1>
  <div class="chapter__content">
   <p>
    If someone offered you $250 to run a marathon, you
    <em>
     might
    </em>
    do it, right? But, at less than $10 per mile, it hardly seems like enough. If running that marathon contributed to the cure of AIDS or cancer, however – well, you might, like thousands of other people every year, even pay to run it. Why is that?
   </p>
   <p>
    When it comes to motivation, meaning trumps money.
   </p>
   <p>
    So to motivate and engage your team, you’ve got to make its purpose meaningful. And that means forging personal bonds with team members and showing them the bigger picture.
   </p>
   <p>
    Whether during breakfast meetings or one-on-one sit-downs, talk to the people in your company. Invest time both in learning how they feel about the work they do and in communicating how that work helps the company’s overall success.
   </p>
   <p>
    For example, when chatting with a tech writer, you could enumerate the ways that detailed product documentation boosts customer satisfaction, and also mention how it reduces tech-support expenses and increases company competitiveness.
   </p>
   <p>
    Making explicit the connection between the writer’s work and the company’s success will infuse his job with meaning – and, as you know, meaning adds up to engagement.
   </p>
   <p>
    Engaged employees won’t only take ownership of projects and solve problems on their own; they’ll also contribute ideas and insights that may help the company. For example, employees may be able to point out that one department’s progress is misaligned with another’s – something that could derail your company’s long-term transformation strategy. So when employees volunteer information, give them your full attention. You won’t regret it.
   </p>
   <p>
    Of course, your employees won’t get behind the company mission unless
    <em>
     you
    </em>
    , their leader, are fully dedicated to it. Indeed, the author has noticed that when she wholeheartedly shows that she cares about her company’s mission, her team not only gets inspired; they also feel free to show their own caring and engagement.
   </p>
   <p>
    Emotion drives meaning, so even if you’re a leader at a company that produces lint removers, you’ve got to find something you can care about. If the business is as uninspiring as the product, then seek meaning in the people you serve or the people who work for you.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5b2ffa26b238e10006f8a898" data-chapterno="5">
  <h1>
   Lead with grit and persistence.
  </h1>
  <div class="chapter__content">
   <p>
    So your team is organized, and you’ve planned out the Middle phase. But when your team actually
    <em>
     enters
    </em>
    that phase and experiences all the attendant doubt and tedium, a new set of challenges will arise. Exhausted and discouraged, employees will start asking hard questions. Is this even working? Can we go back to what we know? When will we see results?
   </p>
   <p>
    During this tough period, you’ll need
    <em>
     Valor
    </em>
    – the V in the MOVE model.
   </p>
   <p>
    Being valorous is a matter of drawing on inner and outer resources. Remember, your team members won’t be the only ones plagued by doubts and fears; you’ll have misgivings, too. But here’s the difference: rather than being paralyzed by fear and deciding to turn back, you, the leader, will accept these uncomfortable feelings and push forward.
   </p>
   <p>
    Welcome fear as you’d welcome an old friend. “Hello, fear,” you could say, “it’s fine if you want to join me on this journey, but you’ll have to take a back seat and you certainly won’t be allowed to drive.”
   </p>
   <p>
    In the meantime, seek out more desirable companions – that is, experts and mentors who can advise you on your change strategy. If a change initiative can be likened to Mt. Everest, then these advisors are the Sherpa guides; you’ll have a hard time getting to the top without them.
   </p>
   <p>
    Having valor also means being firm. Once the change initiative begins, there is no going back.
   </p>
   <p>
    Just as Hernán Cortés, the sixteenth-century Spanish colonizer, set fire to all his ships when his fleet landed on the banks of what is now Mexico, you too should destroy all means of retreat. The only option should be to move forward.
   </p>
   <p>
    Your change strategy may involve adding new processes. For instance, you might add checkpoints or guidelines to the company’s software-development program – changes that may prompt complaints from engineers. Yes, it may be challenging for them to learn the new process, but you’ll have to hold your ground. You’ll only see the benefits of a change initiative if you stay firm and implement all necessary changes. No retreat!
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5b2ffa3db238e10007cdd53a" data-chapterno="6">
  <h1>
   Prioritize ruthlessly and don’t get bogged down in the details.
  </h1>
  <div class="chapter__content">
   <p>
    Here’s a common pitfall. In the quest for organizational change and growth, companies get bogged down in the details. They spend so much time dealing with the “next” pressing issue that they lose sight of the big picture.
   </p>
   <p>
    It’s a classic dilemma. If you’re expending all your energy on becoming a one-hundred-million-dollar company, how will you ever develop a strategy for becoming a one-billion-dollar company?
   </p>
   <p>
    Well, if you really want to change and grow, you’ve got to use
    <em>
     ruthless prioritization
    </em>
    . No more nitpicking tiny, unimportant details; it’s time to tackle the tough decisions that will actually make a major difference.
   </p>
   <p>
    Ruthless prioritization will inevitably involve trade-offs. So, in order to identify what should grow and what should go, make a list of the three things crucial to your company’s success. These are necessary and non-negotiable.
   </p>
   <p>
    Let’s say you’re a business-to-business (B2B) company with low conversion rates. At the top of your list might be stepping up your referral marketing by implementing a new, more effective, process.
   </p>
   <p>
    After you’ve made your list, ask your team for feedback. Other priorities will doubtless be brought to the table – but under no circumstances should they jeopardize the three top priorities. For your B2B company, the referral marketing process
    <em>
     will
    </em>
    be implemented.
   </p>
   <p>
    Of course, your company isn’t infallible. If the referral marketing process doesn’t improve your conversion rate, it’s time to acknowledge that a mistake was made and cut your losses. What you don’t want to do is double down. Learn from your missteps and keep on walking.
   </p>
   <p>
    And remember: Don’t get bogged down in the details. When upper-level managers have to review all the details, progress ceases. All managers, at each level of management, should distill details into helpful insights for the managers above them. By no means should the top manager have to spend hours doing the work of lower management – a fact encapsulated in one of the author’s rules of thumb: “Insights move up. Details stay down.”
   </p>
  </div>
 </div>
 <div class="chapter supplement" data-chapterid="5b2ffa3db238e10007cdd53a" data-chapterno="6">
  <h1>
  </h1>
  <div class="chapter__content">
   <p>
    <em>
     “The companies that scale are the ones who choose to do less stuff.”
    </em>
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5b2ffa53b238e10006f8a899" data-chapterno="7">
  <h1>
   Establish long-term change by making it familiar, safe and normal.
  </h1>
  <div class="chapter__content">
   <p>
    No matter how valorous, a leader won’t succeed in bringing her team through the Middle phase without the active involvement of the people around her. And this is where the E – for
    <em>
     Everyone
    </em>
    – in MOVE comes in.
   </p>
   <p>
    Getting everyone involved in the change initiative is a matter of substituting conversation for command. Rather than telling employees what the change strategy is, talk to them about how it will work. This, hopefully, will encourage them to discuss it among themselves.
   </p>
   <p>
    When team members talk strategy of their own accord, it’s a sign that real change is taking place. Indeed, it means that company strategy has become familiar, safe and normal.
   </p>
   <p>
    Another way to get employees talking is to create spaces where strategic conversations can take place.
   </p>
   <p>
    Utopia Village, a Honduran resort, created exactly such a space by starting a WhatsApp group called “Utopia Stars.” In this space, every employee could discuss the resort’s new service guidelines, which prioritized excellence above all else.
   </p>
   <p>
    Staff members began documenting instances of excellent service, posting pictures of each other going above and beyond for customers or writing a brief description of what they’d seen other staff members do. Thus, the group sparked a company-wide conversation and gave rise to a new sense of camaraderie. This helped Utopia Village earn a number-one rating on TripAdvisor.
   </p>
   <p>
    And don’t forget – when change begins to happen, be sure to highlight it. This will also encourage conversation and drive further change.
   </p>
   <p>
    Successful highlighting involves two things: everyone has to be able to see it and everyone has to be able to talk about it.
   </p>
   <p>
    For instance, Heifer International, a nonprofit that gives support to livestock-owning families, discovered that it could increase its impact if it convinced families to pass on their livestock’s offspring to new families.
   </p>
   <p>
    So Heifer International created a ceremony celebrating the birth of new livestock. When, for example, a goat provided by the company has a kid, there’s a ceremonial celebration and the goat-owning family passes the newborn on to a new family.
   </p>
   <p>
    This ceremony draws people into the conversation and keeps them involved. In one community, this ceremonial tradition has been carried on for 17 years – without any involvement from Heifer International!
   </p>
  </div>
 </div>
 <div class="chapter supplement" data-chapterid="5b2ffa53b238e10006f8a899" data-chapterno="7">
  <h1>
  </h1>
  <div class="chapter__content">
   <p>
    <em>
     “If you want something to happen, make sure everyone keeps talking about it.”
    </em>
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5b2ffa69b238e10006f8a89a" data-chapterno="8">
  <h1>
   Establish trust and productivity by talking to team members and keeping everyone in the loop.
  </h1>
  <div class="chapter__content">
   <p>
    The author once brought a middle-aged man to tears with a single question. No, it wasn’t a query filled with malice or implied insult; she simply asked him what he really cared about.
   </p>
   <p>
    So why the outburst of emotion?
   </p>
   <p>
    Well, this man explained that, over the course of his entire career, nobody had ever asked him such a question. The recognition implied by the author’s question – the understanding that the man wasn’t merely a cog in a machine but a human being with his own hopes and feelings – touched him on a deep level.
   </p>
   <p>
    Asking similar questions and paying attention to the answer will help you forge deep bonds with your employees – and such bonds are the foundation of trust and loyalty.
   </p>
   <p>
    Such personal communication will also help you identify which issues are truly troubling your team members, something the author learned in her first real leadership position.
   </p>
   <p>
    During the first two weeks of her time in this role, she talked face-to-face with all her employees – 81 in total. These conversations shaped her entire strategy. For instance, she found out that a certain manager was bullying his team members and damaging company culture, something she wouldn’t have learned otherwise because he was adept at putting on a good face for his superiors.
   </p>
   <p>
    But communication must go beyond one-on-one discussions. You’ve got to keep everyone in the loop.
   </p>
   <p>
    Communicating information both improves engagement and enables people to coordinate with each other. A case in point is an entrepreneurship course offered at Monmouth University in New Jersey.
   </p>
   <p>
    Students are split into four departments: marketing, sales, manufacturing and product development.
   </p>
   <p>
    Every week, each department’s leader posts a status update on an online platform, and each student within that department is required to read both it and all comments.
   </p>
   <p>
    According to the professor, the course’s structure, which centers around communication and information-sharing, is what accounts for the students’ remarkable success rate, with most business plans going from zero to profitable within a mere four months.
   </p>
   <p>
    So stay communicative. An environment where personal and business information can be easily shared will have a much easier time making change a reality.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5b2ffa7bb238e10007cdd53b" data-chapterno="9">
  <h1>
   Final summary
  </h1>
  <div class="chapter__content">
   <p>
    The key message in these blinks:
   </p>
   <p>
    <strong>
     The MOVE model is more than a mere process for implementing change initiatives in any company. Rather, it’s the surest way to achieve lasting change. It does this in four ways – by helping you through the
    </strong>
    <strong>
     <em>
      Middle
     </em>
    </strong>
    <strong>
     phase of the change process with clear and concrete strategy; by establishing an
    </strong>
    <strong>
     <em>
      Organization
     </em>
    </strong>
    <strong>
     structure with capacity for change; by giving you the
    </strong>
    <strong>
     <em>
      Valor
     </em>
    </strong>
    <strong>
     to make the tough decisions and prioritize change; and by showing leaders how to engage
    </strong>
    <strong>
     <em>
      Everyone
     </em>
    </strong>
    <strong>
     , thus making the change an inextricable part of company culture.
    </strong>
   </p>
   <p>
    Actionable advice:
   </p>
   <p>
    <strong>
     Create a Timeline.
    </strong>
   </p>
   <p>
    A timeline is a fantastic way to communicate your change strategy and track your progress.
   </p>
   <p>
    When making one, be sure to write major milestones below the line (for instance, “June 1 – Version 2.0 Release”) and specific tasks or initiatives above the line (“April 20 – Launch new media strategy”). Also include a “You are here” marker and make sure it’s not placed at the timeline’s very beginning, as this can be demotivating. Then use the timeline whenever you communicate strategy and update it as your company progresses. Eventually, your team will find motivation in this visual representation of progress.
   </p>
   <p>
    <strong>
     Got feedback?
    </strong>
   </p>
   <p>
    We’d sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
   </p>
   <p>
    <strong>
     Suggested
    </strong>
    <strong>
     further
    </strong>
    <strong>
     reading:
    </strong>
    <strong>
     <em>
      Radical Candor
     </em>
    </strong>
    <strong>
     by Kim Scott
    </strong>
   </p>
   <p>
    <em>
     Radical Candor
    </em>
    (2017) offers valuable tools that any team leader or manager can use to establish the best possible relationship with their employees. You’ll find an insightful approach to management that creates a working environment where great ideas emerge, and individuals can reach their full potential. It’s time to stop doubting yourself and become the kickass boss your employees will be proud to follow.
   </p>
  </div>
 </div>
</article>
