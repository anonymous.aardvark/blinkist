---
layout: post
title: "Patrick Lencioni - The Truth about Employee Engagement"
description: "The Truth About Employee Engagement (2007) tells you how to turn any type of job from miserable to meaningful. Using three key tips, you’ll find joy and satisfaction in your work. The blinks offer guidance for employers on creating an environment in which employees can thrive, and advice for job seekers on how to find the job that will be the perfect fit for them."
image: https://images.blinkist.com/images/books/5ca9c1ce6cee07000759a4c7/1_1/470.jpg
---

<article class="shared__reader__blink reader__container__content" lang="en">
 <div class="chapter chapter" data-chapterid="5ca9c1f06cee07000759a4c8" data-chapterno="0">
  <h1>
   What’s in it for me? Make any job work for you.
  </h1>
  <div class="chapter__content">
   <p>
    Life’s too short to have a miserable job. But don’t blame the job itself. If you take the right approach, any role has the potential to become a meaningful one.
   </p>
   <p>
    In
    <em>
     The Truth about Employee Engagement,
    </em>
    Lencioni explores the effects unhappy employees can have on the success of a business, as well as the employee’s family and social life.
   </p>
   <p>
    You’ll learn to identify the three root causes of job dissatisfaction and gain the tools you need to improve your own – and other people’s – work lives.
   </p>
   <p>
    No matter what your job, whether you’re managing or being managed, you can benefit from the advice in these blinks.
   </p>
   <p>
    In these blinks, you’ll learn
   </p>
   <ul>
    <li>
     what a withered plant has to do with an unhappy employee;
    </li>
    <li>
     why your boss should ask about your after-work pottery class; and
    </li>
    <li>
     that a garbage collector may well be happier than Madonna.
    </li>
   </ul>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5ca9c2116cee070007843459" data-chapterno="1">
  <h1>
   Job satisfaction does not depend on salary alone, and the cost of miserable employees is high.
  </h1>
  <div class="chapter__content">
   <p>
    What does it mean when somebody tells you they have a bad job? There are lots of reasons they might feel less than happy with their role. It might leave them physically exhausted or offer low pay. But there are less obvious reasons why people might be dissatisfied.
   </p>
   <p>
    Say you land a job in the sector in which you’ve always dreamed of working or secure a move to a position with an attractive salary. Neither of these things can guarantee job satisfaction.
   </p>
   <p>
    Have you ever found yourself heading to work each morning, dreading arriving at the office? Have you ever counted the hours all day long, waiting for it to be over? Then you’ve had a miserable job that may have left you feeling deeply cynical, without motivation to apply yourself or perform well.
   </p>
   <p>
    It has nothing to do with the job title or the sector; a doctor can be just as miserable as a construction worker. A professional sportsperson might be as disillusioned as an amusement park designer. A wealthy executive might be deeply unhappy in her job, whereas the waiter who serves her lunch may find his job deeply rewarding. You get the picture.
   </p>
   <p>
    Being so unhappy in your job obviously has a personal cost – your general well-being. What isn’t as obvious is the high cost to companies.
   </p>
   <p>
    Let’s look at efficiency. There’s plenty of research to show the negative impact dissatisfied employees can have on a company’s efficiency. When a worker is less engaged and less happy with their work, they’re less likely to perform well, and both employee and company will suffer.
   </p>
   <p>
    Not only that – an unhappy employee’s misery and cynicism will filter into their home life, affecting their partner and children. The same people are also more likely to find it hard to perform their other social responsibilities, such as caring for loved ones and taking part in the wider community.
   </p>
   <p>
    If we’re going to solve job misery, we’ll first have to understand what causes it. Luckily, the next blink has the answer.
   </p>
  </div>
 </div>
 <div class="chapter supplement" data-chapterid="5ca9c2116cee070007843459" data-chapterno="1">
  <h1>
  </h1>
  <div class="chapter__content">
   <p>
    "
    <em>
     Job misery leads to even more immediate and tangible problems, like drug and alcohol abuse, or violence.
    </em>
    ”
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5ca9c2356cee07000784345a" data-chapterno="2">
  <h1>
   There are three root causes of job misery and these can affect anyone.
  </h1>
  <div class="chapter__content">
   <p>
    Picture a plant. If the roots are rotten, no nutrients can be absorbed, the rot spreads to the rest of the plant and the whole thing soon withers and dies. Well, it’s much the same for humans in bad jobs. Three different rotten roots can potentially cause good jobs to wither into dismal ones.
   </p>
   <p>
    The first of these roots is
    <em>
     anonymity
    </em>
    . People need to have their efforts recognized to feel valued. It’s crucial that somebody – ideally their employer – takes the time to understand their potential and appreciate their abilities. When people feel invisible, and that their presence wouldn’t be missed, they become despondent.
   </p>
   <p>
    The second root problem is
    <em>
     irrelevance
    </em>
    . When you do a job that makes a positive impact on other people – even if it’s just one other person – you feel your work is worthwhile. If nobody is benefitting from your work, you will feel as though your efforts are pointless.
   </p>
   <p>
    The final root of job misery is
    <em>
     immeasurement
    </em>
    . This comes about when people have nothing against which to measure their achievements. If you don’t know what your goal is, how can you determine whether you have achieved it? It helps to have a clear idea of what it looks like to perform well, and what can be done to improve. Otherwise, you will end up without motivation and ultimately dissatisfied.
   </p>
   <p>
    It’s not just those at the bottom of the ladder who experience anonymity, irrelevance and immeasurement; even managers can be affected by the root causes of job misery.
   </p>
   <p>
    To illustrate this point, imagine the leader of the marketing section of a software company. Let’s call her Nancy. Nancy has quite a powerful job, but she’s miserable. Why is that? Well, she reports directly to the CEO, who is so busy that she has no time to take an interest in Nancy’s professional development or personal life. She feels anonymous.
   </p>
   <p>
    Nancy also fails to see how the company is of any help to its customers. She’s not even sure her work makes a difference within her team. She feels irrelevant.
   </p>
   <p>
    And even though Nancy recognizes the company is performing well – as shown by performance data – Nancy has no way to measure her contribution to the company’s success. This is immeasurement.
   </p>
   <p>
    All three root causes of job misery have combined to leave Nancy feeling deeply unhappy.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5ca9c2556cee07000759a4c9" data-chapterno="3">
  <h1>
   There are benefits to increasing employee engagement, and obstacles to overcome.
  </h1>
  <div class="chapter__content">
   <p>
    Say you’re running a business. It’s not making enough profit – what do you do? Look for cheaper suppliers? Use more efficient production methods? Maybe you should take a look at your staff first – increasing employee engagement can solve many problems.
   </p>
   <p>
    The most obvious benefit of employee engagement is higher productivity. When people feel that their job has meaning, they’re much more willing to take responsibility to get things done. This may involve working longer hours or being more diligent, which will mean more accurate, higher-quality results.
   </p>
   <p>
    The less obvious – but equally important – benefit is that engaged employees will stay loyal to their company. They will even recommend it to other good employees. In the long term, this will save the company time and money on training and recruitment.
   </p>
   <p>
    It’s also worth bearing in mind in these highly competitive times that having a reputation for fostering a positive workplace can boost a company’s profile. This is easily done by looking after your employees and making sure they’re motivated.
   </p>
   <p>
    Doesn’t this all sound like common sense? Well, sadly, there are many obstacles to overcome to achieve such employee engagement.
   </p>
   <p>
    As the employee, you might focus solely on salary and promotion prospects when choosing a role. While these are important, it’s even more crucial to find out if the company will be a good cultural fit for you. Does it support employees’ development? Does it nurture their existing talents?
   </p>
   <p>
    On the other hand, the main obstacle for employers is an inability to communicate. It can be awkward for an employer to ask questions that require an emotional response, particularly questions about how an employee is feeling or whether they’re enjoying their work. Luckily, this is a skill that can be developed if managers work a little harder every day to make sure their employees stay engaged.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5ca9c2726cee07000784345b" data-chapterno="4">
  <h1>
   Employers need to get to know their employees and show what employees are contributing.
  </h1>
  <div class="chapter__content">
   <p>
    What do you have in common with megastars like Madonna? More than you’d think. In spite of wealth and success, many actors and movie stars find themselves dissatisfied – under constant pressure to reinvent themselves because of a deep fear of becoming irrelevant. This fear affects office workers just as much as Grammy Award winners.
   </p>
   <p>
    Employers can do a lot to help their employees feel needed. Firstly, by making sure each employee knows what their contribution is. For example, employers might encourage their employees to ask “Who am I helping?” In the hospitality industry, the answer is straightforward: the customer. But even though it’s clear who they’re helping, it’s important to remind employees that their work is having an impact by making sure to relay customer feedback to them.
   </p>
   <p>
    It’s not quite as easy to motivate employees who aren’t working directly with customers, such as secretaries and office assistants. Their work largely benefits just one person directly – the boss. In this situation, it’s a good idea for the boss to motivate employees by acknowledging how their hard work makes her life easier.
   </p>
   <p>
    As well as making the effort to give praise, employers need to get to know their employees. Employees whose bosses take an interest in them will feel more satisfied in their roles. So what’s the best way to make an employee feel listened to? Easy – just take time to sit down and talk.
   </p>
   <p>
    It may be hard to overcome workplace formality at first. When recruiting, managers are trained not to ask candidates personal questions in interviews. However, managers should get to know their staff. This includes their dreams, what motivates them, and important events in their home life. Sadly, managers often neglect to do this, but taking a genuine interest in colleagues and checking in with them regularly is one of the most effective ways to raise employee engagement.
   </p>
  </div>
 </div>
 <div class="chapter supplement" data-chapterid="5ca9c2726cee07000784345b" data-chapterno="4">
  <h1>
  </h1>
  <div class="chapter__content">
   <p>
    “
    <em>
     To manage another human being effectively requires some degree of empathy and curiosity about why that person gets out of bed in the morning.
    </em>
    ”
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5ca9c28a6cee07000759a4ca" data-chapterno="5">
  <h1>
   Employees need to know exactly what they are contributing and have a reliable way of measuring this.
  </h1>
  <div class="chapter__content">
   <p>
    “Great job!” That’s the sort of thing you might hear from a manager trying to keep you motivated. It’s nice, but it’s not enough. Employers need to be a lot more specific in their praise. Why? Because employees need to know exactly what their contribution is to feel their work is worthwhile.
   </p>
   <p>
    This often involves playing on the human element of any job. Take a worker at an airport hotel. It may be their job to deliver breakfast trays to guests’ rooms. If they only see their job as delivering food, they won’t find much satisfaction in the task. But if they understand that the guests receiving the breakfast have likely been traveling for hours, are very tired, and that having breakfast outside their door will make a huge difference to their day, the job suddenly becomes much more meaningful.
   </p>
   <p>
    Accounting might not seem like the most caring of professions, but an accountant may have just as great an impact on other humans. Say a sick person has lost the receipt from a doctor’s appointment but needs to be reimbursed as soon as possible. If the accountant helps find that receipt, she’s not only finding a piece of paper; she’s also giving the patient peace of mind. That lets the patient take a break from worrying about their finances and spend their time on something enjoyable.
   </p>
   <p>
    It should be part of the employer’s job to remind employees who they’re helping.
   </p>
   <p>
    As well as having a clear idea of who benefits from their hard work, employees need to be able to measure their contribution. It’s very important that success isn’t measured by how happy a superior is with the work. If success is evaluated subjectively like this, it creates a competitive environment, with people playing politics and sucking up to the boss.
   </p>
   <p>
    Instead, employees should be able to track their progress using clear metrics. It’s no use giving individuals the general company revenue target to aim for; there should be a feedback system. This allows employees – like the hotel worker – to get direct feedback from customers.
   </p>
   <p>
    The hotel worker was just a quick example. Let’s look at some more examples in the next blink.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5ca9c29c6cee07000784345c" data-chapterno="6">
  <h1>
   The job of a box boy or of a star athlete can both be made more meaningful.
  </h1>
  <div class="chapter__content">
   <p>
    You’ve already heard why a glamorous job isn’t necessarily a happy one, just as a menial job isn’t always miserable. If you need convincing further, here are some more examples of how to make any job more meaningful.
   </p>
   <p>
    What about a box boy? The boy in this example is called Josh. He works at the supermarket packing customers’ bags and carrying them to the car. Josh may be at the bottom of the hierarchy, but the boss would be unwise to treat him anonymously. The boss might start a conversation, find something the two have in common and by doing so increase Josh’s interest in and loyalty to the store. Josh could also make the work more interesting for himself by giving the customers extra benefits, such as a weather report or an inspiring quote while he’s doing his job.
   </p>
   <p>
    Now let’s look at the other end of the hierarchy and consider the job of a star athlete. In this case, he’s called Michael, and he plays American football. Michael’s lucky enough to earn several million dollars each year, but he’s not necessarily a happy man. Major athletes, like Michael, are treated like commodities – traded anonymously by clubs and agents for huge fees. Moving to a new city, working with a new coach, the star athlete risks feeling very lonely, unless the new coach makes an effort to get to know Michael personally.
   </p>
   <p>
    The other occupational hazard is that Michael loses sight of what he’s doing for others. Playing a sport can come to seem irrelevant. Even if Michael and his teammates have thousands of followers on social media, or receive media praise for their playing record, this is no replacement for personal feedback. That means it’s up to the coaches and managers to remind sportspeople how much they’re valued by the fans who watch their games. A win will keep the supporters happy through the whole week, and keeping that in mind can make all the difference.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5ca9c2ae6cee07000759a4cb" data-chapterno="7">
  <h1>
   Managers should serve their employees; there are simple actions any manager can take to do that.
  </h1>
  <div class="chapter__content">
   <p>
    What do you think of when you think of service? An underpaid waiter serving a rich diner? An ordinary worker serving a more powerful manager? Actually, service should go both ways, and managers should aim to serve their employees.
   </p>
   <p>
    When you’re a manager in a corporate organization, it’s easy to feel you’re not as generous or caring as a nurse or a doctor. But don’t forget that managers are in a position to give a great deal to their employees. To do this, they have to make an effort to improve the well-being of their employees – physically and mentally – to ensure they have the best possible working conditions. The great news is that everybody benefits in this situation; the employees feel valued and motivated, while the manager enjoys the satisfaction of having improved the well-being of many people and their families.
   </p>
   <p>
    Making changes needn’t be intimidating either. There are a wealth of simple things managers can do to be of service to their staff.
   </p>
   <p>
    First, a manager should take a look at what they’re already doing to make their employees feel good. They can do this by asking how well they really know their employees, whether they are aware of their hobbies, passions and home life.
   </p>
   <p>
    It’s also the manager’s responsibility to put measures in place that will keep each employee aware of their contribution to the company, so they can keep track of the impact their work is having.
   </p>
   <p>
    But managers shouldn’t just trust their own feelings. They need to ask for feedback to see which areas still leave room for improvement when it comes to helping employees find meaning in their work. If there are shortcomings, then the manager can make a decisive plan of action for changes. This will involve further discussions with individuals and the whole team.
   </p>
   <p>
    If individuals and managers alike make the effort to connect and make each other feel valued, both people and business will thrive.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5ca9c2c56cee07000784345d" data-chapterno="8">
  <h1>
   Final summary
  </h1>
  <div class="chapter__content">
   <p>
    The key message in these blinks:
   </p>
   <p>
    <strong>
     Don’t put up with a miserable job – find a way to make it meaningful. Managers and employees alike have the tools to bring joy and meaning to each other’s work. The key is to focus on your impact on other people, what you contribute to the whole, and have a clear way of measuring your success. The more engaged the employees, the more successful the business.
    </strong>
   </p>
   <p>
    Actionable advice:
   </p>
   <p>
    <strong>
     Find a meaningful job.
    </strong>
   </p>
   <p>
    If you’re job-searching, it’s wise to ensure there’s potential for the role for which you’re applying to be meaningful. You can do this at the interview stage. Tell prospective employers that you’d like to know them better and that you want them to know you. Tell them you need a clear vision of what your work is doing for others and a clear way to measure success. If they don’t seem interested in answering the questions, that’s a red flag. Find a company that can offer you a workplace where you’ll look forward to arriving every morning.
   </p>
   <p>
    <strong>
     Got feedback?
    </strong>
   </p>
   <p>
    We’d sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
   </p>
   <p>
    <strong>
     What to read next:
    </strong>
    <strong>
     <em>
      Build It
     </em>
    </strong>
    <strong>
     , by Glenn Elliott and Debra Corey
    </strong>
   </p>
   <p>
    In these blinks, you got a good general overview of the importance of having engaged employees. When you’ve mastered the basics, and you’re ready to take employee engagement to the next level, then
    <em>
     Build It
    </em>
    should be next on your list.
   </p>
   <p>
    A self-described “rebel playbook,”
    <em>
     Build It
    </em>
    is the result of a decade of thorough research. Having studied employee engagement in 2,000 companies, the authors have plenty of insights to share on how top firms keep their people productive. They also let you in on the secret that links them all. Let’s just say you can put that old HR rulebook on eBay...
   </p>
  </div>
 </div>
</article>
