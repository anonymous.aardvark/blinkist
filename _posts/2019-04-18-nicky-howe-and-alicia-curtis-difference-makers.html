---
layout: post
title: "Nicky Howe and Alicia Curtis - Difference Makers"
description: "Difference Makers (2016) makes a compelling case for the value of diversity at the top of today’s companies. Written by two leading champions of inclusive leadership, these blinks guide readers through personal and boardroom strategies to overcome bias, foster open dialogue and spark innovation by getting more voices to the table."
image: https://images.blinkist.com/images/books/5c967e7c6cee070007970923/1_1/470.jpg
---

<article class="shared__reader__blink reader__container__content" lang="en">
 <div class="chapter chapter" data-chapterid="5c967eab6cee070008b872bd" data-chapterno="0">
  <h1>
   What’s in it for me? Learn what diversity can do for your company.
  </h1>
  <div class="chapter__content">
   <p>
    Most people agree that diversity in the workplace is a good thing. The problem, however, is getting there. Corporate bids to improve the representation of minorities by means of quotas often lead to controversy. Worst of all, new appointees end up feeling like they’ve been hired to make up the numbers rather than because of their talents.
   </p>
   <p>
    So what’s the solution? Well, that’s the question two of Australia’s top entrepreneurs and diversity champions set out to to answer in these blinks. The best place to start, Nicky Howe and Alicia Curtis argue, is to redefine the term itself. What really matters aren’t visible differences between people but their unique perspectives on the world – call it “diversity of thought.”
   </p>
   <p>
    And creating a corporate culture that’s committed to fostering open-ended, inclusive dialogue is something that begins long before applicants’ résumés land in your inbox. The key is to change the way you think and behave in the boardroom. Luckily, there’s a wealth of cognitive tools out there to help you do just that. So read on to learn how to start making your company more diverse!
   </p>
   <p>
    Along the way, you’ll learn
   </p>
   <ul>
    <li>
     why diversity is so important in today’s world;
    </li>
    <li>
     how our biases prevent companies becoming more inclusive; and
    </li>
    <li>
     why “groupthink” is responsible for poor decision-making in the boardroom.
    </li>
   </ul>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5c967ecc6cee070007970924" data-chapterno="1">
  <h1>
   Diversity is about more than visible differences, and it’s essential to success in today’s market.
  </h1>
  <div class="chapter__content">
   <p>
    What is diversity? Lots of people answer this question by making reference to visible differences such as gender or race. That’s an okay start, but it doesn’t tell the whole story. Just think of what makes you unique. Chances are, intangibles like your personality, life experiences and beliefs would rank pretty high, right? Well, that’s pretty much the point: there’s more to people than meets the eye.
   </p>
   <p>
    That’s why the
    <em>
     Oxford Handbook of Human Resource Management
    </em>
    shifts the focus from obvious markers of difference to the wide variety of qualities of people within organizations. Diversity, in other words, is about recognizing that every person is a rich tapestry woven together from multiple threads. Age, education, abilities, disabilities, culture, experience, ideology, profession and, of course, race and gender – to name just a few factors – all shape who we are.
   </p>
   <p>
    Unfortunately, we often focus on what we can see and ignore the invisible threads. That leads to the common fallacy that people who look alike have the same views. Nothing, however, could be further from the truth. And that’s the foundation stone of a broader, and more accurate, understanding of diversity: what really matters is
    <em>
     diversity of thought
    </em>
    .
   </p>
   <p>
    And why does it matter to companies? Well, equal representation is not only good in its own right, it’s also an essential part of doing business in today’s world. That’s because we live in an age which is increasingly
    <em>
     Volatile, Uncertain, Complex and Ambiguous
    </em>
    – call it “VUCA” for short.
   </p>
   <p>
    The world is constantly changing. Just think of Asia. By 2050, two thirds of the global middle class and 20 of the largest 50 cities will be Asian – up from only eight cities in 2007. Then there’s digital innovation. Once iconic household names like Kodak and Blockbusters have been eclipsed by upstarts like Uber, Facebook and Airbnb. Globalization has meanwhile created a business environment in which companies have to scout for top talent across borders and organizations, not just across the city.
   </p>
   <p>
    If you want to keep pace with those changes and capitalize on the opportunities they present, your company’s board and management needs to be every bit as diverse as the markets in which they operate. And that’s all about being open, adaptable and flexible – something that’s a whole lot easier if you’re drawing your leaders from the largest possible pool!
   </p>
  </div>
 </div>
 <div class="chapter supplement" data-chapterid="5c967ecc6cee070007970924" data-chapterno="1">
  <h1>
  </h1>
  <div class="chapter__content">
   <p>
    “
    <em>
     How can we effect change in the world when only one half of it is invited or feels welcome to participate in the conversation?
    </em>
    ” – Emma Watson
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5c967edf6cee070008b872be" data-chapterno="2">
  <h1>
   Bias is a major obstacle to diversity, but it can be unlearned.
  </h1>
  <div class="chapter__content">
   <p>
    Diversity pays. That’s the view of the business world’s leading analysts. Take management consultancy McKinsey’s 2014 report
    <em>
     Diversity Matters
    </em>
    . It shows that highly diverse companies are 35 percent more likely to enjoy above-industry-average returns than their less diverse counterparts.
   </p>
   <p>
    That’s hardly surprising. According to the same report, women and minority groups make the majority of consumer decisions, so it stands to reason that companies which better represent these groups are good at attracting their business. But if it’s such a no-brainer, why aren’t more companies cashing in?
   </p>
   <p>
    In a word, bias. Humans are naturally prone to judging people according to clichés rather than their actual merits. These biases come in a number of different forms.
    <em>
     Implicit stereotypes
    </em>
    link certain groups with supposedly typical traits: think of the association between men and science or Asians and math, for example. Then there’s
    <em>
     in-group favoritism
    </em>
    . That’s an age-old human tendency to prefer people who are like us. Finally, there’s
    <em>
     outgroup homogeneity
    </em>
    , the readiness to believe that other groups are less “complex” than our own.
   </p>
   <p>
    These snap judgments have a huge effect on hiring policies and boardroom behaviors. Ever wondered why so many firms’ boards consist entirely of middle-aged white men, why everyone in leadership positions went to the same school or why meetings are held at times that exclude people with family commitments? Put it down to bias!
   </p>
   <p>
    Bias might be innate, but that doesn’t mean you can’t unlearn it. That’s why today’s most effective leaders take time to identify their false assumptions and adopt corrective strategies to overcome behavior that reproduces narrow group advantages.
   </p>
   <p>
    A great place to start is by taking surveys like the Harvard Implicit Association Test (IAT), an online tool which can help you identify subconscious patterns of association concerning weight, race, gender and age. Simply becoming mindful of these biases provides an opportunity for self-correction, allowing you to ask yourself questions like, “Did I just assume that a male candidate is more ambitious than a female candidate because she has two kids?”
   </p>
   <p>
    But the most effective tactic against bias is to become more aware of the world around you – call it
    <em>
     cultural competency
    </em>
    . Whether it’s learning a new language, traveling with an open mind or simply going out of your way to have meaningful conversations with people who aren’t like you, you can become a greater champion of diversity by living it out on a day-to-day basis.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5c967ef06cee070007970925" data-chapterno="3">
  <h1>
   Using different “thinking hats” can help you avoid falling into the trap of groupthink.
  </h1>
  <div class="chapter__content">
   <p>
    So we’re prone to favoring people like us and forming small, like-minded groups. In this blink, we’ll take a look at one of the most dangerous effects of this phenomenon –
    <em>
     groupthink
    </em>
    – and how to overcome it.
   </p>
   <p>
    Groupthink is the tendency of highly cohesive groups to favor consensus and make bad calls when they’re under pressure. The term was coined by psychologist Irving Janis, the author of a 1972 study analyzing US foreign policy fiascos, such as the failed Bay of Pigs invasion of Cuba in 1961 and the escalation of the Vietnam war.
   </p>
   <p>
    But it’s not just military strategists that are susceptible to groupthink. Boards are just as likely to overrule valid objections and steamroll ahead with their plans when they need to make a quick decision.
   </p>
   <p>
    The best way of avoiding that trap is to cultivate an open-minded approach to solving problems. How? Well, that’s where psychologist Edward de Bono’s idea of the
    <em>
     Six Thinking Hats
    </em>
    comes in. It’s essentially a cognitive tool to help facilitate critical thought, collaboration, communication and creativity. Here’s how it works:
   </p>
   <p>
    Imagine six hats, each a different color representing a different style of thought. Each piece of headgear is as easy to don as it is to take off. That’s handy because each member of the board will be trying out each hat in turn. The first is the
    <em>
     White Hat
    </em>
    , representing a sober, unbiased consideration of the facts. The
    <em>
     Red Hat
    </em>
    is for emotions: its wearer can discuss their feelings on a matter.
   </p>
   <p>
    The
    <em>
     Black Hat
    </em>
    stands for pessimism – wearing it provides a chance to discuss the downsides of a decision. Then there’s the
    <em>
     Yellow Hat
    </em>
    , the optimistic hat that lets you dwell on the pros rather than the cons. The
    <em>
     Green Hat
    </em>
    meanwhile stands for creativity – when you’re wearing it, you should feel free to try out new ideas without worrying about being judged. Finally, the
    <em>
     Blue Hat
    </em>
    is to ensure that the all six hats are being used correctly.
   </p>
   <p>
    This exercise works best when you’re posing specific questions for each color. So rational White might ask, “What are the facts?” while Black would want to know, “What are the consequences of failure?” and Red would ask, “How do you feel about it?” By using it in the boardroom, you can make sure that everyone is working hard to look at an issue from every possible angle.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5c967f076cee070008b872bf" data-chapterno="4">
  <h1>
   Recognizing your mood can help you take charge of emotional situations.
  </h1>
  <div class="chapter__content">
   <p>
    Emotions play a huge role in the day-to-day life of boardrooms and the decisions made there. Just think of how often you intuitively
    <em>
     sense
    </em>
    that something’s up. That’s a physical reaction triggered by the limbic system, the part of the brain that controls emotional reactions. That means simply trying to suppress your feelings won’t work – in fact, your best bet for improving your decision-making is to learn to observe and manage your moods.
   </p>
   <p>
    So how do you do that? Well, let’s start with a definition. A mood is basically a filter that determines the way you see and respond to the world around you. According to life coach Alan Sieler, there are six
    <em>
     Basic Moods of Life
    </em>
    . These come about as the result of negative or positive responses to what he calls
    <em>
     Facticity
    </em>
    ,
    <em>
     Possibility
    </em>
    and
    <em>
     Uncertainty
    </em>
    . Let’s break those down.
   </p>
   <p>
    Facticity refers to things that are beyond your control – your age, say, or who is the current CEO of the company. Acceptance of these facts leads to a mood of
    <em>
     peace
    </em>
    while opposition leads to
    <em>
     resentment
    </em>
    . Possibility, on the other hand, is about things you see as changeable, like getting a new puppy or making your board more diverse. Accepting possibility here leads to
    <em>
     ambition
    </em>
    , opposing it leads to
    <em>
     resignation
    </em>
    . Finally there’s Uncertainty, which refers to the things you can’t predict, such as organizational change or the weather. Embracing that randomness leads to a mood of
    <em>
     wonder,
    </em>
    while rejecting it leads to
    <em>
     anxiety
    </em>
    .
   </p>
   <p>
    That’s pretty abstract, right? Well, let’s put it into context. Imagine you’ve had a run-in with a sharp-tongued colleague and it’s left you in a foul mood. Now, the past can’t be undone. That means you’re confronted with an example of facticity – something you can’t change. The only thing you
    <em>
     do
    </em>
    have some influence over is your response. If that’s something like, “I can’t believe she talked to me like that,” you’re opposing that reality and likely to become resentful.
   </p>
   <p>
    But what happens if you accept her behavior? It suddenly becomes much easier to say, “Oh well, these things happen,” or “Maybe she was just having a difficult morning.” That doesn’t just shift your mood towards one of peace – it also puts you in a position to get out of your head and reach out to her. Rather than resentfully ruminating, you can ask your colleague if something’s wrong and if you can help!
   </p>
  </div>
 </div>
 <div class="chapter supplement" data-chapterid="5c967f076cee070008b872bf" data-chapterno="4">
  <h1>
  </h1>
  <div class="chapter__content">
   <p>
    “
    <em>
     The key point here is that as a human being you are an emotional being. This isn’t good or bad, it just is.
    </em>
    ”
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5c967f1b6cee070007970926" data-chapterno="5">
  <h1>
   Frank conversations and an avoidance of “tokenism” are the best ways to build a diverse board.
  </h1>
  <div class="chapter__content">
   <p>
    So far, we’ve looked at some personal strategies to become more open to diverse ways of thinking. In this blink, we’ll be shifting our focus to collective boardroom game plans that you can use to help your company commit itself to becoming a champion of diversity. So where do you start? Well, the key is to make sure the entire board is on the same page. That means you’ll need to start a conversation.
   </p>
   <p>
    Diversity, as we’ve learned, has plenty of advantages, but there’s more than one way to get there. If you want to find the policy that’s right for your organization, you’ll need to generate ideas that suit your needs. This is known as a
    <em>
     speculative conversation
    </em>
    – an attempt to figure out where you are right now and where you want to go. In other words, does the governance of your organization reflect the diversity of the communities and customers which it serves?
   </p>
   <p>
    To answer that, you’ll need the facts and figures at hand. Those are actually easier to come by than you might think. Your own firm’s customer relations management systems should have plenty of hard data on who your customers actually are. Hitting up local government statistics bureaus should take care of the rest.
   </p>
   <p>
    Right, now you’re ready to start talking! This can be a sensitive matter – after all, no one likes being confronted with their own biases – so it’s best to be patient and remember that becoming truly inclusive takes time.
   </p>
   <p>
    Here are a few questions to get the ball rolling: If your company doesn’t reflect the demographic diversity of its customers, how can you reach out to underrepresented groups? Could you, for example, advertise positions in community newspapers? Do you take knowledge of other languages and cultures into account when you make hiring decisions? Do you ask candidates for leadership positions how they feel about working with people from diverse backgrounds?
   </p>
   <p>
    One important thing to remember as you go through this process is that “tokenism” often does more harm than good. As the authors discovered while running a program to encourage young leaders onto boards, nothing undermined the confidence and morale of “diverse” people so much as the feeling that they’d been hired to fill a quota.
   </p>
   <p>
    Diversification strategies work best when companies enable diverse newcomers to make an impact from the get-go by giving them real responsibilities. Doing that not only makes new board members feel welcome – it also allows you to start acting on their unique perspectives, thus enriching your company!
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5c967f6b6cee070008b872c0" data-chapterno="6">
  <h1>
   Final summary
  </h1>
  <div class="chapter__content">
   <p>
    The key message in these blinks:
   </p>
   <p>
    <strong>
     Diversity isn’t just desirable in and of itself – it’s also great for business. That’s hardly surprising. We live in an increasingly complex world, so it stands to reason that the companies which best represent their diverse customer bases should prosper. The reason more companies aren’t embracing a more inclusive approach to leadership, however, is simple: human bias. Luckily, the tendency to favor in-groups can be unlearned. By adopting the strategies in these blinks, you can start recognizing harmful stereotypes, allowing you to foster an open and inclusive workplace and build a more diverse boardroom.
    </strong>
   </p>
   <p>
    Actionable advice:
   </p>
   <p>
    <strong>
     Listen to what your body’s telling your about your mood!
    </strong>
   </p>
   <p>
    We’ve already talked about the importance of moods in shaping your perception of the world, but how do you actually recognize these often subtle emotional states? Well, it’s usually a safe bet that you’re body is already trying to tell you. So start by observing your body language: tightness in your face, jaw, neck and shoulders, for example, is a tell-tale sign that you’re battling with negative emotions. Once you’ve checked in with your body, ask yourself some follow-up questions: “What am I rejecting in this situation?,” “What am I opposed to?” and, finally, “What is my current mood?” That should help you clarify your feelings and start putting a positive spin on things.
   </p>
   <p>
    <strong>
     Got feedback?
    </strong>
   </p>
   <p>
    We’d sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
   </p>
   <p>
    <strong>
     What to read next:
    </strong>
    <strong>
     <em>
      The Inclusion Dividend
     </em>
    </strong>
    <strong>
     , by Mark Kaplan and Mason Donovan
    </strong>
   </p>
   <p>
    “I’m not sexist, it’s just that the top candidates are all guys.” “I don’t believe in quotas – surely you should pick the best person for the job, whatever the color of their skin?” You’ve probably come across these kinds of arguments against diversity before, but just how well do they stack up against the evidence? Well, that’s exactly the question that Mark Kaplan and Mason Donovan wanted to answer when they looked into the matter. So if you’re keen to learn more about diversity in practice, why not check out our blinks to
    <em>
     The Inclusion Dividend
    </em>
    .
   </p>
  </div>
 </div>
</article>
