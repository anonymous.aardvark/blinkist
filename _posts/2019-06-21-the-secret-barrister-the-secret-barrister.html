---
layout: post
title: "The Secret Barrister - The Secret Barrister"
description: "The Secret Barrister (2019) takes a behind-the-scenes look at the often chaotic and frighteningly disorganized world of England and Wales’ criminal justice system. As revealed by an experienced criminal barrister, the current system is woefully underfunded and suffering from a lack of resources, yet is also under threat from proposed reforms that would impose further cuts. However, there are some reasonable ways for the system to improve."
image: https://images.blinkist.com/images/books/5ce9cb406cee0700073a5aba/1_1/470.jpg
---

<article class="shared__reader__blink reader__container__content" lang="en">
 <div class="chapter chapter" data-chapterid="5ce9cb596cee070007a4b8dc" data-chapterno="0">
  <h1>
   What’s in it for me? Discover the many problems facing England’s criminal justice system.
  </h1>
  <div class="chapter__content">
   <p>
    Our perceptions of criminal justice systems come from what we see on TV or read in legal thrillers. We enjoy the twists and turns of the plot as the hero gets bounced around the courts and thank our lucky stars that we’ll never have to defend ourselves against a criminal accusation or be cross-examined on the witness stand. But don’t be so sure.
   </p>
   <p>
    There’s an extremely good chance that at some point in your life, you or someone you know will find themselves thrust into the machinery of your local legal system. So it stands to good reason that we should all know how that machinery works.
   </p>
   <p>
    The Secret Barrister wants people to better understand the everyday realities of the criminal justice system in England and Wales, and it’s not a pretty picture. Due to politically influenced budget cuts and a general lack of knowledge about what really goes on, the system is suffering from a lack of money and resources. So much so, that the very principles of fairness and justice upon which the system was founded are under threat.
   </p>
   <p>
    In these blinks, you’ll discover
   </p>
   <ul>
    <li>
     how untrained volunteers ended up deciding who goes to jail;
    </li>
    <li>
     why criminal case files are routinely incomplete or inaccurate;
    </li>
    <li>
     why it’s wrong to think England’s criminal barristers are getting rich on taxpayer money.
    </li>
   </ul>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5ce9cb736cee0700073a5abb" data-chapterno="1">
  <h1>
   The criminal justice system of England and Wales has its own rules and traditions.
  </h1>
  <div class="chapter__content">
   <p>
    Even if you didn’t grow up in the United Kingdom, you’ve probably seen a TV show or movie featuring a scene in a British courtroom. You may even have wondered, why are they wearing those peculiar wigs and gowns?
   </p>
   <p>
    Like many aspects of the criminal justice system in England and Wales, the gowns and wigs are part of a long tradition going back hundreds of years. Before we get into some of the traditions that may need revising, let’s first look at what this system really entails.
   </p>
   <p>
    As in justice systems in many other places, an English or Welsh criminal trial will involve a judge, a jury and the person accused of the crime. Then there are barristers and solicitors.
   </p>
   <p>
    Since the fourteenth century, there’s been a distinction made in English courts between the courtroom-based lawyer who presents the prepared cases to the court – the barrister – and the solicitor, the lawyer who deals directly with the witnesses while advising both the barrister and the client. As we’ll see, there are good solicitors who advocate tirelessly on behalf of their clients, and exploitative solicitors who give lawyers a bad name.
   </p>
   <p>
    Another important piece of the criminal justice puzzle is the Crown Prosecution Service, or CPS. This is a relatively new addition to the system, established in 1985 with the Prosecution of Offenses Act. Prior to that, the police were responsible for charging suspects with a crime, but in an effort to improve consistency and efficiency in filing charges and preparing cases across all of England and Wales, the CPS was created. Now, every case goes through the CPS machinery for preparation before landing in the hands of the barrister.
   </p>
   <p>
    It’s worth noting that, unlike in some other countries, barristers who handle criminal cases, like the author, regularly bounce between serving for the defense and prosecution, sometimes within the same day. This may sound unusual, but in the author’s experience, doing so can make you a better, more well-rounded lawyer. After all, knowing first-hand how both sides of the adversarial process operate can only make you better at knowing what to expect before your case begins.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5ce9cb8a6cee070007a4b8dd" data-chapterno="2">
  <h1>
   All criminal cases begin at the Magistrates’ Court, a place often rife with problems.
  </h1>
  <div class="chapter__content">
   <p>
    In England and Wales, different types of crimes and disputes have different courts. This is the case in many places around the world, but in England, the current division of courts goes all the way back to 1195, when Richard I decided to give local communities their own Justice of the Peace (JP), so they could tend to their own small-time offenders such as rioters and fraudsters.
   </p>
   <p>
    Over the centuries that followed, these JPs gained a bit more authority as their purview grew to include petty crimes such as fine violations and jury trials involving misdemeanors and more complex crimes like larceny. These local JPs were also known as magistrates. For a long time, it wasn’t unusual for a magistrate to hold a trial in his living room as these officials were local men who volunteered for the job and received no formalized training or education.
   </p>
   <p>
    These days, the Magistrates’ Courts still exist, and some of these old traditions along with new complications have made them problematic places.
   </p>
   <p>
    To begin with, the courts are now the first stop for any criminal case to determine some basic but important things like whether or not the accused will receive bail. And while the Magistrate’s Court is no longer held in living rooms, those overseeing the cases are still untrained volunteers.
   </p>
   <p>
    In other words, the decision as to whether someone should be stripped of their liberties and placed in horrid prison cells for upward of six months while they await their trial – potentially losing their jobs, friends, family and home – are made by laymen who have no special training or legal knowledge. If that isn’t disconcerting enough, due to budget cuts and insufficient resources within the CPS, decisions on bail are routinely made based on the information found in incomplete or inaccurate case files.
   </p>
   <p>
    Now, laws dating back to the seventeenth century, including the Habeas Corpus Act and Bill of Rights, state that it is unlawful to imprison people on accusations alone. The European Convention on Human Rights also frowns on locking people up without “adequate reasons.” Nevertheless, this is essentially what happens when magistrates make decisions, often with just a few minutes of deliberation based on incomplete or erroneous evidence.
   </p>
   <p>
    In the next blink, we’ll take a closer look at what’s going wrong with the CPS and their case files.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5ce9cba26cee0700073a5abc" data-chapterno="3">
  <h1>
   Due to harmful budget cuts, the guilty can end up walking free.
  </h1>
  <div class="chapter__content">
   <p>
    There’s an ongoing legislative push to process criminal cases as quickly and cheaply as possible. There’s a general policy of “stack ‘em high, sell ‘em cheap,” which is the result of cutting costs and forcing the people working in the criminal justice system to do more with less.
   </p>
   <p>
    For example, since 2009, the CPS budget has been cut by 27 percent, and over the past eight years around a third of the staff has been let go. There have been suggestions that digitization of records and using email to move files would require less money and resources.
   </p>
   <p>
    But in fact, despite the valiant efforts of the remaining CPS staff, one out of five cases arrives in the hands of the barristers with incorrect information on which charges were filed by the police. One in ten cases fails to recognize critical evidence such as identifying instances where the accused was acting in self-defense. In one in six cases, the file is pushed through without any CPS lawyer having reviewed the case at all.
   </p>
   <p>
    When barristers get their daily case assignments along with the corresponding case files, it is usually a matter of hours before the first case is due to be presented in court. So there is little time to chase down missing evidence or resolve any other issues with the file. Sometimes, this results in criminals getting away with it.
   </p>
   <p>
    In one particularly troubling case, a young woman, Amy, was spotted by a passing taxi driver being pulled out of her flat by her hair and viciously beaten by her boyfriend. Amy suffered a broken jaw as well as a fractured wrist and eye socket. Yet the vital evidence of Amy’s statement and the medical records were missing.
   </p>
   <p>
    So the author did what they could and requested that the police speak to Amy, get her statement and pick up her medical records. What they received back was a memo saying that the police spoke with Amy and she was still willing to give a statement and have her medical records released.
   </p>
   <p>
    Even the magistrate was confused that the police had seemingly visited Amy and yet not taken the statement or picked up the medical records, so he granted another seven days for this to be taken care of, or else the case would be dismissed. The author sent numerous requests and follow-ups in the days that followed, yet nothing came back. As a result, Amy’s boyfriend walked free.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5ce9cbb76cee070007a4b8de" data-chapterno="4">
  <h1>
   Some solicitors have given defense counsel a bad name, but they’re critical in keeping the innocent out of jail.
  </h1>
  <div class="chapter__content">
   <p>
    Ultimately, the criminal justice system was designed to act in the public’s interest. Of course, this means prosecuting people who’ve broken the law, but it also means protecting people from being falsely convicted of crimes they didn’t commit. And this is where the defense comes in.
   </p>
   <p>
    Popular culture likes to paint defense lawyers as dubious characters willing to lie through their teeth to get a not-guilty verdict. As far as barristers go, this is far from the case. Barristers must follow strict laws against purposefully lying, and in the author’s experience, no one wants to break those rules or knowingly help a guilty defendant go free.
   </p>
   <p>
    That said, there are a small minority of vulturous defense solicitors who contribute to the negative image of a defense counsel. For a period of time, among the worst was the firm of Keres &amp; Co. Mr. Keres would routinely offer his services to the recently arrested, receive the legal aid fee and then never be heard from again.
   </p>
   <p>
    The author experienced this first hand when trying to defend a young man by the name of Darius who was arrested after a fight with his father ended with him taking five pounds out of his father’s wallet. His father reported this as a robbery, and Darius was picked up and put in jail. Darius was represented by Mr. Keres, who never once showed up to explain why Darius was in jail, what charges he was facing or to help him get hold of the medication he was supposed to be taking. Weeks went by with no visit despite the author’s efforts to push Keres into action.
   </p>
   <p>
    Fortunately, Keres’ operation has since gone under, but there are still plenty of vultures out there. And there’s also the issue of legal aid.
   </p>
   <p>
    In England and Wales, legal aid is offered to those deemed to have an insufficient disposable income. Currently, this is anyone who has a combined disposable household income of less than £37,500. Nevermind the fact that most private defense counselors can cost upward of £100,000 to £300,000.
   </p>
   <p>
    In the meantime, in 2016, 15 percent of the people who were remanded to a jail cell without bail were later acquitted. And while it’s hard to put a number on the people wrongfully convicted, as we’ll see in the next blink, it does happen, and the chances only increase when we make a reliable defense harder to obtain.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5ce9cbcb6cee0700073a5abd" data-chapterno="5">
  <h1>
   The inquisitorial justice system has some advantages over the accusatorial.
  </h1>
  <div class="chapter__content">
   <p>
    Few defense barristers will enjoy cross-examining a witness who is testifying that someone sexually assaulted or abused them. Yet that is an all-too-common feature of a criminal trial conducted in an accusatorial fashion, as in England and Wales. In fact, the accusatorial process practically demands that the victim relive their experience on the witness stand, and then have their story picked at by the defense.
   </p>
   <p>
    When the author was given the case of defending Jay, a man accused of repeatedly raping his children throughout their lives, there was a sense of having to face an imperfect aspect of the accusatorial process: trying to discredit witnesses. Though Jay never admitted to any wrongdoing, something about his mannerisms, like the way he dispassionately declared his innocence, while his children were so passionate in their claims against him, made the author doubt that innocence. It also made the author wonder if it was really in the public’s interest to go through the psychiatric history of Jay’s children to find evidence that might cast doubt on their assertions?
   </p>
   <p>
    Now, some say that the inquisitorial process used in the courtrooms of continental European nations such as Germany and France is a fairer style of justice.
   </p>
   <p>
    Unlike the accusatorial process, in which the prosecution present their case, and then the defense attempt to undermine it, the inquisitorial process largely sidelines the lawyers altogether. So, rather than two lawyers fighting to suppress certain evidence that may hurt their case, an inquisitorial trial relies on a single judge who reviews all the evidence that has been collected by the state. And instead of the complainant being potentially victimized all over again, in the inquisitorial process, they can be empowered in this process by acting as a “subsidiary prosecutor.” This allows them to make suggestions, such as which questions the judge might find useful when interviewing a witness.
   </p>
   <p>
    There are no demeaning cross-examination questions, no plea-bargaining to avoid undesirable verdicts, no motions to exclude damning evidence. Rather than indulging in efforts to obscure the truth of what happened, the inquisitorial process tries to get to the bottom of it with minimal drama.
   </p>
   <p>
    So, is the inquisitorial system better at getting justice than the two-sided approach of the accusatorial? Let’s press on in the next blink.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5ce9cbe06cee070007a4b8df" data-chapterno="6">
  <h1>
   The inquisitorial process relies on the state being unbiased, which is why a thorough defense is needed.
  </h1>
  <div class="chapter__content">
   <p>
    There may be reasons why the unfussy pursuit of truth at the core of the inquisitorial process may sound more appealing. However, this process relies heavily on the evidence presented by the police or the state, and we needn’t look hard to see that this evidence is often far from perfect or impartial.
   </p>
   <p>
    In 2017, a joint report led by Chief Inspector Kevin McGinty looked at the quality of cases being prepared by the police and CPS. Twenty-two percent were found to be “wholly inadequate,” 33 percent were found to be “poor,” and over half had no apparent suggestions as to which evidence should be shared with the defense.
   </p>
   <p>
    These are just the honest human mistakes made by the state and don’t include the number of cases where biased police officers falsified evidence to get an arrest. These kinds of things aren’t happening in the majority of cases, but they do happen, and it’s why we benefit from a rigorous examination by the defense in an accusatorial system. At its best, it’s what keeps the process honest.
   </p>
   <p>
    Ultimately, the criminal justice system is not immune to biased policing or political influence. Many powerful men and women escape prosecution because authorities are scared of the political repercussions of tackling them.
   </p>
   <p>
    That said, we may be able to learn some lessons from the inquisitorial process, and the first would be to stop harassing witnesses in cross-examination, especially in sex crimes. Why can’t we take a witness’ first statement on record as their final statement, as the inquisitorial process does, rather than making them recount the incident months or even years after the fact? After all, distance from the event doesn’t make their memory any fresher.
   </p>
   <p>
    The inquisitorial process has another advantage worth considering: offering a reason for a verdict. Currently, when a jury offers their guilty or not-guilty verdict, there’s no explanation for why they reached that conclusion. Instead, we end up with an air of mystery about what went on during their deliberations. But since this mystery can make an unwelcome verdict all the more difficult to swallow, we may want to consider taking a page from the inquisitorial process and offering reasons for the decisions we hand down in court.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5ce9cbf46cee0700073a5abe" data-chapterno="7">
  <h1>
   The guidelines for sentencing have become incomprehensible, and prisons are failing to be a deterrent to crime.
  </h1>
  <div class="chapter__content">
   <p>
    When a guilty verdict is reached, the process isn’t necessarily over. Then it’s time for the sentencing portion of the trial. This is complicated as the guidelines for sentencing a convicted criminal are a massive and largely incomprehensible mess that takes up around 1,300 pages. In the words of Supreme Court judge Lord Phillips, “Hell is a fair description” of the sentencing framework.
   </p>
   <p>
    So, maybe it isn’t too surprising to find out that sentencing in England and Wales isn’t always going according to plan. In a 2012 study, legal expert Robert Banks reviewed 262 cases and found that in 36 percent of them, the sentencing handed down in court was “unlawful” and should never have been given by the judge.
   </p>
   <p>
    Once a person is sentenced to prison, new problems arise. In theory, prison should be a deterrent to crime and a place of rehabilitation – but in reality, it’s neither.
   </p>
   <p>
    For instance, the statistics reveal that 60 percent of those sentenced to less than a year in prison will commit another crime. And among all released prisoners, 46 percent commit another crime within a year of being released.
   </p>
   <p>
    Yet despite all these problems, there are still some relatively simple steps that could be taken to improve the criminal justice system and get it the money and resources it needs.
   </p>
   <p>
    For starters, the media and politicians could do a better job of presenting an accurate picture of how the system works. Too often, they make it look as though public funding fills the pockets of rich barristers, gives criminals a free defense and provides prisoners with luxuries like Sky TV.
   </p>
   <p>
    But, in reality, a barrister is likely to get somewhere between £9.28 to £18.95 per hour. And the reality of prison is being locked up for 23 hours a day in violent, cockroach- and rat-infested surroundings while eating meals next to an open toilet.
   </p>
   <p>
    If people know what is really going on and how their money keeps the guilty from going free and the innocent from getting locked up, then maybe the proper funding and resources wouldn’t be so hard to come by. After all, there’s a chance you or someone you love may need to rely on this system working properly some day.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5ce9cc0e6cee070007a4b8e0" data-chapterno="8">
  <h1>
   Final summary
  </h1>
  <div class="chapter__content">
   <p>
    The key message in these blinks:
   </p>
   <p>
    <strong>
     The criminal justice system of England and Wales needs reform – not the kind of budget cuts and belt-tightening for which politicians are calling. Already, cuts to funding have caused the system to be woefully underfunded and understaffed, to the point where criminal case files often appear before barristers with missing or incorrect information. Criminals are slipping through the cracks, and innocent people are in danger of being locked up. Ideally, the laws surrounding the criminal justice system should align with our values around justice and fairness. There needs to be a reckoning about how the current system works.
    </strong>
   </p>
   <p>
    <strong>
     Got feedback?
    </strong>
   </p>
   <p>
    We’d sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
   </p>
   <p>
    <strong>
     What to read next:
    </strong>
    <strong>
     <em>
      Doing Justice
     </em>
    </strong>
    <strong>
     , by Preet Bharara
    </strong>
   </p>
   <p>
    If this look at the legal system in England and Wales has you thinking about how law is practiced elsewhere in the world, then the blinks to
    <em>
     Doing Justice
    </em>
    will likely satisfy that curiosity.
    <em>
     Doing Justice
    </em>
    offers a first-hand account of criminal investigation in the district of the United States that includes New York City.
   </p>
   <p>
    Obviously, this is a busy area, and there are some fascinating cases to look at. But perhaps most of all,
    <em>
     Doing Justice
    </em>
    offers lessons learned from a seasoned prosecutor, giving those conducting criminal investigations some helpful tips on how to stay focused on the truth.
   </p>
  </div>
 </div>
</article>
