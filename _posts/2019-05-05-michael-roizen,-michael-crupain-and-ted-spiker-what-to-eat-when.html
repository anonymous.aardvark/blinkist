---
layout: post
title: "Michael Roizen, Michael Crupain and Ted Spiker - What to Eat When"
description: "What to Eat When (2018) shows how it’s not just what you eat, but when you eat that matters. This practical and fun guide dives deep into the science of eating to show you how you can enhance your health, energy and intellect through healthier eating habits. It provides a blueprint for eating right, all the time."
image: https://images.blinkist.com/images/books/5c69f80c6cee070007fe682c/1_1/470.jpg
---

<article class="shared__reader__blink reader__container__content" lang="en">
 <div class="chapter chapter" data-chapterid="5c69f8576cee0700079ef253" data-chapterno="0">
  <h1>
   What’s in it for me? Learn what to eat, and when to eat it.
  </h1>
  <div class="chapter__content">
   <p>
    Most of us know that what we eat is important. We all know that if we regularly eat a large pizza with extra cheese for dinner, we’ll put on weight. But that doesn’t necessarily mean that we’re eating as we should. The research is clear – huge numbers of deaths from cardiovascular diseases, diabetes and other causes could be avoided if people embraced healthier diets.
   </p>
   <p>
    The diet industry has spent a lot of time talking about what we eat. But until now, people haven’t given so much thought to
    <em>
     when
    </em>
    we eat. The latest science shows that the when is also crucial. So how can you start to eat the right things at the right times?
   </p>
   <p>
    In these blinks, we’ll look at the science of when to eat and set out practical advice for taking your body’s natural rhythms into consideration.
   </p>
   <p>
    In these blinks, you’ll learn:
   </p>
   <ul>
    <li>
     how damaging simple carbohydrates like pasta and white bread can be;
    </li>
    <li>
     why you should be eating your dinner for breakfast; and
    </li>
    <li>
     why being angry, tired or on the move doesn’t have to mean making bad food choices.
    </li>
   </ul>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5c69f86b6cee070007fe682d" data-chapterno="1">
  <h1>
   Understanding how food works is an essential first step to a healthier life.
  </h1>
  <div class="chapter__content">
   <p>
    We don’t need to understand how everything works in order to enjoy it. You can enjoy the photos on Instagram without knowing how the app functions. But food is different. Eating without understanding is a surefire recipe for a bigger waistline, more trips to the doctor and an early death. So it’s important to understand at least the basics – the macronutrients that we need in large quantities.
   </p>
   <p>
    First up are carbohydrates, sugar molecules that our body breaks down into glucose. Once it hits the bloodstream, glucose provides energy to the body, so we definitely need carbohydrates in our diet. But it’s far better to get this energy from
    <em>
     complex carbohydrates
    </em>
    like whole grains and fiber than from
    <em>
     simple carbohydrates
    </em>
    like white flour or refined sugar.
   </p>
   <p>
    Complex carbohydrates release glucose into the blood slowly, giving us sustained energy. Simple carbohydrates such as a sugary treat or white bread act quickly, providing an instant energy boost. But they are also linked to all sorts of problems, from diabetes and weight gain to impotence.
   </p>
   <p>
    Proteins are another source of energy, but their real purpose is to serve as building blocks. Made up of amino acids, they can combine together into structures that the cells in our body need to run properly. Everything from celery to ground beef contains protein, but proteins in animal cells contain different amino acids than those in plant cells. That’s why, if you are a vegetarian, you should strive to maintain a diverse vegetarian diet to get the full variety of amino acids that your body needs.
   </p>
   <p>
    The next key macronutrient is fat. Like carbohydrates, it’s a source of energy. But fat contains a lot more energy than carbohydrates; 2.25 times more, in fact. Fat is an essential component of our diets, but it’s important to know that you should concentrate on good fats –
    <em>
     unsaturated fats
    </em>
    from olives, avocados and nuts or fats like the omega-3 oil found in salmon
   </p>
   <p>
    In contrast, you should avoid
    <em>
     saturated fats
    </em>
    that are typically found in animal products such as cheese and butter. There’s considerable evidence that swapping out saturated for unsaturated fats in your diet reduces inflammation, risk of cancer and heart disease and even cognitive decline.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5c69f87f6cee0700079ef254" data-chapterno="2">
  <h1>
   Understanding the blood sugar problem and how your body stores fat can help you make better food choices.
  </h1>
  <div class="chapter__content">
   <p>
    Eat too much and you’ll get fat, right? Well, the science of obesity isn’t quite that simple.
   </p>
   <p>
    The first thing you need to understand is the
    <em>
     blood sugar problem
    </em>
    , meaning when you eat too many simple carbohydrates so that your blood sugar spikes suddenly. In response, your body generates a lot of glucose – so much, in fact, that it isn’t able to burn it all as fuel, so it releases a substance called insulin.
   </p>
   <p>
    Insulin’s job is to get glucose out of the blood and deliver it to muscle and fat cells, which absorb it. At the same time, insulin stops the body from using fat cells as fuel – there’s no need, after all, when there’s so much glucose available. Thanks to evolution, our bodies are more than happy to store
    <em>
     some
    </em>
    fat. After all, it could come in handy in a time of scarcity.
   </p>
   <p>
    But if you consume too many simple carbohydrates frequently, your body can develop insulin resistance. That means that insulin can’t get the glucose out of your blood like it should.
   </p>
   <p>
    The result?
   </p>
   <p>
    Well, there’s an accumulation of fat, which no longer needs to be burned for energy with so much sugar floating around. There’s also high blood sugar levels, meaning an increased risk of diabetes and a fatty liver. Obviously, this is bad news.
   </p>
   <p>
    When we eat is relevant here. The human body tends to get more insulin resistant as the day goes on. So a carbohydrate-heavy late night snack is a surefire way to raise your blood sugar and do damage to your body.
   </p>
   <p>
    The good news is that knowing all this also helps us understand how to lose weight.
   </p>
   <p>
    Sure, you should avoid eating too much of anything. Consuming fewer calories than you burn every day is the first step. But avoiding simple carbohydrates is particularly important because this will force your body to burn fat as fuel instead of glucose.
   </p>
   <p>
    And take timing into consideration too: your goal should be to eat healthier foods at healthier times. Think more salmon and broccoli at breakfast rather than fries at midnight. In the next blink, we’ll take a closer look at why
    <em>
     when
    </em>
    we eat matters so much.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5c69f8a16cee070007fe682e" data-chapterno="3">
  <h1>
   Your body’s circadian rhythm means it is naturally primed to eat at certain times.
  </h1>
  <div class="chapter__content">
   <p>
    Maybe you’ve used the term
    <em>
     circadian rhythm
    </em>
    in relation to sleeping or fertility, but chances are you’ve never applied it to deciding when to devour an omelet. Well, you should! Aligning your eating with the natural rhythm of your body gets your food working for you rather than against you.
   </p>
   <p>
    Your biological clock is constantly telling you what to do. It tells you when to sleep and when to eat on a natural 24-hour cycle – that’s the circadian rhythm. At night, for example, your biological clock encourages you to sleep by triggering a small drop in your body’s temperature and an increase in the chemical melatonin, which results in you feeling drowsy.
   </p>
   <p>
    And it sends you clear signals about when to eat too. That’s why you often feel hungry around the same times every day. All of us are able to resist our circadian rhythms. If you’ve ever partied all night, you’ve done that already. But you probably also know that you pay for it later. This is also true for people who oppose their circadian rhythm when it comes to eating.
   </p>
   <p>
    Studies, including one by the US Center for Disease Control, have looked at people who work nights or irregular hours. Unsurprisingly, they found that these people have sleep issues, but they also tend to put on more weight than those with a standard nine-to-five schedule. It seems that eating at unusual hours leads to greater weight gain. One study even found that nurses burned fewer calories performing their duties when they worked night shifts compared to day shifts.
   </p>
   <p>
    So it seems that our bodies are naturally primed to eat at certain regular times of day, and they don’t appreciate being stuffed with food late at night. What else do we know about our daily rhythm when it comes to eating?
   </p>
   <p>
    Well, we know that we’re more resistant to insulin as the day goes on. We’ve already seen that eating a lot of carbs causes a damaging blood sugar spike. What’s more,  if you eat the same carb-heavy meal in the morning and in the evening, the blood sugar spike will be even higher in the evening. Let’s explore this in more detail in the next blinks.
   </p>
  </div>
 </div>
 <div class="chapter supplement" data-chapterid="5c69f8a16cee070007fe682e" data-chapterno="3">
  <h1>
  </h1>
  <div class="chapter__content">
   <p>
    <em>
     “...major scientific breakthroughs have begun to prove that when it comes to nutrition, timing is everything.”
    </em>
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5c69f8c06cee0700079ef255" data-chapterno="4">
  <h1>
   Front-loading your eating and eating only while the sun’s up are great ways to be healthier.
  </h1>
  <div class="chapter__content">
   <p>
    All too often, we just don’t seem to have time for breakfast in the morning. We wake up, hit the snooze button a few times, check social media, shower and dash out the door. But all the evidence shows that eating more of our calories earlier in the day – ideally, at breakfast or lunch – is a great way to be healthier.
   </p>
   <p>
    A number of studies show just how damaging it is when we back-load our calories onto the end of the day. One looked at the eating habits of a group of overweight women and found that subjects who ate more at breakfast lost more weight over the course of the study than those who ate more late in the day. Eating more of the day’s calories at breakfast also led to reductions in glucose, insulin and
    <em>
     ghrelin
    </em>
    , a hormone related to hunger.
   </p>
   <p>
    These findings were mirrored by a study from the University of Turin, which tracked over 1200 people for six years and discovered that those who ate a larger proportion of their daily calories in the evening had a greater risk of obesity.
   </p>
   <p>
    It seems that eating early means you’re more likely to be healthier. So does eating only during the hours of sunlight. It’s no surprise that our bodies deal with food better during daylight hours – after all, early humans had to confine activities like eating to daylight hours since they had no electric lighting.
   </p>
   <p>
    The benefits of restricting food consumption to a specific period of time have been demonstrated in studies on both animals and humans. For example, when mice are fed around the clock, they gain weight and show signs of metabolic conditions such as elevated blood pressure. But when they consume the same number of calories during the eight-hour period when they’re most active, they don’t become obese. Human studies aren’t yet conclusive, but they are just as interesting. One, from the Salk Institute in California, showed that reducing people’s window for eating from 14 hours to 11 hours led to weight loss and improved sleep.
   </p>
   <p>
    Front-loading food consumption into the early part of the day and then tapering off as the day progresses brings eating into harmony with your natural rhythms, but how do you actually do this? Let’s find out in the next blink.
   </p>
  </div>
 </div>
 <div class="chapter supplement" data-chapterid="5c69f8c06cee0700079ef255" data-chapterno="4">
  <h1>
  </h1>
  <div class="chapter__content">
   <p>
    <em>
     “Stack your meals so that three quarters (or more) of what you eat comes before 2 p.m., the rest between then and sundown.”
    </em>
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5c69f8d36cee070007fe682f" data-chapterno="5">
  <h1>
   Eating dinner for breakfast and keeping your eating consistent add up to a better diet.
  </h1>
  <div class="chapter__content">
   <p>
    Maybe you’re thinking: but
    <em>
     how
    </em>
    can I make breakfast the biggest meal of the day? There’s only so much toast and granola I can eat!
   </p>
   <p>
    The answer to that conundrum lies in abandoning the assumptions you hold about food.
   </p>
   <p>
    Our culture reinforces the idea that certain foods are for certain times of the day. Toast is a breakfast food, while a black bean burger...well, it just isn’t. But why not? There’s absolutely no reason why you can’t eat a black bean burger for breakfast. They taste great, and they’ll fill you up for much longer than breakfasts heavy in simple carbs.
   </p>
   <p>
    So it’s time to start eating your dinner for breakfast. One practical trick for doing this is to make your usual dinner in the evening but save most of it for the next morning. One of the authors loves to eat salmon burgers with broccoli and quinoa, so at dinnertime, he makes four small burgers, eats one, and then saves the rest for breakfast.
   </p>
   <p>
    If that sounds like too big a change, start your new breakfast off slowly. Try sprinkling berries on Greek yogurt, which is full of healthy fat and rich in protein. Then at lunchtime, aim for a meal based around plants and whole grains. Dinner could be a simple piece of protein – like grilled chicken – with a salad.
   </p>
   <p>
    The final step toward optimizing your eating is to eat regularly and consistently. Unfortunately, our bodies love consistency while our brains crave novelty. That’s problematic because it means that our brains adore trying new and exciting meals, but all our bodies want is a steady, regular and efficient diet. One study in the
    <em>
     International Journal of Obesity
    </em>
    , for instance, showed that people whose energy intake varies from day to day are more likely to develop metabolic syndrome, a cluster of conditions that can lead to heart disease.
   </p>
   <p>
    The best way to keep the size and timing of your meals regular is to simply reduce the variety of what you eat. Settle on a few recipes that you and your body love and make these the only options for at least two of your regular meals and snacks. Once you’re in a regular cycle with these meals, the temptation to deviate and make bad food choices will decrease.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5c69f8f26cee0700079ef256" data-chapterno="6">
  <h1>
   A one-month plan is all you need to improve your health with better food habits.
  </h1>
  <div class="chapter__content">
   <p>
    All of us need a little guidance when trying something new. So here’s a practical guide to getting started with your new dietary habits.
   </p>
   <p>
    The key is to start slowly by purposefully shifting things over the course of a month.
   </p>
   <p>
    For the first few days of the month, record what you eat and when you eat it. Then estimate what proportion of your daily calories each meal provides. If you’re like most people, you’ll probably find that you eat 10 percent of your daily calories in the morning, 30 percent at lunchtime, and as much as 60 percent in the afternoon and evening.
   </p>
   <p>
    Once you’ve figured out your normal routine, you can start to change it. If you’re eating 50 percent of your day’s calories at dinner, for example, you’ll want to halve that. Start by dividing your dinner into four quarters and saving one of those quarters for breakfast or lunch the next day. Once you’re used to that, hold back another quarter. Soon, you’ll have shifted half of those dinnertime calories to the morning – that’s 25 percent of your daily total! The next step is to start eating dinner earlier so that you only eat when the sun is out.
   </p>
   <p>
    In the second half of the month, focus on
    <em>
     what
    </em>
    you’re eating. Ask yourself: How often am I eating processed foods? How many times a day do I snack on simple carbs? And how many servings of vegetables and nuts or seeds am I getting? Be honest with yourself, and soon you’ll have a clear picture of where you need to make changes.
   </p>
   <p>
    Now, for the rest of the month, you can focus on swapping out bad choices for healthier ones. Still chowing down on a white bagel for lunch? Swap it out for one made with whole grains and replace cream cheese with avocado and tomato. Still hooked on butter? Try picking up a few cans of artichoke hearts – blended with garlic and olive oil, they make a fabulous alternative.
   </p>
   <p>
    So that’s it. One month to change your habits. One month of thinking carefully about what you eat and when you eat it and you’ll be on the path to better health. And in fact, you’ll discover that simply being thoughtful about eating brings its own rewards.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5c69f9086cee070007fe6830" data-chapterno="7">
  <h1>
   Being mindful about your food and how you eat it will help you take control of your diet.
  </h1>
  <div class="chapter__content">
   <p>
    Eating is like breathing – we don’t think about it, we just do it.
   </p>
   <p>
    But that’s a problem for lots of us. We’ve lost touch with food as a sensory experience; we’re taking bite after bite without consideration for what we’re tasting and digesting. That means we’re losing out on a lot of the joy food has to offer. It also means that we’re unconsciously overeating, because we never stop to think about whether we actually need to keep going.
   </p>
   <p>
    The good news is that being more mindful about how you eat – eating slowly and consciously – is an effective way to take control of your relationship to food.
   </p>
   <p>
    A 2018 analysis in the
    <em>
     Journal of the International Association for the Study of Obesity
    </em>
    looked at 19 studies and found that mindful eating produced effective weight loss and improved dietary behaviors. Another study showed that people who ate mindfully lost significantly more weight than those who ate normally. That’s because carefully focusing on each bite of food made them likely to eat more slowly. This helped them avoid gaining weight because it allowed more time for them to understand they were full.
   </p>
   <p>
    In fact, after you’ve eaten enough food, there’s a twenty-minute lag before you realize you’re full. That’s why eating quickly is more likely to lead to overeating.
   </p>
   <p>
    So how can you embrace mindful eating? Well, there are some great, practical things you can do like putting down your fork after every bite, or chewing for a little longer than you might normally do.
   </p>
   <p>
    But perhaps the best introduction to the power of mindful eating is to eat a single raisin. Don’t just wolf it down. Place it on your tongue. Let it sit there a while. Feel its shape and its texture. Notice its taste. Roll it around your mouth a little, before you start to slowly chew. Focus on the sensation of your teeth against the raisin and the flavors as your teeth sink into it. It’s just a raisin, but you’ll be surprised at the full range of senses you can experience when you slowly, mindfully eat it.
   </p>
   <p>
    Try it today. You’ll start to realize that slowing down and controlling your eating doesn’t just make you healthier – it also opens up a whole new world of experiences. What’s not to like about that?
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5c69f91e6cee0700079ef257" data-chapterno="8">
  <h1>
   When the going gets tough, the tough get healthy alternatives to our classic crutch foods.
  </h1>
  <div class="chapter__content">
   <p>
    Now you’ve learned the thinking and theory behind improved eating behaviors. But while knowing the theory of something is all well and good, life sometimes gets in the way of our good intentions and commitments.
   </p>
   <p>
    That’s particularly true when times are tough. All of us know the feeling: you’re tired, stressed or just feeling a little low, and you turn to food for a boost. So how can you make sure that the next time you’re in a tough spot, you reach for the right food?
   </p>
   <p>
    When you’re tired, you often want a quick pick-me-up. We reach for a chocolate bar, a soft drink or a bag of candy. That’s because our body’s instinctive reaction is to crave a quick energy boost. But the high you get from simple carbs is followed by a low as your body crashes, leaving you even more fatigued than when you started.
   </p>
   <p>
    So the next time you’re running low on energy, go for a snack that combines healthy fats, protein and fiber, giving you a slow energy release. It’s one of the reasons avocado toast has become so popular at breakfast: it gives you some healthy fat and fiber at the beginning of the day.
   </p>
   <p>
    What about when you’re
    <em>
     hangry
    </em>
    ? That’s stressed out, angry and hungry all at once – a classic recipe for overeating. Whether your go-to food to calm down is a pint of ice cream or the satisfying crunch of a bowl of chips, most of us make bad food choices when we see red.
   </p>
   <p>
    But one food that will soothe your short-term hanger without causing long-term harm is roasted chickpeas. They’re a great snack to prepare in advance and keep on hand. Dry chickpeas on paper towels, put them on a baking sheet and sprinkle them with extra virgin olive oil and your favorite spices. The authors are partial to cayenne, garlic and rosemary. Roast them for 30 minutes at 425 degrees Fahrenheit and presto! An incredibly satisfying, tasty snack to chomp on. The best thing? Legumes like chickpeas get blood sugar and hunger under control better than animal protein.
   </p>
   <p>
    That solves the problem of unhealthy snacks, but there’s another common obstacle to maintaining a healthy diet: being on the move.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5c69f9376cee070007fe6831" data-chapterno="9">
  <h1>
   Being on the go or on vacation doesn’t have to be disastrous for your eating.
  </h1>
  <div class="chapter__content">
   <p>
    When you’re at home or at the office, it’s relatively easy to stick to healthy eating habits. But step outside your regular environment and things suddenly get a whole lot harder.
   </p>
   <p>
    Even just being on the go is a common reason for losing touch with your healthy eating routine. You’ve been dashing between appointments all day, you’ve only got minutes to spare before your next meeting and your stomach’s growling like a tiger. Grabbing a hot dog or a large soda from a convenience store is, well, convenient. But convenience foods are also a fast-track to diabetes. So what to do?
   </p>
   <p>
    Even if your only option is a convenience store, there are still some good options. Nuts are a great source of protein and healthy fats, and they’re easy to eat on the go. Just make sure you avoid salted or sugary versions. Also, check the cooler for small packages of hummus, and get pretzels for dipping - a fill of healthy fats and protein. No hummus? Grab a Greek yogurt that’s rich in protein.
   </p>
   <p>
    Necessity can lead us to bad food choices when we’re on the move. But when we lose control while on vacation, we’re usually doing it by choice. In vacation mode, it’s all too easy to let loose and abandon carefully thought-out eating habits. Of course, it’s OK to loosen up a little on holiday, but be careful you don’t overindulge and bring home a string of health issues along with your souvenirs.
   </p>
   <p>
    A great way to stay healthy on holiday is to split things up. A full two-thirds of Americans say that they always finish their entrées while eating out. That’s a problem, when portion sizes are as large as they are today. So try splitting an entrée with your travel companion, or simply ask for the meal to be split in two, with half boxed up before its even served. As you eat the boxed half for breakfast the next day, enjoy the feeling that you’ve not only saved money, but calories too.
   </p>
   <p>
    And try to keep a focus on the bigger picture. Sure, you want to sample a lobster roll in New England or try the pasta in Rome. But you probably aren’t just on holiday for the food. So put your focus on exploring rather than just on restaurants. Enjoy your surroundings!
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5c69f94f6cee0700079ef258" data-chapterno="10">
  <h1>
   Protecting your heart and reducing the risk of cancer is easy with the right diet choices.
  </h1>
  <div class="chapter__content">
   <p>
    We all want to lead a long, happy life. But we don’t always behave like we do. So ask yourself, are you eating to protect your future health? Or is your diet damaging it?
   </p>
   <p>
    According to British charity Cancer Research UK, lifestyle changes like maintaining an ideal weight, eating well, reducing alcohol intake, avoiding smoking and staying active can prevent a full four in ten cancers.
   </p>
   <p>
    And for people who already have cancer, a combination of good diet and regular physical activity can have a big impact. Research from 2007 showed that people with breast cancer could reduce their risk of death by 50 percent over ten years if they ate five or more servings of fruit and vegetables every day and did 30 minutes of exercise six times a week.
   </p>
   <p>
    So what’s the best way to build a cancer-fighting diet? Well, raw or lightly cooked vegetables are at the top of the list. Cruciferous veggies like broccoli, cabbage and arugula are particularly powerful – one study showed that women eating them the most had a breast cancer risk 50 percent lower than women eating them the least. But avoid red and processed meats – these contain nitrates, chemicals that, when eaten, can generate carcinogens. Swap them for white meat and fish instead.
   </p>
   <p>
    Just as with cancer, all of us need to take the risk of heart disease seriously. In the United States, after all, heart disease and stroke are the leading causes of death. And there’s a close connection to poor diet, because eating fatty foods can damage the arteries which deliver blood to the heart. In the worst cases, blocked arteries can lead to heart attacks.
   </p>
   <p>
    So protect those all-important arteries. Avoid foods which contain sugar and saturated fats such as red meat, egg yolks and dairy. Focus instead on a Mediterranean-style diet, in which most of the calories come from plants, plant-based fats like avocados and olive oil, as well as some oily fish like trout or salmon. A study in the
    <em>
     New England Journal of Medicine
    </em>
    found that the Mediterranean diet reduced the risk of heart attacks and related cardiovascular diseases by as much as 30 percent.
   </p>
   <p>
    Sure, you might need to give up frequent plates of fried steak and eggs. But is that such a high price to pay for a long, healthy life, really?
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5c69f9686cee070007fe6832" data-chapterno="11">
  <h1>
   Final summary
  </h1>
  <div class="chapter__content">
   <p>
    The key message in these blinks:
   </p>
   <p>
    <strong>
     If you want to eat healthily, then you need to think not just about what you eat, but when you eat it. A healthy diet is one based around whole foods, healthy fats, and plant- and sea-based proteins. And it’s one in which 75 percent of your calories are consumed at breakfast and lunch.
    </strong>
   </p>
   <p>
    Actionable advice:
   </p>
   <p>
    <strong>
     Whip up a healthy, stealthy dessert to hold off sweet cravings.
    </strong>
   </p>
   <p>
    For many of us, desserts are our downfall. We might eat perfectly well all day long, but then after an evening meal, our sweet tooth gets the better of us. Before we know it, we’re devouring a tub of cookie dough ice cream! If that sounds familiar, try whipping up a healthy snack stealthily disguised as an indulgent sweet. Mix up Greek yogurt with almond butter. Add a little cocoa powder, and a spoonful of dark chocolate. The result? A healthy, luxurious tasting dessert with just a hint of sweetness. This way you can take the edge off your craving while still staying on track for a healthy day.
   </p>
   <p>
    <strong>
     Got feedback?
    </strong>
   </p>
   <p>
    We’d sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
   </p>
   <p>
    <strong>
     What to read next:
    </strong>
    <strong>
     <em>
      How Not to Die
     </em>
    </strong>
    <strong>
     , by Michael Greger, MD, and Gene Stone.
    </strong>
   </p>
   <p>
    As you’ve just discovered, if you want to stay healthy, it’s time to ditch the red meat, fries and sugary drinks in favor of avocados, chickpeas and trout. But Dr. Roizen and Dr. Crupain aren’t the only experts who recommend a focus on plant-based foods if you want to stay trim and lead a healthy life.
   </p>
   <p>
    In
    <em>
     How Not to Die
    </em>
    (2015), Michael Greger, MD and Gene Stone explain how a plant-based diet can not only extend your life, but also transform your quality of living. By following the advice of Dr Greger, an internationally recognised physician and nutritional expert, you can lead a happy, healthy and long life.
   </p>
  </div>
 </div>
</article>
