---
layout: post
title: "John C. Maxwell - Leadershift"
description: "Leadershift (2019) helps leaders and would-be leaders develop the ability and desire to make the changes in their leadership that will positively boost not just their growth, but that of their organizations. It shows how, with the right mind-set and the most up-to-date thinking, we can all achieve great things as leaders."
image: https://images.blinkist.com/images/books/5ca27bc56cee0700079f2c3b/1_1/470.jpg
---

<article class="shared__reader__blink reader__container__content" lang="en">
 <div class="chapter chapter" data-chapterid="5ca27bdb6cee070007a590d0" data-chapterno="0">
  <h1>
   What’s in it for me? Discover the secrets of true leadership in today’s world.
  </h1>
  <div class="chapter__content">
   <p>
    The world is changing more rapidly than ever. This means two things for leadership. First, it means that leaders are needed more than ever. Sure, in times of stability we can survive with just managers. But when facing the unknown, we need strong leaders to guide us through uncertainty. Second, a changing world means that leaders themselves need to stay agile.
   </p>
   <p>
    Today’s secret to enduring success as a leader is the ability to be nimble and agile, and to
    <em>
     leadershift
    </em>
    : to make leadership changes that will boost not just the leader’s growth, but that of his or her organization.
   </p>
   <p>
    These blinks will take you through the personal leadershifts the author, John C. Maxwell, has made throughout his career that have helped him make a positive impact on his organizations and on the world.
   </p>
   <p>
    In these blinks, you’ll learn
   </p>
   <ul>
    <li>
     what dancing the tango has to do with leadership;
    </li>
    <li>
     why focusing on growth is more important than hitting your goals; and
    </li>
    <li>
     how a modest farmer called Claude helped Maxwell understand influence.
    </li>
   </ul>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5ca27bfa6cee0700079f2c3c" data-chapterno="1">
  <h1>
   Great leaders bring out the best in everyone, rather than seeking to shine themselves.
  </h1>
  <div class="chapter__content">
   <p>
    Too many would-be leaders, like Maxwell himself early in his career, think mostly of themselves. Focused on their own goals and aspirations, they don’t realize that being a leader is not about what you can do for yourself; it’s about what you can do for others.
   </p>
   <p>
    To be a true leader, you need to make the mind-set shift from
    <em>
     me
    </em>
    to
    <em>
     we
    </em>
    .
   </p>
   <p>
    A great way to think of this is in terms of an orchestra. Many would-be leaders are stuck in the mind-set of the soloist – the elite performer whom everyone else serves. True leaders behave more like the conductor, focusing on how they can help everyone else produce a great result. After all, as South Korean cellist-turned-conductor Han-Na Chang says, conducting offers the opportunity to draw limitless potential from an entire group of people.
   </p>
   <p>
    Key to making the shift from soloist to conductor is ensuring that you truly understand the people around you. One evening in Buenos Aires, Argentina, where Maxwell was speaking on leadership, he was invited to watch two hundred people dance the tango, in perfect rhythm and harmony.
   </p>
   <p>
    His host explained that the secret to the exquisite perfection on show was for each dance pair’s leader truly to understand his partner; to be able to lead effectively, you need to know what it’s like to be led. Well, the trust, cooperation and mutual understanding required to make the tango look beautiful are just as important in the world of work.
   </p>
   <p>
    So just like any good conductor or tango lead, you should be focused on helping others to shine. You can do this by making sure you have the right attitude toward your people and your relationship to them. Center your leadership around their needs. Listen to them first, before you expect them to listen to you. Work out what your people do well, and compliment them on it. If you set out a big picture for your organization, make sure they are in it. And don’t just tell them your vision – invite them to help you achieve it.
   </p>
   <p>
    These actions are simple. But they’ll only work if they’re backed with an intentionality that says, “My focus is to help others shine.” And none of this is to say that you shouldn’t focus on your own growth. You just need to do that in the right way. Let’s take a look at what that right way is.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5ca27c0e6cee070007a590d1" data-chapterno="2">
  <h1>
   Developing a growth mind-set is a better approach than focusing on hitting simple goals.
  </h1>
  <div class="chapter__content">
   <p>
    When Maxwell was a young church leader in a small town in Ohio, he set himself a goal of making his church the largest in the state. He doubled the size of his congregation in a single year. And by 1975, his church was recognized as the fastest-growing in Ohio.
   </p>
   <p>
    But after celebrating, he started to reflect on his achievements. And what he realized was that the personal growth he had experienced along the way – his experience and understanding of leadership – was much less important than hitting his numerical goal of growing church membership. That led to another key leadershift: from a
    <em>
     goal
    </em>
    mindset to a
    <em>
     growth
    </em>
    mind-set.
   </p>
   <p>
    A goal mind-set emphasizes achievement and status, whereas a growth mind-set values development and stretching oneself. A goal mind-set prioritizes hitting a target and asking how long it will it take to get there, whereas growth mind-sets simply ask, “How far can I take this?”
   </p>
   <p>
    If you do adopt a growth mind-set, you may be surprised by the results you achieve. When Maxwell was younger, he met Elmer Towns, a Liberty University professor and someone he admired. He discovered that Towns had sold 110,000 books, and decided that selling that many copies of his own books would be his goal.
   </p>
   <p>
    But in time, his shift from goals to growth occurred, and he focused instead on simply becoming a better writer, and not worrying about sales. Years later, his publisher presented him with a crystal trophy engraved with the words “one million books sold.” It turned out that by focusing on growth, he had achieved far more than he would ever have set for himself as a goal.
   </p>
   <p>
    The key to embracing a growth mind-set is to have a
    <em>
     teachable spirit
    </em>
    . That means not just saying you want to learn, but taking practical steps to do so. Think of it like gardening: just wanting your garden to grow into something beautiful won’t achieve anything. You need to plan, prepare and work at cultivating it. So each day, recognize that opportunities to learn and grow are there, whatever you are doing and whoever you are with. Stay curious, and be intentional about learning.
   </p>
   <p>
    And make sure you are surrounded by other people with a growth mind-set. Elmer Towns, who became a mentor to Maxwell taught him something he called the
    <em>
     hot-poker principle
    </em>
    . Likening people with a growth mind-set to fire, he’d say that if you can keep your poker near the fire, it stays hot. Take it away, and after a while, it grows cold.
   </p>
   <p>
    Stay close to the fire.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5ca27c206cee0700079f2c3d" data-chapterno="3">
  <h1>
   Great leaders don’t simply climb up the ladder; through mentoring, they build ladders for others to climb.
  </h1>
  <div class="chapter__content">
   <p>
    You probably know one or two supposed leaders who are self-focused. The question at the forefront of their minds is, “How high up the ladder can I climb?” True leaders, however, make the shift from climbing up the ladder themselves to thinking about how they can build ladders for others.
   </p>
   <p>
    Now, it’s certainly true that climbing the ladder successfully yourself is a prerequisite if you want to help others do the same. A good rule of thumb, according to Maxwell, is to aim to be in the top ten percent of your chosen field. That’s the magic zone in which you’ll stand apart from the rest. Get to that top ten percent, and you can safely assume that you have a lot to offer others.
   </p>
   <p>
    And that’s the best way to look at your success – as a great resource to use to help others. As Kevin Myers, leader of 12Stone, a Wesleyan church based in the US state of Georgia, says, true leaders want more
    <em>
     for
    </em>
    their people than they want
    <em>
     from
    </em>
    them.
   </p>
   <p>
    So how can you build those ladders for others to climb? Well, if you’ve hit that magic top ten percent, then think about mentoring.
   </p>
   <p>
    The first thing to do is decide whom to mentor. Think carefully. Time is limited, so if you can only invest in one or two people, they’d better be the right ones. To ensure you choose wisely, ask yourself a few key questions.
   </p>
   <p>
    First, are these people just hopeful, or are they truly hungry for knowledge and learning? There are plenty of people in the world who hope for better things, but only a few who are hungry for it, who don’t simply say, “There should be a way,” but rather, “I’ll find a way.” Invest in these people.
   </p>
   <p>
    Second, ask yourself whether your candidate has true leadership potential. That’s because a leader will influence many other people. So investing in shaping their future has a wider impact than if you mentored a follower.
   </p>
   <p>
    Once you’ve selected your mentee, what should you offer that person? Well, any good mentor offers bite-sized truths distilled from the complexity of life, and options and considerations for the future.
   </p>
   <p>
    But as we’ll see, it isn’t enough for a leader simply to tell people what to do.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5ca27c3a6cee070007a590d2" data-chapterno="4">
  <h1>
   Connecting with people achieves better results than simply directing them.
  </h1>
  <div class="chapter__content">
   <p>
    Maxwell witnessed the benefits of leadershifting from directing people to connecting with them when he visited the locker room of the University of Tennessee Lady Volunteers basketball team, coached by Pat Summitt, a legendarily successful coach.
   </p>
   <p>
    When the players came in at half time, instead of talking to Summitt straight away, they huddled around a whiteboard on which were written three questions:
    <em>
     What did we do right?
    </em>
    <em>
     What did we do wrong?
    </em>
    and
    <em>
     What should we change?
    </em>
    Only after the team had discussed and agreed upon its answers did Summitt come and talk to them. She heard them out, reflected on their answers and made a few observations before sending them out onto the court for the second half of the game.
   </p>
   <p>
    Too many leaders lead based on assumptions, Summit told Maxwell. Instead of assuming where her players were mentally and simply telling them what to do, Summit preferred truly to understand them by asking questions and listening to them.
   </p>
   <p>
    Summit had made a leadershift to leadership based on connection, not direction – leadership that embraced collaboration rather than authority, and listening rather than talking. If you want to take the same step, you need to do the same. So learn to listen well.
   </p>
   <p>
    Maxwell spends a great deal of time in meetings, and if you are a leader, you probably do, too. So try out what Maxwell does. Every time you meet with someone, take out a pad to write notes. And at the top of that pad, write a big L, standing for “Listen.” That will act as a reminder that when you meet with people as a leader, your job is less to talk than to listen.
   </p>
   <p>
    If you really want to improve your listening, tackle it with your growth mind-set. Be brave enough to ask your colleagues – or friends or family members – how good a listener you are, on a one-to-ten scale. Pay attention to their answers, and act on them. And ask them to let you know any time they feel you aren’t listening to them in the future.
   </p>
   <p>
    Leading with connection is better for everybody. It leads to better relationships, better communication and a two-way flow of ideas. Embrace this leadershift, and you’ll soon start to see results flow.
   </p>
   <p>
    Now let’s consider another attitude shift: how to embrace diversity.
   </p>
  </div>
 </div>
 <div class="chapter supplement" data-chapterid="5ca27c3a6cee070007a590d2" data-chapterno="4">
  <h1>
  </h1>
  <div class="chapter__content">
   <p>
    <em>
     “When you interact with others as a leader, what is your mind-set? Is your intention to correct them or connect with them?”
    </em>
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5ca27c4e6cee0700079f2c3e" data-chapterno="5">
  <h1>
   Making the shift to valuing diversity will bring greater value and richness to your leadership and life.
  </h1>
  <div class="chapter__content">
   <p>
    When Maxwell was a young man, in Circleville, Ohio, there was absolutely nothing diverse about his environment. His community was almost completely white. The leaders in his protestant church were white men. They looked alike, and they acted alike. As a young pastor, conformity to tradition was drilled into him.
   </p>
   <p>
    But over time, he came to realize that the most important lessons and insights often come from outside your own group. He learned this from his first discussions with a Catholic priest, who reinforced Maxwell’s faith, despite coming from a different denomination; and, later in life, from his move to Atlanta, a city rich in African-American culture and far removed from Circleville.
   </p>
   <p>
    Similarly, teams are more effective and valuable when they embrace diversity, because diversity brings insight and perspective that can fill the
    <em>
     knowledge gap
    </em>
    . As a leader, you can’t know everything. You rely on your team to fill in those gaps, and it can do so most effectively when it brings different perspectives to the table.
   </p>
   <p>
    If you need convincing, why not look to some of history’s great leaders? Abraham Lincoln built his cabinet from a very diverse group for that time. Its members were sworn political rivals, not allies. But the tough demands of the civil war required the skills of the best thinkers and diversity of perspective, not homogenous group thinking, if the Union was to be a success. Or look to Winston Churchill, Great Britain’s masterful wartime leader, who brought the leader of the opposition, Clement Attlee, into his cabinet and making Attlee instrumental in strategy meetings in his underground London bunker.
   </p>
   <p>
    If you’d like to bring more diversity into your life, first take a critical look at your friendship and professional circles. If you are like most Americans, they likely reflect your age, social background and skin color. Cheryl Moses, founder of Urban MediaMakers, an organization in Atlanta dedicated to promoting diversity in the media-arts industry, was so struck by reading a study that stated that 75 percent of white Americans had no non-white friends that she decided to host an event called “Come Meet a Black Person,” focused on starting more diverse conversations and connections.
   </p>
   <p>
    So if looking at your friends and contacts is a little too much like looking in the mirror, make an effort to get to know and to learn from people of different races, ages and political persuasions from you. You may be surprised by how refreshing it is to be surrounded by people with new, interesting and different ways of thinking from yours.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5ca27c646cee070007a590d3" data-chapterno="6">
  <h1>
   Embracing moral authority rather than positional authority is the pathway to great leadership.
  </h1>
  <div class="chapter__content">
   <p>
    As Maxwell often says, leadership
    <em>
     is
    </em>
    influence. But where does a leader’s influence come from?
   </p>
   <p>
    Well, in his first weeks as pastor of a small church in Indiana, fresh out of college, Maxwell learned that influence doesn’t come from the position you hold. By the rules of the church, he was its leader. But as his first meeting of the church’s board kicked off, a respected farmer and church member, Claude, took command. Claude asked Maxwell to open the meeting with a prayer, and Maxwell then said almost nothing until Claude politely asked him for a closing prayer at the end of the meeting.
   </p>
   <p>
    It was a tough, early lesson that titles do not equate to leadership. So Maxwell started thinking about what gave Claude influence. He wasn’t particularly rich, well-educated or impressive in any obvious way. But he had
    <em>
     moral authority
    </em>
    as a good, honest, fair and hardworking man who had lived these values with consistency for decades. Claude would never have called himself a leader, but he was. Every inch.
   </p>
   <p>
    But what are the qualities inherent in moral authority? Well, integrity – the ability to align your actions with your words and live your values consistently – is important. Integrity makes you dependable and trustworthy – followable, in other words – in the eyes of your team because they know that you will do what you say you will, and that your actions are rooted in strong moral values. When we look at a moral hero like the late Nelson Mandela, we see someone we can rely on to behave in a way that is in line with his values, time and time again.
   </p>
   <p>
    Also important is courage. That’s because acting with courage allows not just you but those around you to achieve their full potential as well.
   </p>
   <p>
    You probably know the biblical story of the Israelite David and his defeat of the Philistine giant Goliath. What you may not know is that while David stepped out to meet the giant, the rest of the army of Israel, including its king, was hiding in fear. But David’s heroic courage inspired his fellow Israelites to find their own. With the giant slain, they stepped out of their fear and defeated the Philistines.
   </p>
   <p>
    Combine courage with integrity, and you’ll be a leader people will be happy to follow, whatever the destination.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5ca27c796cee0700079f2c3f" data-chapterno="7">
  <h1>
   The best lives have meaning, so embrace the leadershift from career to calling.
  </h1>
  <div class="chapter__content">
   <p>
    According to a famous proverb, in the Middle Ages, a passerby came across three craftsmen at work and paused to ask them what they were doing. The first said that he was laying stones, the second that he was building a wall. The third? He replied, with a sense of pride, that he was creating a magnificent cathedral.
   </p>
   <p>
    Some people do a job. Others have careers. And still, others are lucky enough to have found their callings in life. The members of this last group are the lucky ones, who have found something greater than themselves to pursue. As Presbyterian author and minister Frederick Buechner wrote in his 1973 book
    <em>
     Wishful Thinking
    </em>
    , they have found a place where their deep gladness intersects with one of the world’s deep needs.
   </p>
   <p>
    Wouldn’t you too like to discover a calling and a richer way forward in life, with a clear reason and purpose for living? Well, there’s no reason why you can’t. If you understand what a
    <em>
     calling
    </em>
    is, you will be well-placed to find your own.
   </p>
   <p>
    The first thing to know about a calling is that it matches who you are. No one ever got called to something not suited to them. So ask yourself: Is there one thing that you could do for hours on end, that you would happily do for the rest of your life, and that can make a positive difference to others? If so, that might just be your calling.
   </p>
   <p>
    The second thing is that your calling will be something you are passionate about. Maxwell was a fan of the advice of Harold Thurman, the famous African-American philosopher and civil rights leader. Thurman advised people not to ask what the world needs, but to ask what makes them come alive. Because what the world needs is people who are truly alive.
   </p>
   <p>
    But remember that a calling isn’t just about you. The significance of a life dominated by a calling, and not just a career or a job, comes from giving, thinking and serving beyond yourself.
   </p>
   <p>
    Live without a calling, and you are likely to feel a nagging anxiety that your life has not achieved its true meaning. Find it, and everything changes. Nothing else in life is as satisfying. Why do you think so many celebrities champion causes? They are pursuing the richness of life that can only come from a calling. Make the leadershift from career to calling, and a fulfilled life lies ahead of you.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5ca27c8c6cee070007a590d4" data-chapterno="8">
  <h1>
   Final summary
  </h1>
  <div class="chapter__content">
   <p>
    The key message in these blinks:
   </p>
   <p>
    <strong>
     The best leaders change and adapt. They
    </strong>
    <strong>
     <em>
      leadershift
     </em>
    </strong>
    <strong>
     – that is, they make leadership changes that boost both their own and their organization’s growth. They focus not just on directing but on connecting with their teams, and they pursue not just short-term goals and career moves, but deep personal growth, moral authority and their true calling in life.
    </strong>
   </p>
   <p>
    Actionable advice:
   </p>
   <p>
    <strong>
     File away everything you learn.
    </strong>
   </p>
   <p>
    To maintain your personal growth and learning, file away whatever you learn. Whenever Maxwell reads an article of interest, he clips it and files it away, arranged by theme. The same goes for quotations. Do the same, and you’ll always have access to the knowledge you’ve acquired and easily be able to reinforce your learning. You could even write a note about these blinks and file them away right now.
   </p>
   <p>
    <strong>
     Got feedback?
    </strong>
   </p>
   <p>
    We’d sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
   </p>
   <p>
    <strong>
     What to read next:
     <em>
      5 Levels of Leadership
     </em>
     , by John C. Maxwell
    </strong>
   </p>
   <p>
    Now that you’ve heard the latest thoughts of the internationally renowned leadership expert John C. Maxwell, why not take a look at one of his classic works?
    <em>
     5 Levels of Leadership
    </em>
    is a clear and engaging step-by-step guide to becoming not just a leader, but a leader with a lasting influence.
   </p>
   <p>
    Drawing on real-life anecdotes from Maxwell’s prolific career and inspiring quotations from other leaders, these blinks set out the key barriers that hold people back, and explore how you can overcome them.
   </p>
  </div>
 </div>
</article>
