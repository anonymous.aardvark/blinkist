---
layout: post
title: "Lisa Damour - Under Pressure"
description: "Under Pressure (2019) explores the particular challenges that school-aged girls face throughout their education, at home and in society at large. Drawing on her wealth of experience as a clinical psychologist, Lisa Damour explains how parents, teachers and mentors can help girls overcome the stress and anxiety that disproportionately affects young women."
image: https://images.blinkist.com/images/books/5ca337916cee070007a59384/1_1/470.jpg
---

<article class="shared__reader__blink reader__container__content" lang="en">
 <div class="chapter chapter" data-chapterid="5ca338126cee070007a59386" data-chapterno="0">
  <h1>
   What’s in it for me? Welcome to girl world.
  </h1>
  <div class="chapter__content">
   <p>
    It’s not always easy being a girl. Girls are subject to all kinds of pressures that their male counterparts aren’t, be it at home, at school or in a wider cultural context. And this shows. Rates of stress and anxiety among girls have risen sharply in recent years, leaving parents, teachers and mentors wondering what they can do to address this mental health crisis among young women.
   </p>
   <p>
    Fortunately, as a Clinical Psychologist specializing in children’s development, Lisa Damour has plenty of advice for helping girls become resilient, confident and self-assured. Looking at all aspects of the girl world, including school, societal expectations, friendship and interactions with boys, you’ll discover some of the most common pressures that girls face. You’ll also learn what to say and do to help them cope with the challenges they face.
   </p>
   <p>
    In these blinks, you’ll discover
   </p>
   <ul>
    <li>
     why girls and boys interpret their report cards differently;
    </li>
    <li>
     how our culture’s expectations of girls contribute to their anxiety; and
    </li>
    <li>
     what to say to a girl who is experiencing sexual harassment.
    </li>
   </ul>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5ca338306cee0700079f2f43" data-chapterno="1">
  <h1>
   Depending on the circumstances, stress and anxiety are not always unhealthy.
  </h1>
  <div class="chapter__content">
   <p>
    Being a girl can be tough. Even in the twenty-first century, when many of the legal, social and political barriers to female advancement are being removed, girls are still under pressures that their male counterparts aren’t. And these pressures take their toll. Over the last few years, stress and anxiety among girls have skyrocketed. But before moving on to the real and damaging pressures that girls face, first, it’s crucial to understand that a little stress is not always a bad thing. In fact, it can be helpful.
   </p>
   <p>
    For instance, psychological research has found that the stress caused by pushing ourselves outside of our comfort zone can result in personal growth. We experience helpful stress when we undertake unfamiliar challenges, like speaking in front of a large group of people. These sorts of challenges help us build resilience so we can face future hardships with strength.
   </p>
   <p>
    Similarly, experiencing some anxiety from time to time, characterized by feelings of dread, panic and fear, is not necessarily a cause for concern.
   </p>
   <p>
    Why is this? Anxiety lets us know that something’s wrong. Sometimes this is an appropriate reaction. For instance, if a teenager expresses anxiety at the thought of an important upcoming exam, it may be because they haven’t revised enough yet. Ultimately, though, there is a tipping point at which stress and anxiety become unhealthy.
   </p>
   <p>
    This is when anxiety negatively impacts one’s mental wellbeing. Importantly, this impact is determined less by the actual source of stress, and more determined by whether or not the individual has adequate emotional, financial or social resources available to tackle the issue.
   </p>
   <p>
    For instance, breaking an arm could be a resilience-boosting source of stress for a girl who has friends to take notes in class for her. But what about a girl whose university hopes are riding on an athletic scholarship? Chances are, that same broken arm could severely interfere with her mental health.
   </p>
   <p>
    When it comes to anxiety, we can distinguish between healthy and unhealthy levels by noticing if our alarm system is going off far too frequently. If this happens, we find ourselves constantly feeling panicked and fearful, and our thoughts race from one anxious thought to the next – will my teacher make me look stupid in front of my classmates? Will my ride home leave without me?
   </p>
   <p>
    When our anxieties go into overdrive, it interferes with our sleep, happiness and concentration, and quickly becomes unhealthy.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5ca338506cee070007a59387" data-chapterno="2">
  <h1>
   Lessen girls’ anxiety by helping them approach, rather than avoid, their problems.
  </h1>
  <div class="chapter__content">
   <p>
    As an educational psychologist, Dr. Damour often sees girls in the midst of a meltdown. One day, a teenage girl named Jamie came to see her in a flood of tears. She was worried she would fail an upcoming chemistry test and was desperate to find a way to avoid taking it. Though Dr. Damour empathized with the girl’s stress, she didn’t help her get out of the exam. This is because avoidance only makes anxiety worse.
   </p>
   <p>
    What would have happened if Dr. Damour helped Jamie avoid that chemistry test? At first, it might have seemed like the right course of action – Jamie would have instantly felt an overwhelming sense of relief.
   </p>
   <p>
    But Jamie would have to take the test eventually, and thus, her extreme anxiety would quickly return. And she wouldn’t have learned a valuable lesson – that failing a test isn’t always such a terrible thing. After all, this exam was not particularly important.
   </p>
   <p>
    Additionally, had Jamie dodged it that day, she would have continued to feel that difficult tests were something to be afraid of, and she may have continued to avoid them in the future. When we help our teenage girls avoid something simply because it makes them anxious, they may eventually develop a phobia.
   </p>
   <p>
    For instance, consider a girl who is somewhat scared of dogs. Whenever she sees one, she crosses the street and instantly feels relieved. But in doing this, she never has the opportunity to meet a friendly dog. Thus, she continues to associate avoiding dogs with relief and seeing them with fear.
   </p>
   <p>
    So, how did Dr. Damour help Jamie that day?
   </p>
   <p>
    Quite simply, she helped her
    <em>
     approach
    </em>
    her fear rather than
    <em>
     run away
    </em>
    from it. She advised her to ask her teacher for more help in the hour before the exam and to look at online video tutorials on the subjects she wasn’t clear on. The result? A few days later, Dr. Damour caught up with a now-calm and collected Jamie, who reported that the test still hadn’t gone that well, but she now felt better after having taken it.
   </p>
   <p>
    Help your teenage girl take small steps towards the thing that scares them. They may succeed or fail, but either way, they’ll learn not to be afraid.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5ca338856cee0700079f2f44" data-chapterno="3">
  <h1>
   A shy and cautious girl does not necessarily have an anxiety problem.
  </h1>
  <div class="chapter__content">
   <p>
    As a psychologist, Dr. Damour often sees parents who are worried about their children’s social skills and ability to make friends. 10-year-old Alina’s parents had these concerns. Alina was socially anxious, they said, had only a couple of friends and had been wary of unfamiliar people ever since she was an infant. What could they do, they asked, to make her more socially outgoing? To their surprise, Dr. Damour didn’t see Alina’s behavior as a problem to be solved.
   </p>
   <p>
    In recent years, Dr. Damour has seen an increasing number of clients, like Alina’s parents, who are keen to label their children ‘socially anxious.’ In reality, many of these children are just naturally shy.
   </p>
   <p>
    Research has established that children have different personalities, even as babies. Specifically, there are three infant personality types: easy infants, who are easygoing and accepting of change and unfamiliar people; difficult infants, who are irritable and hate change; and slow-to-warm-up infants, who are generally cautious and slow to adapt to change. Crucially, research also shows that babies with any of the three personality types have the potential to grow into balanced, secure adults.
   </p>
   <p>
    In Dr. Damour’s opinion, Alina was merely a child who was slow to warm up. This sort of child is likely to meet any change or a new person with wariness or shyness at first. However, as Dr. Damour pointed out to her parents, this was not necessarily a bad thing. Instead of constantly comparing Alina to her more confident and sociable brother, she encouraged her parents to reassure their daughter that it was alright not to rush straight into new experiences or relationships.
   </p>
   <p>
    If you have a daughter who seems anxious about socializing or unfamiliar situations, it can be useful to remember that her first reaction to social situations may not be her final one.
   </p>
   <p>
    For instance, a naturally shy child who receives an invitation to a birthday party might initially tell you they don’t want to go. In this case, calmly acknowledge his reaction, without judgment, and then suggest that they see how they feel in a few days. For children like Alina who are slow-to-warm-up and cautious, their second reaction, after they have had some time to think about it, may well be more positive.
   </p>
  </div>
 </div>
 <div class="chapter supplement" data-chapterid="5ca338856cee0700079f2f44" data-chapterno="3">
  <h1>
  </h1>
  <div class="chapter__content">
   <p>
    “
    <em>
     Having parents who will work with, not against, their inborn traits, is crucial in helping children to adapt and thrive.”
    </em>
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5ca338a86cee070007a5938d" data-chapterno="4">
  <h1>
   Despite outperforming their male counterparts, girls worry more about academic achievement.
  </h1>
  <div class="chapter__content">
   <p>
    When it comes to education in America, girls are bounding ahead of their male peers. At school, girls outperform boys in all subjects. When they get to university, they outnumber their male peers, and they also graduate at a higher rate and earn more advanced degrees than men. Unfortunately, this academic excellence comes at a cost for young American women and girls. Indeed, girls, far more than boys, report that school stresses them out.
   </p>
   <p>
    So, how can we help alleviate some of the pressure girls feel in the classroom? First, we need to understand a key difference between male and female children. Namely, that girls worry far more about their academic achievement than boys do.
   </p>
   <p>
    Research has found that girls place more emphasis on the feedback they receive from their teachers than boys do. Girls tend to see their grades as an all-important signal about what they are, or are not, capable of achieving. In other words, they believe that their grades are a reflection of their abilities. Predictably, this tendency often leaves girls feeling hopelessly deflated when they receive a bad grade. Why? Because they interpret it as a damning indictment of their academic abilities.
   </p>
   <p>
    In contrast, research finds that boys are more confident in their attitudes toward schoolwork. They are less bothered by bad feedback from teachers and are more likely to attribute a poor grade to their lack of effort, rather than a lack of ability.
   </p>
   <p>
    To help a girl overcome her assumption that poor or mediocre grades are an indication of her innate abilities, try to persuade her that she has the power to improve her performance next time. Explain that an exam, or a piece of homework, only tests how well she understands the material at that particular moment in time. If she works hard and improves her knowledge of the subject, she can improve her performance next time.
   </p>
   <p>
    Sending the message that she can improve her academic achievement herself is crucial. Research has found that pupils who believe their skills can be developed through hard work tend to worry less about school than pupils who believe that their grades reflect their true abilities and cannot be changed.
   </p>
  </div>
 </div>
 <div class="chapter supplement" data-chapterid="5ca338a86cee070007a5938d" data-chapterno="4">
  <h1>
  </h1>
  <div class="chapter__content">
   <p>
    <em>
     “Research shows that girls are more disciplined about their schoolwork than boys, which explains why they get better grades.”
    </em>
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5ca338c16cee0700079f2f45" data-chapterno="5">
  <h1>
   Girls face widespread sexual harassment and often blame themselves.
  </h1>
  <div class="chapter__content">
   <p>
    As well as meeting with girls one-on-one, Dr. Damour also meets with groups of girls to discuss issues that they collectively face. In a recent conversation with a group of 15- to 16-year-old girls, the subject of the #metoo movement came up, that is, sexual abuse committed by those in power. Dr. Damour then asked whether any of the girls had experienced harassment themselves. The response she received was shocking, if not at all surprising.
   </p>
   <p>
    Research by the American Association of University Women has found that almost fifty percent of all American girls in middle or high school have experienced unwanted touching or groping at school. Although the teenage girls that Dr. Damour spoke to attended an all-girls school, they still experienced harassment in their daily lives.
   </p>
   <p>
    Many of the girls reported that the boys they knew grabbed their butts, pulled their bra-straps, and casually called them names like ‘hoe’ or ‘slut.’ Additionally, Dr. Damour noted that the girls were often unsure of whether or not the sexual harassment they experienced was their own fault.
   </p>
   <p>
    One girl in the group expressed that because she and her friends often wore leggings, they were perhaps encouraging boys to degrade them. It is this sense of shame that often prevents girls from telling anyone about harassment, in that they feel they must have done or worn something that brought it on.
   </p>
   <p>
    This sense of self-blame demonstrates how important it is for parents of girls to reassure their daughters that they are never responsible for someone harassing them.
   </p>
   <p>
    If you have a teenage daughter, consider starting a conversation about sexual harassment. Take the time to ask her about boys’ behaviors toward her, and ask whether she is being treated with respect. If she opens up about any degrading treatment she has received, let her know that you’re glad she feels able to tell you. Express that you are ready to help her tackle that problem.
   </p>
   <p>
    Alternatively, if she shuts down and doesn’t want to share anything with you, simply let her know that harassment is widespread. It is only shameful for the harasser, never the victim. Also express that you will never make her regret asking for your help with issues like this, either now or in the future.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5ca339006cee070007a5938e" data-chapterno="6">
  <h1>
   The cultural expectation that girls should be agreeable is making them anxious.
  </h1>
  <div class="chapter__content">
   <p>
    It’s a sad fact that our culture often holds girls to standards that we do not impose on boys. Dr. Damour saw this in action when a 15-year-old girl, Nicki, came to see her. Nicki was feeling anxious and overloaded with schoolwork and extracurricular commitments. She was having problems sleeping as a result.
   </p>
   <p>
    Gradually, the root cause of Nicki’s stress and anxiety emerged. She felt unable to say no to people.
   </p>
   <p>
    As it turned out, Nicki was a skilled gymnast but had decided to quit the sport due to increasing schoolwork. Unfortunately, her gym coach wouldn’t accept her decision. Instead, she not only pressured Nicki to continue her long hours of gymnastics training but also asked her to start teaching a group of younger gymnasts. Reluctantly, Nicki acquiesced to both requests. Why? Because she didn’t want to disappoint her coach.
   </p>
   <p>
    As a society, we typically teach girls to be more obedient and agreeable than boys. In other words, we expect them to do what we tell them to do. Not convinced?
   </p>
   <p>
    Just consider all the words we use to label women who disagree with requests. For instance, a girl who declines to help clean up a mess that she didn’t make might be called inconsiderate, or more damaging words may be thrown her way, such as
    <em>
     diva
    </em>
    or
    <em>
     bitch
    </em>
    . Now, ask yourself whether there are any male-specific equivalents. At worst, we might call a disagreeable man a
    <em>
     dick
    </em>
    , but even this word doesn’t quite capture the same scorn. Indeed, when a boy doesn’t behave as he’s supposed to, the response is often simply, ‘boys will be boys.’
   </p>
   <p>
    These differing expectations put pressure on girls like Nicki, and they often find themselves caught in an impossible bind. It is unsustainable for them to agree to everything that people ask of them. Yet, they dread saying no for fear of people calling them unpleasant names or thinking badly of them.
   </p>
   <p>
    Thus, girls are often filled with anxiety about turning people down. For instance, a girl who receives an invitation to a party she doesn’t want to attend may worry what people will think of her if she doesn’t go. In order to help your daughter to protect her valuable time and minimize stress and anxiety, teach her that it’s acceptable to say no. Don’t add your voice to the chorus of disapproval that often surrounds women who set boundaries.
   </p>
  </div>
 </div>
 <div class="chapter supplement" data-chapterid="5ca339006cee070007a5938e" data-chapterno="6">
  <h1>
  </h1>
  <div class="chapter__content">
   <p>
    <em>
     “We want our daughters to become self-assured advocates for their own best interests.”
    </em>
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5ca339316cee0700079f2f46" data-chapterno="7">
  <h1>
   Final summary
  </h1>
  <div class="chapter__content">
   <p>
    The key message in these blinks:
   </p>
   <p>
    <strong>
     Girls face particular challenges, both at school and in society at large, which can negatively impact their stress and anxiety levels. However, with the right support from the adults in their lives, the adverse effects of the pressure girls face can be successfully mitigated.
    </strong>
   </p>
   <p>
    Actionable advice:
   </p>
   <p>
    <strong>
     Don’t believe everything you read online.
    </strong>
   </p>
   <p>
    In the age of endless news and clickbait articles available 24 hours a day on our digital devices, it can be easy to feel pessimistic. Often, the media makes it sound like teenage girls are in crisis and that everything is getting worse. But despite what those clickbait articles say, teenagers are comparatively safe. In fact, research shows that teens today are experimenting
    <em>
     less
    </em>
    with drugs, alcohol and smoking, having less casual sex, and more frequently wearing seatbelts and bike helmets. So, the next time you read the latest article on teenage doom, bear in mind that your teenager is probably a lot better behaved than you were.
   </p>
   <p>
    <strong>
     Got feedback?
    </strong>
   </p>
   <p>
    We’d sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
   </p>
   <p>
    <strong>
     What to read next:
    </strong>
    <strong>
     <em>
      Untangled
     </em>
    </strong>
    <strong>
     , by Lisa Damour
    </strong>
   </p>
   <p>
    Now that you’re up to speed on the pressures girls face, it’s time to learn how you can continue to support them as they transition into adulthood. Penned by the same author, Lisa Damour,
    <em>
     Untangled
    </em>
    is an invaluable guide for anyone seeking to understand the particular challenges that girls face as they navigate into an independent future.
   </p>
   <p>
    So whether you’re a mother, father, teacher or mentor, discover what your teenage girl is really going through and get advice on how to overcome some of the most common pitfalls when relating to her by heading over to the blinks to
    <em>
     Untangled
    </em>
    .
   </p>
  </div>
 </div>
</article>
