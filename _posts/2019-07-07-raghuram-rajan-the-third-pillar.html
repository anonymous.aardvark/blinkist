---
layout: post
title: "Raghuram Rajan - The Third Pillar"
description: "The Third Pillar (2019) traces the evolving relationship between the three “pillars” of human life – the state, markets and communities – from the medieval period to our own age. Economist Raghuram Rajan argues that, throughout history, societies have struggled to find a sustainable balance between these pillars. Today is no different: caught between uncontrolled markets and a discredited state, communities everywhere are in decline. That, Rajan concludes, is jet fuel for populist movements. But a more balanced kind of social order is possible."
image: https://images.blinkist.com/images/books/5cf797796cee070008ddcab6/1_1/470.jpg
---

<article class="shared__reader__blink reader__container__content" lang="en">
 <div class="chapter chapter" data-chapterid="5cf7a3606cee070008ddcace" data-chapterno="0">
  <h1>
   What’s in it for me? A blueprint for a better world.
  </h1>
  <div class="chapter__content">
   <p>
    Society rests on three “pillars” – the state, markets and communities. Each pillar has a different role. The state guarantees law and order as well as providing the infrastructure that makes social life possible. Markets provide an outlet for ingenuity and wealth creation. Finally, communities create a sense of attachment, identity and solidarity.
   </p>
   <p>
    But societies only create the conditions for human flourishing when each of these three supports is equally strong – undermine one pillar and the whole structure begins to look pretty shaky. That balance has been historically elusive. Medieval society had strong communities but lacked both a state and markets. Commercial nations in the eighteenth and nineteenth centuries, on the other hand, had thriving marketplaces but sorely lacked a state capable of creating a level playing field.
   </p>
   <p>
    Today, we’re suffering from our own imbalances. After the failure of the state-driven models, which delivered unprecedented growth in the wake of the Second World War, Western societies attempted to construct a new order that emphasized efficiency and profit-making. The result? Inequality has exploded, creating a resentful class poorly equipped to deal with the challenges of globalization. That, in turn, has fuelled the great anti-establishment crusade of today’s populists.
   </p>
   <p>
    But as Raghuram Rajan shows, it doesn’t have to be this way. In these blinks, we’ll explore his blueprint for a better and more balanced world.
   </p>
   <p>
    Along the way, you’ll learn
   </p>
   <ul>
    <li>
     how the nation-state eventually replaced the medieval social order;
    </li>
    <li>
     why China will have to rethink its current economic model; and
    </li>
    <li>
     what an Indian city struggling with littering can teach us about localism.
    </li>
   </ul>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5cf7a3756cee070007e05270" data-chapterno="1">
  <h1>
   The feudal manors of medieval Europe were swallowed up by emerging nation-states.
  </h1>
  <div class="chapter__content">
   <p>
    Medieval Europe was a patchwork quilt of self-governing manors owned by the continent’s leading noble families. Peasants pledged allegiance to their masters and paid taxes. In return, they were permitted to work part of their land.
   </p>
   <p>
    The manors were self-contained communities governed by lords, who settled disputes among tenants and saw that justice was done. Goods produced on these estates were traded internally rather than exported. All in all, it was a social arrangement dominated by the community pillar.
   </p>
   <p>
    The fact that the medieval Church prohibited usury – the practice of charging interests on loans – cemented the sense of community felt by the inhabitants of manors. Rather than being given in the expectation of a monetary reward, help was a social obligation: people gave each other a hand knowing that their neighbors would return the favor.
   </p>
   <p>
    In the fifteenth century, technological developments disturbed this social order. Why? Well, innovations like the siege cannon changed the rules of the game. If you wanted to survive, you needed enough cash to fund a large army and defensive structures. Small manors just didn’t have the means to do that, so enterprising rulers began combining their estates.
   </p>
   <p>
    By the end of the century, the number of sovereign entities in Europe had halved to just 500. This was the beginning of a new era – the age of nation states. These new states eventually became so powerful that they eclipsed the Church, an institution whose laws had traditionally been seen as trumping secular law.
   </p>
   <p>
    When Henry VIII ascended to the English throne in 1485, for example, he was determined to expand his kingdom’s military prowess. He made up for shortfalls in his budget by seizing Catholic monasteries and selling their lands to lower status nobles. The most astute investors in these former Church properties became known as the “gentry” – an entrepreneurial agricultural class that invested in the productivity of their holdings and used the profits to buy up even more land.
   </p>
   <p>
    By the end of the sixteenth century, states had come into their own. Monarchs ruled a unified people and levied taxes on the gentry to cover the costs of increasingly expensive wars. The state was in the ascendance but, as we’ll see in the next blink, it’s dominance would soon be challenged by the market.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5cf7a3956cee070008ddcacf" data-chapterno="2">
  <h1>
   Free markets enjoyed a period of rapid expansion before being checked by a popular backlash.
  </h1>
  <div class="chapter__content">
   <p>
    The golden age of the state lasted from the consolidation of nation-states at the end of the fifteenth century until the late sixteenth. That’s when a new rival emerged to challenge their power: the market.
   </p>
   <p>
    Unproductive land previously owned by the aristocracy and the Church gradually fell into the hands of the commercially minded gentry. Monarchs were happy enough with this development – after all, the wealthier the gentry became, the higher their tax revenues were. But this new class also extracted its own price: greater freedom.
   </p>
   <p>
    One of the most important moments in this shifting relationship came in 1688, when English Parliamentarians deposed King James II and replaced him with a more pliant monarch, William of Orange. This event became known as the
    <em>
     Glorious Revolution
    </em>
    .
   </p>
   <p>
    Liberated from overweening royals, the gentry flourished. As the state receded, the market pillar became central to the lives of European nations. Philosophers sang the praises of the market. Adam Smith’s 1776 treatise
    <em>
     The Wealth of Nations
    </em>
    , for example, argued that the “invisible hand” of competitive markets allowed manufacturers – and thus nations – to flourish.
   </p>
   <p>
    What Smith hadn’t seen coming were the so-called “robber barons” of late nineteenth-century America who abused these newfound freedoms. Take John D. Rockefeller, an industrialist who became the world’s richest man by ruthlessly eliminating competition to his oil refining business.
   </p>
   <p>
    How? In a word, cartels. Rockefeller cut a deal with railroads in Cleveland so that they would charge his competitors a fee for transporting their oil barrels, which were then passed on to Rockefeller. In return, they received a share of Rockefeller’s own oil profits!
   </p>
   <p>
    Dodgy dealing like Rockefeller’s provoked a public backlash, and legislators withdrew Rockefeller’s main shell company’s charter. Poorly paid workers toiling in industrialists’ exploitative factories were meanwhile beginning to speak up for their rights, demanding better representation in government.
   </p>
   <p>
    Their pressure paid off. By the early twentieth century, most Western countries had expanded the right to vote to male workers. After centuries of retreat, the community pillar was once again returning to the forefront of social and economic life.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5cf7a3ae6cee070007e05277" data-chapterno="3">
  <h1>
   WW2 was followed by a period of extraordinary growth and prosperity, but it didn’t last for long.
  </h1>
  <div class="chapter__content">
   <p>
    The free market held out even as arguments in favor of stricter regulation became more common, but following the stock market crash of 1929 it suffered what looked like a killer blow – the Great Depression. States cast around for a culprit for the global crisis.
   </p>
   <p>
    The rampant capitalism of the previous decades seemed to fit the bill, and governments soon rushed through anti-market measures. The Smoot–Hawley Act, for example, which raised tariffs on over 20,000 import products, was passed in the US in 1930.
   </p>
   <p>
    Full recovery, however, had to wait until the outbreak of the Second World War. Spurred on by the huge demand created by mobilization, Western economies finally clawed their way out of the recession. But it was in the postwar era that they really took off.
   </p>
   <p>
    The first three decades after the conclusion of the war witnessed a period of unprecedented growth. Between 1946 and 1975, average real income per person grew by around 6.0 percent every year in Germany and 4.2 percent in France.
   </p>
   <p>
    That newfound prosperity allowed governments to make grand promises to their voters. Take Britain – in 1946, the Labour government created the National Health Service, the universal healthcare system that still provides coverage to UK citizens today.
   </p>
   <p>
    The creation of welfare states wasn’t the only outcome of dramatic economic growth. Immigration also exploded, as former belligerents sought to plug holes in their labor forces with foreign workers. By 1973, one in nine workers in France had been overseas. In West Germany, it was one in eight.
   </p>
   <p>
    The miracle couldn’t last, however, and by the 1960s Western economies were beginning to look shaky. That’s when the problems began. Government deficits increased as their spending commitments outstripped GDP growth. And as economies slowed down, unemployment and inflation rates began skyrocketing. Something had to give.
   </p>
   <p>
    The US and the UK led the way. Their answer? Roll back the state through deregulation and the privatization of state-owned industries. That meant a showdown with workers’ movements, but both the American and British governments proved adept at crushing resistance.
   </p>
   <p>
    In 1981, Ronald Reagan fired 11,000 unionized air traffic controllers and banned them from ever working for the federal government again. In the UK, Margaret Thatcher meanwhile waged a grueling but ultimately successful one-year war against striking coal miners, closing down their mines.
   </p>
   <p>
    The result of such conflicts was that the relationship between the three pillars once again swung in the market’s favor. As we’ll see, that wasn’t good news for communities.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5cf7a3c26cee070008ddcad6" data-chapterno="4">
  <h1>
   Deregulating markets created inequalities that new technologies have since exacerbated.
  </h1>
  <div class="chapter__content">
   <p>
    The expansion of markets from the 1980s onwards meant that the maximization of shareholder value gradually displaced other aims. Take corporations. In the 1960s, large businesses were commonly expected to contribute to society as a whole. A decade later, that view had fallen into disfavor as influential economists like Milton Friedman argued that the only “social responsibility” of business was “to increase its profits.”
   </p>
   <p>
    Friedman’s argument was simple. If managers focused on maximizing profits and the value of shareholders’ shares, their companies would survive and contribute more to society in the long run. The best way of aligning the interests of shareholders and managers, Friedman argued, was to make their pay – ideally in stocks – dependant on performance. That would incentivize them to do everything in their power to boost share value, including laying off workers.
   </p>
   <p>
    This new ethos was brilliant at encouraging efficiency, but it also led to greater inequality. The pay gap between the highest and lowest levels of organizations widened, while the positions of those at the bottom of the ladder became ever more precarious.
   </p>
   <p>
    These changes weren’t just the product of new ideas, however – technological innovation also played its role. As the author sees it, technological developments have created a “winner-takes most” economy, in which a small pool of superstars end up with most of the cake while the rest have to make do with crumbs.
   </p>
   <p>
    The music industry is a case in point. Today, anyone with a smartphone is seconds away from their favorite musician’s latest heavily hyped videos. The empires and unprecedented fortunes of a select few depend on that technology. Without it, it’s highly unlikely that Taylor Swift would’ve made $170 million in 2016. Less well-known musicians like the singer-songwriters who play in local bars, however, lose out. It’s easier and cheaper to listen to the biggest names of the day than supporting smaller acts.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5cf7a3dd6cee070007e0527e" data-chapterno="5">
  <h1>
   Social stratification laid the foundations for the populist revolts triggered by recent upheavals.
  </h1>
  <div class="chapter__content">
   <p>
    The music industry isn’t the only sector that’s been transformed by technological innovation – in fact, labor markets as a whole are being shaken up by new tech. This cuts two ways, as established jobs are being eliminated while new positions are being opened up. But here’s the problem: there’s a huge gap between the winners and losers of such change.
   </p>
   <p>
    Menial jobs like assembly line work are currently being decimated by automation. That means job losses for less-skilled workers, but it also creates new niche positions for specialists capable of overseeing automated processes and correcting errors. The folks who’ll fill those positions are invariably highly educated and at home in increasingly globalized labor markets.
   </p>
   <p>
    The workers whose jobs are being automated out of existence, by contrast, aren’t prepared for that kind of competition. That’s not just an issue affecting blue-collar workers, however – even moderately well-educated workers in developed countries have to compete with ever better-equipped overseas competitors. Outsourcing means that companies can transfer tasks to newly educated workforces in developing countries who can do the same work for a fraction of the cost.
   </p>
   <p>
    Inequalities created in the workplace are exacerbated by
    <em>
     residential sorting
    </em>
    – the tendency of wealthy families to live together in affluent neighborhoods. That’s driven by their desire to access the best schools and give their kids a head start in life. But as wealthy families flock to zip codes with high-achieving schools, prices increase and poorer families are driven out. The result? Social stratification.
   </p>
   <p>
    That’s a political powder keg. Moderately educated groups have become increasingly resentful of their elite peers in the wake of the 2008 financial crisis. What lit the fuse in the US, however, was Obamacare. The conviction that the Democrats’ healthcare reforms were a handout to minorities and ignored the interests of tax-paying blue-collar households led to the creation of the populist Tea Party movement.
   </p>
   <p>
    In Europe, it was the 2015 immigration crisis that triggered a populist backlash. After Germany admitted over one million refugees from war-torn countries in the Muslim world, voters anxious about their culture and welfare systems took aim at the EU – the institution they blamed for limiting their ability to control population movements. In the UK, those sentiments fed into the decision to leave the EU in 2016.
   </p>
  </div>
 </div>
 <div class="chapter supplement" data-chapterid="5cf7a3dd6cee070007e0527e" data-chapterno="5">
  <h1>
  </h1>
  <div class="chapter__content">
   <p>
    <em>
     “Populism, at its core, is a cry for help, sheathed in a demand for respect, and enveloped in the anger of those who feel they have been ignored.”
    </em>
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5cf7a3f86cee070008ddcad7" data-chapterno="6">
  <h1>
   So-called emerging markets face their own challenges.
  </h1>
  <div class="chapter__content">
   <p>
    The future of developed nations can’t be separated from that of so-called emerging nations. Whether it’s immigration or the export of high-tech goods and investments, the fate of both groups of countries are interlinked. That’s something policymaking in emerging nations will have to take into account. So what needs to be done? Well, let’s look at two case studies – China and India.
   </p>
   <p>
    China’s recent economic gains are impressive. Between 1980 and 2015, the country’s annual GDP growth averaged out 8.7 percent every year. That expansion was mostly driven by a couple of key, state-owned businesses. These companies were given access to guaranteed cheap credit and subsidized inputs like steel by the government, which in turn funded such largesse through taxes on regular households and foreign investment.
   </p>
   <p>
    Recent shifts in global markets and a return to protectionism mean that foreign firms are no longer interested in investing in Chinese production markets to export to other parts of the world. Instead, they’re focused on selling their own goods in China’s booming consumer market.
   </p>
   <p>
    That, in turn, means that China’s future growth will have to be domestic – something it will only be able to achieve if it eliminates the kind of market-distorting measures that have previously been used to give state-owned enterprises an artificial advantage. Can China liberalize its markets while maintaining central party control? That’s the big question facing the nation.
   </p>
   <p>
    India – a vast democracy with one billion inhabitants, 22 official languages and some 700 dialects – faces a different issue: corruption. So far that hasn’t hampered its economic growth. Over the last 25 years, India averaged 7.0 percent annual GDP growth. But it is undermining democracy.
   </p>
   <p>
    In the early 2000s, for example, it was revealed that government officials had been selling state assets, including land and mineral deposits, to their cronies at a steep discount. That’s indicative of the entanglement of the state and markets. If India wants to thrive, it’ll need to cut those ties and encourage a more independent private sector.
   </p>
   <p>
    But here’s the issue. The rise of a Hindu populism that promises to use the state to protect Hindu identity threatens to reverse the progress of liberalization just as it becomes more important than ever.
   </p>
  </div>
 </div>
 <div class="chapter supplement" data-chapterid="5cf7a3f86cee070008ddcad7" data-chapterno="6">
  <h1>
  </h1>
  <div class="chapter__content">
   <p>
    “
    <em>
     Elections are not enough, it is what happens between elections also that make for a vibrant economy.
    </em>
    ”
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5cf7a4176cee070007e0527f" data-chapterno="7">
  <h1>
   Inclusive localism offers an alternative to populism and a means to rebalance the three pillars of our economies.
  </h1>
  <div class="chapter__content">
   <p>
    As we’ve seen, the three pillars of our economies – state, market and community – are out of balance. The power of markets has eroded communities, while the state is increasingly identified with an out-of-touch establishment. That’s a recipe for populism.
   </p>
   <p>
    Why? Well, populism claims to offer a solution to the problems caused by such imbalances. On the one hand, it promises to revitalize society’s sense of community by bolstering national identity along ethnic and class lines. On the other hand, it proposes economic measures like tariffs on imported commodities like steel to counteract the erosion of job security.
   </p>
   <p>
    But these policies are ultimately counterproductive, undermining both domestic economies and international relations. Antagonizing today’s minorities will also lead to future retaliation when the identity of the majority group changes, as it inevitably will. So what’s the alternative? Call it
    <em>
     inclusive localism
    </em>
    .
   </p>
   <p>
    While the details will vary from country to country, all variants of inclusive localism share a common feature: the decentralization of power. That’s essentially all about delegating as many responsibilities as possible from the state to local communities, giving them the power to decide their own economic and political fates.
   </p>
   <p>
    In practice it would mean that, say, a small town could decide whether it prefers chains like Walmart, or would rather reserve its retail space for local businesses. Communities would also be empowered to preserve the traditions they value. What it doesn’t mean is an archipelago of isolated communities fending for themselves. That’s where the “inclusive” part comes in.
   </p>
   <p>
    The state’s role in this arrangement is to help bridge the gap between individual communities. That can take a couple of forms. Most literally, it will build concrete bridges; more generally, it will provide communications infrastructure as well as encouraging mobility through legislative action, especially for lower-income families.
   </p>
   <p>
    Take schools. One way the state can facilitate social mobility is to make use of already existent digital technologies to create a new national curriculum of lectures, reading materials and assignments available to every child.
   </p>
   <p>
    Affluent parents would no longer need to flock to wealthy neighborhoods with the best schools and poorer children would have access to the same education as their better-off peers. Teachers would meanwhile play a coaching role and use classroom time to motivate children with projects better suited to their individual needs and interests.
   </p>
  </div>
 </div>
 <div class="chapter supplement" data-chapterid="5cf7a4176cee070007e0527f" data-chapterno="7">
  <h1>
  </h1>
  <div class="chapter__content">
   <p>
    <em>
     “In the trade-off between inclusion and localism, inclusion should be given more weight.”
    </em>
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5cf7a42b6cee070008ddcad8" data-chapterno="8">
  <h1>
   Both the state and local communities play a key role in revitalizing communities.
  </h1>
  <div class="chapter__content">
   <p>
    Reviving local communities won’t be easy – in fact, it’ll take sustained work, inspired leadership and enthusiastic engagement. But it is possible, as a number of communities have already proven.
   </p>
   <p>
    Take Indore in Madhya Pradesh, India. When Malini Gaud was elected as the mayor of Indore in 2015, the city was in a bad way. Locals were using the city center as an open trash can while stray dogs, pigs and cows roamed the streets eating from the mountains of discarded trash and defecating in the gutters.
   </p>
   <p>
    Gaud was determined to change this sorry state of affairs. Her first act? Sprucing up the municipal cleaning team’s image. Workers were given new uniforms and their old rickshaws were replaced with state-of-the-art trucks fitted with GPS. Gaud also introduced biometric attendance tracking, suspended 300 underperforming employees and fired another 600.
   </p>
   <p>
    The revamped services got to work and began collecting household trash every day. The difference was stark and appreciative locals agreed to a new monthly collection fee, offsetting the costs of Gaud’s investments in municipal cleaning services.
   </p>
   <p>
    The introduction of fines, meanwhile, encouraged local eateries and shops to install bins. Gaud’s most inventive move was to create “drum squads” to patrol the streets and create an attention-grabbing racket every time they spotted someone using public spaces as their personal toilet.
   </p>
   <p>
    By 2017, Indore was ranked as the cleanest city in India. Even better, residents felt proud of their community and were united in a common effort to keep their streets clean.
   </p>
   <p>
    But how will these projects be funded? Well, in most cases, private sources like philanthropists and financial institutions with an interest in supporting local businesses will provide the bulk of the funding. That can work in different ways. If a community needs to provide equity for loans, for example, it can lease assets – a local park, say, could double up as a corporate event location after closing hours.
   </p>
   <p>
    The government also has a role to play. One idea is a system of top-ups subsidizing low earners’ incomes. These could be tied to community service, encouraging engagement and offering less affluent citizens a valuable source of additional income.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5cf7a4406cee070007e05280" data-chapterno="9">
  <h1>
   States should encourage fair competition at home and internationally.
  </h1>
  <div class="chapter__content">
   <p>
    Inclusive localism will require states to take a back seat and cede control to communities. At the national and international levels, however, they need to promote fair competition and innovation, as well as ensuring that markets aren’t dominated by a small number of overly powerful corporations.
   </p>
   <p>
    The best place to start when it comes to the state’s regulatory roles is to reform data rights laws. Take e-commerce platforms like Alibaba and Amazon. Currently, these companies own the online transaction histories of the merchants who use their services. That gives them valuable data concerning businesses’ cash flow and allows them to assess whether they’re eligible for loans.
   </p>
   <p>
    Over time these platforms develop a monopoly over users’ transaction and credit histories. Eventually, no one else has access to the hard data necessary to calculate credit scores. The upshot? They’re the only providers of credit in the market, allowing them to charge hefty interest fees and further consolidate their economic power. That’s not fair. The alternative? Give individuals ownership of their own data and allow them to decide who to share it with.
   </p>
   <p>
    Internationally, states need to find the right balance between global trade and national sovereignty. Keeping tariffs as low as possible, at least for goods and services, is in everyone’s interests. That said, governments should resist the urge to harmonize non-tariff barriers like regulations and safety standards if they want to protect diversity and national sovereignty.
   </p>
   <p>
    Financial trade, by contrast, should be more tightly regulated – after all, no one wants a repeat of the 2008 crisis. The same goes for information flows, in an age of increasing cybercrime and social media meddling.
   </p>
   <p>
    Finally, states should exercise full sovereignty over domestic issues like monetary policy. Policies that affect other nations such as the manipulation of exchange rates, by contrast, should be prohibited by the international community. Other areas requiring joint action like overfishing and carbon emissions should be regulated through global agreements.
   </p>
   <p>
    Globalization has its benefits, but it needs to be managed. The best way of doing that is to delegate sovereignty to countries and their communities so that they can pursue their own interests without harming others. Inclusive localism provides a template for how they might be able to do just that.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5cf7a4666cee070007e05281" data-chapterno="10">
  <h1>
   Final summary
  </h1>
  <div class="chapter__content">
   <p>
    The key message in these blinks:
   </p>
   <p>
    <strong>
     The central pillars of our society are out of balance. Rising inequalities and growing class resentment are undermining communities around the world. This is fertile ground for populist insurgents. But their ideal of state-backed nationalism doesn’t hold the answers to the complex problems posed by globalization and technological innovation. If we want to avoid being led up the garden path, we need to come up with a solution that balances the three pillars of social life – the state, markets and communities. The best option we have of aligning those three pillars is inclusive localism.
    </strong>
   </p>
   <p>
    <strong>
     Got feedback?
    </strong>
   </p>
   <p>
    We’d sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts.
   </p>
   <p>
    <strong>
     What to read next:
    </strong>
    <strong>
     <em>
      Fault Lines
     </em>
    </strong>
    <strong>
     , by Raghuram G. Rajan
    </strong>
   </p>
   <p>
    The 2008 financial crisis had far-reaching consequences. As we’ve seen in these blinks, the tide of populism currently sweeping the world has its roots in that great global crash. That means we’d better figure out how to avoid a repeat of those disastrous years if we want a return to political stability.
   </p>
   <p>
    That’s just what Raghuram Rajan set out to do in his study of the financial crisis. Unsatisfied with the simplistic argument that greedy bankers tanked the global economy, Rajan unravels the complex interplay of structural and individual factors underpinning the worst crash since the Great Depression. So if you’d like to learn more about preventing another crisis, check out our blinks to
    <em>
     Fault Lines
    </em>
    , by Raghuram Rajan.
   </p>
  </div>
 </div>
</article>
