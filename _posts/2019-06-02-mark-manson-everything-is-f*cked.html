---
layout: post
title: "Mark Manson - Everything is F*cked"
description: "Everything is F*cked (2019) is a no-holds-barred look at the state of the modern human condition and why so many feel like the world is a lot worse off than it really is. Author Mark Manson looks to pillars of human philosophy, including Immanuel Kant and Friedrich Nietzsche, to reveal how both the trappings of modern society and concepts such as hope have people focused on the wrong things in life."
image: https://images.blinkist.com/images/books/5cd6a6126cee070008f573a9/1_1/470.jpg
---

<article class="shared__reader__blink reader__container__content" lang="en">
 <div class="chapter chapter" data-chapterid="5cd6a6386cee070008f573aa" data-chapterno="0">
  <h1>
   What’s in it for me? Discover how hope can get in the way of living a satisfying life.
  </h1>
  <div class="chapter__content">
   <p>
    Most of us have hopes and dreams of a better future. But what if those hopes are actually getting in the way of us living a more satisfying life? What if we’re all looking for a future that can never live up to our expectations? This may sound like a big downer, but there’s an important and uplifting message here as well.
   </p>
   <p>
    As author Mark Manson points out, there’s been a lot of progress worldwide in the past couple of generations, especially in the areas of poverty, starvation and child mortality, yet we still see rising rates of depression and anxiety all around us. In Manson’s estimation, a lot of this has to do with hope, and the way that it links people’s happiness to unrealistic visions of a perfect future. In the pursuit of happiness, people have lost sight of the virtues and characteristics that can truly help us in the present, like courage, honesty and humility.
   </p>
   <p>
    Manson has some tough words for people obsessed with comfort, ease, life hacks and happiness, but his advice is constructive and meant to keep us focused on the here, the now, and the things that really matter.
   </p>
   <p>
    In these blinks you’ll find out:
   </p>
   <ul>
    <li>
     why pure logic doesn’t lead to the best decisions;
    </li>
    <li>
     why the pursuit of happiness is impossible; and
    </li>
    <li>
     why putting AI in control might not be so bad after all.
    </li>
   </ul>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5cd6a64f6cee0700085dfc92" data-chapterno="1">
  <h1>
   Hope has seen people through some difficult times, but it may not work when times are good.
  </h1>
  <div class="chapter__content">
   <p>
    There’s an uncomfortable truth to life that many of us prefer not to dwell on: you and everyone you know will someday be dead, and all of your concerns and efforts, in the grand scheme of things, are pretty insignificant.
   </p>
   <p>
    No one likes staring into the void of this uncomfortable truth, because you can easily slip into nihilism and think, “If everything is meaningless, I might as well either stay in bed or consume the best drugs I can find and go play in traffic.”
   </p>
   <p>
    Throughout the ages,
    <em>
     hope
    </em>
    has been the main thing getting people out of bed in the morning and sustaining them through seriously tough times. Whether it’s for our own future or that of our family or community, hope is a powerful driver of human behavior.
   </p>
   <p>
    Take Witold Pilecki, for instance. He had one hope: to see an independent Poland. That hope drove him to join the resistance movement and volunteer to be arrested by the Nazis in order to infiltrate Auschwitz and help the prisoners there. He then spent the next two years smuggling food and medicine into the camp and keeping up contact with the outside world.
   </p>
   <p>
    After WWII, he continued fighting for Poland – this time against Communist forces. As a result, he was arrested and tortured for two years before being executed in 1948. Yet even while facing his imminent death, Pilecki had hope; he said that he could die with joy in his heart since he’d done everything he could to help liberate his people.
   </p>
   <p>
    Pilecki’s story shows just how powerful hope can be when everything in the world seems bleaker than bleak. But the problem is that hope is intrinsically linked to the future, and for a good number of people in the world, the present is better than ever. Indeed, countless facts and figures show how rates of violence, racism, poverty, child mortality and war are at all-time lows worldwide, while human rights are on a steady upward trajectory.
   </p>
   <p>
    As a result, there’s less of a sense of hope and more of a sense of having a lot to lose. This might help explain why rates of anxiety and depression in the US have been going up in the past thirty years while all of these improvements have been ongoing.
   </p>
   <p>
    In the blinks ahead, we’ll look at some other reasons for our continued anxiety, and why hope may be the real culprit.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5cd6a6656cee0700085dfc93" data-chapterno="2">
  <h1>
   The classic assumption that the rational mind is capable of making better decisions is wrong.
  </h1>
  <div class="chapter__content">
   <p>
    Authors like Steven Pinker and Hans Rosling have recently written big books full of charts that show how much better the world is now than just a couple of generations ago. It all seems to say, “C’mon, cheer up! All things considered, we’re doing pretty well!”
   </p>
   <p>
    But this scientific approach, with its charts and bar graphs, has one big flaw: it speaks to our Thinking Brain, where logic and reason rules, not to our Feeling Brain, where our emotions reside. And if we want to make better decisions and understand the problem with hope, we have to appeal to both sides.
   </p>
   <p>
    There’s a common misconception that we’d all have a better grip on life and be more productive if we could only get our emotions out of the way and put our logical minds in control. It turns out this isn’t the case, though.
   </p>
   <p>
    Consider the case of Elliot, who had a baseball-sized tumor removed from the frontal lobe of his brain. As it turned out, the removal of the tumor also took away Elliot’s capacity for emotion. But he didn’t become a cold-blooded efficiency machine – quite the opposite, in fact. He skipped an important work meeting to go buy a better stapler, he skipped his kid’s baseball game to watch TV – he basically stopped giving a fuck about anyone or anything.
   </p>
   <p>
    This cost Elliot his job and his family, but doctors couldn’t explain what was going on until they checked his emotional responses. When Elliot was shown horrifying war photos of dead children, even he acknowledged that he should’ve had an emotional reaction – yet he didn’t.
   </p>
   <p>
    The mysterious case of Elliot shows how we really need harmonious communication between our Thinking and Emotional Brains if we’re going to stop falling prey to the same hope-related problems.
   </p>
   <p>
    Let’s say you hope to stop eating junk food. The logical, objective Thinking Brain knows that these things are bad for your health – it’s good with facts and data. But the subjective Feeling Brain is the part that takes the facts and data and uses them to decide what’s “good” and “bad”. So making the right decision takes some real negotiation, because it’s all too easy for the Feeling Brain to take over and decide that eating junk food is actually a
    <em>
     good
    </em>
    idea.
   </p>
   <p>
    In the next blink, we’ll take a closer look at how your emotions can undermine you.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5cd6a6be6cee070008f573ad" data-chapterno="3">
  <h1>
   Four laws govern our emotions and can make hope a losing proposition.
  </h1>
  <div class="chapter__content">
   <p>
    There’s more than one way in which our Feeling Brain can undercut our hopes. As the author sees it, there are four laws dictating our emotional state, and they can play a big role in explaining why hope can be a recipe for unhappiness.
   </p>
   <p>
    The first law of human emotion is that
    <em>
     for every action, there is an equal and opposite emotional reaction
    </em>
    .
   </p>
   <p>
    Let’s say that something bad happens to you, like getting punched in the face. At that moment, a
    <em>
     moral gap
    </em>
    opens up between what you think is fair and just and what’s actually happening. Your natural response is as strong as the punch itself - you feel that you’ve been wronged, get angry, and want to close the gap by getting even.
   </p>
   <p>
    But what if you couldn’t? The second law states that
    <em>
     our self-worth equals the sum of our emotions over time
    </em>
    . If you kept getting hit and were unable to do anything about it, your brain would begin to compensate; which is essentially what happens to abused children. Unable to close the moral gap, their brains instead make a moral shift that causes them to believe they
    <em>
     deserve
    </em>
    to be hit. This happens in all kinds of situations, when people who are unable to act essentially come to see pain as the new normal.
   </p>
   <p>
    So when bad things happen to you at an early age, that can instill some unfortunate beliefs in you that can be hard to get rid of.
   </p>
   <p>
    Which leads us to the third law of emotion:
    <em>
     your identity will stay your identity until a new experience acts against it
    </em>
    .
   </p>
   <p>
    If you’ve ever met a political extremist, either on the left or the right, you know how useless it can be to try to explain why a more open-minded, moderate approach is better for democracy. People develop narratives around their formative experiences, and these narratives add up to an identity, so it will take another formative experience to bring about change.
   </p>
   <p>
    Finally, there’s the law of
    <em>
     emotional gravity
    </em>
    , which states that the people in your personal orbit tend to be a lot like you.
   </p>
   <p>
    Most people want the same things, like good food and a roof over their heads. But unfortunately, we tend to focus on the relatively small differences that separate us rather than those larger commonalities. We’re drawn to people with the same specific likes and dislikes as us, and we start conflicts with those who don’t share our preferences.
   </p>
   <p>
    With these laws governing us, you can see how even the most hopeful among us can end up unable to change or stuck believing that we deserve bad things. And that can keep hope forever out of reach.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5cd6a6d86cee0700085dfc95" data-chapterno="4">
  <h1>
   All belief systems create problems, so it’s better to accept life on its own terms.
  </h1>
  <div class="chapter__content">
   <p>
    If you ever wanted to start your own religion, all you’d need to do is follow a few basic steps. First, you’d sell a particular kind of hope to a particular group of hopeless people; say, the promise of heaven for people unhappy with their lives. Then you’d find a way to make any criticisms invalid, like telling your followers that anyone who doesn’t believe in your religion is in league with Satan. Next, create some rituals for your people to follow, while promising them that heaven or hell is on the way. Now, all you have to do is tell them to give you money, vote you into office or do whatever it is that you wanted followers for in the first place.
   </p>
   <p>
    This may sound cynical, but it isn’t hard to see that religions, including ideological belief systems like capitalism or communism, all end up corrupted by the all-too-human individuals behind the scenes. It may be the pursuit of money, political power, narcissism or some other human foible, but corruption is inevitable, even if the religion was begun with the best intentions.
   </p>
   <p>
    As the German philosopher Friedrich Nietzsche saw it, the fatal flaw in any belief system is that it’s run by fallible human beings who will eventually find a way to corrupt it or pit it against other belief systems. And hope is no exception. Just like any other kind of belief, for hope to be “good,” something else must be seen as “bad.” A hopeful person, after all, is essentially saying, “I’m unhappy with how things are now and I hope they change.” So while it might feel like hope gives things more meaning, it’s really just creating more unhappiness and conflict! Or, as the author puts it, “Everything is fucked because of hope.”
   </p>
   <p>
    This is where Nietzsche asked us to look beyond the good and evil espoused by any belief system. He wanted us to accept life and death for what they are, warts and all. That means that we stop avoiding the uncomfortable truth of death and insignificance. Once we do that, we can go about focusing on the amazingness of all that’s in front of us now, rather than concerning ourselves with hope. Nietzsche even had a name for this embrace-the-void approach: he called it
    <em>
     amor fati
    </em>
    , which translates to “love one’s fate.”
   </p>
  </div>
 </div>
 <div class="chapter supplement" data-chapterid="5cd6a6d86cee0700085dfc95" data-chapterno="4">
  <h1>
  </h1>
  <div class="chapter__content">
   <p>
    <em>
     “This is our challenge, our calling: To act without hope. To not hope for better. To
    </em>
    be
    <em>
     better. In this moment and the next.”
    </em>
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5cd6a6ee6cee070008f573ae" data-chapterno="5">
  <h1>
   Immanuel Kant suggested a way of life, and a way of being an adult, that complements Nietzsche’s amor fati.
  </h1>
  <div class="chapter__content">
   <p>
    Even if you’re not familiar with the eighteenth-century philosopher Immanuel Kant, you’re probably familiar with some of the things he inspired. In his writing, Kant looked to a future in which world peace would be achieved under a global government; this essentially served as the inspiration for the United Nations. He was also an early proponent of the idea that every single human being has an inherent dignity that deserves respect. And if that weren’t cool enough, he was even an early advocate for animal rights.
   </p>
   <p>
    Perhaps most impressive of all is that Kant had a relatively simple formula for humanity, and it also suggests that we do away with hope.
   </p>
   <p>
    It states: “Act that you use humanity, whether in your own person or in the person of any other, always at the same time as an end, never merely as a means.”
   </p>
   <p>
    What this means is that you shouldn’t behave in transactional ways. So don’t be kind to your partner in the hopes of getting laid. Instead, be kind as an end, period – because it’s the right thing to do. Likewise, don’t decide not to steal because it’ll help you get into heaven. Instead, decide not to steal because stealing is a shitty thing to do. Yes, Kant is basically saying “don’t be an asshole.”
   </p>
   <p>
    Kant’s formula for humanity fits nicely with Nietzsche’s amor fati, because it asks people not to do things simply in the hope that their behavior will lead to a favorable outcome. Every act should be an end unto itself, taken without the expectation of receiving something in return.
   </p>
   <p>
    In other words, Kant explains how to be an adult.
   </p>
   <p>
    As children, we’re all about pleasure and doing what feels good. Then, as adolescents we start developing principles. These provide a more personal motivation for our actions, and we begin to weigh our desire for pleasure against them. In adulthood, these principles should then become the main motivator for our behaviors.
   </p>
   <p>
    So while an adolescent may think, “I won’t steal because I’ll get caught,” an adult should recognize that stealing is wrong on principle. Adults also accept that while certain things may be difficult, uncomfortable or downright painful, it’s necessary to do them anyway when they’re the right thing to do.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5cd6a7016cee0700085dfc96" data-chapterno="6">
  <h1>
   The pursuit of happiness is both a risk to democracy and impossible to achieve.
  </h1>
  <div class="chapter__content">
   <p>
    Winston Churchill once said, “Democracy is the worst form of government, except all the others.”
   </p>
   <p>
    That said, democracy is still the
    <em>
     best
    </em>
    system, since it both recognizes the corrupting nature of politics and allows other social and ideological beliefs to exist within it. But hope and the pursuit of happiness aren’t really compatible with democracy. In fact, by pursuing happiness, we’re actually putting democracy in danger.
   </p>
   <p>
    The pursuit of happiness is essentially the avoidance of pain and discomfort; it’s never dealing with the difficulties of life, such as people with different opinions. But if we never face adversity, we never have the chance to strengthen the virtues of
    <em>
     honesty
    </em>
    ,
    <em>
     courage
    </em>
    and
    <em>
     humility
    </em>
    in ourselves
    <em>
     .
    </em>
    And in order for democracy to flourish, we need these virtues. They help us acknowledge and accept the hard work and diverse viewpoints that democracy requires.
   </p>
   <p>
    People are increasingly unwilling to accept the discomfort of opposing views these days, though, and that spells trouble for democracy. A big part of the problem, as philosophers like Nietzsche have pointed out, is our self-serving drive towards happiness; it makes human beings ill-suited to democracy, because it tends to outweigh our tolerance for dissenting opinions. It can even lead us to believe that our own opinions and happiness are more important than democracy itself! When people who think this way band together, extremist groups form. And when these groups are able to tear democracy down, tyranny rises.
   </p>
   <p>
    But perhaps the most sensible reason to stop pursuing happiness is that it’s a futile exercise to begin with. First of all, studies show that while we might feel an uptick in happiness when life takes a turn for the better, we soon level out to our normal baseline mood. And even if, hypothetically, we were able to remove every unpleasant thing from our lives, we wouldn’t stop seeing problems – we’d only become more sensitive to smaller things that never bothered us before.
   </p>
   <p>
    This phenomenon is known as the
    <em>
     Blue Dot Effect
    </em>
    , which was discovered during a series of studies in which participants were told to look at a screen and indicate when they saw certain things, such as blue dots or people with threatening expressions. As the number of blue dots and threatening expressions decreased, people didn’t stop seeing them; they just moved the line for what qualified as “blue” or “threatening” and convinced themselves that those things were still appearing.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5cd6a71b6cee070008f573b0" data-chapterno="7">
  <h1>
   At a certain point, innovation shifts to diversion and freedoms diminish.
  </h1>
  <div class="chapter__content">
   <p>
    When considering the reasons for today’s high levels of anxiety and depression, we shouldn’t ignore two major culprits: advertising, and the kind of diversion that’s disguised as innovation.
   </p>
   <p>
    Advertising’s influence over our mood changed significantly in 1920s when, for the first time, advertisers began targeting the Emotional Brain instead of the Thinking Brain.
   </p>
   <p>
    Previously, ads had described how efficient a product was or highlighted a special ingredient. But in the late 1920s, products began to be pitched in ways that exploited a person’s “pain point,” or insecurity. The question wasn’t,
    <em>
     How can we convince them our product is worth buying?
    </em>
    but rather,
    <em>
     How can we convince them our product will make them feel better about themselves?
    </em>
   </p>
   <p>
    This change, in and of itself, is bad enough for the human psyche, but there’s another shift that may have had even more profound effects – the one in which innovation turned into diversion.
   </p>
   <p>
    All over the world, when a developing nation begins to experience growth, there is a period of innovation. It’s usually marked by advances in medicine and an increase in available jobs, and people generally become happier during this time. But once a nation reaches First World status, those happiness levels tend to flatline and even drop, while levels of depression and anxiety increase. This is because innovation is turning to diversion; advertisers start preying on consumers’ insecurities and selling them things they don’t really need.
   </p>
   <p>
    Businesses like to say that this is just “giving the people what they want.” And in the US, the fact that supermarkets have things like a massive selection of breakfast cereals is even considered a sign of how much freedom there is. And more freedom should equal more happiness, right? But often, when you have more choice, all you really have are more diversions, and this can actually lead to less freedom.
   </p>
   <p>
    With the abundance of diversions we have now, we’ve become obsessed with using technology to make things easier to do. But we’re also developing new, compulsive behaviors in the way we use technology, which diminishes our freedom. True freedom comes from reducing things in your life, like when you delete a social media account to free up your time and attention. When your sense of well-being becomes dependent on distractions, creature comforts and unnecessary technologies, you’re moving in the opposite direction from freedom.
   </p>
   <p>
    As troubling as it is that we’re willing to give up so much freedom in return for convenience, there may be a silver lining to these technological distractions. We’ll look at that in the next blink.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5cd6a7306cee0700085dfc97" data-chapterno="8">
  <h1>
   AI is likely to change our lives, and maybe not for the worse.
  </h1>
  <div class="chapter__content">
   <p>
    A crazy thing happened in 2018.
   </p>
   <p>
    Google showed up to a chess competition with its artificial intelligence (AI) program, AlphaZero. The reigning champ at the time had been Stockfish, an open-source chess program that had been kicking everyone’s ass for four years straight. On paper, Stockfish was the favorite, as it was capable of analyzing
    <em>
     70 million
    </em>
    positions per second, while AlphaZero had capacity for only 8,000. And prior to the morning of the event, AlphaZero had never played a single game of chess. Yet it took the competition by storm, either beating or reaching a draw against Stockfish in a full one hundred matches.
   </p>
   <p>
    But that’s not all. That same day, AlphaZero went on to play Shogi, a Japanese version of chess, for the first time. And it destroyed Elmo, the program that was reigning champion in that game, winning 90 out of one hundred matches.
   </p>
   <p>
    AI, clearly, is incredible, and it and algorithms are already expanding into many aspects of our day-to-day life. As far as the author is concerned, it’s probably best that we just go ahead and bow down to our new AI overlords now and let them run things as they deem fit. Sure, some smart people, like inventor Elon Musk, suggest that AI poses a serious threat. But it could also be the best thing that ever happened to us.
   </p>
   <p>
    After all, human beings aren’t as logical as algorithms – we’re inefficient walking contradictions that make no sense. We’ve created things like chemical warfare, domestic violence, and money laundering. And that’s just to name a few of our sins. Generally speaking, we tend toward self-loathing and self-destruction.
   </p>
   <p>
    So how much worse are the AI algorithms going to be? What are the chances that AI might look at the fact that there are currently five genocides in progress and come up with a better way of running the planet?
   </p>
   <p>
    Maybe AI will convince people that we can in fact treat the world around us much better and still be very prosperous. Maybe AI will be the thing that finally helps humans reach a post-hope world and see beyond good and evil, to find the “something greater” that finally puts an end to ideological and religious warfare.
   </p>
   <p>
    So if there’s one thing you should hope for, it’s that we don’t blow ourselves up before we can make the changes that give us the chance to be the best versions of ourselves.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5cd6a7466cee070008f573b1" data-chapterno="9">
  <h1>
   Final summary
  </h1>
  <div class="chapter__content">
   <p>
    The key message in these blinks:
   </p>
   <p>
    <strong>
     While many of us rely on hope to get us through tough times, the reality is that hope may be causing most of our anxiety and depression to begin with. Hoping for more happiness is a losing game, since the fewer difficulties we encounter, the more sensitive we become to smaller problems. Rather than making happiness, convenience and comfort our primary values, we should accept that life is difficult and instead concentrate on being more virtuous human beings who accept adversity and rise to the challenges of life.
    </strong>
   </p>
   <p>
    Actionable advice:
   </p>
   <p>
    <strong>
     Embrace life’s uncomfortable truths through meditation.
    </strong>
   </p>
   <p>
    Meditation is about letting thoughts, especially the dark ones, rise up, acknowledging them and then letting them go. As such, it’s a great tool for embracing the uncomfortable truths of life and getting on with it despite everything being fucked. With meditation, you can become more comfortable with the fact that pain is inevitable, and learn to understand that suffering doesn’t have to be.
   </p>
   <p>
    <strong>
     Got feedback?
    </strong>
   </p>
   <p>
    We’d sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
   </p>
   <p>
    <strong>
     What to read next:
    </strong>
    <strong>
     <em>
      The Subtle Art of Not Giving a F*ck
     </em>
    </strong>
    <strong>
     , by Mark Manson
    </strong>
   </p>
   <p>
    If you enjoyed these blinks, then you may find author Mark Manson to be a refreshing change of pace from the usual non-fiction authors. So this may be the perfect time to check out the blinks to
    <em>
     The Subtle Art of Not Giving a F*ck
    </em>
    .
   </p>
   <p>
    In his 2016 bestseller, Manson offers a better way of living by reducing the number of things that get your blood boiling. After all, we can only affect so much change in the world, so if we want to live a less stressful life, it may be best to stop giving a f*ck about some things.
   </p>
  </div>
 </div>
</article>
