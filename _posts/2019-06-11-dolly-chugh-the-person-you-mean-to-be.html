---
layout: post
title: "Dolly Chugh - The Person You Mean to Be"
description: "The Person You Mean to Be (2018) offers an accessible guide to the complex world of unconscious biases. Unconscious biases are the assumptions and associations we all have about people who are of a different gender, race, sexual orientation or class than we are. Author Dolly Chugh explains how these unconscious biases work and what we can do to overcome them."
image: https://images.blinkist.com/images/books/5cdfef5d6cee0700086aa1ff/1_1/470.jpg
---

<article class="shared__reader__blink reader__container__content" lang="en">
 <div class="chapter chapter" data-chapterid="5cdfef796cee0700086aa200" data-chapterno="0">
  <h1>
   What’s in it for me? Learn the realities of biased thinking and how to free your mind.
  </h1>
  <div class="chapter__content">
   <p>
    At one point or another, you’ve probably questioned your own beliefs about discrimination and equality. But what you may have overlooked is your unconscious biases. No matter how open-minded and progressive you consider yourself to be, like everyone else, you most likely have biases informed by your upbringing and life experience.
   </p>
   <p>
    Author Dolly Chugh draws from the most cutting-edge psychological studies to present a fascinating picture of what’s informing our instinctual behaviors and decisions. These unconscious biases show – to just give one example – how even the most anti-racist of us can have racist tendencies resting just beneath the surface.
   </p>
   <p>
    Fortunately, by learning more about ourselves, others and how our minds work, we can start to correct these biases and become more aware of what we’re paying attention to and what we’ve been neglecting.
   </p>
   <p>
    In these blinks, you’ll learn
   </p>
   <ul>
    <li>
     why Hollywood still lacks equality;
    </li>
    <li>
     why white people need other white people to tell them they’re racist; and
    </li>
    <li>
     why you should never say you’re color-blind when it comes to race.
    </li>
   </ul>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5cdfef926cee0700081dc302" data-chapterno="1">
  <h1>
   Having a growth mindset means being willing to learn, but prejudices are hard to overcome.
  </h1>
  <div class="chapter__content">
   <p>
    When you’re deciding whether or not to do something, do you prefer to stick with the familiar or are you open to exploring new things?
   </p>
   <p>
    For documentary filmmaker Perrin Chiles, new things offer an exciting challenge. In the early 2000s, when Chiles was preparing for his next film, he chose the subject of autism, even though he had little first-hand knowledge of the subject. This attitude is a perfect example of what’s known as
    <em>
     growth mindset
    </em>
    , as it shows a willingness to learn new things. Having a growth mindset can lead to amazing things, and yet, many people have the opposite attitude, known as a
    <em>
     fixed mindset
    </em>
    .
   </p>
   <p>
    For example, someone has a fixed mindset if they believe they’re terrible at drawing and would never take an art class. They believe people are either good or bad at something, and no class will ever change that. Someone with growth mindset, however, may admit that their drawing skills are weak now, but acknowledge that with practice and effort their stick figure can one day become a realistic portrait. In other words, those with a growth mentality know that they can improve, and take advantage of opportunities to do so.
   </p>
   <p>
    For Chiles, his film on autism was an opportunity to learn about people who are different from him. And as ideas coming from a growth mindset often do, it led to great things. In this case, a wonderful documentary called
    <em>
     Autism: The Musical
    </em>
    . Released in 2007, the film touched millions of people and made significant strides in opening people’s eyes to the realities of autism.
   </p>
   <p>
    Unfortunately, not everyone is as open-minded as Chiles. Often, fixed mindsets can lead to stubborn prejudices that prevent people from exploring new things. This narrow-mindedness is especially true of Hollywood. If an extraterrestrial being were to make assumptions about human beings based strictly on Hollywood movies, they would likely think that nearly everyone is a straight, white male with no physical disabilities.
   </p>
   <p>
    In an overview of the highest-grossing films in recent years, only 27 percent of the speaking roles were female. As for the top films of 2015, 48 of them didn’t contain a single black actor in a speaking role. What’s more, only 4 percent of Hollywood’s new movies are directed by women.
   </p>
   <p>
    The following blinks explore why built-in prejudices like those found in Hollywood are so difficult to overcome.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5cdfefb16cee0700086aa201" data-chapterno="2">
  <h1>
   Unconscious bias can now be measured, and the results are usually disappointing.
  </h1>
  <div class="chapter__content">
   <p>
    The term
    <em>
     unconscious bias
    </em>
    has become something of a buzzword lately. Essentially, it refers to the fact that people can unintentionally discriminate against others, or have prejudicial beliefs they’re not necessarily aware of. However, since researchers found a way to measure unconscious bias it’s now possible to increase this awareness.
   </p>
   <p>
    Remarkably, studies have shown that the average person processes around 11 million pieces of information every second. Yet, we only process around 40 of those on a conscious level!
   </p>
   <p>
    Therefore, you could say that 99.999 percent of the information we take in gets processed unconsciously. And this would include our unconscious biases. These are the automatic associations we make based on accumulated information, such as associating black people with weapons for instance.
   </p>
   <p>
    So, how do you measure unconcious bias? One way to do it is through an Implicit Association Test (IAT), an online test developed by Harvard psychologists Mahzarin Banaji, Anthony Greenwald and Brian Nosek. It measures the extent of your unconscious biases by asking you to react as quickly as possible to a series of questions that speak to your unconscious associations. For example, do you associate men or women with subjects like career and family or sciences and the arts? By requiring rapid decision-making, the test accesses your unconscious brain and assess your biases.
   </p>
   <p>
    As you may suspect, many supposedly open-minded participants were shocked by their IAT results.  Since the test went online in 2011, many people who identify as progressives and support gender equality have taken it. Yet, about 75 percent of all participants have shown a conservative-minded bias by strongly associating women with nurturing and household activities, and men with career and working.
   </p>
   <p>
    A similar bias was revealed regarding race, as 85 percent of white Americans associated black people with dangerous objects, such as knives and guns. Ultimately, people who were under the impression they had no racial or gender bias had to face the surprising results of the test. This just proves how unconscious these biases truly are.
   </p>
  </div>
 </div>
 <div class="chapter supplement" data-chapterid="5cdfefb16cee0700086aa201" data-chapterno="2">
  <h1>
  </h1>
  <div class="chapter__content">
   <p>
    <em>
     “Our behavior seems most prone to our implicit biases when we are under great time pressure or stress.”
    </em>
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5cdfefd36cee0700081dc303" data-chapterno="3">
  <h1>
   People tend to discount their privileges unless they are given something positive to focus on.
  </h1>
  <div class="chapter__content">
   <p>
    How difficult do you think your childhood was? When considering your personal history, don’t forget that white Americans, for example, generally have a much more privileged upbringing in terms of education, healthcare and standard of living than black Americans.
   </p>
   <p>
    Now, you might think that a reminder to keep things in perspective would result in more white Americans saying that their childhood wasn’t so bad. However, when this question was posed in just this fashion by Stanford psychologists in 2015, the opposite was true. White Americans actually emphasized the difficulty in their childhood more after being reminded of white privilege.
   </p>
   <p>
    In other words, people can accept that one group is disadvantaged while disregarding the advantages they had. Most people end up maintaining the falsehood that they too had to overcome adversity. This is because people believe that acknowledging their privilege makes their achievements appear unearned or undeserved.
   </p>
   <p>
    A similar trend exists in the workplace. Another study revealed how employees with high salaries and benefits like quality healthcare and legal services are more likely to emphasize the strenuous effort and difficulty their job requires after they’d been reminded about their perks. Interestingly enough, it’s also been shown that people stop discounting their privileges when they’re given a positive achievement to focus on.
   </p>
   <p>
    The 2015 Stanford study also revealed that the participants had different responses if, prior to being reminded of their privilege, they were asked to reflect on an impressive past accomplishment or given positive feedback on a test they’d taken. Now, when asked about their childhoods, the participants were more likely to recognize their privilege, since they no longer felt that their sense of self-worth was under threat.
   </p>
   <p>
    So, if you ever feel like someone needs a reminder about their privilege, you should remember to hand out a compliment before confronting them. Otherwise, they’ll likely deny that privilege played a role in helping them get where they are today.
   </p>
  </div>
 </div>
 <div class="chapter supplement" data-chapterid="5cdfefd36cee0700081dc303" data-chapterno="3">
  <h1>
  </h1>
  <div class="chapter__content">
   <p>
    <em>
     “Should you be willing to hand out cookies, the research suggests they will help the listener hear what you have to say next.”
    </em>
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5cdfeff06cee0700086aa202" data-chapterno="4">
  <h1>
   Unconscious biases are often subtle and pervasive, and it takes work to overcome them.
  </h1>
  <div class="chapter__content">
   <p>
    At this point, you may be wondering what an unconscious bias looks like.
   </p>
   <p>
    Well, let’s look at the example of Kimberly Davis, an African American executive and friend of the author. Once, when Davis was attending a convention for female executives, she entered a large room full of white women gathered in groups socializing. As Davis made her way around the room, none of the groups opened up to her. Davis didn’t feel like the groups were purposely or consciously rejecting her. She just assumed they didn’t recognize her as a fellow executive since a black businesswoman probably didn’t conform to their expectations.
   </p>
   <p>
    This is just one example of how subtle and prevalent unconscious biases can be.
   </p>
   <p>
    Or take Joe Lentine, who grew up just outside of Detroit. Lentine came from a white middle-class family. As he got older, he realized how odd it was that he only ever interacted with other white people, despite living so close to a multicultural and diverse city like Detroit.
   </p>
   <p>
    But statistics show that Lentine’s experience is quite common. According to Detroit Area studies taken during the 1980s and 1990s, white families tended to have little to no interaction with their black neighbors, even in predominantly black neighborhoods. The curious nature of this racial bias only struck Lentine after he bonded with a black member of his college fraternity. And this isn’t unusual, as overcoming an unconscious bias often requires making a proactive effort.
   </p>
   <p>
    As he moved on from college and took a job with General Motors, Lentine’s awareness of his racial bias continued to grow as well. To help get rid of it, he made a point of immersing himself in foreign cultures as much as possible by frequently traveling to places like India, Japan and South Korea.
   </p>
   <p>
    In 2009, when Lentine became the owner of Dental Plans Company, he addressed his biases even more proactively by partnering with an organization that helped transgender youth find jobs. He also reached out to his town’s US-Arab chamber of commerce to make sure his business truly reflected the diversity of his community.
   </p>
   <p>
    Just because we have unconscious biases, that doesn’t mean we have to accept them. With some effort, we can address them head-on and change how we interact with the world around us.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5cdff0066cee0700081dc304" data-chapterno="5">
  <h1>
   Privileged people have the greatest power to counter unconscious biases and support minorities.
  </h1>
  <div class="chapter__content">
   <p>
    If you’ve ever overheard a racist comment and assumed it’s not your place to speak up, you may want to think again.
   </p>
   <p>
    The African-American poet Christopher Owens got fed up with the endless number of messages he received on Facebook from people asking him to address someone else’s racist comments. This shows just how many people think that it’s up to people of color to confront racists. Yet, research shows that this isn’t the best solution.
   </p>
   <p>
    A 2003 study by psychologists Alexander Czopp and Margo Monteith showed that objections to racist statements were taken much more seriously when they came from other white people, rather than people of color. According to the researchers this is because there’s a common unconscious bias that associates privileged people with power and so, when they counter a racist remark or behavior, it has a greater impact.
   </p>
   <p>
    This is especially true in the workplace. In a 2016 study done by psychologists and management researchers, 350 North American executives were interviewed to assess how effective they were in promoting diversity within their teams. After speaking with the executives, the researchers surveyed the executives’ bosses to find out how they perceived the ongoing diversification efforts of their employees.
   </p>
   <p>
    The results showed that white male executives are routinely perceived in a positive light, whether or not they’re successful in creating a diverse team. Meanwhile, executives who are women or people of color are far more likely to be harshly criticized.
   </p>
   <p>
    Not only that, but a white man can hire anyone he wishes – be that women, minorities or a team entirely made up of other white men – with little worry of receiving negative scrutiny from his boss. At the same time, a black person is likely to be criticized if they hire other black people, and a woman will be viewed negatively if she hires other women. So, even if women and minority executives are promoting diversity in the company, they could still receive criticism for doing so!
   </p>
   <p>
    Since white males have the power to enact change without getting in trouble, they have a greater responsibility to help curb racism and promote workplace diversity.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5cdff01e6cee0700086aa203" data-chapterno="6">
  <h1>
   It takes time to develop racial identity consciousness, and it is important not to give up too soon.
  </h1>
  <div class="chapter__content">
   <p>
    If you follow popular literature, you may have heard of the American author Jodi Picoult. Picoult sees herself as a progressive person, so she was taken aback when her son Kyle and his partner Kevin explained that she was sure to have unconscious racist biases based on her privileged white upbringing.
   </p>
   <p>
    Prior to this discussion, Picoult equated racism with the behavior of white supremacists. But Kyle and Kevin were eventually able to show Picoult how unconscious bias works. She became so eager to correct this lack of awareness that she enrolled in an anti-racism workshop. It was a lengthy process, but Picoult was able to start recognizing her own biases, and the experience led her to write her 2016 book
    <em>
     Small Great Things
    </em>
    .
   </p>
   <p>
    Picoult’s experience is quite typical, and it points to the three stages of changing your consciousness around racial identity. The first stage is denial. At this point, people must confront their misconceptions. But here’s the problem. Not only do many people refuse to believe they have any prejudices, but also think that racism in general no longer exists. This belief takes time to correct. The second stage is acceptance, and only some people will make it this far. By now, the person will have taken some active measures toward recognizing their biases. This is followed by the third stage: deeper understanding. Here, white people can finally begin to understand that their experience is different from that of people of color. At this stage, a person will also question their own racial identity and what influence it has had on their life.
   </p>
   <p>
    So, if you’re really interested in changing your awareness of racial identity, you should embark on these three stages. Be aware that many people try to skip straight to stage two because they don’t want to confront the ugly truth of their own denial. In fact, when we strongly believe in something, we have an inclination not to examine it too closely.
   </p>
   <p>
    For example, a 2005 study by management scientists Kristine Ehrich and Julie Irwin showed that consumers who are strongly opposed to child labor are likely to overlook how their clothes or electronics are made. Similarily, it can be difficult to stay aware of equality and not be overwhelmed by all the racism in society. But it’s important to look closely and not turn away so that you can make lasting changes to your consciousness.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5cdff0376cee0700081dc305" data-chapterno="7">
  <h1>
   It is not helpful to pretend to be color-blind or to categorize people in any way.
  </h1>
  <div class="chapter__content">
   <p>
    Recently, it’s become increasingly common for people to deflect any suspicion of racism by saying they’re color-blind, or that they don’t see people in terms of color. That may sound laudable, but is this a good thing?
   </p>
   <p>
    In a 2008 study, Harvard University researchers Evan Apfelbaum and Michael Norton showed that this isn’t a sensible attitude to have. Participants were divided into pairs. Some pairs had two white participants, while others had one black and one white participant. The participants were then asked to choose a piece of paper with a face on it. They then had to ask each other questions until they could guess which face the other person chose.
   </p>
   <p>
    In white-white pairs, 51 percent of the participants asked their partner the obvious question of whether the face was that of a black or a white person. But in black-white pairs, many of the white participants pretended to be color-blind, with only 21 percent asking this question. And as it turned out, the “color-blind” participants were perceived by their black partners as being more racist – not less so.
   </p>
   <p>
    Another unhelpful yet common trait is to categorize people as having certain qualities or behaviors based on race. Even if the intention is to be complementary, such stereotypes suggest that certain people have less individuality than others.
   </p>
   <p>
    For example, it’s sometimes said that Asian people tend to be hard-working and well-behaved, or that Asian Americans are wealthy and academically talented. These may seem like nice attributes to have, but they can be detrimental if an Asian American is applying for financial assistance or for a job that requires physical strength. If the people processing these applications believe these stereotypes, this might affect their decision to provide financial aid or hire the applicant.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5cdff0546cee0700086aa204" data-chapterno="8">
  <h1>
   To foster inclusion in companies, listen to all voices and share credit honestly.
  </h1>
  <div class="chapter__content">
   <p>
    There’s a good chance you’ve had the experience of being distracted while someone is telling you something. This inability to really listen to others is a big problem, and the constant presence of distracting technology is only making it worse. If we hope to make our companies truly inclusive, we need to listen carefully to make sure every voice is heard. Now, statistics show that minority voices are at high risk of being ignored.
   </p>
   <p>
    In 2006, when the author was researching her PhD on unconscious bias, she went out on the streets of Boston with a container of jelly beans and asked random people to guess how many were in the jar.
   </p>
   <p>
    To help them find the right answer, the author played them recordings of a wide range of people all providing good advice. The author purposefully recorded actors with voices that were recognizably white, black or Latino. As you may expect, the participants most often took the white man’s advice, while ignoring the other voices. Keep this kind of unconscious bias in mind at your next meeting by making sure you listen to people who aren’t white men.
   </p>
   <p>
    You should also pay attention to who receives credit for work.
   </p>
   <p>
    In a University of Washington study from 1979, psychologists Michael Ross and Fiore Sicoly revealed that individuals who are part of a group project tend to give themselves more credit than an objective project supervisor would. But this also means that people tend to give others
    <em>
     less
    </em>
    credit than they deserve.
   </p>
   <p>
    Now, based on everything that we’ve learned about unconscious bias so far, we can assume that, since minority voices are more likely to be discounted than non-minorities, they’re also likely to receive less credit from their peers on a group project. So the next time you’re wrapping up a successful project at work, make sure that credit is given to everyone who deserves it and that coworkers with a minority background don’t get overlooked.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5cdff06d6cee0700081dc306" data-chapterno="9">
  <h1>
   Final summary
  </h1>
  <div class="chapter__content">
   <p>
    The key message in these blinks:
   </p>
   <p>
    <strong>
     Unconscious biases are very real, even if the vast majority believe they have no prejudices. Research shows that many of us have negative biases against people of color. We’re also less willing to listen to advice that isn’t coming from a white man. It is possible, however, for all of us to increase our level of consciousness on matters of racial identity. To do so, we must be prepared to learn about what life is like for those who don’t have our personal experience and be persistent in questioning our unconscious biases.
    </strong>
   </p>
   <p>
    Actionable advice:
   </p>
   <p>
    <strong>
     Be more selective about the media you consume.
    </strong>
   </p>
   <p>
    The media we take in is crucial to our perception of people, since it provides a window into worlds we’re not familiar with. That’s why it’s beneficial to watch inclusive series and films, such as
    <em>
     All in the Family
    </em>
    ,
    <em>
     Black-ish
    </em>
    or
    <em>
     Modern Family
    </em>
    . If we only engage with series that reflect a limited social reality, such as shows where every character is white and straight, our ability to understand other people decreases. Being picky about your media intake is an effective way to correct your unconscious biases.
   </p>
   <p>
    <strong>
     Got feedback?
    </strong>
   </p>
   <p>
    We’d sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
   </p>
   <p>
    <strong>
     What to read next:
    </strong>
    <strong>
     <em>
      The Laws of Human Nature
     </em>
    </strong>
    <strong>
     , by Robert Greene
    </strong>
   </p>
   <p>
    As the previous blinks make clear, knowing about what goes on in your unconscious mind is key to making better decisions.
    <em>
     The Laws of Human Nature
    </em>
    is the perfect place to continue learning about this topic. It reveals many of the instinctual and unconscious aspects of our everyday behaviors and decisions.
   </p>
   <p>
    A lot of people strive to be the best version of themselves, and a big part of that noble endeavor is to understand what human nature is all about. Once that happens, you can begin to make lasting changes that will improve your life.
   </p>
  </div>
 </div>
</article>
