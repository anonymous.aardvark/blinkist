---
layout: post
title: "Emily Oster - Cribsheet"
description: "Cribsheet (2019) provides a unique and insightful perspective on early-childhood parenting – that of an economist. Given its focus on decision-making, cost and benefit analysis, risk assessment, and data interpretation, the academic discipline of economics provides a surprisingly useful framework for thinking about the difficult decisions that new parents have to make when raising their babies."
image: https://images.blinkist.com/images/books/5cf2b6386cee070008ddbc50/1_1/470.jpg
---

<article class="shared__reader__blink reader__container__content" lang="en">
 <div class="chapter chapter" data-chapterid="5cf2b64f6cee070008ddbc51" data-chapterno="0">
  <h1>
   What’s in it for me? Get a crib sheet to pass one of the ultimate tests of adult life: having a baby.
  </h1>
  <div class="chapter__content">
   <p>
    To avoid the hard work of rote memorization, many students resort to using
    <em>
     a crib sheet
    </em>
    : a small piece of paper where they write down information that will be useful to them on their exams. That information could range from particular facts to general principles about the subject on which they’re being tested. Hidden in their hands, the resulting crib sheets provide the student with a simple but effective technique for cheating their way to academic success. Ethics aside, it’s a rather nifty trick.
   </p>
   <p>
    If only it were so easy to pass the tests that adult life sends our way – especially the most difficult ones, such as giving birth to children and raising them through the first few years of their lives. A crib sheet for parenting – now that would be useful! Imagine having a set of guiding principles at your fingertips, ready to be applied to any tough parenting decisions you need to make.
   </p>
   <p>
    Well, economist Emily Oster has exactly that, and these blinks will give you the crib sheet to
    <em>
     her
    </em>
    crib sheet! But wait a minute – a crib sheet about parenting based on the ideas of an economist? Not a pediatrician or a child psychologist? That sounds like an odd combination. But it actually makes a lot more sense than you might think.
   </p>
   <p>
    In these blinks, you’ll learn
   </p>
   <ul>
    <li>
     how economic reasoning can help you to make difficult parenting decisions;
    </li>
    <li>
     why parenting advice is often informed by questionable research; and
    </li>
    <li>
     what to look for when searching for the most reliable research.
    </li>
   </ul>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5cf2b6646cee070007e047e3" data-chapterno="1">
  <h1>
   It’s hard to make parenting decisions.
  </h1>
  <div class="chapter__content">
   <p>
    The world of parenting advice is an incredibly confusing place, full of conflicting opinions on what to do and how to do it.
   </p>
   <p>
    If you’re a new parent, that’s true right from the get-go, when important questions and decisions confront you immediately after the birth of your child. For example, if your newborn baby is a boy, should you circumcise him? And if you’re the mother, should you “room” with him after birth – that is, have him sleep with you in your hospital room? Or should you send him to the hospital’s nursery?
   </p>
   <p>
    You might get completely contradictory answers to questions like these, depending on which friends, family members, doctors, journalists or online forums you ask. For instance, some will say that you should definitely circumcise; it’s medically beneficial. Others will say that it’s dangerous and unnecessary.
   </p>
   <p>
    Adding to the confusion is the fact that both sides usually come armed with all sorts of evidence in favor of their positions, ranging from peer-reviewed scientific studies and established biological facts to personal anecdotes and that one newspaper article your aunt vaguely remembers reading a couple of years ago.
   </p>
   <p>
    Worse still, people don’t just give their advice to you in a neutral, take-it-or-leave-it sort of way. Instead, they deliver it laden with moral judgment. For example, if you’re a mother, it’s not just that some people think you should breastfeed your baby because the practical benefits outweigh the drawbacks; many of them think you’re an outright bad mother if you resort to feeding your baby with formula.
   </p>
   <p>
    Now, that would be a lot of confusion and pressure to deal with even under normal circumstances – but after you or your partner gives birth, you’re likely to be sleep-deprived, stressed and exhausted. The stakes, meanwhile, will feel incredibly high. After all, you’re trying to ensure the survival and welfare of the fragile little person who’s suddenly become the most important being in your life.
   </p>
   <p>
    So when you’re faced with questions like whether or not to breastfeed, circumcise or room with your baby, how can your frazzled mind figure out which answer is right?
   </p>
   <p>
    Well, if you approach these sorts of questions like an economist, you’ll see that the short answer is that there are never any “right” answers. Or, to be more precise, that’s simply the wrong way of framing the questions in the first place!
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5cf2b6796cee070008ddbc52" data-chapterno="2">
  <h1>
   Economics can help us realize that parenting decisions are personal.
  </h1>
  <div class="chapter__content">
   <p>
    At first glance, there might not seem to be much common ground between the arid academic discipline of economics and the highly personal, practical task of parenting a baby. But there is, in fact, a significant overlap between the two.
   </p>
   <p>
    Parenting revolves around making difficult decisions, and decision-making is one of the main facets of modern economics. For example, economists ask: Given a choice between two services, how does someone decide which one to purchase?
   </p>
   <p>
    For the economist, this comes down to a few factors. First, there are the costs and benefits of the choices involved, which economists call “inputs.” Inputs are both monetary and non-monetary, but let’s start with the monetary ones, since they’re the simplest variables to grasp and quantify.
   </p>
   <p>
    Imagine you can afford to hire a nanny for your child, and you’re trying to decide between doing that and putting your child in daycare. Which option should you choose? Well, the nanny is probably more expensive than daycare, so in terms of financial costs, daycare is likely to be the better choice.
   </p>
   <p>
    But here’s the thing: whether and to what degree the money matters to you depends on your circumstances and preferences. If you’re a middle-class person without a lot of disposable income, the price difference between the two options might matter to you – whereas if you’re rich, it might be trivial.
   </p>
   <p>
    But what about non-monetary inputs? Well, if you’re a middle-class person who strongly prefers the personalized service of a nanny to daycare, then for you, giving that up is a non-monetary cost. Or maybe the social opportunities that daycare can provide your child with are important to you – then giving that up is one of the costs of hiring a nanny.
   </p>
   <p>
    But what if we assume that the monetary and non-monetary inputs are the same for anyone making the daycare vs. nanny decision? Well, even given these same inputs, different people can, will and should arrive at different decisions, depending on how their circumstances and preferences lead them to weigh things.
   </p>
   <p>
    In other words, from an economic standpoint, there’s no single right decision for everyone. One decision can be right for one person and wrong for another, while for the opposite decision its vice versa. It all depends on people’s personal preferences and circumstances, along with how those mesh with the choices at hand.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5cf2b68b6cee070007e047e4" data-chapterno="3">
  <h1>
   Economic reasoning can provide us with a useful framework for parental decision-making.
  </h1>
  <div class="chapter__content">
   <p>
    If modern economics is correct, then there’s no single right answer when you’re making a decision that hinges upon your preferences and circumstances. But then how can you have a crib sheet for parenting?
   </p>
   <p>
    Well, you might not be able to have all of the answers at hand when reality tests you as a parent, but you can equip yourself with a general framework for making the decisions that parenthood forces upon you. This framework can be broken down into a few steps.
   </p>
   <p>
    To begin with, identify the costs and benefits associated with the choices in front of you. For example, let’s say you’re trying to decide between going back to work or staying home with your child after you give birth. What are the costs and benefits of these two choices?
   </p>
   <p>
    Well, that depends on your priorities. If it’s primarily your baby you have in mind, then you’re presumably wondering about the impact of the decision on her childhood development and subsequent adulthood. Which choice will make her happier or more successful in the long run? This is an empirical question; the answer can be established by looking at scientific research that tracks the child-development outcomes of a parent staying at home versus going back to work.
   </p>
   <p>
    If it’s yourself you have in mind, then you’re probably wondering about the impact of these two choices on your own happiness and success. Will staying at home undermine your career or make you feel sick of your child? Or will going back to work make you feel like you’re missing out on the full experience of parenthood? These are partially empirical questions, but they’re also subjective; the answers depend on how you feel about being a parent and how much you want to spend time with your child.
   </p>
   <p>
    Finally, if it’s your family’s budget that you have in mind, then you’re just trying to determine the proverbial bottom line. This is an economic question; the answer depends largely on the monetary calculations involved. For example, how much income would you earn if you went back to work?
   </p>
   <p>
    After determining the answers to each of these different versions of the original question, you then have to take into account your preferences and circumstances in weighing them against each other. Then you can make a decision. But all of that is easier said than done, for reasons we’ll look at in the next blink.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5cf2b6a06cee070008ddbc53" data-chapterno="4">
  <h1>
   Applying the economic decision-making framework to parenting involves assessing trade-offs.
  </h1>
  <div class="chapter__content">
   <p>
    When we’re applying an economist’s decision-making framework to the difficult choices that a parent needs to make, no subject seems more cut-and-dried than finances. Which option will bring more money into your bank account – going back to work, or staying at home with your baby? Some simple calculations can answer that question fairly easily.
   </p>
   <p>
    But even here, the nature and significance of the answer are going to vary a lot depending on your circumstances and preferences, which will shape the trade-offs you’ll be making. Those trade-offs, in turn, will depend on a pair of economic concepts called
    <em>
     opportunity cost
    </em>
    and
    <em>
     marginal value.
    </em>
    Let’s illustrate these ideas through a hypothetical example.
   </p>
   <p>
    Imagine you don’t have a relative to look after your baby, and you live in a country where free childcare isn’t available to you. To go back to your job, you’re going to have to pay for daycare or a nanny.
   </p>
   <p>
    That means that if your salary is low and the cost of childcare in your area is high, you might actually
    <em>
     lose
    </em>
    money by going back to work. Conversely, if your income is greater than the cost of childcare, going back to work will improve your finances – but perhaps not by much. For example, if you’d make $25,000 per year from going back to work and childcare would cost you $18,000 per year, you’d only be netting $7,000.
   </p>
   <p>
    If you need that $7,000, or you love your job or you just want some time to be an independent adult away from your child, then it might make sense to go back to work anyway. But if you don’t need it and you place a lot of value on spending as much time with your child as possible, then the $7,000 probably isn’t worth it. The
    <em>
     opportunity cost
    </em>
    of losing time with your child would be higher than the
    <em>
     marginal value
    </em>
    of the additional $7,000 you’d make from going back to work.
   </p>
   <p>
    But then again, maybe you could use that $7,000 to go on a vacation or save for retirement. Depending on how much you value those things relative to spending time with your child, the benefit could also outweigh the cost.
   </p>
   <p>
    As we’ll see in the next blink, this same reasoning can be applied to non-monetary trade-offs as well.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5cf2b6b06cee070007e047e5" data-chapterno="5">
  <h1>
   Applying the economic decision-making framework to parenting also involves risk assessment.
  </h1>
  <div class="chapter__content">
   <p>
    With many parenting decisions, weighing the potential risks and advantages of one choice versus another should, in theory, be a very rational process. You would simply put the risks on one side of your mental scale and the advantages on the other side, then see which one is heavier, and voila: decision made!
   </p>
   <p>
    In practice, however, our emotions can easily muddle our thinking. That’s because the prospect of putting our children at any risk whatsoever tends to provoke a panicked, knee-jerk reaction in our minds.
   </p>
   <p>
    Consider the decision of whether to share your bed with your baby, also known as
    <em>
     co-sleeping
    </em>
    . If you do this, you’re putting your child at risk. You could roll on top of him and crush his tiny body, or he could get tangled up in your sheets and suffocate. Scientific research also suggests that he’ll be at a higher risk of Sudden Infant Death Syndrome, or SIDS – the tragic phenomenon in which babies die unexpectedly, without any discernible reason.
   </p>
   <p>
    Hearing about this risk, you might think, “Okay, well, that seems pretty clear, then; I shouldn’t share a bed with my baby.” But would you say the same thing about driving in a car with your baby? Probably not – but the risk of infant death from car accidents is considerably higher than that of co-sleeping: 0.20 versus 0.14 per 1,000 births, respectively.
   </p>
   <p>
    Still, 0.14 per 1,000 births
    <em>
     is
    </em>
    a risk – but some risk is unavoidable in life, so that doesn’t automatically mean we should avoid it. The question is whether it’s a risk worth taking. And by now, you can probably anticipate the answer: that depends on your preferences and how they lead you to weigh the benefits against the (potential) costs posed by the risks.
   </p>
   <p>
    What are the benefits to co-sleeping? Well, the main one isn’t for your baby; it’s for you, the parent. If you’re a breastfeeding mother and your baby wakes up in the middle of the night, you don’t have to get up to feed him. You can just roll over, nurse him and let him go back to sleep. You yourself will probably get more sleep as a result.
   </p>
   <p>
    Sleep deprivation is a very common problem among parents, and it can lead to depression, so that’s a considerable benefit. But whether it outweighs the risks of co-sleeping depends on how much you value your convenience, sleep and well-being relative to your baby’s safety.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5cf2b6c56cee070008ddbc54" data-chapterno="6">
  <h1>
   The research that informs parental decision-making is complicated by confounding variables.
  </h1>
  <div class="chapter__content">
   <p>
    The variables of parental decision-making can be split into two columns. In the more subjective, personal column are preferences and circumstances. And then there’s the more empirical, factual column: the costs and benefits of the options that you’re choosing from.
   </p>
   <p>
    You’d think the first group would be more nebulous, while the second side would be clearer. But in reality, things are often the other way around. Determining the actual facts about the potential risks and benefits of, say, breastfeeding can be rather difficult. And that’s not in spite of, but
    <em>
     because of
    </em>
    the fact that there’s a lot of scientific research floating around out there about the topic.
   </p>
   <p>
    Here’s the problem: much of that research centers around collecting data about which parenting practices correlate with which outcomes in the children they affect. For example, there are many studies that correlate breastfeeding with children’s IQs. These studies find that breastfed children tend to have higher IQs than non-breastfed children – seven points higher on average, to be exact.
   </p>
   <p>
    But, as the saying goes, correlation does not necessarily equal causation. Just because two things like breastfeeding and a higher IQ are often found together doesn’t mean one of them causes the other. There could be additional,
    <em>
     confounding
    </em>
    variables involved – variables that are common to both breastfeeding and IQ, and that provide the real causal link between the two. For example, in most developed societies, women who breastfeed tend to have higher IQs, incomes and educational levels than women who don’t. Each of those variables is also associated with higher IQs in children.
   </p>
   <p>
    Now, if you’re a researcher, you can try to adjust for these variables when analyzing your data. For instance, to adjust for the education variable, you can compare only the children of mothers with the same levels of education. The more that researchers adjust for the variables, the less of a correlation they find between breastfeeding and higher IQ.
   </p>
   <p>
    After adjusting for all of the variables they can identify, some researchers still find a slight correlation between the two – but some skepticism is warranted here.
   </p>
   <p>
    Reality is an incredibly complicated place, with myriad variables involved in any given set of phenomena. If the correlation between breastfeeding and IQ decreases with each additional variable you adjust for, what’s more likely – the possibility that you’ve adjusted for every conceivable variable and there actually is a causal connection between the two, or the possibility that there are simply more variables than you can think of and that would decrease the correlation even further, to the point where it eventually vanishes or becomes negligible?
   </p>
   <p>
    Probably the latter.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5cf2b6e46cee070007e047e6" data-chapterno="7">
  <h1>
   Large-scale randomized controlled trials provide the most reliable data for parental decision-making.
  </h1>
  <div class="chapter__content">
   <p>
    If you want your parental decision-making process to be evidence-based, you should review the scientific research available on the choices you’re weighing – but when you do that, you should keep in mind that not all research is of equal value. This brings us to another important part of applying economic reasoning to parental decision-making: weighing not only your choices, but also the evidence that informs those choices.
   </p>
   <p>
    As we’ve just seen, all research is muddled by the existence of confounding variables and the possibility that there are additional variables that the researchers have simply failed to identify and adjust for. But some types of research do a better job of dealing with this problem than others.
   </p>
   <p>
    If they were competing in the scientific equivalent of the Olympics, large-scale
    <em>
     randomized controlled trials
    </em>
    would win the gold medal. To conduct such a trial when researching the benefits of breastfeeding, you’d recruit a large number of mothers and randomly split them into two groups: a
    <em>
     treatment group
    </em>
    of mothers who breastfeed and a
    <em>
     control group
    </em>
    of mothers who don’t breastfeed.
   </p>
   <p>
    Because you’ve randomized which mothers are in which group, the mothers of the treatment group will tend to have the same characteristics on average as the mothers of the control group. The only difference will be whether or not they breastfeed. You can thus test for this variable and this variable alone. If you then find a correlation between breastfeeding and a particular outcome, such as a lowered risk of gastrointestinal disorders, you can thus be relatively confident that there’s an actual causal link between the two phenomena.
   </p>
   <p>
    The larger the groups, the truer this will be. And if, in addition to a correlation between two phenomena, you can also establish a causal mechanism that explains the link between them, then you can feel even more confident about the causality at issue. For example, breastfeeding is linked to a decreased risk of a mother developing breast cancer – and this link can be causally explained by the fact that breastfeeding lowers the mother’s levels of estrogen production, which is a risk factor in developing breast cancer.
   </p>
   <p>
    To date, there’s been only one large-scale randomized controlled trial conducted about breastfeeding. It established only two significant links between breastfeeding and child health outcomes: a four-percent decrease in diarrhea and a three-percent reduction in skin rashes, such as eczema. It looked at a range of other possible effects, including the alleged boost in children’s IQs, and it didn’t find any significant correlations.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5cf2b6fa6cee070008ddbc55" data-chapterno="8">
  <h1>
   Well-conducted observational studies can also provide reliable data for parental decision-making.
  </h1>
  <div class="chapter__content">
   <p>
    If the gold medal for research goes to randomized controlled trials, what kind of research gets the silver?
   </p>
   <p>
    Second place goes to the type of research that involves
    <em>
     observational studies
    </em>
    . To conduct such a study for breastfeeding, you’d basically just collect a bunch of data about both breastfeeding and non-breastfeeding mothers and their children, then compare the two groups and see what kind of different outcomes you find.
   </p>
   <p>
    The larger the groups, the more the differences between their respective members will tend to wash out on average. And the more you control for confounding variables, the closer you can come to achieving the ideal scientific study, in which all variables are controlled except for the one you’re testing.
   </p>
   <p>
    Thus, the larger an observational study’s sample size and the more it controls for confounding variables, the more confidence you can feel in the results. Conversely, the smaller the sample size and the less it controls for those variables, the less confidence you should feel in its conclusions. If the sample size is really small, skepticism is probably warranted.
   </p>
   <p>
    The best observational studies tend to be ones that compare siblings from the same families – for example, a first child who was breastfed and a second child who wasn’t. Because these children grew up in the same families with the same socioeconomic backgrounds, many of the confounding variables that could be at issue when comparing breastfed with non-breastfed children just sort of control themselves in these studies.
   </p>
   <p>
    The second-best observational studies are those that collect lots of data about the children’s familial and socioeconomic backgrounds. These confounding variables can then be controlled for to a large extent – although, as we’ve seen, there will always be nagging questions about whether there are variables that are being overlooked.
   </p>
   <p>
    If a number of large-scale, well-controlled observational studies come to the same conclusions, then you can be fairly confident in them. In the case of breastfeeding, these studies seem to establish that breastfeeding reduces the risk of children developing ear infections – and that’s about it.
   </p>
   <p>
    In the popular imagination and discourse, there are many other benefits that breastfeeding allegedly provides to children, such as a reduced risk of diabetes, juvenile arthritis, meningitis, obesity, pneumonia and cancer. Unfortunately, there simply isn’t enough reliable data from randomized controlled trials or observational studies to back these claims up. That doesn’t mean they’re necessarily false; it just means there’s currently no compelling reason to believe them.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5cf2b70d6cee070007e047e7" data-chapterno="9">
  <h1>
   When informing your decisions as a parent, you should be skeptical of case-control studies.
  </h1>
  <div class="chapter__content">
   <p>
    Finally, it’s time to give out the bronze medal for scientific research into the potential risks and benefits of certain parenting practices. Third place goes to the type of research that involves
    <em>
     case studies.
    </em>
   </p>
   <p>
    To conduct a case-control study, you basically go out and look for children who exhibit the same outcomes or symptoms. Then you try to find out all of the things they have in common. After adjusting for confounding variables, you see what you have left, and then you try to identify a causal connection between whatever remains and the outcomes or symptoms in question.
   </p>
   <p>
    For example, in 1998, a then-doctor named Andrew Wakefield conducted a case-control study with 12 children who exhibited symptoms of autism. Each of them had been given a vaccine for measles, mumps and rubella. He alleged a causal connection between the symptoms and the vaccine, which had to do with digestion. This study helped to spark the anti-vaccine movement, in which growing numbers of parents are refusing to vaccinate their children.
   </p>
   <p>
    Unfortunately, case studies are far more problematic than even very small observational studies. Like their observational counterparts, the data of case studies can be muddied by unthought-of confounding variables and underlying differences among the people being studied. Now, remember, the larger the sample size, the more those differences tend to wash out on average. If we’re talking about a sample size of thousands of mothers or babies, then we don’t have to worry too much about the differences among them.
   </p>
   <p>
    But a sample size of just 12 children? That’s much too small. And in the case of Wakefield’s study, it was small for a reason: scientific malfeasance. For his sample, Wakefield purposely selected children whose circumstances supported his conclusion and excluded children who didn’t. He also falsified his data, changing the dates of the onset of the children’s autism symptoms so that they appeared to arise closer to the times when the children were vaccinated.
   </p>
   <p>
    But such falsification isn’t always intentional. For example, case studies often involve asking parents to recall actions they took in the distant past. Let’s say your child turns out to be an early reader, and some enterprising researchers want to find out the reason why by conducting a case study. They might ask you when you started reading to your child and how often you read to her. But that might have been years ago, and your memory could be faulty or colored by subsequent events.
   </p>
   <p>
    So in summary: Trust the findings of well-conducted randomized control trials and observational studies, and approach other research with a healthy dose of skepticism.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5cf2b76c6cee070007e047e8" data-chapterno="10">
  <h1>
   Final summary
  </h1>
  <div class="chapter__content">
   <p>
    The key message in these blinks:
   </p>
   <p>
    <strong>
     When making parental decisions in the first few years of your child’s life, you need to weigh two sets of factors. The first set consists of your personal preferences and circumstances. The second set consists of the potential costs and benefits of your choices to your child, yourself and your family. In weighing these factors, you can inform yourself with scientific research, which varies in reliability. You can be confident about well-conducted randomized control trials and observational studies, but you should be skeptical about case-control studies.
    </strong>
   </p>
   <p>
    Actionable advice:
   </p>
   <p>
    <strong>
     Relax.
    </strong>
   </p>
   <p>
    Worried about the possibility of her baby being stung by a bee for the first time on a vacation she was planning, the author received the following piece of advice from her doctor: “I’d probably just try not to think about that.” The author considers this the single-best piece of parenting advice she ever received from anyone. There are millions of things that
    <em>
     could
    </em>
    happen to your child, and most of them are pretty unlikely. If you try to anticipate and prepare for all of them, you’ll just make yourself anxious, which, ironically, will probably make you a worse parent, since you’ll be too frazzled to think about your parenting decisions properly. So follow the doctor’s advice: Try to avoid thinking about everything that could go wrong with your child, and try to simply enjoy your time with him or her instead.
   </p>
   <p>
    <strong>
     Got feedback?
    </strong>
   </p>
   <p>
    We’d sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
   </p>
   <p>
    <strong>
     What to read next:
     <em>
      Bringing Up Bébé,
     </em>
     by Pamela Druckerman
    </strong>
   </p>
   <p>
    After reading these blinks, you’ve come away with a good idea about how to make parenting decisions when raising your baby, and you’ve learned that your own personal preferences are some of the key factors to consider. But what are your preferences? What kind of parenting style do you actually want to adopt? What would the ideal situation with your baby look like ?
   </p>
   <p>
    Well, how does this sound: a child who sleeps through the night, eats her vegetables and does what you tell her to do. If that sounds appealing, then take a trip to France with our blinks to
    <em>
     Bringing Up Bébé
    </em>
    , which looks at how and why French parents raise cooperative children without breaking a sweat.
   </p>
  </div>
 </div>
</article>
