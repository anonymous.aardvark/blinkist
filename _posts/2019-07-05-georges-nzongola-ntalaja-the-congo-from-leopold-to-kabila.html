---
layout: post
title: "Georges Nzongola-Ntalaja - The Congo from Leopold to Kabila"
description: "The Congo from Leopold to Kabila (2002) is the history of the Congolese democratic movement in the twentieth century. The history begins with Belgian colonial rule, working its way through Mobutu’s reign of terror, before looking at the Congo Wars and concluding with the prolific unrest still rampant at the turn of the century. This survey illuminates how exploitative external interests and internal weaknesses have hampered the Congolese democratic movement and proposes how it might still advance."
image: https://images.blinkist.com/images/books/5cfb9c726cee070007f2802b/1_1/470.jpg
---

<article class="shared__reader__blink reader__container__content" lang="en">
 <div class="chapter chapter" data-chapterid="5cfb9c886cee070007117666" data-chapterno="0">
  <h1>
   What’s in it for me? Unpick one of the most complex and interesting histories of Africa.
  </h1>
  <div class="chapter__content">
   <p>
    The mighty Congo River in central Africa is 4,380 kilometers long. In the English-speaking world, it is associated with one of the greatest pieces of English literature: Joseph Conrad’s
    <em>
     Heart of Darkness
    </em>
    . The river gave its name to two modern countries that border it, the Republic of the Congo and the massive Democratic Republic of the Congo (DRC). These blinks look at the fascinating and terrifying history of DRC as its people fought for their democratic rights.
   </p>
   <p>
    Like much of Africa, DRC emerged out of decades of colonialism and interference by European powers, before gaining independence. Even after independence, many ex-colonies found themselves essentially still subject to external coercion. DRC was no exception to that trend once it was granted independence in 1960.
   </p>
   <p>
    Throughout the Cold War, DRC found itself a mere pawn in a global struggle. In its fight against global Communism, the supposedly democratic USA supported the installation of the strong man General Mobutu, whose appointment stalled the progress of the democratic movement for decades. To this day, the fight for democracy in the Congo continues.
   </p>
   <p>
    In these blinks you’ll learn:
   </p>
   <ul>
    <li>
     why the colonial racial mentalities of the Belgians led to the Rwandan genocide;
    </li>
    <li>
     how the US stymied parliamentary democracy in the Congo; and
    </li>
    <li>
     why Mobutu renamed the country “Zaire” during his dictatorship.
    </li>
   </ul>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5cfb9c9c6cee070007f2802c" data-chapterno="1">
  <h1>
   After King Leopold II’s reign of terror, the Congo became a colony that fueled Belgian economic development.
  </h1>
  <div class="chapter__content">
   <p>
    In the middle of Africa lies the massive territory today known as the Democratic Republic of the Congo (DRC). It originally comprised numerous African kingdoms and the majority of its 250 different ethnic groups spoke varieties of Bantu languages.
   </p>
   <p>
    During the “Scramble for Africa” that began in the nineteenth century, the major European powers sought to colonize and control swathes of Africa for profit. In 1885, King Leopold II of Belgium claimed the Congo for his own personal territory. Leopold’s cover was a humanitarian mission aimed at the region’s inhabitants – but there was nothing humanitarian about it.
   </p>
   <p>
    Instead, what emerged was an inhumane system with the sole purpose of filling Leopold’s personal coffers. And there was much to exploit in this resource-rich region. Leopold’s new “subjects” in the Congo Free State were coerced into extracting rubber and minerals.
   </p>
   <p>
    It is estimated that 10 million people were murdered as part of this exploitation. Gruesomely, mutilation and rape were used routinely and systematically as punishment when extraction quotas were not met or when slaves refused to work.
   </p>
   <p>
    The rest of the world slowly became aware of the horrors that were taking place. Joseph Conrad’s 1899 novella
    <em>
     Heart of Darkness
    </em>
    was, for instance, famously set there.
   </p>
   <p>
    The Congo Reform Association (CRA), founded in the UK in 1904, aimed to instigate an international movement protesting Leopold’s rule and succeeded in winning over the US Government. International diplomatic pressure finally forced Leopold to hand over his personal fiefdom to the Belgium parliament in 1908.
   </p>
   <p>
    Although this hand over of control was theoretically a step forward, Belgian’s rule was still colonial in nature; Belgian economic growth was fueled by the brutal oppression of Congolese people and the stripping of the region’s natural resources. Beyond rubber, mineral resources such as copper, gold, diamonds and uranium were plundered. Timber was also highly sought after, as were agricultural products such as coffee, tea and cotton.
   </p>
   <p>
    The legacy of external colonial interests in the Congo’s natural resources is still felt to this day. In fact, it may explain why democracy has struggled to gain a foothold ever since.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5cfb9cb16cee070007117667" data-chapterno="2">
  <h1>
   The democratic movement in the Congo emerged from an alliance between anti-colonialism factions.
  </h1>
  <div class="chapter__content">
   <p>
    The new Belgian rulers of the Congo were pretty contented. As far as they were concerned their “model colony” was just that. Unfortunately, this perception was far removed from reality.
   </p>
   <p>
    For starters, they were still colonizers employing all the usual tricks of repression. Most notably this included a
    <em>
     divide and rule
    </em>
    strategy designed to preemptively strangle any possibility of revolt. As part of this, they bribed local kings to betray their own subjects to the colonizers.
   </p>
   <p>
    In spite of these efforts, an atomized anti-colonial resistance still materialized. Specifically, this opposition was found in three strata of Congolese society.
   </p>
   <p>
    The first stratum was led by colonial army mutineers, African chiefs and professional soldiers. They displayed their resistance to colonial rule as early as 1892.
   </p>
   <p>
    Secondly, there was the Kimbanguist movement, a religious movement founded in 1921 by the Baptist prophet Simon Kimbangu who preached liberation from colonial rule. His ideology was pan-African in nature. Even after Kimbangu’s arrest, the movement stayed strong.
   </p>
   <p>
    The third stratum consisted of peasants and workers, who demanded better wages and working conditions from their colonial rulers. This was not resistance against colonial rule in and of itself, but represented a strong critique of its operation.
   </p>
   <p>
    The Congolese democracy movement proper was finally born, after some time, in 1956, with these three factions joining to fight together for independence. The added impetus came from leadership among the so-called
    <em>
     évolués
    </em>
    , who were an educated bourgeoisie that the racist Belgian colonialists considered to be a “more evolved” African elite. The size of demonstrations and public rallies assisted the évolués in negotiating independence with the colonialists. No doubt the perceived threat of possible violence also helped.
   </p>
   <p>
    But then, on 4 January 1959, violence did emerge. The Kinshasa uprising left an estimated 300 people dead, leading the Belgians to realize their limited ability to maintain control over the large colony. Independence was then just a matter of time.
   </p>
   <p>
    The Belgian Congo finally became independent on 30 June 1960. It was now to be known as the Republic of the Congo. Patrice Lumumba, a Congolese nationalist, was elected as its first Prime Minister.
   </p>
   <p>
    Despite these developments, however, change was not entirely forthcoming. The Republic of Congo still found itself at the whim of global powers willing to use their influence to sway domestic affairs in the country.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5cfb9cc36cee070007f2802d" data-chapterno="3">
  <h1>
   The Congo Crisis began as messy decolonization but ended with international forces engineering a new status quo.
  </h1>
  <div class="chapter__content">
   <p>
    Lumumba was installed as the new Prime Minister, but it didn’t take long for the first cracks in his government to form; the “Congo Crisis” had begun.
   </p>
   <p>
    What precipitated the crisis was a power vacuum. Only a few trained bureaucrats remained after Belgian administrators left in 1960, which meant that tribal leaders suddenly found themselves more powerful than Lumumba’s own government.
   </p>
   <p>
    It was then, in the midst of this political confusion, that Congolese soldiers staged a mutiny against their superiors. These higher ranks were actually mostly made up of white Europeans who, despite the country’s newly won independence, still held the strings of power.
   </p>
   <p>
    Then, to compound the crisis, the country’s two most affluent provinces, Katanga and Kasai, attempted to secede.
   </p>
   <p>
    Lumumba was stuck, even appealing to the UN for help. He was rebuffed, and had to look for help from elsewhere – his new ally would be the Soviet Union.
   </p>
   <p>
    Suddenly, the birth pangs of independence had morphed into a proxy conflict for the Cold War. The UN quickly dropped its earlier reticence under the threat of Communist expansion into Africa.
   </p>
   <p>
    The US – with support from Belgium and the UN – set about trying to replace Lumumba with a moderate. Of course, by “moderate,” they meant someone they could manipulate more efficiently for their own economic and political ends. There would be no place for a nationalist figure like Lumumba, who desired a strong central government and mass political parties. The US settled on Colonel Joseph-Désiré Mobutu, backing him when he led a coup.
   </p>
   <p>
    The coup was a success. Lumumba was arrested on 14 September 1960 and replaced with Prime Minister Moïse Tshombe, who had himself been a leader during the Katanga secession. Mobutu, meanwhile, was installed as the head of the military.
   </p>
   <p>
    Despite his imprisonment, Lumumba’s supporters outside continued to conspire on his behalf. By January 1961, Lumumba was still deemed to be such a threat that he was beaten and shot by the Katangan military. Belgian officers stood by and watched.
   </p>
   <p>
    In many ways, the brutal events of the Congo Crisis are a microcosm of how the country was viewed throughout the twentieth century. There, world events seemed to crystallize into violence, in a region that was little short of being a powder keg.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5cfb9cd46cee070007117668" data-chapterno="4">
  <h1>
   The “second independence” movement failed due to internal weaknesses and external interference.
  </h1>
  <div class="chapter__content">
   <p>
    The Congo Crisis had come to an end. However, US interference had not succeeded in putting the cork back in the bottle. After all, this new “moderate” government was simply taking instructions from abroad, and it stank of neocolonialism. It did not take long before a “second independence” movement emerged. Like the first, it demanded democracy, economic rights for the non-ruling class and freedom from foreign powers.
   </p>
   <p>
    The second independence movement began in earnest in 1963, with resistance revolving around two separate fronts.
   </p>
   <p>
    In the western Kwilu province, a guerrilla movement was led by Pierre Mulele who had been trained in guerrilla warfare in China between 1962 and 1963. Mulele’s guerrilla campaign began in January 1964. Mulele emphasized that his soldiers would not steal as they moved through the country, hoping to distance his movement from the greed and corruption that had marked Congolese politics to date.
   </p>
   <p>
    Mulele had a problem, though. His movement lacked military resources, and support from outside the country was not forthcoming. The fact that this diffuse front was not based in a large city but rather spread throughout his home province also impeded outside assistance. It made it seem like his revolution was unrelated to events in the rest of the country.
   </p>
   <p>
    The eastern independence movement was different. It was led by a group of intellectuals and went by the name of the Conseil National de Libération (CNL).
   </p>
   <p>
    The CNL was effective in terms of military strategy, defeating provincial governments across eastern Congo. Unfortunately, their strategy for filling the subsequent political vacuum was lacking, and the group splinted as they vied for power amongst themselves. Nepotism, repression and corruption were soon rife.
   </p>
   <p>
    Aside from the inherent frailties of the second independence movement, its demise was assured by counterinsurgency led by the US.
   </p>
   <p>
    The US became involved in the Congo for the exact same reason as it had before. Namely, the conflict had become a proxy for the Cold War. Eastern and western Congolese resistance movements acquired the backing of the Soviet Union and its allies. Consequently, the Western allies once more sought to redirect power to governing moderates.
   </p>
   <p>
    Therefore, in November 1964 Prime Minister Moïse Tshombe, backed by western financial and military support, instigated Operation Red Dragon. This counterattack successfully vanquished the CNL regime.
   </p>
   <p>
    It was only a matter of time before the second independence movement was snuffed out. The final act came on 3 October 1968, when Mobutu’s generals assassinated the guerrilla leader Mulele.
   </p>
   <p>
    If the second independence movement did succeed in doing anything, it was in establishing a culture of resistance to illegitimate state authority – a culture that defines Congolese politics to this day.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5cfb9ce46cee070007f2802e" data-chapterno="5">
  <h1>
   Mobutu was propelled into power by the US in 1965. His rule left the Congo in moral and economic decay.
  </h1>
  <div class="chapter__content">
   <p>
    For champions of democracy, the repression of the second independence movement might have seemed like the light had gone out. However, in the mid-1960s it briefly looked as though the momentum would swing back in their direction. In 1964 Tshombe and his moderate allies enacted a new constitution. Then, in May 1965, the first national elections without violence or need for outside observers since independence were held.
   </p>
   <p>
    The US, however, had other ideas. For them, parliamentary democracy was too “chaotic.” They saw this new moderate leadership as too weak to prevent the country dissolving. In the name of stability, then, the Western powers went in search of a strongman to keep the region secure and amenable to their interests.
   </p>
   <p>
    Their candidate was General Mobutu, who the CIA had already begun grooming in the early 1960s when he headed Lumumba’s army.
   </p>
   <p>
    On 24 November 1965, a coup placed Mobutu in power.
   </p>
   <p>
    An early decision Mobutu made was to rename the country the Republic of Zaire, meant to reflect efforts for authentic Africanization and decolonization. “Zaire” comes from a local term for the Congo river. Mobutu then began a drive – known as Zairianization – to rid the country of colonial-era influences.
   </p>
   <p>
    But it was a scam. In fact, the entire effort was supported by the West.
   </p>
   <p>
    Mobutu was able to amass a huge amount of personal wealth, achieving, essentially, a form of autocratic kleptocracy. Profits generated from mines and petroleum sales disappeared into his pockets, and those of his ruling elite. By 1975, the country – the 11th largest in the world, and rich in natural resources – was in tatters. The economy had collapsed, inflation was soaring, and starvation was widespread.
   </p>
   <p>
    In contrast, Mobutu and his circle were pampered, with splendid homes, expensive gifts and lavish entertainment with ill-gotten gains as the norm. The corruption became known as “Zairian sickness.”
   </p>
   <p>
    Mobutu’s growing number of opponents had a clear-cut political objective: the elimination of corruption, and a new and moral order.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5cfb9cf46cee070007117669" data-chapterno="6">
  <h1>
   Mobutu lost foreign backing after the Cold War, allowing the movement for multiparty democracy to advance.
  </h1>
  <div class="chapter__content">
   <p>
    Ironically, external forces would ultimately be how Mobutu’s dictatorship was brought to an end. The groundwork was laid, nevertheless, by numerous Congolese factions and organizations that fought for multiparty democracy.
   </p>
   <p>
    The struggle had been building since the first decades of Mobutu’s rule, with the Catholic Church and political groups in exile playing no small part. A student movement also pushed democratization of processes on campuses.
   </p>
   <p>
    The first inroads were taken during the Shaba Wars of 1977 and 1978, when left-wing Katangese rebels from Angola in the southwest invaded Zaire. As a consequence, Mobutu sought military assistance from Western powers – a request which amply demonstrated just how tentative his grip on his own country really was. The favor, it turned out, had conditions. In return for US support, Mobutu had to agree to political reform, specifically, to not rig the 1977 election so that only his candidates were selected as parliamentarians.
   </p>
   <p>
    As a direct consequence, the new parliament thought it had found its own assertive voice. In 1980, 13 of the new members sent a 52-page letter to Mobutu, demanding political reform, including the creation of an extensive Sovereign National Conference (CNS) to debate the future of society and the country. They might have succeeded, but already the US had shifted its gaze elsewhere, and there was no one to compel Mobutu to act receptively to this demand for democracy. The “Group of 13” were arrested, tortured and banished.
   </p>
   <p>
    It was ultimately the end of the Cold War which signaled that multiparty democracy was possible in Zaire.
   </p>
   <p>
    Mobutu had only been able to maintain his control with support from the US and other Western powers, who hoped that his regime would be a defense against communists in Africa. But once the Soviet Union dissolved, there was no reason for the US to rally behind him.
   </p>
   <p>
    Finally, on 24 April 1990, Mobutu yielded to internal and external pressure. He announced that a transition to multiparty democracy was on the cards, and that the Sovereign National Conference would meet to further that agenda.
   </p>
   <p>
    In reality, however, Mobutu was not in the slightest bit inclined to relinquish his power.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5cfb9d056cee070007f2802f" data-chapterno="7">
  <h1>
   The CNS created a united front for democracy, but it failed to implement a framework.
  </h1>
  <div class="chapter__content">
   <p>
    The CNS did not operate by halves. Its 2,842 delegates came from all rungs of society, with TV and radio broadcasters gathering to communicate proceedings to the entire country. Everyone felt that it was sure to be a major landmark in Congolese history but, ultimately, it was unsuccessful.
   </p>
   <p>
    The conference sought to examine the country’s past, evaluating how expectations for independence had fallen flat. Then, it aimed to look forward, to find a way out of the political, social, economic, cultural and moral crisis that engulfed the country.
   </p>
   <p>
    One of the conference's primary tasks was to establish a visionary framework for Zaire that would assist in the transition to democracy. The framework would consider the country’s future democracy and guidelines for the operation of multiparty democracy.
   </p>
   <p>
    The CNS’s goals were ambitious, and it was certainly successful when it critiqued the country’s past. However, it fell short in getting its framework implemented, or in curbing Mobutu’s dictatorship.
   </p>
   <p>
    The first stumbling block was Monsignor Monsengwo. He was head of the CNS and, as it turned out, a bit of a political opportunist. He went out of his way to sabotage the CNS by deferring to Mobutu.
   </p>
   <p>
    Secondly,
    <em>
     le compromis politique global
    </em>
    was useless. This document was supposed to detail the two-year government transition, treading a careful path of political compromise. However, it was riddled with loopholes. Its drafters were much more concerned with which one of them would become the next prime minister than in actually writing decent policy.
   </p>
   <p>
    Finally, Mobutu used his powers to quell the conference and its desire for Zaire to become a democracy. He presented his own candidate for prime minister, successfully managing to throw the election into disorder. Then, on 12 and 13 August 1992, he bombed the Ugandan People’s Defence Force headquarters in Limete in the West of Zaire. The aim was to use the bombing to call a state of emergency and suspend the conference. In this, he failed.
   </p>
   <p>
    Mobutu’s candidate ultimately lost to Étienne Tshisekedi, who was elected on 15 August 1992 with 71 percent of the vote.
   </p>
   <p>
    But success was short lived. As international support was lacking, Mobutu was able to precipitate his third coup on 1 December 1992. The CNS was shut down five days later.
   </p>
   <p>
    In the event, Mobutu held on to power for another five years. But it was not through lack of determination from the people to see change – the US had decided to intervene once more, preoccupied with its interests alone.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5cfb9d176cee07000711766a" data-chapterno="8">
  <h1>
   The conflict in Rwanda brought about Mobutu's fall from power. The First and Second Congo Wars followed.
  </h1>
  <div class="chapter__content">
   <p>
    The failure of the CNS signaled the return of dictatorship to Zaire. Things were also taking a turn for the worse in the neighboring states of Rwanda and Uganda.
   </p>
   <p>
    The roots of the ethnic violence that emerged lay in the colonial era. Rwanda had been home to two once amicable ethnic groups, the Hutus and the Tutsis. However, once it became a Belgian territory, individuals’ ethnicities were required to be stated on identity cards. The Belgians also decided that they would rule Rwanda through the Tutsis, turning them into a ruling elite.
   </p>
   <p>
    By the end of the 1950s, a Hutu counter-elite began to rise up. They demanded emancipation from the Tutsis – whom they saw as their oppressors – and the formation of a Hutu republic. By the 1990s this became a call for full-scale ethnic cleansing. Millions of Tutsis sought refuge in neighboring countries as the Rwandan genocide of 1994 rapidly gained traction. Between 800,000 and one million Tutsis, 70% of the ethnic group, were killed by the Hutu-majority government.
   </p>
   <p>
    The genocide only ceased when the Rwandan Patriotic Front (RPF), mostly comprised of soldiers from the Tutsi diaspora in Uganda, entered the fray. They defeated the Rwandan Armed Forces (FAR) and took control of the country. Aware that the country’s Tutsis might use their new freedom for ill, one million Hutus fled over the border into Congolese Zaire.
   </p>
   <p>
    The Hutu presence in the Congo gave the military apparatus in both Rwanda and Uganda an excuse to enter Zaire. In 1996, in the name of border security, a coalition force invaded the country once they were given the go-ahead by the US.
   </p>
   <p>
    The coalition was led by Congolese rebel Laurent Kabila, a Tutsi ally, there to overthrow Mobutu and end his crumbling regime.
   </p>
   <p>
    Since the 1960s, Kabila had been an active leader in Congolese radical leftist groups. He had all the necessary insider knowledge and was well placed to assist Rwanda and Uganda in their invasion.
   </p>
   <p>
    However, the coalition force’s true motive, which was kept secret, was merely to plunder the Congo for its natural resources. This conflict, which is now known as the First Congo War, resulted in the murder of thousands of Hutus in the Congo.
   </p>
   <p>
    Mobutu fled the country on 16 May 1997. Just one day later, Kabila was installed as president.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5cfb9d296cee070007f28030" data-chapterno="9">
  <h1>
   Now that conflict is over, the future of the Congo depends upon national dialogue.
  </h1>
  <div class="chapter__content">
   <p>
    The country – now renamed the Democratic Republic of the Congo – was left weakened after the war with Rwanda and Uganda.
   </p>
   <p>
    The Congolese people were vulnerable; a new enemy had emerged in the shape of their own president. Kabila refused to act as a puppet for Rwanda and Uganda in their desired control over the Congo’s resources, as had been intended. Instead, Kabila was keen to be seen as a president in his own right, cutting off ties with the Rwandans and Ugandans to establish his dictatorship.
   </p>
   <p>
    The two countries did not take this news well and invaded in 1998, resulting in the Second Congo War.
   </p>
   <p>
    However, the Rwandans and Ugandans did not meet with as much success as before. The very bloody war dragged on to 2003. Millions perished, mostly through malnutrition; an estimated 1,700,000 Congolese died between 1998 and 2000 alone.
   </p>
   <p>
    The democracy movement at this time faced a serious struggle. It somehow had to contend with both the threat of external invasion, as well as Kabila’s own Congolese authoritarianism.
   </p>
   <p>
    Kabila had begun by rendering political parties illegal, therefore fracturing the democratic movement. While some elements sought to resist Kabila, others turned their efforts to face down external aggression – resistance against which has taken on many forms. Civil society organizations have led a nonviolent campaign, while rebel groups, such as the Mai-Mai and Simba, have turned to armed struggle.
   </p>
   <p>
    However, neither faction has had the resources or political structures in place to become truly effective.
   </p>
   <p>
    Laurent Kabila was assassinated in 2001. The circumstances remain unclear but, supposedly, the act was committed by his bodyguard Rashidi Kasereka – a child soldier.
   </p>
   <p>
    Soon after, Kabila’s son Joseph Kabila assumed the office of president.
   </p>
   <p>
    If there is a lesson here, then it is that the Congolese people remain at the whim of greed, imperialism and war. The interminable conflicts surely prove that democracy must be how fairness and order are to be established. Only this can ensure the just distribution of wealth, and guarantee human rights for people of all classes.
   </p>
   <p>
    At the time of publication in 2002, the author strongly advocated that a Congolese national dialogue, such as that which the CNS desired, be instituted. It could signal the route to a democratic future.
   </p>
   <p>
    But it will take more than this. If DRC is ever going to transition to democracy, the country and the international community cannot allow violent authoritarian leaders to effect change. Instead, reconciliation, inclusivity and justice must be the watchwords to take DRC forward.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5cfb9d446cee07000711766b" data-chapterno="10">
  <h1>
   Final summary
  </h1>
  <div class="chapter__content">
   <p>
    The key message in these blinks:
   </p>
   <p>
    <strong>
     Throughout the twentieth century, the ordinary people of the Congo sought to institute democracy wherever possible. From King Leopold II onward, whether the United States or Rwandan forces, external players have used humanitarian and political pretexts to push their political and economic agendas on the country. In order to ensure a future in which the basic needs of all Congolese people are met, rulers need to listen to the people. The development of a true democracy in which civilians determine electoral processes and systems of governance is possible, but it will take serious effort.
    </strong>
   </p>
   <p>
    <strong>
     Got feedback?
    </strong>
   </p>
   <p>
    We’d sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
   </p>
   <p>
    <strong>
     What to read next:
    </strong>
    <strong>
     <em>
      The Hot Zone
     </em>
    </strong>
    <strong>
     , by Richard Preston
    </strong>
   </p>
   <p>
    The history of the Democratic Republic of the Congo is complex and fascinating. After reading these blinks, you’re sure to know more about the region than you did before. No doubt your appetite has been whet. This is where the blinks to
    <em>
     The Hot Zone
    </em>
    come in, which explore the origins of the Ebola virus that recently impacted so many people in the Congo and other states in the region. They also explain how the virus spreads, its origins and its possible future effects.
   </p>
  </div>
 </div>
</article>
