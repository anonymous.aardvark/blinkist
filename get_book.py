#!/usr/bin/env python3

import json
import os
import re

import requests
from bs4 import BeautifulSoup


def main():
    response = requests.get("https://www.blinkist.com/nc/daily")
    soup = BeautifulSoup(response.text, "html5lib")

    book = soup.select_one("div.dailyV2__free-book")
    date = book["data-free-at"]
    img = book.select_one("img")["src"]

    title = soup.select_one("div.dailyV2__free-book__title").string.strip()
    author = soup.select_one("div.dailyV2__free-book__author").string.strip()
    description = soup.select_one("div.dailyV2__free-book__description").string.strip()

    url_rel = soup.select_one("div.dailyV2__free-book__cta").a["href"]
    url = f"https://www.blinkist.com{url_rel}"

    response = requests.get(url)
    soup = BeautifulSoup(response.content, "html5lib")

    article = soup.select_one("article")
    article.select_one("div.reader__container__buttons").decompose()

    directory = "_posts"
    if not os.path.exists(directory):
        os.mkdir(directory)
    filename = re.sub(r"\s+", "-", f"{directory}/{date}-{author}-{title}.html").casefold()

    with open(filename, "w") as f:
        f.write(f"""\
---
layout: post
title: "{author} - {title}"
description: "{description}"
image: {img}
---

""")
        f.write(article.prettify())


if __name__ == "__main__":
    main()
