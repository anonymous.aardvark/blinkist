---
layout: post
title: "Robert Wright - The Moral Animal"
description: "The Moral Animal (1994) delves into the fascinating – and occasionally controversial – field of evolutionary psychology to ask what really motivates human behavior. Drawing on the work of Darwin as well as a wealth of anthropological sources, Robert Wright sheds new light on a range of familiar everyday situations in the animal kingdom and our own societies."
image: https://images.blinkist.com/images/books/5ca0149f6cee0700079edee6/1_1/470.jpg
---

<article class="shared__reader__blink reader__container__content" lang="en">
 <div class="chapter chapter" data-chapterid="5ca014cc6cee070007a55b29" data-chapterno="0">
  <h1>
   What’s in it for me? An evolutionary look at human history.
  </h1>
  <div class="chapter__content">
   <p>
    What is morality? We often think of it in terms of imperatives like treating others as we’d like to be treated ourselves. Principles like the so-called “golden rule” suggest that moral behavior is a free choice – a decision to be good. It’s an attractive view, but does it stand up to the evidence?
   </p>
   <p>
    Evolutionary psychologists don’t think so. According to them, morality can only really be understood by delving into the evolutionary development of the human species. Dig deep enough, they suggest, and you’ll find that some of our most cherished ideals serve altogether more instrumental ends – above all, the Darwinian urge to ensure survival and the propagation of genes.
   </p>
   <p>
    Such claims made evolutionary psychology a pretty controversial discipline when it first got off the ground in the early 1990s. But, as Robert Wright’s now-classic
    <em>
     The Moral Animal
    </em>
    shows, there’s plenty of data backing up these kinds of often-counterintuitive insights.
   </p>
   <p>
    Whether you’re looking at hunter-gatherer communities in Peru, the behavior of primates or our own modern societies, accounts emphasizing evolutionary mechanisms offer plausible explanations for everything from sibling rivalry to jealousy and altruism.
   </p>
   <p>
    In the following blinks, you’ll learn
   </p>
   <ul>
    <li>
     why men and women experience different forms of envy;
    </li>
    <li>
     why social status determines whether families favor girls or boys; and
    </li>
    <li>
     what chimpanzees can teach us about dominance and leadership.
    </li>
   </ul>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5ca014e96cee0700079edee7" data-chapterno="1">
  <h1>
   Intelligence, youth and beauty are attractive attributes because they indicate an ability to bear and care for children.
  </h1>
  <div class="chapter__content">
   <p>
    Men, as the old cliché has it, are notoriously unfussy when it comes to casual hookups. If the answer’s “yes,” they’ll partner up with pretty much anyone. Finding a long-term mate, however, is a different business.
   </p>
   <p>
    Take it from the American sociobiologist Robert L. Trivers. In a study published in 1990, Trivers suggested that while the average male isn’t particularly choosy about sexual partners, both men and women apply exacting standards when selecting long-term partners. His evidence? The majority of participants in his study stated that potential mates had to demonstrate above-average intelligence to be taken into consideration.
   </p>
   <p>
    That’s easy enough to explain in terms of evolutionary psychology: when men seek a long-term partner, they’re looking for attributes that suggest their mate will be a capable guardian of their future children. Intelligence is an obvious sign of just that. It’s important to note that this isn’t a conscious choice, however; according to evolutionary psychologists, the preference for a smart and competent mother is an entirely unconscious calculation designed to ensure the survival of children.
   </p>
   <p>
    That said, intelligence isn’t the only characteristic that’s attractive to men – youth and beauty are just as important. According to a 1989 study by evolutionary psychologist David Buss that looked at men’s preferences in cultures across the globe, this isn’t mere male superficiality. Rather, typical youth and beauty markers – think big eyes and small noses – are pretty reliable indicators of female fertility.
   </p>
   <p>
    That means these choices once again come down to the importance of passing on one’s genes: the greater the chance that a partner will bear children, the more likely it is that a male’s genetic makeup will survive and thrive over many future generations.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5ca015026cee070007a55b2a" data-chapterno="2">
  <h1>
   Jealousy is multifaceted and differs between men and women.
  </h1>
  <div class="chapter__content">
   <p>
    The view that jealousy is a negative characteristic is pretty widespread. Plenty of people simply rule out relationships with anyone who demonstrates jealous tendencies. But is a life without jealousy even possible? Well, it depends on your stance. Ask evolutionary psychologists, for example, and they’ll tell you that feeling jealous is entirely natural.
   </p>
   <p>
    That doesn’t mean it’s experienced the same way by both sexes. In fact, males and females experience quite different forms of jealousy. That’s the conclusion of a 1982 study by Martin Daly and Margo Wilson. The two psychologists placed electrodes on the bodies of men and women to measure their physiological reactions to various ideas – specifically, their responses when asked to imagine their partners having sex or bonding emotionally with other people of the opposite sex.
   </p>
   <p>
    The experiment showed that men and women had radically different reactions. Men, for example, displayed signs of distress and anger such as elevated heart rates and sweating when they pictured the scenario. The idea of their partners enjoying a platonic relationship with other males, by contrast, left them pretty much unmoved. For women, the opposite was true: they were generally much more distressed by the notion that their husbands might form a close emotional bond with other women than by the idea of sexual infidelity.
   </p>
   <p>
    But there’s another difference. According to evolutionary psychologists, male sexual jealousy is driven by men’s (again, unconscious) desire to ensure the propagation of their genes. That’s why picturing their partners having sex with other men stung the study’s participants so badly: the idea that someone else might impregnate a man’s partner and that he could then end up raising a child who doesn’t carry his genes is enraging.
   </p>
   <p>
    The widespread availability of contraceptive pills and condoms has, of course, severed that automatic link between sex and pregnancy. Today, affairs are much less likely to result in women bearing children. So why do men still react so forcefully to a hypothetical act of sexual cheating? Well, that response is basically an old evolutionary reflex that hasn’t yet caught up with the modern age!
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5ca0151e6cee0700079edee8" data-chapterno="3">
  <h1>
   Cheating on male partners is an evolutionary strategy to ensure the best outcomes for children.
  </h1>
  <div class="chapter__content">
   <p>
    If you’ve seen the 1949 movie
    <em>
     Gentlemen Prefer Blondes
    </em>
    , you’ll probably remember Marilyn Monroe singing the jazz standard “Diamonds Are a Girl’s Best Friend.” But is it true – are women really that mercenary? Well, according to quite a few evolutionary psychologists, yes.
   </p>
   <p>
    That’s where cheating comes in. According to proponents of the discipline, few things motivate infidelity like the desire for material wealth. The idea that women receive gifts from their lovers suggests that the more partners they have, the more material goods they’ll receive. It’s pretty much a cliché in popular culture, but there’s also scientific data to back it up. Take a 1979 study by the American anthropologist Donald Symons that looked at bonobos. According to Symons, female bonobos regularly exchange sexual favors with male suitors in exchange for large chunks of meat.
   </p>
   <p>
    Another anthropologist, Marjorie Shostak, found similar behavior in human communities when she examined the life and culture of the !Kung people in the Kalahari Desert. The !Kung didn’t beat about the bush when it came to such matters. In fact, they were pretty up-front about the reasons why a woman would engage in multiple sexual relations, calmly explaining to Shostak that keeping numerous lovers meant access to a wider range of foods as well as material goods like beads and money.
   </p>
   <p>
    Children provide a second explanation for infidelity. According to the author, it’s conceivable that women were traditionally drawn to the idea of having several partners because they were keen to give their children every possible advantage, both genetic and material. Women wanted two things: On the one hand, there was the preference for athletic and intelligent male sexual partners – a reflection of the desire to select the best genes. On the other hand, remaining in a long-term relationship was the best bet for giving kids a stable, supportive environment in which to grow up.
   </p>
   <p>
    If those two attributes couldn’t be found in a single partner, the only option was to bear the children of the sexual partner and trick the person they lived with into raising those children as his own.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5ca015346cee070007a55b2b" data-chapterno="4">
  <h1>
   Social status and wealth play an important role in determining which gender of child families favor.
  </h1>
  <div class="chapter__content">
   <p>
    You’ve probably read a hundred fairy tales about a rich young prince falling for the lowly scullery maid and sweeping her away to a life of wealth and happiness. It’s a pretty common theme in stories from around the world. That might just be because there’s some solid evolutionary psychology lurking behind these fantasies.
   </p>
   <p>
    Here’s the basic insight: social status and wealth play a huge role in determining which gender a family’s favorite child will be. According to evolutionary psychologists, the choice of a favorite reflects an understanding of which child has the greatest potential to propagate his or her genes in the most advantageous way. In poor, low-status families, that’s usually a girl. Why? Well, women have a much greater chance than men of marrying “up” into wealthier families.
   </p>
   <p>
    In the latter kind of family, by contrast, the favorite is likely to be a boy. After all, with money and power at his disposal, he has a virtually unlimited ability to spread his genes, whether by marrying often or by having multiple mistresses.
   </p>
   <p>
    But let’s get away from fairy tales for a second and look at the scientific evidence. Take anthropologist Mildred Dickemann’s 1970s studies of infanticide in nineteenth-century Asia and medieval Europe. The data Dickemann gathered led her to the conclusion that female infanticide was much more common in aristocratic and wealthy families than among poorer folk. And even when the extreme option of killing baby girls was disregarded, it was much more common for well-off families to pass down inheritances to the eldest son rather than the eldest daughter.
   </p>
   <p>
    Another study conducted in 1986 by anthropologists Laura Betzig and Paul Turke showed that this dynamic could also be observed in societies like those of Micronesia. When Betzig and Turke looked at family life in the island communities they were studying, they found that high-status families, on the whole, spent much more time with their sons. Low-status families, on the other hand, were usually much more attentive to their daughters.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5ca015536cee0700079edee9" data-chapterno="5">
  <h1>
   Altruism is self-serving, and morality is largely driven by what other people think of us.
  </h1>
  <div class="chapter__content">
   <p>
    Why do we empathize with someone starving in the streets? Well, isn’t altruism just part and parcel of what it means to be human? According to evolutionary psychologists, it’s not quite as simple as that. In fact, as they see it, altruistic behavior is primarily self-serving: we’re kind to others because it establishes a debt in our favor.
   </p>
   <p>
    Think of it like this: if you give a person who hasn’t eaten for three days something to eat, the chances are he’ll be inclined to go out of his way to help
    <em>
     you
    </em>
    . That makes a lot of sense when you think about the small, tight-knit communities in which humans evolved. Doing someone a good turn in your community was likely to stand you in good stead. After all, you’d be pretty much guaranteed to see him every day and thus remind him that he owed you a favor. Of course, it doesn’t work out like that in bigger communities, but the evolutionary instinct to help others for self-serving ends still endures.
   </p>
   <p>
    That suggests morality might not be what we often make it out to be. Moral behavior isn’t simply selfish, however – what really motivates it is our preoccupation with what other people think of us. Take a 1966 experiment by Robert Trivers. During this study, participants were led to believe they’d broken an expensive piece of laboratory equipment. In some cases, scientists pretended to discover the damage and became upset. In others, they acted as though they hadn’t noticed anything was amiss.
   </p>
   <p>
    Trivers tested whether participants’ guilty feelings would motivate them to take part in a potentially painful experiment to help the scientists further their research. Interestingly, the participants who’d been “caught” were much more likely to agree. Their peers, who believed no one had noticed that they’d broken a pricey bit of equipment, were much more comfortable with declining the follow-up study.
   </p>
   <p>
    That’s a great example of how our obsession with other people’s opinions about us drives our moral behavior. Ultimately, that can be traced back to the kind of tight-knit communities we mentioned earlier in this blink. Living in a small group means that debts, grudges and favors aren’t quickly forgotten, and that changes the way people act.
   </p>
  </div>
 </div>
 <div class="chapter supplement" data-chapterid="5ca015536cee0700079edee9" data-chapterno="5">
  <h1>
  </h1>
  <div class="chapter__content">
   <p>
    “
    <em>
     In our ancestral environment, just about everyone encountered was someone we might well encounter again.
    </em>
    ”
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5ca015746cee070007a55b2c" data-chapterno="6">
  <h1>
   Human society might be inherently hierarchical, but hierarchy isn’t genetic.
  </h1>
  <div class="chapter__content">
   <p>
    Plenty of twentieth-century anthropologists like Franz Boas and Margaret Mead were charmed by the seemingly egalitarian communities of hunter-gatherers they encountered during their travels. They believed they’d found an alternative to the hierarchical class societies of the West. Unfortunately, further research has shown that hierarchies are inherent in virtually every human society.
   </p>
   <p>
    Take the Ache people of Eastern Paraguay. On the face of it, their society is highly egalitarian. The meat brought back by hunters, for example, is always pooled and divided equally among village members. But when anthropologists Kim Hill and Hillard Kaplan took a closer look in 1988, they realized just how deceptive appearances can be.
   </p>
   <p>
    So what did they discover? Well, the hunters with the biggest hauls were being given plenty of evolutionary perks. They had more affairs and thus more children. Those kids also turned out to have better survival rates than other children, suggesting that they were being given preferential treatment due to their relationship to the best hunters. Ultimately, the village’s egalitarian social system was underpinned by a subtle – but highly effective – hierarchy.
   </p>
   <p>
    Okay, so all human societies are hierarchical – does that mean hierarchy is hardwired into our genes? Well, not really. In fact, the best scientific evidence suggests the very opposite might be closer to the truth. Consider a 1984 study by evolutionary biologists M. T. McGuire and M. J. Raleigh, for instance. When the duo examined vervet monkeys and college fraternities, they found that dominant males displayed high levels of the hormone serotonin in both settings.
   </p>
   <p>
    That could be interpreted as proof of a link between genetic makeup and leadership, but the same study also found that serotonin levels only began to rise once those males had assumed their dominant positions. That suggested that anyone could become leader and that their hormone levels were a kind of “leadership effect.”
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5ca015a26cee0700079edeea" data-chapterno="7">
  <h1>
   Status depends on strategic alliances rather than simple brute force.
  </h1>
  <div class="chapter__content">
   <p>
    All right – we’ve seen that leaders are part and parcel of human societies, and that any number of types of people can fill that role. In this blink, we’ll take a closer look at how actors rise through the ranks to become top dogs. Let’s start by turning to our close cousins the chimpanzees to see how it’s done.
   </p>
   <p>
    Brute force plays its part in asserting dominance in the world of chimpanzees, but violence alone isn’t enough. In fact, one of the most effective techniques used by these primates is the
    <em>
     simulation
    </em>
    of strength. That’s something the famous British anthropologist Jane Goodall observed in chimpanzee populations in Gombe, Tanzania, in 1986. During her fieldwork there, she noticed that one of the males – nicknamed Michael – was a lot less brawny than his peers.
   </p>
   <p>
    He didn’t let that stop him trying to get his way, though. In fact, Michael ran the show. So how did he intimidate his bigger rivals? Simple: he charged at them and threw objects like empty kerosene cans around while making a terrible racket. The strategy worked. His peers submitted to his authority and came over to groom him to show they’d understood who was calling the shots.
   </p>
   <p>
    But faking it until you make it isn’t a viable tactic if you want to ensure long-term leadership. If you want that, you’d better be making strategic alliances. Take a study conducted between 1975 and 1981 by Dutch primatologist Frans de Waal. His research was centered on a group of chimpanzees in a zoo in Arnhem in the Netherlands. Waal observed an alpha chimp called Yeroen being challenged by a young upstart named Luit. Yeroen ran over to the females in the enclosure, embraced them and led them in a charge against Luit. Cornered, the pretender had to back down.
   </p>
   <p>
    When Luit tried his luck again, Yeroen enrolled a brawny primate called Nikkie as his representative. Nikkie took over the role of alpha while Yeroen retained his privileged access to the tribe’s female monkeys. In effect, he’d made Nikkie into a figurehead while keeping real power in his hands. In the end, Yeroen and Nikkie killed Luit, symbolically ripping out his testicles and bringing this scary tale of power, status and alliances amongst chimpanzees to its end.
   </p>
   <p>
    We’ve evolved a great deal from our primate ancestors, but – as we’ve learned in these blinks – we’re a lot closer to them than we often assume. And that’s one of evolutionary psychology's most disconcerting insights!
   </p>
  </div>
 </div>
 <div class="chapter supplement" data-chapterid="5ca015a26cee0700079edeea" data-chapterno="7">
  <h1>
  </h1>
  <div class="chapter__content">
   <p>
    “
    <em>
     I found myself fighting this moral judgement, but to this day I cannot look at Yeroen without seeing a murderer
    </em>
    .” – Frans de Waal
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5ca015c46cee070007a55b2d" data-chapterno="8">
  <h1>
   Final summary
  </h1>
  <div class="chapter__content">
   <p>
    The key message in these blinks:
   </p>
   <p>
    <strong>
     What makes us who we are? Well, according to evolutionary psychologists, it’s the genes, stupid. Their account isn’t quite as reductive as it might appear to be, however: our “deep” genetic urges might be simple enough, but the variety of behavior, emotions and ideas they give rise to is anything but one-dimensional. Whether it’s the jealousy we feel at the thought of a cheating partner or the desire to help a stranger, the greater part of our drives is rooted in the evolutionary history of our species.
    </strong>
   </p>
   <p>
    <strong>
     Got feedback?
    </strong>
   </p>
   <p>
    We’d sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
   </p>
   <p>
    <strong>
     What to read next:
    </strong>
    <strong>
     <em>
      Nonzero
     </em>
    </strong>
    <strong>
     , by Robert Wright
    </strong>
   </p>
   <p>
    Nietzsche once wrote that we evolved from worms and that much within us was “still worm.” It was the eccentric philosopher’s way of saying what we’ve learned in these blinks: ancient instincts have a huge effect on who we are and how we behave.
   </p>
   <p>
    But don’t worry – our uncanny resemblance to members of the animal kingdom isn’t all there is to this story; there’s also been plenty of progress. Just how much, and why it occurred, is what Robert Wright set out to discover in his study of how we evolved to prefer win-win situations. So if you’ve enjoyed this look at evolutionary psychology, why not dip into our blinks to
    <em>
     Nonzero
    </em>
    (1999)?
   </p>
  </div>
 </div>
</article>
