---
layout: post
title: "Diane Atkinson - Rise Up, Women!"
description: "Rise Up, Women! (2018) tells the remarkable story of the militant women’s suffrage movement in the United Kingdom. Full of fascinating insights into the women at the heart of the struggle for equality, these blinks illuminate one of the twentieth century’s first great civil rights revolutions."
image: https://images.blinkist.com/images/books/5c9697ba6cee070008b872da/1_1/470.jpg
---

<article class="shared__reader__blink reader__container__content" lang="en">
 <div class="chapter chapter" data-chapterid="5c9697d16cee070008b872db" data-chapterno="0">
  <h1>
   What’s in it for me? A scintillating study of the suffragettes.
  </h1>
  <div class="chapter__content">
   <p>
    The struggle for equal suffrage – a vote for every citizen – took a long time in the United Kingdom. In fact, it took just under a century before the dream was realized. But inequality also defined the struggle itself. The idea that all men should be given the right to choose their representatives was, for many, a lot easier to accept than the notion that such rights should also apply to women.
   </p>
   <p>
    That’s where the so-called “suffragettes” came in. Built around the Women’s Social and Political Union or WSPU, the movement was shaped and dominated by the charismatic Pankhurst family. Between the first years of the twentieth century and the First World War, the WSPU was Britain’s most committed and militant pro-suffrage organization.
   </p>
   <p>
    From attacks on leading members of the governments of the day to smashing windows, burning letterboxes, occasional bombing sprees and massive marches, the suffragettes were committed to taking matters into their own hands and achieving equality in their country. The path they followed wasn’t an easy one: along the way, they faced astonishingly brutal repression at the hands of the state and its police force and gaolers. These blinks tell their story.
   </p>
   <p>
    So read on to find out
   </p>
   <ul>
    <li>
     why the suffrage movement believed there was no alternative to militant direct action;
    </li>
    <li>
     who the Pankhursts were and why they were so prominent in the suffrage movement; and
    </li>
    <li>
     how World War One changed societal attitudes concerning the role of women.
    </li>
   </ul>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5c9698036cee07000797093c" data-chapterno="1">
  <h1>
   The Pankhurst family began advocating for women’s suffrage in early twentieth-century Britain.
  </h1>
  <div class="chapter__content">
   <p>
    Following voting reforms in 1832, around 800,000 property-owning men in the United Kingdom could vote. Women, however, remained disenfranchised. That struck some as deeply unfair. The English philosopher John Stuart Mill, for example, raised the issue in 1867, arguing that it was unjust that female taxpayers couldn’t elect their own representatives.
   </p>
   <p>
    Yet little was done to change the situation. Even the socialist Labour party, which was otherwise committed to equality, kept silent. Its leaders feared that only wealthy women would be given the vote, boosting the Conservative and Liberal parties. By the turn of the twentieth century, the situation looked bleak for supporters of women’s suffrage.
   </p>
   <p>
    Frustrated by this lack of progress, suffragette Emmeline Pankhurst and her daughter Sylvia decided to take matters into their own hands. In 1905, the duo began lobbying members of parliament or MPs directly, but their arguments mostly fell on deaf ears. One exception was John Slack, the Liberal MP for St Albans. Slack decided to raise the question of women’s suffrage in parliament and introduced a private members’ bill – a proposal to change the law.
   </p>
   <p>
    The bill was filibustered. Opponents extended debates concerning other bills. By the time Slack’s proposal was raised for discussion, only 30 minutes remained. As it was announced, MPs laughed and clapped.
   </p>
   <p>
    That was the moment the Pankhursts realized that they’d have to change their tactics. If they wanted to secure the vote for women, politely asking for change just wasn’t going to cut it. So what was the alternative? Well, they’d have to become a great deal more confrontational and shock people. Their organization – the Women’s Social and Political Union or WSPU – adopted a new motto to reflect this change in tactics: “Deeds not words.”
   </p>
   <p>
    Christabel, another of Pankhurst’s daughters, and Annie Kenney, a mill worker from Oldham, led the way. They interrupted a Liberal rally in Manchester and waved a banner reading “Will you give votes for women?” As police officers led them away, Christabel spat at one of them, knowing it’d get her arrested.
   </p>
   <p>
    Both were charged with obstruction while Christabel faced another charge of assault. When they refused to pay their fines, they were sentenced to several days in prison. That got people talking. Newspapers like the
    <em>
     Times
    </em>
    reported on the trial – the first WSPU event to ever feature in the national press!
   </p>
  </div>
 </div>
 <div class="chapter supplement" data-chapterid="5c9698036cee07000797093c" data-chapterno="1">
  <h1>
  </h1>
  <div class="chapter__content">
   <p>
    The term “suffragette” was coined by the
    <em>
     Daily Mail
    </em>
    in 1906 to demean the suffrage movement.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5c9698166cee070008b872dc" data-chapterno="2">
  <h1>
   Pankhurst and her daughters were charismatic – and controversial – leaders of the WSPU.
  </h1>
  <div class="chapter__content">
   <p>
    Emmeline Pankhurst and her daughters had founded the WSPU in 1903. They would remain at the center of the movement throughout its existence, and it was their ideas which shaped its identity. Their strong belief in the effectiveness of deeds rather than words was reflected in its official motto. Even the organization’s colors – purple, white and green – were chosen by the Pankhursts. Their militancy and dedication would inspire women throughout the United Kingdom to fight for their right to vote.
   </p>
   <p>
    So who were these radical women? Well, let’s start with Christabel. She led the WSPU alongside her mother and became such an icon of the cause that women wore brooches bearing her portrait. She had received a first class degree in law but wasn’t allowed to practice the profession because she was a woman. Then there was Sylvia. She had studied at Manchester Art School and the Royal College of Art. She put that background to use by designing many of the WSPU’s banners as well as its membership card.
   </p>
   <p>
    It was a tight clique. Most women in the movement were fiercely loyal to the Pankhursts, but their occasionally dictatorial leadership style also ruffled feathers. In 1907, Teresa Billington-Greig, a supporter of a more democratic movement, decided enough was enough and founded a WSPU splinter group. The Pankhursts weren’t impressed. As far as they were concerned, democratic decision making was simply a waste of time. Christabel responded to the split by writing to the organization’s members to remind them that it was a military movement. “Those who cannot follow the general,” she added, “must drop out of the ranks.”
   </p>
   <p>
    They remained committed to that mind-set. When Fred and Emmeline Pethick-Lawrence, two supporters of the WSPU who had been vital to its money-raising operations, questioned the Pankhursts over the need for more militancy, they were also expelled. When they arrived in the movement’s new offices one day, they found that Emmeline and Christabel had simply decided not to give them desks!
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5c9698296cee07000797093d" data-chapterno="3">
  <h1>
   Women’s protests in prison led to brutal treatment.
  </h1>
  <div class="chapter__content">
   <p>
    The WSPU soon settled on imprisonment as the most effective means of raising the organization’s public profile. The experiences of the women who were jailed for their activism merely confirmed what they’d believed all along. Even as prisoners, women weren’t treated the same way as men.
   </p>
   <p>
    In the nineteenth century, political prisoners were allowed to wear their own clothes, read books, write letters and freely associate with one another. When the suffragettes ended up behind bars, they found that such perks were for men only. They were treated as common criminals. That meant wearing prison garb, scrubbing floors and experiencing solitary confinement.
   </p>
   <p>
    But what the authorities had forgotten was that these women
    <em>
     weren’t
    </em>
    common criminals at all – they were militants! Soon enough, imprisoned suffragettes were organizing hunger strikes to protest against their status. Marion Dunlop-Wallace was one of the first women to do so, beginning her strike in July 1909. Others followed her lead. It was an effective tactic initially, and many women were released after just a few days on health grounds.
   </p>
   <p>
    By September 1909, prisons were cracking down. Their solution? Force-feeding with nasal and stomach tubes. Mary Leigh was one of the first suffragettes to be subjected to this extremely painful and dangerous practice. MPs and medical professionals were horrified. No wonder: it was the first time that the practice had been applied to sane prisoners. Newspapers compared it to torture.
   </p>
   <p>
    Despite these protests, prisons continued to force feed the women. From around 1914 onward, they began using a bromide sedative to render the women less resistant. Kitty Marion, a German-born suffragette who had enjoyed great success as a music hall artist before having her career cut short by the authorities, was the first to report the strange salty taste while being force-fed.
   </p>
   <p>
    Force-feeding took its toll on these women, and many were released when they grew too weak and ill to remain in prison. The authorities attempted to clamp down on this cycle of imprisonment followed by force-feeding and early release by passing what was colloquially called the “Cat and Mouse Act.” This stated that early release prisoners must remain at home and would face rearrest if they began regaining weight. It wasn’t very successful, however, and many militants escaped and continued their fight with the WSPU.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5c96983d6cee070008b872dd" data-chapterno="4">
  <h1>
   Suffragettes saw criminal damage as an effective way of inciting arrest and gaining publicity.
  </h1>
  <div class="chapter__content">
   <p>
    In 1906, suffragettes took stones wrapped in WSPU petitions and threw them through the windows of the British Treasury, Home Office and Privy Council. It was the first example of what would become an increasingly common strategy: criminal damage designed to raise the movement’s public profile.
   </p>
   <p>
    It was a crude tactic, and plenty of the WSPU’s genteel members found the act almost painful. But they pressed on nonetheless – after all, breaking windows was a great deal less painful than being beaten up by the police on the frontlines of protests!
   </p>
   <p>
    Over the following years, WSPU militants ramped up their operations. By the beginning of 1912, some 270 buildings had been targeted, racking up £6,600-worth of damage. Artworks also found themselves in the firing line. In 1914, Mary Richardson attacked Velázquez’s
    <em>
     The Rokeby Venus
    </em>
    in the National Gallery in London with an ax, and Clara Mary Lambert smashed several porcelain objects in the British Museum.
   </p>
   <p>
    That wasn’t the suffragettes’ final word on the matter, however: soon enough, militants escalated matters even further and began starting fires – a course of action which outraged the public. An arson attack on the tea pavilion in Kew Gardens was followed by attacks on the empty houses of anti-suffrage MPs. When that failed to bring results, the movement took up bombs. One device was planted in a holiday cottage belonging to David Lloyd George, then the chancellor of the exchequer. The idea, in Emmeline Pankhurst’s words, was to “wake him up.”
   </p>
   <p>
    Another bomb was found in the Bank of England in 1913. If it had detonated, the results would have been fatal. Because it contained hairpins, many believed it had been planted by suffragettes. The movement, however, never claimed responsibility, and others pointed out that no pro-suffrage literature had been found at the scene.
   </p>
   <p>
    It hardly mattered who had been responsible by this point. The string of attacks had generated a public backlash. The number of attacks on WSPU members grew steadily. The situation looked like it was getting out of hand. The police even had to protect a WSPU meeting on Wimbledon Common from a furious mob!
   </p>
   <p>
    On that particular occasion, the police protected suffragettes from violence. On plenty of other occasions, however, they showed that they were more than willing to brutalize activists.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5c96984e6cee07000797093e" data-chapterno="5">
  <h1>
   Violence wasn’t a one-way street, and suffragettes were just as likely to be on the receiving end.
  </h1>
  <div class="chapter__content">
   <p>
    As we’ve seen, the suffrage movement faced stiff opposition from the get-go. From force-feeding to attacks on militants by members of the public, much of the repression was violent. On other occasions, the police resorted to brutal tactics in their battle against the movement.
   </p>
   <p>
    While it was never an official WSPU policy, some pro-suffrage militants also resorted to physical attacks against their opponents. Take Mary Leigh, the first woman to be force-fed in prison. In 1912, she threw a hatchet through the window of Prime Minister Herbert Asquith’s carriage in Dublin. Then there was Emily Davison. In the same year, she attacked Reverend Forbes Jackson with a horsewhip after mistaking him for David Lloyd George!
   </p>
   <p>
    Police violence was just as common. One of the most notorious cases came on 18 November 1910 – a day that would go down in history books as “Black Friday.” When 300 women led by Emmeline Pankhurst marched on parliament, they were attacked by police officers. Over half of the protestors reported being physically assaulted, while another 21 were sexually assaulted.
   </p>
   <p>
    The level of violence was appalling. Mary Billinghurst, a woman who used a wheelchair, was attacked four times by officers. First, they removed the valves from the wheels on her chair; later, after members of the public had replaced them, officers bent her wheels so she couldn’t move. Another protestor called Daisy Solomon reported that both uniformed and plainclothes police officers had grabbed her breasts. She described the details of the assault in a letter she sent to Winston Churchill, then the home secretary, which was later published in the
    <em>
     Times
    </em>
    .
   </p>
   <p>
    That wasn’t even the worst of it. Two protestors later died as a result of injuries sustained on Black Friday. Mary Clarke passed away on Christmas Day after suffering a brain hemorrhage. Henria Williams, a woman with a weak heart, died from angina after returning from the protest with lips black from suffocation. But, as we’ll see in the next blink, these deplorable deaths would be eclipsed by that of another suffragette in what would become one of the most infamous chapters in the history of the suffrage movement.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5c9698686cee070008b872de" data-chapterno="6">
  <h1>
   Emily Davison’s infamous suicide was met with mixed reactions within the movement.
  </h1>
  <div class="chapter__content">
   <p>
    Even if you don’t know much about the suffragettes, you’ll probably have come across the name of Emily Davison – the woman who died after throwing herself in front of the king’s horse at the Derby in 1913 while holding a banner featuring the colors of the suffrage struggle. So who exactly was Davison?
   </p>
   <p>
    Well, in a word, a maverick. Davison was among the most radical militants in the movement, as her résumé suggests. She was one of the first women to set fire to public postboxes, a common tactic from which the WSPU was careful to distance itself. Her first arson attack came in December 1911 when she lit a kerosene-soaked package and put it into a postbox on Fleet Street in London. She was arrested three days later after burning down another three postboxes.
   </p>
   <p>
    Davison didn’t like taking orders, which made for an uneasy relationship with the official suffrage movement. She smashed windows, for example, during a truce declared by the WSPU. In response, Christabel Pankhurst declared that Davison didn’t represent the organization. Only one suffragette appeared at her trial as a character witness: Eleanor Penn Gaskell. Davison remained influential, however. Other activists like Elsie Howe, a woman whose voice would be permanently damaged as a result of force-feeding, copied her tactic of burning postboxes.
   </p>
   <p>
    Davison’s suicide fits into that pattern. It was the most shocking event in the history of the suffrage movement and received a mixed response. While she saw it as a glorious martyrdom – her last published article hails the idea of dying for a cause – others weren't so sure. As she lay dying in the hospital, letters filled with both admiration and violent hatred flooded in.
   </p>
   <p>
    Suffragettes were also divided. Gaskell, the character witness at Davison’s trial, praised her courage and stated that “her sacrifice would not be in vain.” Philippa Strachey, a member of the altogether less militant London Society for Women’s Suffrage, wasn’t convinced: she believed Davison’s reckless endangerment of others’ lives had ultimately been counterproductive.
   </p>
   <p>
    But whatever individuals made of it at the time, Davison’s death went on to become the most enduring symbol of the suffragettes’ campaign for the vote, and many regard her as a feminist icon to this day.
   </p>
  </div>
 </div>
 <div class="chapter supplement" data-chapterid="5c9698686cee070008b872de" data-chapterno="6">
  <h1>
  </h1>
  <div class="chapter__content">
   <p>
    Davison bought a return ticket to the Derby, perhaps implying that she intended to return home that day. However, a return was cheaper than a single.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5c9698796cee07000797093f" data-chapterno="7">
  <h1>
   The First World War changed both the movement and society’s attitude toward women.
  </h1>
  <div class="chapter__content">
   <p>
    The outbreak of war in Europe in 1914 changed the way people thought about themselves and society. The suffragettes were no different, and there was plenty of debate within the movement about which position it should take on the conflict.
   </p>
   <p>
    Emmeline and Christabel Pankhurst belonged to the pro-war camp. The duo became increasingly jingoistic over time.
    <em>
     The
    </em>
    <em>
     Suffragette
    </em>
    , the WSPU magazine edited by Christabel, reflected this. It condemned the Germans and was even renamed
    <em>
     Britannia
    </em>
    in 1917 to showcase its patriotism. There were reports of WSPU members handing white feathers – a symbol of cowardice – to men not wearing a uniform. Emmeline Pankhurst meanwhile called for a truce during the war period, and all suffragette prisoners were released within ten days.
   </p>
   <p>
    Others rejected such boosterism. Pacifists like the ousted Pethick-Lawrences and the younger Pankhurst sisters Sylvia and Adela condemned the war. They angered Emmeline and Christabel by continuing to campaign for women’s suffrage and speaking out against the bloodshed in Europe.
   </p>
   <p>
    The conflict also had a profound impact on women’s role in society. As more and more men were drafted and sent to the front, women increasingly stepped in to take their places in domestic industries. In 1915, Emmeline Pankhurst was commissioned by Lloyd George to organize a “Women’s Right to Serve” march encouraging women to work in munitions factories. It was a display of patriotism that moved both the public and the press. Even the anti-suffrage
    <em>
     Daily Mail
    </em>
    had to admit that “it was impossible not to be touched.”
   </p>
   <p>
    Women’s entry into traditionally male workplaces changed the country. Women could now be found working as gravediggers, bakers, carpenters, chimney sweeps and ambulance drivers. Although they were paid less, the country was generally grateful for their dedication to the war effort. That led some people to reconsider their views. Herbert Asquith, for example, had opposed giving women the vote during his time as prime minister and chancellor of the exchequer. Their service during the war, however, convinced him that he had been wrong.
   </p>
   <p>
    Things were changing fast. They were about to change still further.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5c9698976cee070008b872df" data-chapterno="8">
  <h1>
   The 1918 Representation of the People Act was the first step on the path to full suffrage for women.
  </h1>
  <div class="chapter__content">
   <p>
    On 6 February 1918, the Representation of the People Act was passed into law. Although it fell short of granting women full equality, it was nonetheless a milestone in the struggle for suffrage. Christabel Pankhurst believed the law had been rushed through parliament for one simple reason: the fear that the suffragettes would return to their pre-war militancy.
   </p>
   <p>
    So what did the law actually say? Well, it gave women over 30 a vote on the condition that either they or their husbands were householders or rented property worth £5 per year. Women who had graduated from a British university or held a comparable qualification were also given the vote.
   </p>
   <p>
    While that still excluded plenty of women, it radically changed the makeup of the electorate: now, 8.4 million women could participate in elections. As Sylvia Pankhurst noted, if all women over 21 had been given the vote, they would have outnumbered males since so many men had died in the war!
   </p>
   <p>
    It was a great achievement, but the reaction in the suffrage movement was pretty muted. The horrors and losses of the war years had left their mark, and no one was in the mood to celebrate. The property and age restrictions contained in the law also irked many – full equality still seemed like a distant prospect.
   </p>
   <p>
    Although it might not have felt that way at the time, the reform was an important step on the road to further reforms. In 1919, for example, a law was passed which made it illegal to exclude women from all professions outside the church simply because of their sex.
   </p>
   <p>
    That same year Nancy Astor became the first female MP. By 1924, another eight female MPs had joined her in parliament. Issues concerning women were now being taken more seriously. That paved the way for important legislation passed in the 1920s finally allowing women to divorce adulterous husbands.
   </p>
   <p>
    The final piece in the puzzle had to wait a full ten years, but in 1928 all restrictions were abolished and every woman over the age of 21 was given the right to vote. Emmeline Pankhurst didn’t live long enough to see her great ambition fulfilled, but there’s no doubt that the movement she had spearheaded was central to this victory for equality.
   </p>
  </div>
 </div>
 <div class="chapter supplement" data-chapterid="5c9698976cee070008b872df" data-chapterno="8">
  <h1>
  </h1>
  <div class="chapter__content">
   <p>
    <em>
     “This is the beginning of our era. I am glad to have suffered for this.” – unknown suffragette to Nancy Astor
    </em>
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5c9698b36cee070007970940" data-chapterno="9">
  <h1>
   Final summary
  </h1>
  <div class="chapter__content">
   <p>
    The key message in these blinks:
   </p>
   <p>
    <strong>
     The UK movement for the women’s vote was one of the early twentieth century’s greatest civil rights struggles. At its heart was an organization molded and directed by the charismatic Pankhurst family: the Women’s Social and Political Union. Committed to making themselves heard, the WSPU’s activists used shock tactics to get their message out there, often facing ferocious repression along the way. Their campaigning and a gradual change in societal attitudes after the First World War paid off when, in 1928, all women were finally given the vote.
    </strong>
   </p>
   <p>
    <strong>
     Got feedback?
    </strong>
   </p>
   <p>
    We’d sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
   </p>
   <p>
    <strong>
     What to read next:
    </strong>
    <strong>
     <em>
      Bloody Brilliant Women
     </em>
    </strong>
    <strong>
     , by Cathy Newman.
    </strong>
   </p>
   <p>
    The Pankhursts weren’t the only women who changed the face of Britain, though you’d hardly know it from conventional accounts. All too often, books written by men leave out the contribution of British women to their nation’s history.
   </p>
   <p>
    That’s a wrong journalist and author Cathy Newman is determined to right. So if you’ve enjoyed this look at the suffragettes’ struggle for equality, why not dig a bit deeper into the feminist history of Britain with the blinks to
    <em>
     Bloody Brilliant Women
    </em>
    !
   </p>
  </div>
 </div>
</article>
