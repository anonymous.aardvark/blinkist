---
layout: post
title: "Carlo Rovelli - The Order of Time"
description: "The Order of Time (2017) unpacks the latest research in physics to turn our everyday concept of time on its head. What we perceive and experience as a linear movement, from past to present and into the future, is little more than a trick of the mind. The reality, Carlo Rovelli shows, is a whole lot more interesting and bizarre."
image: https://images.blinkist.com/images/books/5cd146316cee07000830d5b7/1_1/470.jpg
---

<article class="shared__reader__blink reader__container__content" lang="en">
 <div class="chapter chapter" data-chapterid="5cd1479f6cee070008f3fdf5" data-chapterno="0">
  <h1>
   What’s in it for me? A trip through time with a leading theoretical physicist.
  </h1>
  <div class="chapter__content">
   <p>
    Some ways of analysing the world make the unfamiliar familiar – think of historical descriptions of the rites and rituals of bygone ages, say, or the way psychologists probe the workings of the unconscious mind. Other disciplines, by contrast, make everyday concepts utterly alien.
   </p>
   <p>
    Few disciplines are as firmly entrenched in the latter camp as physics. Ask a physicist to describe the table in front of him, and he’ll tell you that matter has to be in two different places at the same time. Once he’s done, the very idea of a table begins to seem odd.
   </p>
   <p>
    In these blinks, Italian theoretical physicist Carlo Rovelli sets out to unsettle our commonplace ideas about time. Drawing on the latest research in his field, Rovelli carefully guides his readers through the often baffling counterintuitive realities of time. Along the way, he explains why time moves at different speeds in different places, why the world is made up of events rather than things and why human history wouldn’t exist without the sun’s energy.
   </p>
   <p>
    In these blinks, you’ll find out
   </p>
   <ul>
    <li>
     how Einstein’s theory of relativity works;
    </li>
    <li>
     why our everyday conception of time makes us who we are as a species; and
    </li>
    <li>
     what would happen if we tried to communicate with someone in a different galaxy.
    </li>
   </ul>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5cd147bb6cee07000830d5ba" data-chapterno="1">
  <h1>
   Time doesn’t move uniformly and it is fundamentally related to heat.
  </h1>
  <div class="chapter__content">
   <p>
    Appearances can be deceptive. That’s one of the first lessons of the sciences. After all, if you trust your eyes alone, you’re likely to end up believing the world is flat. The same goes for time. In everyday life, we see time as a uniform forward movement – something that just happens, like the ticking of an eternal clock entirely beyond our control. But that’s a false assumption.
   </p>
   <p>
    In reality, time passes at different speeds in different places. Compare two clocks, where one is placed at sea level and the other high up in the mountains, and you’ll find the latter runs faster. Placing one clock on the floor and another on a table has the same effect: the differences are minuscule, and you’d need a precision timepiece to prove it, but the second timepiece will always run faster.
   </p>
   <p>
    It’s not just time that slows down when measured at a lower level – all processes do. Take a simple thought experiment – two friends of the same age part company. One of them goes to live on a beach and the other at the top of a mountain. Years later, they meet. The result? The mountain-dweller will have aged more and lived longer than his pal from the flatlands. Even his houseplants will have grown more!
   </p>
   <p>
    It sounds impossible, but there’s no such thing as one objective or “true” measure of time that can be applied both in the mountains and at sea level. That’s because times are
    <em>
     relative
    </em>
    to one another. Each point on a map has its
    <em>
     own
    </em>
    time. That, to put it in simple terms, was the central insight of Albert Einstein’s theory of general relativity.
   </p>
   <p>
    As if that weren’t baffling enough, heat also plays a part in this dynamic. In fact, heat and time share a fundamental similarity – they can both only travel in one direction. Time moves from past to future, while heat always moves from hotter to colder objects.
   </p>
   <p>
    In both cases, reversing that movement is impossible. But here’s where things get really interesting – we can only tell the past and future apart
    <em>
     because
    </em>
    of heat.
   </p>
   <p>
    Let’s break that down. The past is distinguished from the future by change. But change is only possible if there’s motion. And if you really get down to it, motion is simply heat – the movement of molecules at a microscopic level. Without heat, in other words, nothing would move, and past, present and future would be little more than an indistinguishable mass!
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5cd147d06cee070008f3fdf6" data-chapterno="2">
  <h1>
   Einstein demonstrated that time changes with speed and gave us a new notion of time and space.
  </h1>
  <div class="chapter__content">
   <p>
    We’ve seen that time passes at different speeds in different places, but did you know that it also passes at different rates in the
    <em>
     same
    </em>
    place? That’s something Albert Einstein discovered in the early twentieth century when he linked the passage of time to the speed at which objects are moving. More specifically, Einstein showed that time slows down when we move quickly. That means time moves more slowly for a person in movement than someone standing still.
   </p>
   <p>
    This relationship between time and speed means that our commonplace notion of “now” or “the present” is basically nonsensical. Why? Well, imagine you had a sister on a distant planet – let’s say Proxima b – four light-years, or around 38 trillion kilometers, away. She suddenly crosses your mind and you wonder to yourself what she’s doing “right now.” You grab a telescope and point it at Proxima b. What do you see?
   </p>
   <p>
    Well, not the present moment on that planet. A light-year is the measure of the distance light travels in a year. Proxima b is four light-years from earth, so when you look through your telescope, you’re actually looking at what your sister was doing four years ago! But here’s the thing, even if you tried to find out what she will be doing in four years, you still wouldn’t be in the “now” on Proxima b – after all, by that time, she might be back on Earth and years ahead in terrestrial time!
   </p>
   <p>
    That just goes to show that “the present” only really works as a concept when we’re applying it to the things around us. This understanding of the relativity of time was Einstein’s great gift to us. Before he came along, time was understood as it had been since Newton’s day – namely, as an absolute measure that was true regardless of motion or change. Similarly, space was thought to be independent from the things it contained. That changed after Einstein. For the first time, space and time were conceived of as interwoven in a singular and complex geometry.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5cd1482d6cee070008f3fdf7" data-chapterno="3">
  <h1>
   Quantum mechanics has led to three fundamental discoveries of space and time.
  </h1>
  <div class="chapter__content">
   <p>
    By now, we know that time isn’t a smooth sequence of well-ordered events moving from past to present and future. We’ve also seen that an event “in time” can’t be separated from the space in which it takes place. So, what exactly
    <em>
     is
    </em>
    time? Well, let’s take a closer look at how contemporary physicists understand the concept.
   </p>
   <p>
    The best place to start is
    <em>
     quantum mechanics
    </em>
    . That’s basically a way of studying nature by focusing on its smallest components – so-called
    <em>
     quanta
    </em>
    . These are the building blocks of all physical entities. They range from tiny “packages” of light and energy to elementary particles that constitute the material world. The analysis of quanta has led to three groundbreaking discoveries.
   </p>
   <p>
    The first is known as
    <em>
     granularity
    </em>
    . Physicists have come to understand that energy and matter are “quantized.” In other words, they’re bundled up in small clusters rather than flowing freely between different values. That, in turn, has led to the conclusion that time doesn’t flow continuously, but rather takes on certain discrete values. The upshot of that view is that time, just like the material world, is made up of tiny grains. How minute are they? Well, the smallest unit of time –
    <em>
     Planck time
    </em>
    – is just 10-44 seconds. That’s one divided by one followed by 44 zeros!
   </p>
   <p>
    The second discovery is
    <em>
     indeterminacy
    </em>
    . This comes down to the impossibility of accurately predicting where an electron that’s been observed at one point in time will appear a millisecond later. Between the two observations, the electron’s precise position is virtually impossible to pin down – all scientists can do is make probabilistic guesses. Time is also indeterminate – it fluctuates between past, present and future and events can take place both before and after other events.
   </p>
   <p>
    Finally, there’s the
    <em>
     relational
    </em>
    nature of the world. If scientists want to observe electrons, they have to study their interaction with other physical objects. Electrons, in other words, can only be known through their relationships with other forms of matter. The same goes for time. If you want to analyze it, you have to look at the network of relations that define it.
   </p>
  </div>
 </div>
 <div class="chapter supplement" data-chapterid="5cd1482d6cee070008f3fdf7" data-chapterno="3">
  <h1>
  </h1>
  <div class="chapter__content">
   <p>
    <em>
     “The good Lord has not drawn the world with continuous lines: with a light hand, he has sketched it in dots, like Seurat.”
    </em>
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5cd148446cee07000830d5bb" data-chapterno="4">
  <h1>
   The world is made up of events, not things, and there isn’t a privileged variable of time.
  </h1>
  <div class="chapter__content">
   <p>
    The pharaohs of ancient Egypt and the monarchs who once ruled France might have thought themselves eternal, but even the greatest empires and institutions fade and crumble. History ebbs and flows – the only constant is change.
   </p>
   <p>
    Physicists take a similar view of the world. As they see it, the Earth doesn’t consist of things, but events. What really matters aren’t concrete
    <em>
     substances
    </em>
    and
    <em>
     entities
    </em>
    but
    <em>
     happenings
    </em>
    ,
    <em>
     processes
    </em>
    and
    <em>
     occurrences
    </em>
    . Nothing, ultimately, is, but everything happens. The world, in other words, is in a constant state of flux and becoming, rather than stasis. So what does that actually mean in practice?
   </p>
   <p>
    The difference between things and events is their duration – how long they last in time. A stone, for example, is usually defined as a thing, while a kiss is seen as an event. The stone will outlast the kiss – a fleeting moment – by thousands of millennia. But when you zoom out and take a truly long-term perspective, things look different. After all, a stone is nothing more than an interaction of particles. Eventually, even the sturdiest rock will return to dust as those relationships break down. From that perspective, even something as thing-like as a stone starts to look like a really long event!
   </p>
   <p>
    That view of the world also influences how scientists understand time. We usually only say something is real if it exists in the here and now. Everything else has already concluded or is merely hypothetical. Philosophers call this way of thinking
    <em>
     presentism
    </em>
    . But that just doesn’t square with the findings of physicists. So, what’s the alternative?
   </p>
   <p>
    Well, as we’ve seen, the present can’t be defined globally, because each point in space has its own time. We also know that time isn’t an orderly succession of events from the past through to the present and into the future. That means that everything that exists in any one of these three time frames must be equally real – call it
    <em>
     eternalism
    </em>
    .
   </p>
   <p>
    As a result, physicists have abandoned all attempts to apply independent time variables to the equations they use to describe the world. In the new field of
    <em>
     quantum gravity,
    </em>
    for example, scientists have given up on the idea of mapping out how things change in time, and instead, focus their energies on describing how things change relative to each other.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5cd148656cee070008f3fdf8" data-chapterno="5">
  <h1>
   Time is an “emergent phenomenon,” which arises from our perception of the world.
  </h1>
  <div class="chapter__content">
   <p>
    As we’ve seen, squaring physicists’ understanding of time with our own everyday notions is pretty tricky. But there is a way to reconcile our own experiences with the insights of modern physics – the concept of
    <em>
     emergent phenomena
    </em>
    .
   </p>
   <p>
    Imagine a group of schoolchildren who’ve decided to play a game of soccer. The first thing they’ll need to do is to split themselves into two opposing sides. Let’s say they do that by simply tossing a coin. But once the two teams are chosen, it doesn’t make much sense to ask where they were before they were formed. They weren’t anywhere.
   </p>
   <p>
    So, where did they come from? Well, they emerged as a result of the coin toss. In other words, they’re an
    <em>
     emergent phenomenon
    </em>
    . It’s helpful to think about time in similar terms. It doesn’t exist “out there” in the world as an objective standard – rather, it emerges as a result of our particular perception of the universe.
   </p>
   <p>
    At the center of that worldview is the idea that time flows. That, however, is down to what physicists call
    <em>
     entropy
    </em>
    . Here’s how it works: The only way we can tell past, present and future apart is by observing change. If everything stays the same, those three time frames become impossible to disentangle – everything looks the same. Change, however, is due to an increase in entropy.
   </p>
   <p>
    But what exactly is entropy? Well, it’s basically a way of quantifying how orderly things are. Low entropy refers to a high amount of orderliness, while high entropy refers to growing disorder. If your new car begins rusting and falling apart, for example, you can be sure that entropy has increased.
   </p>
   <p>
    And, according to the second law of thermodynamics, entropy is always increasing – that’s what guarantees that the flow of time is always moving in the same direction. But that view might also be a reflection of our position in the universe. If we were able to find a different perspective, things might look entirely different. Increasing entropy, in other words, doesn’t guarantee that time really is flowing in the direction we believe it is!
   </p>
  </div>
 </div>
 <div class="chapter supplement" data-chapterid="5cd148656cee070008f3fdf8" data-chapterno="5">
  <h1>
  </h1>
  <div class="chapter__content">
   <p>
    <em>
     “The entire coming into being of the cosmos is a gradual process of disordering.”
    </em>
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5cd148836cee07000830d5bc" data-chapterno="6">
  <h1>
   The sun is the source of all human life and history on Earth.
  </h1>
  <div class="chapter__content">
   <p>
    Chances are, you had these two core concepts drummed into you in your high school physics classes. First off, everything we do requires energy. Secondly, energy can’t be created or destroyed, only transferred – for example, into heat. That effectively means energy is always conserved. But here’s a quandary – if energy is always conserved, why do we constantly have to resupply it?
   </p>
   <p>
    To answer that, we need to return to the concept of entropy. What keeps things moving isn’t actually energy, but low entropy or high orderliness. In other words, what’s really making the difference are concentrated energy sources, which can be transformed into more disordered energy. In fact, every change on Earth – every cause and every effect – is driven by increasing entropy. Human history is a process beginning with low entropy, which gains ever higher levels of entropy right down into the present. If entropy didn’t increase, change would simply become impossible. And without change, we wouldn’t perceive the flow of time, making history itself inconceivable!
   </p>
   <p>
    So, what drives entropy on earth? Well, in short, the sun – a rich source of low entropy close enough to our planet for it to be usable. Here’s how that works. The sun radiates hot photons – light particles – toward Earth. The earth, on the other hand, emits ten colder photons for every hot photon it receives. These have less energy, thus balancing out the amount of energy received and emitted.
   </p>
   <p>
    A hot photon, however, has considerably less entropy than ten colder photons. That means the sun serves as a ready source of low entropy, which the earth can use to power its own processes, whether that’s wood burning or water breaching a dam. As a result, low entropy is transformed into high entropy. That applies to all living beings. Chemical reactions in the human body, for instance, depend on the sun’s low entropy energy. Photosynthesis, meanwhile, allows plants to store energy that animals and humans then access by eating them!
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5cd148986cee070008f3fdf9" data-chapterno="7">
  <h1>
   The experience of time is a creation of our mind.
  </h1>
  <div class="chapter__content">
   <p>
    We’ve covered a lot of ground in these blinks, so let’s wrap things up by returning to our starting point – our own relationship with time. It’s a fitting place to end, as it’s the biggest conundrum of them all. We know time isn’t a universal feature of the world, and yet it’d be hard to imagine human life without the passage of time. So, where does our perception of time come from? Well, it’s basically all about our subjective view of the world. In other words, it’s a core part of what gives us our identity as humans.
   </p>
   <p>
    That has three components. The first is our point of view, our identification with a particular way of looking at the world and the standpoint from which we process and assimilate information. Secondly, we tend to break that information down into smaller pieces that make it easier for us to understand the world. Just think of the way we gather a bundle of rocky matter together and give it the name “Mont Blanc.” More importantly, we consolidate the various processes that constitute other human beings to give ourselves a better idea of what a human actually is. And that intellectual move shapes our view of the world and determines how we interact with others. Our notion of self, in other words, isn’t formed through introspection, but through interaction.
   </p>
   <p>
    The third component of our identity is our memory. Our experiences of Mont Blanc or another person aren’t stored randomly in our minds – in fact, they’re filed away neatly on chronologically arranged index cards, marking distinct chapters of our past. That allows us to conceive of the world as the product of a historical chain of events, and our own lives as narrative arcs connecting the past to the present and, through anticipation, the present to the future.
   </p>
   <p>
    And that’s where our understanding of time comes from – the systematic storing of experienced and subjective information and experiences. Time, in other words, is rooted in the internal workings of the mind, and makes us who we are as a species!
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5cd148a96cee07000830d5bd" data-chapterno="8">
  <h1>
   Final summary
  </h1>
  <div class="chapter__content">
   <p>
    The key message in these blinks:
   </p>
   <p>
    <strong>
     Time – at least in the way we imagine it on a day-to-day basis – simply doesn’t exist. Modern physics shows that time isn’t at all like a clock, steadfastly moving toward the future at a regular speed. In fact, its movement depends on where you are and how fast you’re moving. Enter light years into the equation and you soon discover that the concept of “the present” is basically nonsensical. But that doesn’t mean our commonplace notions are worthless, even if they are wrong. In fact, there’s a pretty good argument to be made that it’s our ordinary sense of chronological time that makes us human!
    </strong>
   </p>
   <p>
    <strong>
     Got feedback?
    </strong>
   </p>
   <p>
    We’d sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
   </p>
   <p>
    <strong>
     What to read next:
    </strong>
    <strong>
     <em>
      Seven Brief Lessons on Physics
     </em>
    </strong>
    <strong>
     , by Carlo Rovelli
    </strong>
   </p>
   <p>
    Hooked on the mind-boggling world of modern physics? Want to know more about Einstein’s theory of relativity and quantum mechanics? Well, why not let Carlo Rovelli continue his tour of the mysteries and wonders of the universe with the blinks to his bestselling and highly accessible
    <em>
     Seven Brief Lessons on Physics
    </em>
    (2014).
   </p>
  </div>
 </div>
</article>
