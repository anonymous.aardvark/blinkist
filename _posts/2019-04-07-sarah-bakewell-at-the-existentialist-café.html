---
layout: post
title: "Sarah Bakewell - At The Existentialist Café"
description: "At the Existentialist Café (2016) recounts the birth of existentialism in the early twentieth century. Both a biography and a philosophical text, it tells the stories of individual philosophers as well as their ideas. Above all, it explores how big philosophical questions can illuminate our lives and the way we live them."
image: https://images.blinkist.com/images/books/5c7fc3ce6cee07000722a7c3/1_1/470.jpg
---

<article class="shared__reader__blink reader__container__content" lang="en">
 <div class="chapter chapter" data-chapterid="5c7fca3f6cee07000722a7c5" data-chapterno="0">
  <h1>
   What’s in it for me? Learn how philosophy can be a part of life.
  </h1>
  <div class="chapter__content">
   <p>
    Philosophy can seem like a distant subject to most of us. Perhaps we might imagine a bearded figure in a cave somewhere, pondering the nature of reality. It doesn’t seem to have much to do with our everyday lives. But that’s what made
    <em>
     existentialism
    </em>
    different.
   </p>
   <p>
    Starting in the 1930s, Jean-Paul Sartre, Simone de Beauvoir and others began developing a philosophy that would embrace life and ask questions about how and why we live the way we do. Their philosophy would not only guide them in their own lives – it would sustain them in the dark days of the Second World War.
   </p>
   <p>
    In these blinks, you’ll learn
   </p>
   <ul>
    <li>
     how to use a cocktail in philosophy;
    </li>
    <li>
     how to talk about pain; and
    </li>
    <li>
     why so many people keep seeing women as objects.
    </li>
   </ul>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5c7fca866cee070007fe9934" data-chapterno="1">
  <h1>
   An apricot cocktail started Jean-Paul Sartre on the road to existentialism.
  </h1>
  <div class="chapter__content">
   <p>
    For a lot of us,
    <em>
     existentialism
    </em>
    is a murky term, one that suggests gloomy ideas about the futility of life. But existentialism started with something a lot more cheerful: an apricot cocktail.
   </p>
   <p>
    Around the end of 1932, Jean-Paul Sartre, his girlfriend Simone de Beauvoir and his friend Raymond Aron sat in the Bec-de-Gaz bar in Paris, sipping cocktails and catching up. The three had studied philosophy together at École normale supérieure in Paris, and had graduated restless and unsatisfied. The school’s curriculum was dominated by the same questions that philosophers had been asking since Plato’s time, like “How can I know that things are real?” and “How can I be sure that I know anything for certain?” It was hard to see the point in any of it, and the three friends were hungry for a new kind of philosophy, something that spoke to their dissatisfaction with the stale old questions that had bored them in school.
   </p>
   <p>
    But what other ways of philosophizing could there be? Sartre and Beauvoir had been teaching in the French provinces since they’d graduated, and neither had any new ideas to report. Aron, though, thought he had found an answer. Studying in Berlin after graduation, he had encountered a new and different philosophy that had originated in Germany:
    <em>
     phenomenology
    </em>
    . What made phenomenology so exciting was that it put stale metaphysical questions like the ones they’d studied at the École normale supérieure to one side in order to look at the stuff of real, everyday life. With phenomenology, Aron said, he could even use his apricot cocktail to philosophize!
   </p>
   <p>
    His friends were astounded.
   </p>
   <p>
    Sartre lit up, and went straight to a bookstore. Hungry to know more about phenomenology, he asked for every book on the topic that the shop had. Unfortunately, that only meant a single book. Though Sartre tore through it, he was desperate to know more; soon he was making arrangements to study in Berlin for a year, just as Aron had. There, Sartre created something entirely new, blending the phenomenology books that he read with other philosophers’ ideas and his own literary style and personality. By the time he returned to Paris in 1934, he was ready to found a new philosophy of his own: existentialism.
   </p>
   <p>
    Sartre’s year in Berlin had been fruitful. But ironically, a different city in Germany was actually the heart of phenomenology: Freiburg.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5c7fcaaa6cee07000722a7d2" data-chapterno="2">
  <h1>
   Freiburg was the center of a new philosophy: phenomenology.
  </h1>
  <div class="chapter__content">
   <p>
    Freiburg-im-Breisgau is a university city in southwest Germany bordered by the Rhine river and the Black Forest. In the first decades of the twentieth century, it became the epicenter of phenomenology. Students flocked there to study with the discipline’s founder, Edmund Husserl, who became the university’s philosophy chair in 1916.
   </p>
   <p>
    We’ve already mentioned phenomenology. But what is it exactly?
   </p>
   <p>
    Well, it’s not so much a theory, but rather a method for describing phenomena. Phenomena could be anything, from events to feelings to objects. And describing, in this case, means saying everything you can about the phenomena you encounter through firsthand experience.
   </p>
   <p>
    Take an apricot cocktail. Older styles of philosophy might ask whether the cocktail really exists, or is simply a construction of your mind. Now, there’s nothing wrong with that, but while you’re thinking about the cocktail’s theoretical existence, chances are that you’re still sitting there sipping it. So why not stop fretting about its existence and pay attention to the delicious drink in your hand?
   </p>
   <p>
    To describe it, you could probably start with facts like the way the bartender made it or where the apricots were grown. Or maybe you have memories of other cocktails, like drinking them with your mother as a young adult. But these facts and memories are really just preconceived notions - they won’t help you talk about
    <em>
     this
    </em>
    specific cocktail.
   </p>
   <p>
    That’s why you need to conduct what Husserl called an
    <em>
     epoché
    </em>
    . Epoché is an ancient Greek word meaning “suspension of judgment,” and Husserl used it to mean setting aside preconceived notions so you can really see the phenomena in front of you clearly. It puts your focus, as Husserl said, on seeing “the things themselves” with fresh eyes.
   </p>
   <p>
    But why do this? Well, for one thing, it can be extraordinarily illuminating to look at certain things this way. Take pain: if a patient describes his pain using preconceived notions about pain in general, it won’t help his doctor at all. But an accurate description of the pain he’s really experiencing can lead to an accurate diagnosis.
   </p>
   <p>
    Instead of diagnosing illness, phenomenologists were trying to fully understand life. And Husserl and his devotees didn’t take this lightly. They refused to be satisfied with superficial information; a phenomenologist describing a piece of music couldn’t just call it “lovely.” Instead, more specificity was required – the music might be “plaintive” or “full of a great dignity,” for example. They continued discarding weak, superficial descriptions and formulating better ones like this until they felt like they had hit on what the phenomena really were.
   </p>
   <p>
    In 1918, they were joined in their mission by a young man who would make a greater impact upon phenomenology than anyone else: Martin Heidegger.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5c7fcacb6cee070007fe9935" data-chapterno="3">
  <h1>
   Martin Heidegger was both a giant of philosophy and a deeply flawed man.
  </h1>
  <div class="chapter__content">
   <p>
    Many students end up eclipsing their teachers, creating something entirely new out of the things they are taught. This was definitely the case with Husserl’s brightest student, Martin Heidegger, who revolutionized philosophy with his 1927 book
    <em>
     Being and Time.
    </em>
   </p>
   <p>
    As a student of Husserl, Heidegger had learned to set aside his preconceived notions in order to look more clearly at things. Looking at a cup of coffee, he might have said, “It is rich and dark.” But in
    <em>
     Being and Time
    </em>
    , he asked an important question about that kind of statement: What exactly does “is” mean?
   </p>
   <p>
    Heidegger believed that Husserl and others had made a major mistake by overlooking the notion of
    <em>
     being
    </em>
    . Philosophers tended to see themselves as observing the world and asking questions from an outside perspective. Heidegger pointed out that in order to ask questions, we first have to exist! That’s why
    <em>
     being
    </em>
    should be the real starting point for all of our questioning. By missing that, philosophers had been putting the cart before the horse.
   </p>
   <p>
    And Heidegger noticed another common philosophical habit at the time – philosophers’ tendency to think of themselves as somehow separate from the world they described, as if they were looking at it through a keyhole. Heidegger noted that we exist in the world along with the things we observe. What’s more, we relate to them in practical ways. To capture this, he coined the term
    <em>
     Dasein
    </em>
    from the German words “there” and “being.” In the place of “human being” or “he” or “she,” Heidegger used “Dasein.” This was his way of forcing his readers to remember the idea of being at all times.
   </p>
   <p>
    By 1929, Heidegger’s books and lectures had made him famous. But for all his intelligence, Heidegger was also flawed.
   </p>
   <p>
    The worst example of this came in 1933, when Heidegger accepted the position of rector of Freiburg University. This required him to join the Nazi party and enforce draconian laws requiring that anyone identified as Jewish be removed from university positions. This affected many people Heidegger knew, including his mentor, Husserl, who was stripped of his emeritus status and university privileges.
   </p>
   <p>
    Heidegger later claimed that he had misunderstood the Nazis’ intentions. But in 2014, a batch of his notebooks containing anti-Semitic, Nazi-oriented writings was published. Clearly, Heidegger had not been a member of the Nazi party simply because his university position required him to be.
   </p>
   <p>
    His party membership would also spell his downfall, as friends and colleagues disgusted by his choices abandoned him. As Jean-Paul Sartre might have pointed out, Heidegger’s thoughts weren’t the issue – it was his actions that defined him. We’ll dig deeper into this in the next blink.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5c7fcaf16cee07000722a7d3" data-chapterno="4">
  <h1>
   Existentialism is about the burden of freedom and responsibility.
  </h1>
  <div class="chapter__content">
   <p>
    Sartre was a novelist as well as a philosopher, so his writing about existentialism had a literary feel to it, especially the anecdotes he used to illustrate his ideas, which were often drawn from real life.
   </p>
   <p>
    This made sense, because at the heart of existentialism is the notion of freedom in real people’s lives.
   </p>
   <p>
    Just as phenomenology focused on discarding old preconceptions to look at the things themselves, existentialism focuses on stripping away our preconceptions about what defines us as human beings. A lot of factors such as biology, culture and our personal history influence who we are, of course, but none of those things necessarily define us.
   </p>
   <p>
    Instead, we’re free to define ourselves through the choices we make. As Sartre put it, “Existence precedes essence.” Once we start existing, we start forming the essence of who we are through our actions.
   </p>
   <p>
    Sartre summed this idea up in an anecdote about the German occupation of France during the Second World War: An ex-student of his asked for some advice. He wanted to flee the occupation and join the fight against Nazis, but doing so would leave his mother alone.
   </p>
   <p>
    What should he do?
   </p>
   <p>
    Like most of us, Sartre said, the student believed that he was bound by all sorts of things: morals, psychology and his personal history. But these aren’t really restraints – they’re all just part of the situation he found himself in. The truth was, he had no restraints: he was completely free to do whatever he wished.
   </p>
   <p>
    This freedom is an enormous responsibility. If nothing binds you or tells you what to do, then nobody but you is responsible for what you do. What’s more, what you do is important – your actions have consequences. You can try to avoid responsibility, of course, and pretend that something else is controlling your actions. But over time, the things that you do are adding up and creating who you are. Shirking your responsibilities just means that you’ll end up an inauthentic person.
   </p>
   <p>
    So Sartre’s advice to the student was simple: choose. And in doing so, invent yourself.
   </p>
   <p>
    As we’ll see, nobody assumed responsibility more than Sartre and de Beauvoir, who sought to embody their philosophy in every respect.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5c7fcb136cee070007fe9936" data-chapterno="5">
  <h1>
   To Sartre and de Beauvoir, existentialism was more than a philosophy – it was a way of living.
  </h1>
  <div class="chapter__content">
   <p>
    Sartre and de Beauvoir strove to exemplify their philosophy and ideals in every aspect of their lives. This started with their relationship. They became lovers when they were students and were soon inseparable. Marriage might have been the next logical step for many, but not for Sartre and de Beauvoir. To them, marriage meant assuming confining gender roles, accumulating property and lying to each other about affairs. This didn’t appeal to them and it didn’t fit with their philosophical ideas about freedom and choice either.
   </p>
   <p>
    So they chose something else instead. One evening in 1929, in the Tuileries Garden in Paris, the two made an arrangement. They would agree to a “two-year lease” – meaning that they would be a couple for two years, albeit in an open relationship. After two years, if things were working, they could continue on. If not, they could split up or change the relationship in some way.
   </p>
   <p>
    As it turned out, it worked well – the two philosophers spent the next 50 years together until Sartre’s death in 1980. Each had many other lovers in that time, but their relationship always remained primary for both of them.
   </p>
   <p>
    They were partners in work as well as in life, too. At their core, both Sartre and de Beauvoir were writers. They spent their lives writing side by side, at home and abroad, at desks and in cafés, writing diaries, letters, essays, articles and books. They were each other’s readers and editors, always prodding each other to write more and put more thought into their ideas.
   </p>
   <p>
    They pushed each other to remain true to their ideals in other ways too. Because existentialism was a philosophy of freedom, it inspired people seeking change in the world. The student and worker uprisings that swept Paris in 1968 were inspired by existentialist ideas and for Sartre and de Beauvoir it was only natural that they lend their support at marches, barricades and picket lines. Political activism was a consequence of their philosophical ideals.
   </p>
   <p>
    But let’s take a step back to look at how the writers’ commitment to their ideals didn’t decrease in difficult times – quite the opposite. It was significantly bolstered early on by their living through the occupation of France during the Second World War.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5c7fcb346cee07000722a7d4" data-chapterno="6">
  <h1>
   War upended the existentialists’ lives, but it didn’t stop their work.
  </h1>
  <div class="chapter__content">
   <p>
    Throughout 1939, anxiety was building – war was on the horizon. Following the German invasion of Poland in September, Britain and France declared war on Germany and ordinary people around Europe found their lives upended.
   </p>
   <p>
    For Sartre, this meant going to war. Persistent eye problems meant that he was posted to a meteorological station in Alsace rather than going to the front, but in 1940 he was taken prisoner and sent to a POW camp. In captivity, he took notes and read Heidegger’s
    <em>
     Being and Time
    </em>
    . It was an inspiration to him during this difficult time.
   </p>
   <p>
    De Beauvoir was seeking solace from philosophy too. She remained in occupied Paris, where food and supplies were increasingly scarce. Reading works by Hegel and Kierkegaard consoled her and inspired aspects of her novel
    <em>
     L’Invitée
    </em>
    , known in English as
    <em>
     She Came to Stay
    </em>
    .
   </p>
   <p>
    Meanwhile, Sartre’s reading and writing began to exacerbate his eye problems. When he was allowed to leave the camp to visit an ophthalmologist, he escaped by simply walking out and never returning. He returned to Paris and was reunited with de Beauvoir. The notes he’d been taking soon turned into his 1943 masterpiece,
    <em>
     Being and Nothingness.
    </em>
   </p>
   <p>
    In the book, Sartre argues that we’re literally nothing more than what we decide to be through our actions. But this amount of freedom can make you dizzy, like looking over a cliff. You look over the edge and feel vertigo, which manifests itself in the anxiety that you might impulsively jump off. The freedom to do so is terrifying; only tying yourself down could really take the anxiety – and freedom – away.
   </p>
   <p>
    We restrain ourselves to escape the burden of freedom in our lives in many ways, Sartre argues. Alarm clocks, for example, let us obey a loud noise rather than face the decision of whether or not to get up on our own. It lets us pretend that we aren’t free, thus making our life easier.
   </p>
   <p>
    We pretend in other ways, too. Take a Parisian waiter, zooming between tables with a fully-loaded tray in one hand. He moves with an exaggerated, unnatural grace – it’s definitely not the way he moves in his private life. He moves like this when he’s working, though, and Sartre says that’s because he’s in
    <em>
     bad faith
    </em>
    . This means he’s playing the role of a waiter, pretending that he’s something other than the free, fallible human being he is. Although there’s nothing wrong with this, the trick is to make sure that we never play it so well that we fool ourselves into thinking we aren’t free.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5c7fcb566cee070007fe993c" data-chapterno="7">
  <h1>
   Postwar France embraced the new in the form of existentialism.
  </h1>
  <div class="chapter__content">
   <p>
    After the devastation of the Second World War, it was clear that the Europe of old was gone. The future would require new ways of thinking, and existentialism provided just that.
   </p>
   <p>
    In 1945, existentialism exploded in popularity. On October 28, Sartre gave a public talk in Paris that was so overcrowded it was chaotic. Chairs were broken, people passed out from the heat and the whole event was big news. Before long, everyone was buzzing about Sartre and his philosophy.
   </p>
   <p>
    The epicenter of the existentialist scene was the Saint-Germain-des-Prés area of Paris. Sartre and de Beauvoir lived there for many years, spending a large part of their lives in cafés. They would write all day there and meet with friends, artists, writers, students and lovers. At night, they frequented clubs like the Lorientais or Le Tabou, where bands played live American music like blues, jazz and ragtime.
   </p>
   <p>
    It was a counterculture scene, one that took pleasure in danger, provocation and the rejection of all things bourgeois. De Beauvoir had a favorite story from this time that encapsulated the atmosphere. One day, she and her friend Wols sat drinking on the terrace of the bar when Wols was approached by a wealthy-looking man. Wols was a penniless artist and an alcoholic, but when the wealthy-looking man left, Wols was embarrassed to admit that the man had been his brother, who was a banker.
   </p>
   <p>
    This kind of countercultural switch – the destitute person ashamed to be seen with a wealthy one – doesn’t seem so strange today. But at the time it was new, and it delighted people like de Beauvoir.
   </p>
   <p>
    Existentialist culture was centered in Paris, but most of the people in it were fascinated with anything American. American music, for example, was extremely popular, representing defiance and hope. In the winter of 1943, the singer Juliette Gréco had been arrested and held by the Gestapo. She was soon released, but had to walk eight miles home wearing only a light dress. In defiance, she sang the American song “Over the Rainbow” as loud as she could as she walked.
   </p>
   <p>
    Existentialism was a philosophy and a scene, and it was around that time that Sartre and de Beauvoir developed a friendship with another influential figure, Albert Camus.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5c7fcbc86cee07000722a7d5" data-chapterno="8">
  <h1>
   Albert Camus was a friend, then an antagonist to Sartre and de Beauvoir.
  </h1>
  <div class="chapter__content">
   <p>
    In 1943, Sartre and de Beauvoir met the French-Algerian writer Albert Camus. Camus was handsome, warm, funny and emotional. Sartre and de Beauvoir liked him right away and the three became good friends.
   </p>
   <p>
    Camus didn’t describe himself as an existentialist, but his philosophy and novels had many existentialist elements. Camus also added a little absurdity to things. Take his 1942 book
    <em>
     The Myth of Sisyphus
    </em>
    . In it, Camus looks closely at a story from Homer’s
    <em>
     Odyssey
    </em>
    in which a king named Sisyphus is condemned by the gods to push a boulder up a hill each day, only to have it slip away near the top and roll back down every time.
   </p>
   <p>
    Camus wrote that, like Sisyphus, we go about our daily lives without thinking too much about them. But every once in a while we stop to ask ourselves why we keep going – just like Sisyphus must as he watches his boulder roll back down the hill. In those moments, we’re faced with a choice: give up or carry on. Carrying on means accepting that there’s no meaning to what we do. This wasn’t depressing, in Camus’ view – it was just absurd. He thinks that Sisyphus must have known this too, and imagines him pushing his boulder up the hill with a wry smile on his face.
   </p>
   <p>
    Sartre and de Beauvoir disagreed. They felt that what we do in life is far from meaningless and that meaning in life is different for everyone. To call life absurd did nobody any good.
   </p>
   <p>
    Another major difference between Camus and his friends emerged after the liberation of Paris from German occupation in 1945. Before long, the trials of those who had collaborated with the Germans started and some led to death sentences. Camus took a firm stance against this, convinced that it was always wrong for the state to take part in executions, torture and abuse.
   </p>
   <p>
    Sartre and de Beauvoir disagreed on this point too. If the state committing harm would help honor those who had been killed by the Nazis and ensure a clean slate for the future, it was a necessary evil. They thought Camus was being too idealistic.
   </p>
   <p>
    This was a sign that the war had shifted their politics and over time the friendship between Camus, Sartre, and de Beauvoir became increasingly strained until it fell apart in the early 1950s.
   </p>
   <p>
    Before this though, Simone de Beauvoir would use existentialism to explore another neglected topic – the lives of women.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5c7fcbe46cee070007fe993d" data-chapterno="9">
  <h1>
   Of all existentialist works, The Second Sex dealt the most directly with lived experience.
  </h1>
  <div class="chapter__content">
   <p>
    Existentialism could be applied to anything in life and it often was. So surely there was little ground left uncovered after the existentialist boom of the mid-1940s? Not so. In 1949, Simone de Beauvoir’s groundbreaking work
    <em>
     The Second Sex
    </em>
    examined the state of being as it applied to an overlooked category of people: women.
   </p>
   <p>
    De Beauvoir looked at women’s experience of being in the world, something that’s vastly different from that of men. This difference starts early in life, so people usually just chalk it up as something inherent to femininity. But de Beauvoir says that ideas about what’s “natural” are simply myths. And these myths need to be set aside if we’re going to examine what it means to be raised as a woman.
   </p>
   <p>
    Looking at childhood, for example, we can see boys and girls being taught to behave differently. Boys are encouraged to be active, while girls are told to focus instead on the way they look. By adulthood, this accumulates – most of a woman’s experiences have had the effect of reducing her agency in the world around her.
   </p>
   <p>
    They also learn to assume that a male point of view, or “gaze,” is the normal perspective in most situations, an idea that de Beauvoir developed from Hegel. When one consciousness interacts with another, Hegel wrote, each tends to take on the role of either master or slave. The master consciousness sees everything from his point of view, but the slave
    <em>
     also
    </em>
    tries to see things from the master’s point of view: she ends up seeing herself the way that he sees her.
   </p>
   <p>
    De Beauvoir thus argued that women are socialized into seeing themselves through the gaze of men. Instead of freely looking at the world around them, as men do, women are instead constantly aware of themselves as the ones being looked at. They become an object rather than a subject, even in their own eyes.
   </p>
   <p>
    <em>
     The Second Sex
    </em>
    is one of the great works examining culture, but it never received the acclaim it deserved at the time. It’s hard to say for sure what the reason for this was, but early English-language editions of the book cut out or distorted many of de Beauvoir’s main arguments and included soft-focus photos of nude women on the cover. This hindered the work from being taken seriously.
   </p>
   <p>
    Subsequent generations have come to recognize the importance of
    <em>
     The Second Sex
    </em>
    as a groundbreaking feminist text. More than any other work, it fulfilled the promise of phenomenology and existentialism by cutting through the clutter to give an incisive description of that most important of phenomena: the experience of living.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5c7fcc0c6cee07000722a7d6" data-chapterno="10">
  <h1>
   Final summary
  </h1>
  <div class="chapter__content">
   <p>
    The key message in these blinks:
   </p>
   <p>
    <strong>
     Philosophy has a long history as establishing itself as something separate from the world, in which thinkers pursue deep questions that have no real relationship to life as it’s lived by real people. What’s more, most of it is based on old ideas and preconceived notions. Existentialism broke with all that, doing away with old ideas and grounding itself firmly in the experience of living. That’s what makes it such a powerful and relatable philosophy in difficult times.
    </strong>
   </p>
   <p>
    Actionable advice:
   </p>
   <p>
    <strong>
     Take nothing for granted.
    </strong>
   </p>
   <p>
    Jean-Paul Sartre continued to point out that humans are completely free. All we have to do is accept the freedom and the responsibility that comes with it. That’s why he and Simone de Beauvoir chose to have the kind of relationship that they wanted rather than what was expected of them. So the next time you reach a fork in the road in your own life, ask yourself: Should I do what I think is expected of me, or should I do what will most help me become the person I want to be?
   </p>
   <p>
    <strong>
     Got feedback?
    </strong>
   </p>
   <p>
    We’d sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
   </p>
   <p>
    <strong>
     What to read next:
    </strong>
    <strong>
     <em>
      The Second Sex
     </em>
    </strong>
    <strong>
     , by Simone de Beauvoir
    </strong>
   </p>
   <p>
    As these blinks have shown, existentialism was hugely influential when it came to looking at how people live their lives. And there was no work of existentialism that did this better than Simone de Beauvoir’s
    <em>
     The Second Sex
    </em>
    .
   </p>
   <p>
    In this classic feminist text, de Beauvoir takes a close look at history, myth, biology and life experience to show how women have been relegated to the role of the “second” sex behind men. But more than that, she shows how women can be more aware of the things that make them play the role of “other”, and what they can do about it. So if you want to sink your teeth into a real feminist classic, we highly recommend the blinks to
    <em>
     The Second Sex
    </em>
    .
   </p>
  </div>
 </div>
</article>
