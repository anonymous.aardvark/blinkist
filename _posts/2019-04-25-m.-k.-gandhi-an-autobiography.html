---
layout: post
title: "M. K. Gandhi - An Autobiography"
description: "An Autobiography (First published in two volumes; Volume 1, 1927, and Volume 2, 1929) is the autobiography of one of the world’s most famous political icons – Mohandas Karamchand Gandhi. The book traverses his rebellious childhood, his early activism in South Africa and his work for the Indian Independence Movement up until 1920, and gives insight into Gandhi’s personal philosophy and his lifelong quest for Truth."
image: https://images.blinkist.com/images/books/5c9b86556cee070007d517f7/1_1/470.jpg
---

<article class="shared__reader__blink reader__container__content" lang="en">
 <div class="chapter chapter" data-chapterid="5c9b86df6cee070007d517fa" data-chapterno="0">
  <h1>
   What’s in it for me? Get inspired by a remarkable life story about the world-changing power of Truth and nonviolence.
  </h1>
  <div class="chapter__content">
   <p>
    There are some historical figures who can seem larger-than-life. Whether it’s Martin Luther King, Jr., Amelia Earhart or Albert Einstein, it can often be difficult to imagine them growing up and having some of the same concerns as the average teenager.
   </p>
   <p>
    This is why it’s quite remarkable that we have the autobiography of Mohandas Karamchand Gandhi, one of the most influential people of the twentieth century, providing us with his own story from his own perspective. There’s a good chance you know about his nonviolent approach to activism and to fighting for change, and his autobiography sheds some valuable light on how many of the values that informed his peaceful philosophy developed.
   </p>
   <p>
    These blinks take us through his high-school years as a rebellious young man, to his early days as a shy, inexperienced lawyer, and ultimately to the inspirational human rights leader he became. Gandhi was a man whose life was guided by the pursuit of Truth, with a capital T, and his message continues to be a vital and inspirational one that deserves to be heard.
   </p>
   <p>
    In these blinks you’ll find
   </p>
   <ul>
    <li>
     how Gandhi struggled with doubts and a rebellious streak in his teenage years;
    </li>
    <li>
     how, in his early career as a lawyer, he suffered from others’ corruption and his own shyness;
    </li>
    <li>
     how he made a name for himself amid the segregation of South Africa.
    </li>
   </ul>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5c9b870c6cee070007cb6448" data-chapterno="1">
  <h1>
   Born into the merchant caste in Porbandar, India, Gandhi was wed in a child marriage at age 13.
  </h1>
  <div class="chapter__content">
   <p>
    If you’re familiar with world history and how nations become independent, you know that the process can often be a violent affair. Yet India was able to gain its independence from Britain, and this remarkable feat has a lot to do with the nonviolent beliefs of Mohandas Karamchand Gandhi, the man who led that fight for independence.
   </p>
   <p>
    As an adult, Gandhi’s beliefs, practices and accomplishments changed the world, but his upbringing was quite humble.
   </p>
   <p>
    Gandhi was born in India on October 2, 1869, the youngest child of Karamchand Gandhi, his father, and Putlibai, his mother. His family belonged to the Modh Bania, a class of Hindu merchants, and lived in the harbor town of Porbandar, on the Kathiawar peninsula, in the western state of Gujarat.
   </p>
   <p>
    Gandhi’s father was commonly known by the nickname “Kaba,” and he worked as a
    <em>
     diwan
    </em>
    , or chief minister, of two other Gujarati cities, Vankaner and Rajkot, where Gandhi spent much of his childhood. Kaba didn’t have any formal education, but he did have plenty of life experience, as well as an honest and incorruptible value system that proved to be quite influential with his son.
   </p>
   <p>
    Gandhi’s mother, Putlibai, also left a lasting impression, thanks to her devotion to Hinduism and determination to stay well-informed on current affairs. Gandhi was also raised to be tolerant and inclusive toward others, as the family’s friends were numerous and diverse, and included Muslims, Jains and Parsis.
   </p>
   <p>
    In school, Gandhi was an average student at best, but he did display an early talent for understanding morality.
   </p>
   <p>
    In particular, he admired the honorable characters from the plays that were read in class, such as
    <em>
     Shravana Pitribhakti Nataka
    </em>
    . In this enduring folktale, the protagonist’s devotion to his blind parents is so strong that he carries them on his shoulders.
   </p>
   <p>
    Gandhi also adopted an important guiding principle early on in life in the form of a saying passed down from the Gujarati community – if one receives evil from a person, one should respond with goodness.
   </p>
   <p>
    Certainly, Gandhi’s morality and sense of honor seem already to have been in place when his teacher tried to persuade him to copy his classmates’ work during a spelling test that was being given by an inspector. Young Gandhi simply couldn’t understand what the teacher was asking of him.
   </p>
   <p>
    It was only a few years later, as a 13-year-old, that Gandhi would be wed. Since such wedding ceremonies were quite expensive, the event also included the marriages of one of his brothers and a cousin.
   </p>
   <p>
    At the time, Gandhi was quite excited about getting married, but as an adult, he was a vocal critic of the practice of child marriage.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5c9b87616cee070007cb6449" data-chapterno="2">
  <h1>
   As a teenager, Gandhi had a rebellious phase that was marked by jealousy and lust.
  </h1>
  <div class="chapter__content">
   <p>
    Many people go through a rebellious phase during their teenage years, and though it might surprise you, Gandhi was no different.
   </p>
   <p>
    Indeed, while Gandhi was already a morally minded young man, he still experimented with a range of bad behavior during high school.
   </p>
   <p>
    Much of this behavior had to do with another teenager who had a bad reputation. Gandhi had befriended this troubled youth in an effort to reform him, but the influence often ended up going the other way.
   </p>
   <p>
    To begin with, the new friend convinced Gandhi to eat meat, in defiance of his family’s vegetarian practices. Part of the friend’s persuasive effort was his claim that being carnivores was what made the British occupiers stronger than the native Indians.
   </p>
   <p>
    Despite feeling sick to his stomach, Gandhi continued eating meat in a desire for strength, and even lied to his family about it. The guilt, however, eventually became overwhelming, and he stopped. He not only remained a lifelong vegetarian, he also believed that vegetarianism was the first step toward attaining Truth through nonviolence.
   </p>
   <p>
    Gandhi’s devotion to his friend didn’t stop at meat-eating – he also followed him to a brothel. And even though his nervousness prevented him from having sex outside his marriage, he considered the brothel visit a significant moral failing on his part.
   </p>
   <p>
    Another failing from around the same time was a smoking habit that Gandhi started alongside one of his relatives. What’s worse is that the two of them stole money in order to buy Indian cigarettes.
   </p>
   <p>
    Gandhi also looked back with some regret on the jealousy and lust he felt in the early years of his marriage to his wife, Kasturbai.
   </p>
   <p>
    When Gandhi reflects on Truth, he sees it as the vessel through which God reveals Himself to the world. And so, he saw his passion for Truth as fueling his desire to be faithful to Kasturbai – but it also had a dark side in his demands for Kasturbai to be as faithful as he was. Unfortunately, and for no reason, this resulted in him being a constantly jealous husband.
   </p>
   <p>
    Not only that, he was often driven by lust, taking every opportunity he had to sleep with Kasturbai rather than doing something like teaching her how to read and write. When Gandhi was 16 years old, the power that lust had over him was so strong that he left the bedside of his dying father to sneak into his wife’s bedroom. By the time he returned, his father had passed away.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5c9b87966cee070007d517fb" data-chapterno="3">
  <h1>
   Despite the disapproval of his caste, Gandhi went to England to study law.
  </h1>
  <div class="chapter__content">
   <p>
    Gandhi’s experiences during his high school years were already beginning to shape his future beliefs about things like vegetarianism, celibacy and nonviolence. Following his graduation from high school in 1887, his education continued after a friend suggested he study law at London’s University College. After all, becoming a lawyer would help ensure that he could maintain his family’s esteemed status.
   </p>
   <p>
    The decision to go to school in England wasn’t made lightly, however. His mother wasn’t sure it was a good idea, since the culture there would certainly provide him with many temptations in the form of women, food and alcohol. To strengthen his resolve, Gandhi made a sacred vow, with the help of a monk, to abstain from women, meat and wine.
   </p>
   <p>
    Matters were further complicated by India’s caste system. The caste his family belonged to claimed it was against their religion to travel abroad, and therefore threatened to kick Gandhi out if he went to London to attend University College. The threats, however, didn’t sway Gandhi.
   </p>
   <p>
    While he did end up getting expelled from his caste, Gandhi's time in England was fruitful, as it allowed him to learn in ways that would serve him well in the years to come.
   </p>
   <p>
    Sure enough, there were plenty of temptations to eat meat in London, especially at the boarding house he was living in. Fortunately, he stumbled upon a fine vegetarian restaurant on Farringdon Street that allowed him to honor his vow
    <em>
     and
    </em>
    get a satisfying meal.
   </p>
   <p>
    Gandhi eventually joined the Vegetarian Society and even started a local chapter in his London neighborhood of Bayswater. This was also significant in that it provided him with some early experience in how to run an organization.
   </p>
   <p>
    These college years also taught Gandhi how to live frugally and get by while managing a tight budget. Perhaps more importantly, this time gave him the chance to cultivate his knowledge of law and religion.
   </p>
   <p>
    Gandhi proved to be an apt student, having no trouble with the curriculum. So he decided to challenge himself by going beyond the standard textbooks, and even putting his knowledge of Latin to use by reading
    <em>
     Roman Law
    </em>
    in its original language!
   </p>
   <p>
    Meanwhile, Gandhi was also deepening his understanding of religion, with his studies including the Hindu scripture the
    <em>
     Bhagavad
    </em>
    <em>
     Gita
    </em>
    . He also familiarized himself with the Bible, and became particularly fond of Jesus’s Sermon on the Mount and the passages in the New Testament about altruism.
   </p>
   <p>
    When the time came, Gandhi had no trouble passing his examinations. On June 10, 1891, he was called to the bar, making him an official barrister of the court. Two days later, he was sailing back home to India.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5c9b87be6cee070007cb644a" data-chapterno="4">
  <h1>
   Seeking professional experience, Gandhi went to South Africa and saw the effects of racism firsthand.
  </h1>
  <div class="chapter__content">
   <p>
    Alas, Gandhi's return home was not a happy one, as he soon found out that his mother had died while he’d been at university. Naturally, he was devastated by this news.
   </p>
   <p>
    There was, however, one small piece of good news awaiting his return. There’d been a major division among his caste, causing it to split in two, with one side willing to welcome him back into the fold.
   </p>
   <p>
    While Gandhi was officially a barrister, being a confident, practicing lawyer was another matter. In order to gain some much-needed experience, Gandhi traveled to Bombay, where he began reading books and studying Indian law.
   </p>
   <p>
    Yet, when the time finally came for him to present a case in court, he found himself paralyzed by the shyness that had been with him for much of his life. He handed the case to another lawyer and decided he wouldn’t accept another case until he’d developed enough courage to speak before a judge.
   </p>
   <p>
    With little money to support himself in Bombay, Gandhi had to return to Rajkot, in Gujarat. However, things weren’t much better there. Not only did he see firsthand how corrupt the local courts were, but after he accidentally offended a police officer, Gandhi worried that the officer would use his power to limit Gandhi’s chances of getting work.
   </p>
   <p>
    Things changed in April of 1893, when he found work at Dada Abdulla &amp; Co., a Muslim law firm in South Africa. Once in this new land, however, Gandhi was shocked to see how divided the population was, separated by religion, ethnicity and employment.
   </p>
   <p>
    Not long after his arrival, Gandhi took a stand against a magistrate who wanted Gandhi to remove the turban he was wearing. Gandhi not only left the court, but he also wrote about his ordeal in the press.
   </p>
   <p>
    Around this time, Gandhi became familiar with the different ways in which racism was prevalent in South Africa. Especially significant was the large population of indentured Indian laborers, known as “coolies,” who’d been arriving in South Africa since the 1860s. Eventually, Gandhi would offer legal representation for many in this community.
   </p>
   <p>
    He also had his own share of personal experiences with discrimination.
   </p>
   <p>
    For example, while traveling by train to Pretoria to work a big case, he was asked to give up his compartment seat and move to the back section of the train, even though he’d bought a first-class ticket. Then, at a hotel, the owner came close to forbidding him from eating with other guests.
   </p>
   <p>
    In a display of Gandhi’s magnanimous nature, when a police officer suddenly kicked him, Gandhi forgave the man and made a vow never to take people to court over a personal offense.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5c9b87e06cee070007d517fc" data-chapterno="5">
  <h1>
   In addition to his legal work, Gandhi was committed to public service and studying religion.
  </h1>
  <div class="chapter__content">
   <p>
    In Pretoria, Gandhi’s experiences of racism and segregation were already informing his study of the conditions of oppression. These were the early days of what would become a lifelong fight for justice; at the same time, Gandhi was also finding his feet as a lawyer, an activist and a leader.
   </p>
   <p>
    In his first big case, he represented a Muslim merchant who was suing another merchant over a fraudulent transaction.
   </p>
   <p>
    As befitted his generous nature, Gandhi didn’t just help his client win the case, but he also made sure the other merchant could pay off his debt in installments, thereby saving him the embarrassment of declaring bankruptcy.
   </p>
   <p>
    Meanwhile, Gandhi’s experiences with discrimination were igniting within him a desire to become more involved in what he called “public work,” or what people today would call
    <em>
     activism
    </em>
    .
   </p>
   <p>
    In what was essentially his first public speech, Gandhi spoke before a group from Pretoria’s Indian community, many of whom were Muslim merchants of the Memon community. He began his message by emphasizing the virtues of truth in business. He then addressed the poor conditions of Indian people in South Africa and called upon them to put aside their differences and gain strength by joining together as an association, so that their concerns might be taken seriously by the authorities.
   </p>
   <p>
    This was to be the first of many weekly meetings. And eventually the events did become a place where the Indian community united, regardless of religion or status.
   </p>
   <p>
    During his time in Pretoria, Gandhi also continued to study religion and literature. He found inspiration in many philosophical books, like Tolstoy’s
    <em>
     The Kingdom of God Is Within You
    </em>
    , which makes a strong case for nonviolent resistance.
   </p>
   <p>
    In addition to Gandhi’s ongoing participation in a prayer group, his boss, Abdulla Sheth, helped him to learn more about Islam, and Gandhi’s colleague, the lawyer A. W. Baker, helped him gain insight into Christianity.
   </p>
   <p>
    Ultimately, Gandhi came to doubt the Christian tenets that Jesus was without sin and was the only son of God. Rather than believing that Jesus would one day free him from his sins, Gandhi was more interested in freeing himself from sinful thoughts.
   </p>
   <p>
    He also came to see flaws in Hinduism as well, especially in its justifications for the caste system, and he discussed this problem with Hindu authorities. His career as a leader for peaceful change was gaining momentum.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5c9b88006cee070007cb644b" data-chapterno="6">
  <h1>
   Before spreading his message of equality in India, Gandhi’s southern African work continued in Natal.
  </h1>
  <div class="chapter__content">
   <p>
    Gandhi had assumed that he’d be heading back home once his case in Pretoria wrapped up. But another case in southern Africa caught his attention, this time in the Republic of Natal.
   </p>
   <p>
    In 1893, legislation was enacted that stripped Indians living in Natal of their right to elect members to the country’s legislative assembly.
   </p>
   <p>
    Gandhi accepted the opportunity to work on this case, and even though the legislation had already passed, having Gandhi on their side gave hope to the Indian community. Sure enough, things took a positive turn when a petition for regaining Indian voting rights was submitted to and accepted for consideration by the Secretary of State for the Colonies.
   </p>
   <p>
    Then there was concern when an official objection was made to prevent Gandhi from trying the case in the Supreme Court of Natal, purely because he wasn’t white. Gandhi did offer to take off his turban while in court, citing the old adage, “When in Rome, do as the Romans do.” Thankfully, however, the objection was struck down.
   </p>
   <p>
    To help advance the case, as well as the community in Natal, Gandhi’s team set up a permanent organization called the Natal Indian Congress. This organization would help find new work for any indentured worker who’d been beaten by an employer. As well, through this organization, Gandhi also defeated a proposed annual tax of £25 for indentured Indians.
   </p>
   <p>
    Finally, in 1896, Gandhi returned to India for six months, reuniting with his wife and children after a three-year absence.
   </p>
   <p>
    Of course, he couldn’t stay inactive for long, and during this time back home, he managed to stir up some interest in his new cause by writing the
    <em>
     Green Pamphlet
    </em>
    . This document laid bare the terrible conditions of Indians living in South Africa and Natal.
   </p>
   <p>
    Before long, Gandhi was traveling again, this time to the Indian cities of Poona and Madras to raise more support for Indians in South Africa, as well as to Calcutta to meet with press editors about spreading the word. Unbeknownst to him at the time, Gandhi was planting the seeds for the homegrown activism he would later undertake.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5c9b881f6cee070007d517fd" data-chapterno="7">
  <h1>
   After helping the wounded in the Boer war, Gandhi stayed humble and informed in India.
  </h1>
  <div class="chapter__content">
   <p>
    In 1897, Gandhi returned to Natal, and this time he brought his family with him. Unlike the novice lawyer who’d left India years before, he was now an established leader of the Indian community throughout southern Africa.
   </p>
   <p>
    This newfound role didn’t come without its dangers, however. Enraged by press reports of Gandhi’s efforts for change, a group of young white men tried to lynch him in the streets of Natal, though he managed to escape unharmed.
   </p>
   <p>
    More violence was to come in 1899, but this time it was a full-blown war between the British Empire and southern African Boer States. While Gandhi’s sympathies were with the oppressed Boers, he felt duty-bound to serve the British Empire. He did so by establishing an ambulance corps, made up of around 1,000 South African Indians, as well as Gandhi himself.
   </p>
   <p>
    The tireless work of the ambulance corps did not go unnoticed following Britain’s victory in the war. The Indian workers gained national prestige for their efforts, while Gandhi’s political stature grew even further. And then there was the added benefit of a diverse group of Indian workers emerging from the experience more unified than ever before.
   </p>
   <p>
    Amid all of this activity, Gandhi found himself making a good deal of money, and he was worried that he might succumb to the greed that often accompanies wealth. So he returned to his beloved India and volunteered for the Indian National Congress.
   </p>
   <p>
    It was a humbling experience, since it involved such menial tasks as buttoning up an official’s shirt. When asked about this work, Gandhi claimed that the experience was worthwhile, for it brought valuable insight into how the Congress worked.
   </p>
   <p>
    Meanwhile, Gandhi wrote a resolution on the rights of Indians in South Africa, and though he still suffered from terrible anxiety when it came to public speaking, he defended the resolution in front of the Congress. Remarkably, it received unanimous approval.
   </p>
   <p>
    At this time, Gandhi found a new mentor, Gopal Krishna Gokhale, who served as a senior leader in the Indian National Congress. While living with him for a month, Gandhi got the chance to meet a variety of affluent Indians and raise the many concerns he had about the struggles facing working-class Indians.
   </p>
   <p>
    As his time with Gokhale and the Congress came to an end, Gandhi toured India as a third-class passenger aboard a train. This gave him an honest perspective on the pressing issues at hand, including the poor sanitation and treatment that a great many people in India faced on a daily basis.
   </p>
   <p>
    More than ever, Gandhi’s resolve to help the people of India was strengthening, as was his philosophy regarding how change should happen.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5c9b883a6cee070007cb644c" data-chapterno="8">
  <h1>
   Gandhi was committed to both nonviolence and celibacy.
  </h1>
  <div class="chapter__content">
   <p>
    Thanks to his time with Gokhale, his tour of India and his years of study, Gandhi’s experiences and observations were coalescing into a philosophy that would come to define his life and legacy.
   </p>
   <p>
    At the core of this philosophy was his unwavering belief in nonviolence, or
    <em>
     ahimsa
    </em>
    .
   </p>
   <p>
    The principle of
    <em>
     ahimsa
    </em>
    goes back to Gandhi’s study of the Hindu scriptures, in which it applies to the belief that it’s acceptable to attack a system, but never to attack a person. This is because all people are a reflection of Truth, or God, which makes them deserving of compassion, empathy and an attempt to understand their point of view.
   </p>
   <p>
    Gandhi was using his belief in
    <em>
     ahimsa
    </em>
    when he urged police to do something about white officers in India taking bribes from Indian and Chinese people. When calling for action, he emphasized the fact that this was not a personal attack against the individual officers. Eventually, these officers were tried and found not guilty, primarily because they were white, but they did lose their jobs, which put an end to racialized bribery in the region.
   </p>
   <p>
    A friend once asked Gandhi how his work in the ambulance corps aligned with his nonviolent philosophy. While Gandhi admitted that war wasn’t consistent with
    <em>
     ahimsa
    </em>
    , he explained that he felt duty-bound to the British, and also wanted to help Indians gain more rights by raising their status in the eyes of the average Brit – which is precisely what happened.
   </p>
   <p>
    Years later, however, Gandhi could no longer justify such an approach, and would add the principle of noncooperation to his call for nonviolence.
   </p>
   <p>
    A big part of Gandhi’s philosophy is self-restraint, and this became significantly stronger for him in 1906, when he took the vow of
    <em>
     brahmacharya,
    </em>
    or celibacy. Before making the vow, he did consult his wife, Kasturbai. Together, they’d had four children by this point, but ever since their marriage, Gandhi had felt burdened and distracted by lust.
   </p>
   <p>
    Ultimately, Kasturbai gave her approval, and Gandhi felt freed by this newfound self-restraint. He believed that to best serve the public, one’s focus should be fully on the people – so much so, he even took to eating simple, bland foods that wouldn’t distract his senses.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5c9b885c6cee070007d517fe" data-chapterno="9">
  <h1>
   Gandhi continued to fight for Indians in South Africa and launched a new form of nonviolent resistance.
  </h1>
  <div class="chapter__content">
   <p>
    In his mid-thirties, Gandhi settled in Bombay, with the images and experiences of his recent tour still fresh in his mind. But it wasn’t long before his friends in South Africa were calling him back to help with the ongoing fight for equality.
   </p>
   <p>
    Indeed, thanks to his legal efforts and leadership in uniting South African Indians of all religions and classes, Gandhi was by this time an established leader with devout followers.
   </p>
   <p>
    To help further the cause, Gandhi launched a weekly journal called the
    <em>
     Indian Opinion
    </em>
    , which debuted in 1904. Around the same time, just north of Durban, South Africa, he founded the Phoenix Settlement, a communal farm inspired by John Ruskin’s 1860 book
    <em>
     Unto This Last
    </em>
    , where he and other people could live a simple, egalitarian life.
   </p>
   <p>
    A couple of years later, in 1906, the Zulu mounted a rebellion against the British. Still feeling a duty to serve Britain, Gandhi once again led an ambulance corps of Indian volunteers, and this time the atrocities they witnessed were even worse than those of the Boer War.
   </p>
   <p>
    Gandhi also led an effort to help a mining operation in Johannesburg when a Black Plague outbreak ravaged the miners. He helped Indians to safety and cared for those infected, while the location was burned to the ground to keep the disease from spreading.
   </p>
   <p>
    In 1907, Gandhi began a new form of nonviolent resistance against the Asiatic Registration Act, a discriminatory piece of legislation created by the Transvaal government of South Africa.
   </p>
   <p>
    The new law placed Indians under threat of deportation if they didn’t carry registration papers on them at all times. In other words, it was another way for the government to segregate, discriminate against and control certain members of society.
   </p>
   <p>
    Gandhi took this act as a sign that previous methods of resistance using prayer and legal action had failed. So this marked the start of a new method called
    <em>
     satyagraha
    </em>
    , a name that is derived from the Sanskrit word for truth and firmness:
    <em>
     sadagraha
    </em>
    .
   </p>
   <p>
    While the new name was the result of a call for suggestions that Gandhi had put out among his friends and associates, the philosophy behind the
    <em>
     satyagraha
    </em>
    was inspired by the writings of Tolstoy, Thoreau and Ruskin. These great literary minds advocated that citizens launch campaigns of nonviolent noncooperation against oppressive governments.
   </p>
   <p>
    This philosophy was a culmination of Gandhi’s commitment to Truth and his ideals of pacifism, resistance against injustice and civil disobedience. The concept would continue to evolve in the years ahead, and it would appear frequently in his published writings.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5c9b887e6cee070007cb644d" data-chapterno="10">
  <h1>
   Upon the start of WWI, Gandhi returned to India, where he continued to fight injustice.
  </h1>
  <div class="chapter__content">
   <p>
    Amid his continued progress in South Africa, a series of events began to unfold that had Gandhi seeking the solace of his home in India.
   </p>
   <p>
    In 1914, Gandhi sought to reunite with his mentor, Gopal Krishna Gokhale, who was staying in England. But two days before he was to reach London, WWI broke out and, along with it, much chaos.
   </p>
   <p>
    Around this time, Gandhi’s health took a turn for the worse, with an attack of pleurisy, an inflammation of the lungs and chest, causing enough concern that he returned to India in the hopes of recuperating.
   </p>
   <p>
    By the time Gandhi reached his homeland, in January of 1915, he was 45 years old and a national hero. News of his accomplishments in South Africa had spread across India, and his return was the subject of much fanfare.
   </p>
   <p>
    Among his first activities back home was to start the Satyagraha Ashram in Ahmedabad, similar to his beloved Phoenix Settlement commune, where others in India could live a simple and satisfying life.
   </p>
   <p>
    Meanwhile, there proved to be no shortage of injustice in India, as Gandhi continued to push back against an exploitative social system.
   </p>
   <p>
    A particular target was the
    <em>
     tinkathia
    </em>
    system, which forced tenants to plant indigo on behalf of their landlords, essentially turning them into serfs of the country’s landowners. Gandhi took on this case after being approached by a man named Rajkumar, who opened his eyes to this abusive and corrupt system.
   </p>
   <p>
    Reaction to Gandhi’s involvement came quickly, as he was soon arrested and thrown in jail. But this was no deterrent, since upon his release he continued to litigate for the tenants and eventually brought down the entire system. Remarkably, with pressure from Gandhi, the government finally abolished
    <em>
     tinkathia
    </em>
    , a practice that had been part of India for over a century.
   </p>
   <p>
    Attention was then turned to the Rowlatt Committee, an organization that was in charge of evaluating political terrorism in India. They were in the process of pushing through the Rowlatt Act, legislation that would give the British army the authority to arrest and detain Indians without producing any evidence for cause.
   </p>
   <p>
    This called for
    <em>
     satyagraha
    </em>
    , and before the law was released, Gandhi put out a nationwide call for a day of fasting, prayer and noncooperation. Although the bill would pass, Gandhi was still delighted with the massive response he received. It was just the start of what would become a series of nationwide movements.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5c9b88aa6cee070007d51809" data-chapterno="11">
  <h1>
   Gandhi suspended satyagraha when it triggered violence, but his noncooperation resolution was ultimately passed.
  </h1>
  <div class="chapter__content">
   <p>
    They say that it’s always darkest before the dawn, and that was certainly true in the case of India and its path to independence.
   </p>
   <p>
    Following his return to India, Gandhi was given a position of exclusive executive authority on behalf of the Indian National Congress, and he used this platform to further promote the ongoing Indian Independence Movement.
   </p>
   <p>
    Gandhi also organized the printing and selling of banned books, such as the Gujarati translation of Ruskin’s book
    <em>
     Unto This Last
    </em>
    , in order to raise money for the Independence Movement.
   </p>
   <p>
    However, on April 6, 1919, a protest against the Rowlatt Act turned deadly as the police clashed with protestors. Deeply shaken, Gandhi called off the protest and went on a five-day fast in penance.
   </p>
   <p>
    He considered it a huge mistake on his part to have asked people to take part in
    <em>
     satyagraha
    </em>
    when they’d never studied the principles of
    <em>
     ahimsa
    </em>
    and nonviolence. Following the tragedy, he made it a priority to educate the public on these principles in a weekly newspaper column.
   </p>
   <p>
    Another significant part of this movement was the
    <em>
     Salt Satyagraha
    </em>
    .
   </p>
   <p>
    Since the 1870s, there’d been a heavy tax placed on salt, making it unusually expensive for Indians to obtain, and putting a lot of money into the pockets of British colonizers. In a clever turn, Gandhi began to encourage Indians to use seawater to make their own salt.
   </p>
   <p>
    As for Gandhi’s noncooperation movement, real progress was made when a resolution was passed at the Nagpur annual congress meeting in 1920. The resolution called for an end to cooperation with the British colonial government and the beginning of India’s own constitutional rule.
   </p>
   <p>
    This essentially amounted to a boycott of British institutions, including educational and legal ones, as well as any British products. Instead of British textiles, Gandhi now advocated the use of khadi, homespun cloth garments, and urged both men and women of all ages to spin cloth as a part of the Independence Movement. He also asked any Indians employed by the colonial government to quit their jobs.
   </p>
   <p>
    This is where Gandhi’s autobiography comes to a close, though in many ways his movement for independence was just getting started. He believed that events such as the Salt March, which brought tens of thousands of people from across the country together, were so famous that they didn’t need repeating.
   </p>
   <p>
    Indeed, the commitment to Truth that emanated from Gandhi’s life and work continues to point the way forward for many peace-loving activists determined to see justice and equality for all.
   </p>
  </div>
 </div>
 <div class="chapter supplement" data-chapterid="5c9b88aa6cee070007d51809" data-chapterno="11">
  <h1>
  </h1>
  <div class="chapter__content">
   <p>
    <em>
     "But all my life though, the very insistence on truth has taught me to appreciate the beauty of compromise.”
    </em>
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5c9b88c36cee070007cb644e" data-chapterno="12">
  <h1>
   Final summary
  </h1>
  <div class="chapter__content">
   <p>
    The key message in these blinks:
   </p>
   <p>
    <strong>
     Even through his rebellious teenage years, Gandhi had an innate urge to find Truth, which he cultivated further through experiments with vegetarianism during his time in London. Gandhi’s philosophy of nonviolent resistance,
    </strong>
    <strong>
     <em>
      satyagraha,
     </em>
    </strong>
    <strong>
     was then developed through his study of racial prejudice against South African Indians before being applied to the injustices suffered by Indians under British rule. Always trying to understand the opposing side’s perspective and continually developing himself through dietary or other measures, Gandhi and his quest for Truth encourage us to continue to question the world around us and the truth within ourselves.
    </strong>
   </p>
   <p>
    Actionable advice:
   </p>
   <p>
    <strong>
     Regular exercise is crucial for a healthy mind.
    </strong>
   </p>
   <p>
    Although Gandhi disliked sports as a child, from an early age he developed the habit of taking long walks. Throughout his autobiography, Gandhi emphasizes the importance of exercising in order to enhance one's work. Of course, for Gandhi, exercise went hand-in-hand with maintaining a good diet. While you might not want to follow Gandhi’s more extreme experiments, like the time he only ate fruit and nuts, committing to a healthy diet and regular exercise can help you maintain a healthy mind.
   </p>
   <p>
    <strong>
     Got feedback?
    </strong>
   </p>
   <p>
    We’d sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
   </p>
   <p>
    <strong>
     What to read next:
    </strong>
    <strong>
     <em>
      India After Gandhi
     </em>
    </strong>
    <strong>
     , by Ramachandra Guha
    </strong>
   </p>
   <p>
    The autobiography ends on a tantalizing cliff-hanger, with the country on the verge of independence from British colonial rule. So if you want to keep the story going, and find out more about a tumultuous and sometimes brutal time in the nation’s history, we recommend the blinks to
    <em>
     India After Gandhi
    </em>
    .
   </p>
   <p>
    Plus, India’s history is about much more than the country’s break from Britain. While it holds the title of the largest democratic nation in the world, India is also on an exciting upswing, with massive development happening and a hugely influential economy. In short, it’s a place with which everyone should be more familiar.
   </p>
  </div>
 </div>
</article>
