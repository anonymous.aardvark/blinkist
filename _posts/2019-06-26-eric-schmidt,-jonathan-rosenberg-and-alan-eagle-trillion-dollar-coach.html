---
layout: post
title: "Eric Schmidt, Jonathan Rosenberg and Alan Eagle - Trillion Dollar Coach"
description: "Trillion Dollar Coach (2019) pays homage to Bill Campbell, a coach and mentor whose advice and insights helped some of Silicon Valley’s brightest lights build multi-billion dollar companies. In these blinks, Google leaders Eric Schmidt, Jonathan Rosenberg and Alan Eagle chart Campbell’s remarkable life, from the Columbia University football field to the Californian boardrooms in which the digital revolution was planned and rolled out. Along the way, they shed light on Coach Bill’s leadership philosophy."
image: https://images.blinkist.com/images/books/5ce515c56cee0700081dcd8f/1_1/470.jpg
---

<article class="shared__reader__blink reader__container__content" lang="en">
 <div class="chapter chapter" data-chapterid="5ce516636cee0700086ab003" data-chapterno="0">
  <h1>
   What’s in it for me? A look at the life and philosophy of a Silicon Valley legend.
  </h1>
  <div class="chapter__content">
   <p>
    What comes to mind when you think of Silicon Valley? Chances are that black turtlenecks, vast fortunes and Facebook rank high on your list of associations. But what about a working-class Pennsylvanian football coach who cursed like a sailor and blew kisses to his colleagues across the boardroom?
   </p>
   <p>
    Well, meet Bill Campbell, a business guru every bit as unorthodox as he was instrumental in helping start-ups like Apple and Google become the household names and multi-billion dollar ventures they are today. A friend and mentor to everyone from Steve Jobs to Google’s ex-CEO Eric Schmidt, Bill’s unique insights into leadership and team-building made him one of the world’s most influential and important business minds by the time of his death in 2016.
   </p>
   <p>
    That might sound like the work of a lifetime, but Bill crammed his amazing achievements into just a few decades. And unlike so many other would-be gurus, Bill was a one-off, a true original who never forgot his roots or the lessons he’d learned as a college football coach.
   </p>
   <p>
    Based on over 80 interviews with the people who knew and loved him, these blinks explore the life and ideas of a true Silicon Valley legend. Along the way, you’ll learn
   </p>
   <ul>
    <li>
     what flat hierarchies can and can’t do;
    </li>
    <li>
     why the most effective leaders don’t shy away from showing their emotions; and
    </li>
    <li>
     why trust is the most important currency in the boardroom.
    </li>
   </ul>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5ce516786cee0700081dcd91" data-chapterno="1">
  <h1>
   Bill Campbell started out as a football coach.
  </h1>
  <div class="chapter__content">
   <p>
    Silicon Valley is often associated with whip-smart university dropouts who revolutionize the world from their garages before they hit their mid-twenties. But innovation in the Golden State isn’t just a young man’s game – in fact, one of the tech mecca’s greatest pioneers didn’t arrive in California until he was already in his forties.
   </p>
   <p>
    Born in the Pennsylvanian steel town of Homestead in 1940, Bill Campbell was the son of a physical education teacher who moonlighted at the local mill. A quick-witted and determined student, Bill was set on making something of himself early on in life. In his teens, he took to the op-ed pages of the school newspaper to remind his peers of the importance of good grades and to warn them against “loafing.”
   </p>
   <p>
    But it wasn’t an academic career that he’d set his sights on – his true passion was football. After heading to New York to study economics at Columbia University in 1958, he joined the college team, the Lions. He wasn’t the most likely candidate: weighing 165 pounds and standing five foot ten, he was the team’s smallest member by some margin. But what he lacked in physical stature, he made up for in fearlessness and willpower – qualities that earned him the nickname “Ballsy.” Under his inspired captaincy, the Lions won the Ivy League title in 1961, a feat they’ve never managed to repeat.
   </p>
   <p>
    At the end of his studies, Bill was offered a position as an assistant football coach at Boston College. He jumped at the chance and moved north in 1964. Over the next decade he established himself as a highly capable coach, and offers from other universities began flooding in. One came from Penn State, home of the nation’s top college football coach, Joe Paterno. It was a golden opportunity but Bill turned it down. Why? In a word, loyalty – his alma mater had also offered him a job.
   </p>
   <p>
    Returning to Columbia in 1974 was a sentimental rather than a practical decision. The university’s football facilities were in poor shape and badly underfunded. This showed through in the team’s results. During Bill’s tenure, the Lions won just 12 games and lost 41, a run of bad form which ended in a humiliating 69-0 drubbing at the hands of Rutgers at Giants stadium. It was time to move on, and in 1979 Bill resigned.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5ce516936cee0700086ab004" data-chapterno="2">
  <h1>
   Bill’s decision to move to California launched his business career.
  </h1>
  <div class="chapter__content">
   <p>
    Bill’s coaching career was over and, aged 39, he entered the business world. His first job was at the ad agency J. Walter Thompson. He showed the same enthusiasm in his new role as he had on the gridiron, and clients loved him – especially Kodak, who decided to poach Bill from the agency and install him as their head of consumer products in Europe. It was a brilliant start to a late-blooming career.
   </p>
   <p>
    Things got even better in 1983, when Bill received a call from an old Columbia buddy. John Sculley had just left Pepsi to become the CEO of a tech start-up called Apple. Convinced he was on to the next big thing, John offered Bill a chance to get in on the ground floor. It was a huge move, but Bill accepted.
   </p>
   <p>
    Why? Well, he believed he’d climbed as high up the corporate ladder as he could: more senior positions would be unattainable for him because of his unorthodox background as a football coach. California, however, was different. It was easygoing and more of a meritocracy – in other words, a great place to take his career to the next level.
   </p>
   <p>
    He wasn’t wrong. Nine months later, Bill had been made vice president of sales and tasked with overseeing the launch of Apple’s new flagship computer, the Macintosh. It was in that position that he made one of the smartest decisions in the company’s history.
   </p>
   <p>
    In 1984, Apple bought an advertising spot during that year’s Super Bowl. Bill’s team had come up with an ad that riffed on George Orwell’s dystopian novel
    <em>
     1984
    </em>
    . It showed a young woman running from armed guards before bursting into a monumental chamber filled with grey-uniformed men with shaved heads watching a “Big Brother” figure giving a speech on a large screen. As she tosses a mallet at the screen, causing it to explode, a narrator promises that “1984 won’t be like
    <em>
     1984
    </em>
    .”
   </p>
   <p>
    Steve Jobs loved it, but Apple’s board hated it. Worried it was too controversial, they tried to sell the slot to another company. That’s when a sales executive asked for Bill’s final verdict. Throwing caution to the wind, he gave the go-ahead. The rest is history. The Big Brother-themed spot became one of the most famous ads of all time and ushered in the era of Super Bowl advertising we know today.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5ce516a96cee0700081dcd92" data-chapterno="3">
  <h1>
   After leaving Apple, Bill set himself on the path to coaching and mentoring.
  </h1>
  <div class="chapter__content">
   <p>
    The first leg of Bill’s journey with Apple came to an end in 1990. In his last years at the company, he headed up a spin-off software venture called Claris. It did well under his stewardship, but Apple ultimately decided against making Claris public. That was Bill’s signal to leave and begin a new chapter in his career. He would spend the next decade at the tablet computer startup GO and software manufacturer Claris, before trying his hand as a business coach.
   </p>
   <p>
    And his first client? His old employer, Apple. We’ve already seen that it was Bill’s sense of loyalty that brought him back to Columbia as a coach. Well, that quality ran deep. When Steve Jobs was forced out of Apple back in 1985, Bill was one of the only prominent team members to argue that the company couldn’t afford to lose their talented figurehead.
   </p>
   <p>
    Jobs remembered that loyalty. When he was reinstated as Apple’s CEO in 1997, he named Bill one of the company’s directors – a position he retained until 2014. But there was more to it than that. Whenever he needed advice or to talk things out, Jobs called Bill. Over the following years, Bill worked side by side with Jobs as he rescued Apple from near-bankruptcy and put it on the path to becoming a trillion-dollar company.
   </p>
   <p>
    Word of Bill and Jobs’ Sunday afternoon walks around their Palo Alto neighborhood soon spread. In 2001, Eric Schmidt decided to find out what all the fuss was about. Schmidt, a software engineer and entrepreneur who’d worked with an array of top Silicon Valley companies, had just been named CEO of a small start-up called Google. A star in his own right, Schmidt was initially skeptical and wondered what on earth a gruff ex-football coach could teach him.
   </p>
   <p>
    It didn’t take him long to change his mind. At their first meeting, the two men instantly hit it off. Over the next 15 years, Bill met up with Eric and other Google leaders just about every week. By the end of that decade and a half, Google – like Apple – was worth billions of dollars.
   </p>
   <p>
    So what exactly did Bill’s trillion-dollar coaching entail? Well, that’s what we’ll be exploring in the following blinks.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5ce516c36cee0700086ab005" data-chapterno="4">
  <h1>
   True leaders champion their companies’ core values and break ties rather than dictating terms.
  </h1>
  <div class="chapter__content">
   <p>
    In 2001, when Bill started working with Google, the company was in the middle of a radical experiment. Fed up with top-down interference in creative projects, Google’s co-founder Larry Page had decided to do away with managers altogether.
   </p>
   <p>
    Larry’s so-called “disorg” model seemed to be working, but Bill thought it wasn’t sustainable. Their argument went back and forth until Bill made a suggestion: Why not ask the folks at the sharp end of the action – Google’s engineers – what
    <em>
     they
    </em>
    thought?
   </p>
   <p>
    The response was unanimous – they wanted managers. Why? They needed leaders to resolve stalemates. Because everyone was on an equal footing, disagreements about which projects needed prioritizing weren’t being resolved.
   </p>
   <p>
    There’s plenty of evidence to back that intuition up. As a 2005 study in the
    <em>
     American Journal of Sociology
    </em>
    notes, flat hierarchies are great at fostering creativity but less adept at implementing the innovation that results. Rolling out projects like Google’s search engine is all about logistics, and that means someone needs to make tough calls about how resources are going to be allocated.
   </p>
   <p>
    However, autocratic management isn’t a viable solution either. After all, if you start dictating terms to talented subordinates, you’ll end up driving them away. But if neither democracy nor autocracy works, how
    <em>
     do
    </em>
    you make those kinds of calls?
   </p>
   <p>
    Here’s Bill’s answer: When you’re in a tricky situation and can’t reach an agreement, it’s a leader’s duty to remind everyone of the company’s
    <em>
     first principles
    </em>
    – the values that define its mission and purpose. That’s a lesson he learned back in the 1990s, when he worked with Tellme Networks.
   </p>
   <p>
    Tellme had just created the first cloud-based speech recognition software, but it was strapped for cash. That’s when telecommunications giant AT&amp;T offered to pay tens of millions of dollars to license the software if Tellme agreed to exit the market.
   </p>
   <p>
    Half the board, including CEO John LaMacchia, wanted to take the deal. Bill was against it, but knew he’d lose the team if he ramrodded his view through. So he decided to take a walk with Tellme founder Mike McCue and talk principles.
   </p>
   <p>
    Bill reminded Mike that the company had the best product in the business. AT&amp;T’s deal was lucrative, but it would ultimately mean withdrawing Tellme’s innovative technology and gifting market dominance to a substandard alternative. Was that really why Mike had set up the company? It was the wake-up call Mike needed. When he laid out Tellme’s principles to the board, the deadlock was broken and the offer was rejected.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5ce516d96cee0700081dcd93" data-chapterno="5">
  <h1>
   Effective leaders aren’t afraid to show their emotions.
  </h1>
  <div class="chapter__content">
   <p>
    There’s a common idea that people who show their emotions in the workplace aren’t as competent as their less “touchy-feely” peers. But that’s a fallacy. Few people know that better than the folks who worked with Bill.
   </p>
   <p>
    Bill was famous for his personal warmth and informality. He gave his colleagues bear hugs, had a breezy – and often downright profane – way of talking, and wouldn’t hesitate to blow a kiss to a colleague on the other side of a meeting room. More importantly, he’d drop everything to help people out if they were in trouble. When Steve Jobs was incapacitated by cancer, for example, Bill visited him in the hospital every day.
   </p>
   <p>
    This wasn’t just a personal quirk – showing that you care about the people you work with is a telltale sign of an effective leader.
   </p>
   <p>
    Take a 2014 study by leadership and HR experts Sigal Barsade and Olivia O’Neill. They found that organizations which foster
    <em>
     companionate love
    </em>
    – a kind of emotional openness that treats everyone as equals – have higher rates of employee satisfaction, better team performance levels and lower absenteeism. That’s because this approach breaks down the barriers between the personal and professional, which in turn means that people don’t feel like they have to check parts of their personalities at the door when they enter the office.
   </p>
   <p>
    Showing your emotions at work might seem like a scary proposition. But don’t worry – you don’t have to be as outgoing as Bill to pull it off. In fact, there all sorts of simple ways to create a more open, accepting environment. When Bill was working at Apple, he made sure that the board responded to presentations they liked by getting out of their chairs and clapping. As Apple’s Phil Schiller recalls, that was like a parent showing his appreciation for a child.
   </p>
   <p>
    Bruce Chizen, a software developer at Claris, remembers observing Bill’s easy-going way of talking to colleagues in the elevator or cafeteria. Mimicking this didn’t come naturally to Bruce, but then he realized how little effort it actually took once he got into the habit. Simply remembering names and asking “how’s it going?” or “what are you working on?” was more than enough to start building personal connections with his coworkers.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5ce516f96cee0700086ab006" data-chapterno="6">
  <h1>
   Bias can prevent top talent getting a hearing, so it’s important to bring everyone to the table.
  </h1>
  <div class="chapter__content">
   <p>
    In the 1980s, when Bill was first starting his Silicon Valley career, most executives at tech firms were men. One of the few women in a senior position at the time was Deb Biondolillo, Apple’s head of HR in the US.
   </p>
   <p>
    Bill had noticed that Deb always chose a chair at the back of the room rather than at the conference table at the company’s weekly staff meetings. He asked her what she was doing all the way back there and told her to “get to the table!” One day, Deb finally took the plunge and grabbed a front-row seat. That’s when gruff executive Al Eisenstat sat down and asked her what she was doing there. Deb nervously told him she was waiting for the meeting. He eyed her suspiciously and then turned to Bill, who subtly indicated that he’d made the call. That, she recalls, was the moment she knew things would work out.
   </p>
   <p>
    Bill’s methods might have been unorthodox, but he was the greatest champion of bringing women to the boardroom table the authors have ever encountered. They put this down to his old sports instincts. As a coach, he had intuitively understood that winning is about selecting the best players, whoever they are. In the boardroom, that meant putting more women on the team.
   </p>
   <p>
    He wasn’t wrong. Take a study published in the journal
    <em>
     Science
    </em>
    in 2010 which examined the “collective intelligence” of teams. Why, the authors asked, are some groups smarter than others? They narrowed their answer down to three factors. First off, higher IQ teams allow everyone to participate, rather than being dominated by one or two voices. Secondly, they display greater emotional intelligence. And, finally, they have more women!
   </p>
   <p>
    Unfortunately, there’s still a lot more work to be done before women achieve equal representation in the tech world. According to the 2016 US Equal Employment Opportunity Commission, just 20 percent of tech executives are women. The 2018 Entelo
    <em>
     Women in Tech
    </em>
    report, meanwhile, puts the figure at just 10 percent. So what can today’s business leaders do?
   </p>
   <p>
    Well, one of Bill’s favored approaches was to encourage mentoring programs. When Shellye Archambeau became CEO of the risk management software company MetricStream, she founded a group of senior women executives to support each other. Bill often attended their meetings to listen and ask questions. As he saw it, groups like Shellye’s are a brilliant way to share experiences and give members new opportunities.
   </p>
  </div>
 </div>
 <div class="chapter supplement" data-chapterid="5ce516f96cee0700086ab006" data-chapterno="6">
  <h1>
  </h1>
  <div class="chapter__content">
   <p>
    <em>
     “To truly include everyone, everyone needs to be at the table
    </em>
    .”
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5ce517106cee0700081dcd94" data-chapterno="7">
  <h1>
   Trust is the most important currency in the boardroom.
  </h1>
  <div class="chapter__content">
   <p>
    In the previous blink, we saw how Bill helped Deb Biondolillo get a place at the table. The reason he could persuade Deb to grab a seat next to an intimidating executive and Al Eisenstat to accept this was simple: they both trusted him.
   </p>
   <p>
    But what is trust? Well, here’s how an influential 1998 article published in the
    <em>
     Academy of Management Review
    </em>
    journal defines the concept: trust is all about a willingness to take a chance because you have
    <em>
     positive expectations
    </em>
    for someone else’s behavior. And that’s often what makes boardrooms tick.
   </p>
   <p>
    Take an example from Bill’s time working with the software company Intuit. The board was split between two factions. One wanted to write off recent losses and focus on long-term growth. The second, led by Bill, believed that tolerating short-term operational failures meant there wouldn’t be a long term. The deadlock was broken when Intuit’s head of sales, John Doerr, argued that they should back the coach. Why? He’d earned their trust.
   </p>
   <p>
    Bill’s method of earning people’s trust was simple: listen! Google computer scientist Alan Eustace calls Bill’s approach
    <em>
     free-form listening
    </em>
    . Rather than fidgeting with his phone or anticipating the moment when he could get his two cents in, he focused his full attention on what people were saying. More importantly, he asked questions.
   </p>
   <p>
    That, as a 2016
    <em>
     Harvard Business Review
    </em>
    paper argues, is something all great listeners do. Practitioners of this kind of respectful inquiry are regularly regarded as the most trustworthy and valuable conversation partners because of their ability to trigger spontaneous insights. This, in turn, heightens the speaker’s feelings of competence, belonging and autonomy.
   </p>
   <p>
    But trust isn’t just great at diffusing boardroom standoffs – it also transforms disagreements into positive rather than toxic experiences. When trust is absent, your feelings can get in the way of objectivity, and criticisms of your ideas feel like personal attacks. Once that happens, it’s highly unlikely that you’ll give a fair hearing to the potentially valuable insights of your sparring partner.
   </p>
   <p>
    When you trust people, by contrast, you focus on the issues at hand rather than taking things personally. And as the authors know from their own experience of working in a company molded by Bill’s philosophy, the best answers usually emerge when you talk things out honestly.
   </p>
  </div>
 </div>
 <div class="chapter chapter" data-chapterid="5ce517286cee0700086ab007" data-chapterno="8">
  <h1>
   Final summary
  </h1>
  <div class="chapter__content">
   <p>
    The key message in these blinks:
   </p>
   <p>
    <strong>
     A working-class ex-football coach with a non-technical degree who’d parachuted into California at the relatively ripe age of 43, Bill Campbell wasn’t exactly your average Silicon Valley star. But that didn’t stop him becoming one of the most important business gurus in the world. With his passion and straight-talking advice, Campbell helped a series of start-ups conquer the world and generate trillions of dollars in revenue.
    </strong>
   </p>
   <p>
    Actionable advice:
   </p>
   <p>
    <strong>
     Don’t waste meetings dwelling on emotional responses
    </strong>
   </p>
   <p>
    One of Bill’s most important lessons, Apple executive Eddy Cue recalls, was his insistence on getting emotional responses to setbacks out of the way at the beginning of meetings and moving on to other issues at hand. Psychologists call this
    <em>
     problem-focused coping
    </em>
    . This is essentially all about saving your energy for constructive problem-solving. So if you’ve got a meeting planned in which you know you’ll be talking about something that’s gone wrong, make sure to schedule some time for venting, but keep it short.
   </p>
   <p>
    <strong>
     Got feedback?
    </strong>
   </p>
   <p>
    We’d sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
   </p>
   <p>
    <strong>
     What to read next:
    </strong>
    <strong>
     <em>
      How Google Work
     </em>
    </strong>
    <strong>
     s, by Eric Schmidt and Jonathan Rosenberg
    </strong>
   </p>
   <p>
    Bill, as Eric Schmidt and his coauthors are quick to point out, was the secret ingredient that gave Google its edge. It was his careful guidance and coaching which helped its leaders transform the company from just another Silicon Valley start-up into the global behemoth it is today. But what about the other ingredients?
   </p>
   <p>
    Well, that’s just what Schmidt and Google’s former senior vice president of products Jonathan Rosenberg set out to explain when they created a confidential slideshow to explain the company’s philosophy to its senior workers back in 2010. Realizing that keeping that knowledge secret went against Google’s ethos of openness and transparency, the duo decided to share their recipe for success with the world. Interested in Google’s secret sauce? Then take a look at our blinks to
    <em>
     How Google Works
    </em>
    , by Eric Schmidt and Jonathan Rosenberg.
   </p>
  </div>
 </div>
</article>
